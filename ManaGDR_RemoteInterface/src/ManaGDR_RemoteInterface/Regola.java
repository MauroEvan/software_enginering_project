package ManaGDR_RemoteInterface;

import java.io.Serializable;
import java.rmi.RemoteException;

public interface Regola extends RegolaComponent, Serializable
{
	public boolean Applica( Object o ) throws RemoteException;
}
