package ManaGDR_RemoteInterface;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;


public interface IGiocatore extends Serializable
{
	public String toString();
	public void setID(int value) throws RemoteException;
	public int getID() throws RemoteException;
	public int getORMID() throws RemoteException;
	public void setNomeGiocatore(String value) throws RemoteException;
	public String getNomeGiocatore() throws RemoteException;
	public void aggiungiScheda(IScheda s) throws RemoteException;
}