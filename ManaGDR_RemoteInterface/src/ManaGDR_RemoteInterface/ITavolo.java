package ManaGDR_RemoteInterface;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Set;


public interface ITavolo extends Serializable
{
	public IID getIdTavolo() throws RemoteException;
	public void setIdTavolo(IID idTavolo) throws RemoteException;
	public ISessione getSessione() throws RemoteException;
	public void setSessione(ISessione sessione) throws RemoteException;
	public HashSet<IGiocatore> getGiocatoriSeduti() throws RemoteException;
	public void AssociaScheda(IGiocatore giocatore, IScheda scheda) throws RemoteException;
	public void mettiSessioneSulTavolo(ISessione nuovaSessione) throws RemoteException;
	public void AttivaSessione() throws RemoteException;
	public void AggiungiGiocatore( IGiocatore g ) throws RemoteException;
	public ArrayList<OpzioneCreazioneScheda> AvviaCreazioneScheda( IGiocatore g ) throws RemoteException;
	public IScheda CreaNuovaScheda( OpzioneCreazioneScheda opz ) throws RemoteException;
	public boolean ValidaScheda( IScheda s ) throws RemoteException;
}
