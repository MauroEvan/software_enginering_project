package ManaGDR_RemoteInterface;

import java.io.Serializable;
import java.rmi.RemoteException;


public interface Campo extends Serializable
{
	public String toString();
	public String getNome() throws RemoteException;
	public Object getValore() throws RemoteException;
	public Object getModificatore() throws RemoteException;
	public void setScheda(IScheda value) throws RemoteException;
}
