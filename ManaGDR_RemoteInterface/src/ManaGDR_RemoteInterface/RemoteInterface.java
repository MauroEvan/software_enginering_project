package ManaGDR_RemoteInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteInterface extends Remote
{
	public String Test() throws RemoteException;
	public IGiocatore getGiocatoreFromName( String nome ) throws RemoteException;

	// Date all'UI ciò che è dell'UI
	public Campo newCampoTestuale(String nome, String valore) throws RemoteException;
	public IID newID(int valore) throws RemoteException;

	public IScaffale getScaffale() throws RemoteException;
	public ILobby getLobby() throws RemoteException;

	public IModuloDiRegolamento newModuloRegolamento() throws RemoteException;
	public RegolaComponent newRegolaComposite() throws RemoteException;
	public RegolaComponent AggiungiRegola( RegolaComponent c, Regola r ) throws RemoteException;

	public Regola NuovaRegola( String classe ) throws RemoteException;

	public void InviaRegolamento( IModuloDiRegolamento m ) throws RemoteException;

	public IModuloDiRegolamento AssociaRegolamento( IModuloDiRegolamento m, RegolaComponent r ) throws RemoteException;
}
