package ManaGDR_RemoteInterface;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;


public interface IModuloDiRegolamento extends Serializable
{
	public void setID(int value) throws RemoteException;
	public ArrayList<Regola> ApplicaSuScheda(IScheda s) throws RemoteException;
	public String toString();
	public int getID() throws RemoteException;
	public int getORMID() throws RemoteException;
	public void setRegolamento(RegolaComponent value) throws RemoteException;
	public RegolaComponent getRegolamento() throws RemoteException;
	public IID getIdModulo() throws RemoteException;
	public void setIdModulo(IID idModulo) throws RemoteException;
	public boolean Verifica();
}
