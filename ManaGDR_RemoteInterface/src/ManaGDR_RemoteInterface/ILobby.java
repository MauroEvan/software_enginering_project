package ManaGDR_RemoteInterface;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;

public interface ILobby extends Serializable
{
	public static ILobby getInstance() throws RemoteException {return null;};
	public ITavolo CreaNuovoTavolo() throws RemoteException;
	public ArrayList<IGiocatore> getGiocatoriConnessi() throws RemoteException;
	public void AggiungiGiocatore(IGiocatore g) throws RemoteException;
}
