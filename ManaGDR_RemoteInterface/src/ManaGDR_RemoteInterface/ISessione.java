package ManaGDR_RemoteInterface;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

public interface ISessione extends Serializable
{
	public void setID(int value) throws RemoteException;
	public Set<IModuloDiRegolamento> getModuloRegolamento() throws RemoteException;
	public Set<IEvento> getEventi() throws RemoteException;
	public Set<IScheda> getSchede() throws RemoteException;
	public void ottieniITuoiDati() throws RemoteException;
	public void associaSchedeLibere() throws RemoteException;
	public ArrayList<Regola> ApplicaRegoleAScheda(IScheda s) throws RemoteException;
	public IScheda AggiungiSchedaVuota() throws RemoteException;
	public boolean RimuoviScheda(IScheda scheda) throws RemoteException;
	public String toString();
	public StatoSessione getStato() throws RemoteException;
	public void setStato(StatoSessione stato) throws RemoteException;
	public void setIdSessione(IID idScheda) throws RemoteException;
	public int getID() throws RemoteException;
	public int getIdSessione() throws RemoteException;
	public int getORMID() throws RemoteException;
	public void setDescr(String value) throws RemoteException;
	public String getDescr() throws RemoteException;
	public void setMappa(IMappa value) throws RemoteException;
	public IMappa getMappa() throws RemoteException;
	public void setModuloRegolamento(Set<IModuloDiRegolamento> value) throws RemoteException;
	public ArrayList<IModuloDiRegolamento> getModuloRegolamento_byList() throws RemoteException;
	public ArrayList<IEvento> getEventi_byList() throws RemoteException;
	public void setEventi(Set<IEvento> value) throws RemoteException;
	public void setSchede(Set<IScheda> value) throws RemoteException;
	public ArrayList<IScheda> getSchede_byList() throws RemoteException;
	public void setGm(IGiocatore value) throws RemoteException;
	public IGiocatore getGm() throws RemoteException;
	public IScheda getScheda(int index) throws RemoteException;
	public boolean isGM(IGiocatore g) throws RemoteException;
}
