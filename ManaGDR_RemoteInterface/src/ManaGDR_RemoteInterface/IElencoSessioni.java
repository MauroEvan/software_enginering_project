package ManaGDR_RemoteInterface;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;


public interface IElencoSessioni extends Serializable
{
	public ISessione at( int index ) throws RemoteException;
	public boolean add( ISessione s ) throws RemoteException;
	public int getSize() throws RemoteException;
}
