package ManaGDR_RemoteInterface;

import java.io.Serializable;
import java.rmi.RemoteException;

public interface IID extends Serializable
{
	public int getId() throws RemoteException;
	public void setId(int id) throws RemoteException;
	public String toString();
}
