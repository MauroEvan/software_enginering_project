package ManaGDR_RemoteInterface;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;


public interface IScaffale extends Serializable
{
	//public static IScaffale getInstance() throws RemoteException {return null;};
	public IElencoSessioni RecuperaSessioni(IGiocatore gmSessione) throws RemoteException;
	public ISessione SelezionaSessione(IID idSessione) throws RemoteException;
	public ISessione getSessioneDaId(IID idSessione) throws RemoteException;
}
