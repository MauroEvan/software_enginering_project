package ManaGDR_RemoteInterface;

import java.io.Serializable;
import java.rmi.RemoteException;

public interface OpzioneCreazioneScheda extends Serializable
{
	public void RiempiScheda(IScheda s) throws RemoteException;
	public String getTipo() throws RemoteException;
}
