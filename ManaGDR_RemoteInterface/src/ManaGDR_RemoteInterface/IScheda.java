package ManaGDR_RemoteInterface;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Set;

public interface IScheda extends Serializable
{
	public void setID(int value) throws RemoteException;
	public String toString();
	public IID getIdScheda() throws RemoteException;
	public void setIdScheda(IID idScheda) throws RemoteException;
	public IGiocatore getGiocatore() throws RemoteException;
	public void setGiocatore(IGiocatore giocatore) throws RemoteException;
	public int getID() throws RemoteException;
	public int getORMID() throws RemoteException;
	public void setCampoResistenze(Set value) throws RemoteException;
	public Set getCampoResistenze() throws RemoteException;
	public void setCampoAbilità(Set value) throws RemoteException;
	public Set getCampoAbilità() throws RemoteException;
	public void setCampoTestuale(Set value) throws RemoteException;
	public Set getCampoTestuale() throws RemoteException;
	public void setCampoNumerico(Set value) throws RemoteException;
	public Set getCampoNumerico() throws RemoteException;
	public void setCampoOggetto(Set value) throws RemoteException;
	public Set getCampoOggetto() throws RemoteException;
	public void setCampoAttributo(Set value) throws RemoteException;
	public Set getCampoAttributo() throws RemoteException;
	public void addCampo(String key, Campo c) throws RemoteException;
	public Campo getCampo(String key) throws RemoteException;
	public Set<String> getChiaviCampi() throws RemoteException;
	public void addCampoResistenze(Campo value) throws RemoteException;
	public void addCampoAbilita(Campo value) throws RemoteException;
	public void addCampoTestuale(Campo value) throws RemoteException;
	public void addCampoNumerico(Campo value) throws RemoteException;
	public void addCampoOggetto(Campo value) throws RemoteException;
	public void addCampoAttributo(Campo value) throws RemoteException;
}
