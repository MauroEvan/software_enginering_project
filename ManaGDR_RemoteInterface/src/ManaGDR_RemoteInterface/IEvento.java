package ManaGDR_RemoteInterface;

import java.io.Serializable;
import java.rmi.RemoteException;

public interface IEvento extends Serializable
{
	public void setID(int value) throws RemoteException;
	public String toString();
	public IID getIdEvento() throws RemoteException;
	public void setIdEvento(IID idEvento) throws RemoteException;
	public int getID() throws RemoteException;
	public int getORMID() throws RemoteException;
}
