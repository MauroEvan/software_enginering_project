package ManaGDR_Client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.Remote;
import java.util.Date;

import UI.*;
import domain.*;
import javax.swing.JFrame;

import ManaGDR_RemoteInterface.*;


public class Client
{
	private static JFrame FinestraPrincipale;
	private static DatiLogin loginInfo;
	private static ClientConfig conf;

	private static RemoteInterface serverInterface;


	public static void main( String args[] ) throws RemoteException
	{
		System.out.println( "Benvenuto in ManaGDR! Collegamento al server in corso..." );

		conf.getInstance().Populate( null );

		try {
			String host = conf.getInstance().get("serverHost");
			int porta = Integer.parseUnsignedInt( conf.getInstance().get("serverPort") );
			String id =  conf.getInstance().get("serverID");

			System.out.println( "In collegamento a " + id + "@" + host + ":" + porta + " ..." );

			Registry registry = LocateRegistry.getRegistry( host, porta );

			System.out.println( "Server trovati:" );
			for( String server : registry.list() )
				System.out.println( server );

			serverInterface = (RemoteInterface) registry.lookup( id );
		} catch( RemoteException re ) {
			re.printStackTrace();
			System.err.println( "Errore nella connessione al server! Impossibile connettersi. " );
			System.exit(-1);
		} catch( NotBoundException nbe ) {
			System.err.println( "Server non trovato sull'host specificato." );
			System.exit(-2);
		}

// Simulazione Login------------------
		IGiocatore g = null;
		try {
			g = serverInterface.getGiocatoreFromName( "Player1" );
			if( g!=null )
				loginInfo.getInstance().setGiocatore(g);
			else {
				System.err.println( "[MANAGDR] Errore: giocatore "+g.toString()+" non trovato! " );
				System.exit(-3);
			}
		} catch( RemoteException re ) {
			System.err.println( "[MANAGDR] Errore: giocatore non trovato! " );
			re.printStackTrace();
			System.exit(-3);
		}
// ------------------------------------

		loginInfo.getInstance().setUltimoLogin( new Date() );
		try {
			System.out.println("Loggato come:" + g.getNomeGiocatore() );
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}

		FinestraPrincipale = new MainWindow();
		FinestraPrincipale.setVisible(true);
	}


	public static RemoteInterface getRemoteInterface()
	{
		return serverInterface;
	}


	public static DatiLogin getDatiLogin() throws RemoteException
	{
		return loginInfo.getInstance();
	}
}
