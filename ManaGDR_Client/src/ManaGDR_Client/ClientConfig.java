package ManaGDR_Client;

import java.util.HashMap;
import domain.*;


// SINGLETON
public class ClientConfig
{
	static final String defaultConfigFile = "ManaGDR.conf";
	private static HashMap<String, String> opzioni;
	private static ClientConfig handle = null;
	private static RegolamentoUploader uploader = null;


	protected ClientConfig() {}; // Per evitare istanziazione diretta


	public static ClientConfig getInstance()
	{
		if( handle == null ) {
			handle = new ClientConfig();
			opzioni = new HashMap<>();
		}
		return handle;
	}


	public String get( String opz )
	{
		if( opzioni.containsKey(opz) )
			return opzioni.get(opz);
		return null;
	}


	public void set( String key, String value )
	{
		opzioni.put(key, value);
	}


	public static int Populate( String filename )
	{
		String path = filename==null?defaultConfigFile:filename;

		opzioni.clear();
		// [TODO] Per futuri casi d'uso: lettura da file!
		opzioni.put( "serverHost", "localhost" );
		opzioni.put( "serverPort", "1099" );
		opzioni.put( "serverID", "ManaGDR_Server1" );
		opzioni.put( "regolamentoUploader", "domain.XMLFile_Adaptee" );
		//
		return opzioni.size();
	}

	public static RegolamentoUploader getRegolamentoUploader()
	{
		if( uploader == null )
		{
			String nomeClasse = ClientConfig.getInstance().get("regolamentoUploader");

			if( nomeClasse == null )
				nomeClasse = "domain.XMLFile_Adaptee"; // default

			uploader = UploaderFactory.getInstance().newUploader(nomeClasse);
		}
		return uploader;
	}
}
