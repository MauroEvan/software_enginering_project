package UI;

import ManaGDR_RemoteInterface.*;
import java.awt.event.ActionEvent;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class USchedeGiocatori extends JPanel {
    
    private javax.swing.JLabel NomeScheda;
    private javax.swing.JComboBox GiocatoreComboBox;
    private MainWindow parent;    
    private IScheda schedaAttuale;
    private ITavolo tavoloAttuale;
    
    // Contratto Operazione CO3: AssociaScheda
    private void GiocatoreComboBoxActionPerformed(ActionEvent evt) throws RemoteException {        
        if (GiocatoreComboBox.getSelectedItem() instanceof IGiocatore)
        {
            tavoloAttuale.AssociaScheda( (IGiocatore)this.GiocatoreComboBox.getSelectedItem(), this.schedaAttuale);
        }
    }
    
    public USchedeGiocatori(IScheda scheda, ITavolo t, MainWindow parentWindow) throws RemoteException
    {    
        NomeScheda = new javax.swing.JLabel();
        GiocatoreComboBox = new javax.swing.JComboBox();
        schedaAttuale = scheda;
        tavoloAttuale = t;
        
        this.setMinimumSize(new java.awt.Dimension(320, 60));
        this.setPreferredSize(new java.awt.Dimension(320, 60));

        NomeScheda.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        NomeScheda.setText(scheda.toString());
        GiocatoreComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] {}));
        GiocatoreComboBox.addItem("Non associato");
        
//        System.out.println(t.getGiocatoriSeduti());
        
        for (IGiocatore giocatore : t.getGiocatoriSeduti())
        {
            GiocatoreComboBox.addItem(giocatore);
            //System.out.println( "Aggiunto alla combo il giocatore " + giocatore.getNomeGiocatore() );
        }
        
        GiocatoreComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    GiocatoreComboBoxActionPerformed(evt);
                } catch (RemoteException ex) {
                    ex.printStackTrace();
                }
            }
        });
        
        javax.swing.GroupLayout SchedaGiocatorePannelloLayout = new javax.swing.GroupLayout(this);
        this.setLayout(SchedaGiocatorePannelloLayout);
        SchedaGiocatorePannelloLayout.setHorizontalGroup(SchedaGiocatorePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SchedaGiocatorePannelloLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(NomeScheda)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 124, Short.MAX_VALUE)
                .addComponent(GiocatoreComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
        );
        SchedaGiocatorePannelloLayout.setVerticalGroup(SchedaGiocatorePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SchedaGiocatorePannelloLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(SchedaGiocatorePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NomeScheda)
                    .addComponent(GiocatoreComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        
        this.parent = parentWindow;
        
    }
    
}
