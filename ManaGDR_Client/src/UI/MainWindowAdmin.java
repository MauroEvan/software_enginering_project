/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import ManaGDR_Client.Client;
import ManaGDR_Client.ClientConfig;
import ManaGDR_RemoteInterface.IModuloDiRegolamento;
import domain.RegolamentoUploader;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

/**
 *
 * @author Mauro
 */
public class MainWindowAdmin extends javax.swing.JFrame {

    private ArrayList<File> fileList;
    private JFileChooser fc;

    public MainWindowAdmin() {
        initComponents();
        fc = new JFileChooser();
        fileList = new ArrayList<>();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        TabbedPaneUploader = new javax.swing.JTabbedPane();
        PannelloUploadXML = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        logTextArea = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(640, 480));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Pannello amministrazione regolamento");

        PannelloUploadXML.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        PannelloUploadXML.setToolTipText("");

        logTextArea.setColumns(20);
        logTextArea.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        logTextArea.setRows(5);
        jScrollPane1.setViewportView(logTextArea);

        jButton1.setText("Apri");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ApriActionPerformed(evt);
            }
        });

        jButton2.setText("Invia");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InviaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PannelloUploadXMLLayout = new javax.swing.GroupLayout(PannelloUploadXML);
        PannelloUploadXML.setLayout(PannelloUploadXMLLayout);
        PannelloUploadXMLLayout.setHorizontalGroup(
            PannelloUploadXMLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PannelloUploadXMLLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PannelloUploadXMLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 589, Short.MAX_VALUE)
                    .addGroup(PannelloUploadXMLLayout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        PannelloUploadXMLLayout.setVerticalGroup(
            PannelloUploadXMLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PannelloUploadXMLLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PannelloUploadXMLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        TabbedPaneUploader.addTab("Upload XML File", PannelloUploadXML);

        jMenu1.setText("ManaGDR");

        jMenuItem1.setText("Login");
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Gestisci Sessioni");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gestisciSessioniActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);
        jMenu1.add(jSeparator1);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("LogOut");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LogoutActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem4.setText("LogOut ed Uscita");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LogoutUscitaActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jMenuBar1.add(jMenu1);

        jMenu4.setText("Regolamenti");
        jMenuBar1.add(jMenu4);

        jMenu2.setText("Profilo");
        jMenuBar1.add(jMenu2);

        jMenu3.setText("?");
        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(TabbedPaneUploader)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TabbedPaneUploader, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void gestisciSessioniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gestisciSessioniActionPerformed
        JFrame sess = new MainWindow();
        sess.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_gestisciSessioniActionPerformed

    private void LogoutUscitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LogoutUscitaActionPerformed
            System.exit( 0 );
    }//GEN-LAST:event_LogoutUscitaActionPerformed

    private void LogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LogoutActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_LogoutActionPerformed

    private void ApriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ApriActionPerformed
			this.fc.setCurrentDirectory( new File(System.getProperty("user.dir")) );
			int returnVal = this.fc.showOpenDialog(MainWindowAdmin.this);

            if( returnVal == JFileChooser.APPROVE_OPTION )
            {
                    File file = fc.getSelectedFile();
                    String estensione = "";
                    String fileName = file.getName();
                    int i = fileName.lastIndexOf('.');
                    int p = Math.max( fileName.lastIndexOf('/'), fileName.lastIndexOf('\\') );
                    if( i > p )
                        estensione = fileName.substring( i+1 );

                    if( estensione.equals("xml") || estensione.equals("XML") )
                    {
                            logTextArea.append("Aggiunto: " + file.getName() + ". \n" );
                            fileList.add(file);
                    }
                    else
                    {
                            logTextArea.append("Errore: estensione non corretta! Inserire esclusivamente file XML. \n" );
                    }
            }
            else
            {
                    logTextArea.append("Aggiunta file cancellata dall'utente. \n");
            }
            logTextArea.setCaretPosition(logTextArea.getDocument().getLength());
            System.err.println(fileList);
    }//GEN-LAST:event_ApriActionPerformed

    private void InviaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InviaActionPerformed
        RegolamentoUploader uploader = ClientConfig.getRegolamentoUploader();
		for (File file : fileList) {

			IModuloDiRegolamento im = null;

			try {
				im = uploader.parse(file.getAbsoluteFile());
			} catch( Exception ex ) {
				ex.printStackTrace();
				logTextArea.append("Errore nel parsing del file: " + file.getName() + ". \n");
			}

			if( im != null ) {
				try {
					Client.getRemoteInterface().InviaRegolamento( im );
				} catch( Exception ex ) {
					ex.printStackTrace();
					logTextArea.append("Errore nel parsing del file: " + file.getName() + ". \n");
				}
				logTextArea.append("File: " + file.getName() + ", inviato con successo. \n");
			}
		}
    }//GEN-LAST:event_InviaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PannelloUploadXML;
    private javax.swing.JTabbedPane TabbedPaneUploader;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JTextArea logTextArea;
    // End of variables declaration//GEN-END:variables
}
