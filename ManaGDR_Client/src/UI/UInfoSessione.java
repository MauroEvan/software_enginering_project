/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import ManaGDR_Client.Client;
import ManaGDR_RemoteInterface.*;
import java.awt.FlowLayout;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.*;
//import org.orm.PersistentException;

/**
 *
 * @author Mauro_Notebook
 */
public class UInfoSessione extends JPanel
{        
	private javax.swing.JLabel NomeSessione;
	private javax.swing.JButton SelezionaSessioneButton;
	private javax.swing.JScrollPane InformazioniSessioneScrollPane;
	private javax.swing.JTextArea InformazioniSessione;
	private IID IDSessione;
	private MainWindow parent;

	public UInfoSessione(ISessione s, MainWindow parentWindow ) throws RemoteException //throws PersistentException 
	{
		NomeSessione = new javax.swing.JLabel();
		SelezionaSessioneButton = new javax.swing.JButton();
		InformazioniSessioneScrollPane = new javax.swing.JScrollPane();
		InformazioniSessione = new javax.swing.JTextArea();

		this.setMinimumSize( new java.awt.Dimension(650,100) );
		this.setMaximumSize(new java.awt.Dimension(650, 100) );
		this.setPreferredSize( new java.awt.Dimension(650, 100) );

		NomeSessione.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
		NomeSessione.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		NomeSessione.setText("Sessione " + s.getIdSessione() );
		NomeSessione.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

		SelezionaSessioneButton.setText("Seleziona");
		
		SelezionaSessioneButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				RemoteInterface ri = Client.getRemoteInterface();
				IScaffale scaffale = null;

				
				try {
					// Contratto Operazione CO2: SelezionaSessione
					ISessione s;
					
					scaffale = ri.getScaffale();
					s = scaffale.SelezionaSessione(IDSessione);

					ITavolo t;

					t = ri.getLobby().CreaNuovoTavolo();

					t.mettiSessioneSulTavolo(s);

					// Caso d'uso di avviamento: aggiungiamo tutti i giocatori al tavolo
//					for (IGiocatore g : ILobby.getInstance().getGiocatoriConnessi()) {
					for (IGiocatore g : ri.getLobby().getGiocatoriConnessi()) {
						t.AggiungiGiocatore(g);
						// System.out.println( "Aggiunto al tavolo il giocatore " + g.getNomeGiocatore() );
					}

					parent.setTavoloConSessione(t);
					parent.MostraPannelloSessione(t);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		InformazioniSessione.setEditable(false);
		InformazioniSessione.setBackground(this.getBackground());
		InformazioniSessione.setColumns(20);
		InformazioniSessione.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
		InformazioniSessione.setLineWrap(true);
		InformazioniSessione.setRows(5);
		InformazioniSessione.setText(s.getDescr());
		InformazioniSessione.setWrapStyleWord(true);
		InformazioniSessione.setBorder(null);
		InformazioniSessioneScrollPane.setViewportView(InformazioniSessione);

		javax.swing.GroupLayout thisLayout = new javax.swing.GroupLayout(this);
		this.setLayout(thisLayout);
		thisLayout.setHorizontalGroup(
			thisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
			.addGroup(thisLayout.createSequentialGroup()
				.addGroup(thisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(thisLayout.createSequentialGroup()
						.addContainerGap()
						.addComponent(InformazioniSessioneScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 419, javax.swing.GroupLayout.PREFERRED_SIZE))
					.addGroup(thisLayout.createSequentialGroup()
						.addGap(96, 96, 96)
						.addComponent(NomeSessione, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
				.addComponent(SelezionaSessioneButton)
				.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		thisLayout.setVerticalGroup(
			thisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
			.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, thisLayout.createSequentialGroup()
				.addGap(0, 0, Short.MAX_VALUE)
				.addGroup(thisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addComponent(SelezionaSessioneButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
					.addGroup(thisLayout.createSequentialGroup()
						.addComponent(NomeSessione)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(InformazioniSessioneScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))
				.addContainerGap())
		);
		
		RemoteInterface temp = Client.getRemoteInterface();
		this.IDSessione = temp.newID(s.getIdSessione());
		this.parent = parentWindow;
	}

	public IID getIDSessione()
	{
		return IDSessione;
	}
}
