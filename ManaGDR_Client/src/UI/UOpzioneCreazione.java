
package UI;

import ManaGDR_RemoteInterface.*;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class UOpzioneCreazione extends JPanel 
{        
    private javax.swing.JLabel OpzioneLabel;
    private javax.swing.JLabel TipoCreazioneLabel;
    private javax.swing.JButton SelezionaTipoButton;
    private JFrame parent;

    
    public UOpzioneCreazione( OpzioneCreazioneScheda opzione, JFrame parentWindow, ITavolo tavolo, MainWindow mainWindow ) throws RemoteException
    {
        OpzioneLabel = new javax.swing.JLabel();
        TipoCreazioneLabel = new javax.swing.JLabel();
        SelezionaTipoButton = new javax.swing.JButton();

        this.setMinimumSize( new java.awt.Dimension(380,61) );
        this.setPreferredSize( new java.awt.Dimension(380, 61) );

        OpzioneLabel.setText( "Creazione: " );

        TipoCreazioneLabel.setText( opzione.getTipo() );

        SelezionaTipoButton.setText( "OK" );

        SelezionaTipoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                try {
                    //CO5 CreaNuovaScheda
                    IScheda s = tavolo.CreaNuovaScheda(opzione);
                    JFrame frame;
                    try {
                        frame = UCreazioneCreator.getInstance().Create( opzione.getClass().getName().substring(7) , tavolo , s , mainWindow );
                        frame.setLocation(10,10);
                        frame.setVisible(true);
                        parent.dispose();
                    } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex){}
                } catch (RemoteException ex){ex.printStackTrace();
}
            }
        });

        javax.swing.GroupLayout thisLayout = new javax.swing.GroupLayout(this);
        this.setLayout(thisLayout);
        thisLayout.setHorizontalGroup(
            thisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(thisLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(OpzioneLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TipoCreazioneLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 189, Short.MAX_VALUE)
                .addComponent(SelezionaTipoButton)
                .addContainerGap())
        );
        thisLayout.setVerticalGroup(
            thisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, thisLayout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addGroup(thisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SelezionaTipoButton)
                    .addComponent(OpzioneLabel)
                    .addComponent(TipoCreazioneLabel))
                .addGap(19, 19, 19))
        );

        this.parent = parentWindow;

    }
}
