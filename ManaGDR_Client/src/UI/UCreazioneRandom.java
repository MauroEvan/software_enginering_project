
package UI;

import ManaGDR_Client.Client;
import domain.*;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JTextField;
import ManaGDR_RemoteInterface.*;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UCreazioneRandom extends javax.swing.JFrame {
    
    private IScheda scheda;
    private ITavolo tavolo;
    private MainWindow mainWindow;

    public UCreazioneRandom( ITavolo t , IScheda s , MainWindow main ) throws RemoteException {
        this.scheda = s;
        this.tavolo = t;
        mainWindow = main;
        initComponents();
        
        this.NomeTextField.setText( (String) this.scheda.getCampo("nome").getValore());
        this.RazzaTextField.setText( (String) this.scheda.getCampo("razza").getValore());
        this.ClasseTextField.setText( (String) this.scheda.getCampo("classe").getValore());
                                              
        if( this.scheda.getCampo("razza").getValore().equals("Umano") || this.scheda.getCampo("razza").getValore().equals("Elfo"))
        {
            TagliaTextField.setText("Media");
        }
        else
        {
            if( this.scheda.getCampo("razza").getValore().equals("Gnomo") ||  this.scheda.getCampo("razza").getValore().equals("Nano"))
            {
                TagliaTextField.setText("Piccola");
            }
        }
        String allineamento = (String) this.scheda.getCampo("allineamento").getValore();
        String[] substr = new String[allineamento.length()];
        substr =  allineamento.split("\\s");
        
        this.Allineamento1TextField.setText(substr[0]);
        this.Allineamento2TextField.setText(substr[1]);
        
        this.forza.setText( Integer.toString( (int) this.scheda.getCampo("forza").getValore()) );
        this.destrezza.setText( Integer.toString( (int) this.scheda.getCampo("destrezza").getValore()) );
        this.costituzione.setText( Integer.toString( (int) this.scheda.getCampo("costituzione").getValore()) );
        this.intelligenza.setText( Integer.toString( (int) this.scheda.getCampo("intelligenza").getValore()) );
        this.saggezza.setText( Integer.toString( (int) this.scheda.getCampo("saggezza").getValore()) );
        this.carisma.setText( Integer.toString( (int) this.scheda.getCampo("carisma").getValore()) );
        
        this.mod_forza.setText( Integer.toString((int) this.scheda.getCampo("forza").getModificatore()) );
        this.mod_destrezza.setText( Integer.toString((int) this.scheda.getCampo("destrezza").getModificatore()) );
        this.mod_costituzione.setText( Integer.toString((int) this.scheda.getCampo("costituzione").getModificatore()) );
        this.mod_intelligenza.setText( Integer.toString((int) this.scheda.getCampo("intelligenza").getModificatore()) );
        this.mod_saggezza.setText( Integer.toString((int) this.scheda.getCampo("saggezza").getModificatore()) );        
        this.mod_carisma.setText( Integer.toString((int) this.scheda.getCampo("carisma").getModificatore()) );
                        
        ArrayList<Integer> res = new ArrayList<>();
        res = (ArrayList<Integer>) this.scheda.getCampo("resistenze").getValore();
        this.tempra.setText( res.get(0).toString() );
        this.riflessi.setText( res.get(1).toString() );
        this.volonta.setText( res.get(2).toString() );
        this.classeArmatura.setText( res.get(3).toString() );
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        SchedaTabbedPane = new javax.swing.JTabbedPane();
        InformazioniGeneraliPannello = new javax.swing.JPanel();
        NomeLabel = new javax.swing.JLabel();
        NomeTextField = new javax.swing.JTextField();
        RazzaLabel = new javax.swing.JLabel();
        RazzaTextField = new javax.swing.JTextField();
        TagliaLabel = new javax.swing.JLabel();
        TagliaTextField = new javax.swing.JTextField();
        ClasseLabel = new javax.swing.JLabel();
        ClasseTextField = new javax.swing.JTextField();
        AllineamentoLabel = new javax.swing.JLabel();
        Allineamento1TextField = new javax.swing.JTextField();
        Allineamento2TextField = new javax.swing.JTextField();
        Caratteristiche = new javax.swing.JPanel();
        Attributi = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        forza = new javax.swing.JTextField();
        mod_forza = new javax.swing.JTextField();
        destrezza = new javax.swing.JTextField();
        mod_destrezza = new javax.swing.JTextField();
        costituzione = new javax.swing.JTextField();
        mod_costituzione = new javax.swing.JTextField();
        intelligenza = new javax.swing.JTextField();
        mod_intelligenza = new javax.swing.JTextField();
        saggezza = new javax.swing.JTextField();
        carisma = new javax.swing.JTextField();
        mod_saggezza = new javax.swing.JTextField();
        mod_carisma = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        Resistenze = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        riflessi = new javax.swing.JTextField();
        tempra = new javax.swing.JTextField();
        volonta = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        classeArmatura = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        Salva = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Creazione casuale della scheda");
        setMinimumSize(new java.awt.Dimension(600, 725));
        setName("CreazioneRandomFrame"); // NOI18N
        setPreferredSize(new java.awt.Dimension(600, 735));

        SchedaTabbedPane.setPreferredSize(new java.awt.Dimension(600, 670));

        NomeLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        NomeLabel.setText("Nome Personaggio");

        NomeTextField.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        NomeTextField.setEnabled(false);

        RazzaLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        RazzaLabel.setText("Razza");

        RazzaTextField.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        RazzaTextField.setEnabled(false);

        TagliaLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        TagliaLabel.setText("Taglia");

        TagliaTextField.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        TagliaTextField.setEnabled(false);

        ClasseLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        ClasseLabel.setText("Classe");

        ClasseTextField.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        ClasseTextField.setEnabled(false);

        AllineamentoLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        AllineamentoLabel.setText("Allineamento");

        Allineamento1TextField.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        Allineamento1TextField.setEnabled(false);
        Allineamento1TextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Allineamento1TextFieldActionPerformed(evt);
            }
        });

        Allineamento2TextField.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        Allineamento2TextField.setEnabled(false);

        javax.swing.GroupLayout InformazioniGeneraliPannelloLayout = new javax.swing.GroupLayout(InformazioniGeneraliPannello);
        InformazioniGeneraliPannello.setLayout(InformazioniGeneraliPannelloLayout);
        InformazioniGeneraliPannelloLayout.setHorizontalGroup(
            InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InformazioniGeneraliPannelloLayout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InformazioniGeneraliPannelloLayout.createSequentialGroup()
                        .addGroup(InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(AllineamentoLabel)
                            .addComponent(ClasseLabel))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(InformazioniGeneraliPannelloLayout.createSequentialGroup()
                        .addGroup(InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, InformazioniGeneraliPannelloLayout.createSequentialGroup()
                                .addComponent(Allineamento1TextField, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(Allineamento2TextField, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(ClasseTextField)
                            .addComponent(RazzaLabel, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(NomeLabel, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(NomeTextField, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(RazzaTextField, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(18, 18, 18)
                        .addGroup(InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TagliaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(TagliaLabel))
                        .addContainerGap(121, Short.MAX_VALUE))))
        );
        InformazioniGeneraliPannelloLayout.setVerticalGroup(
            InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InformazioniGeneraliPannelloLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(NomeLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(NomeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RazzaLabel)
                    .addComponent(TagliaLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RazzaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TagliaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(ClasseLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ClasseTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(AllineamentoLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Allineamento1TextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Allineamento2TextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(363, Short.MAX_VALUE))
        );

        SchedaTabbedPane.addTab("Informazioni Generali", InformazioniGeneraliPannello);

        Attributi.setBorder(javax.swing.BorderFactory.createTitledBorder("Attributi"));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel6.setText("Attributo");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("FOR");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("DES");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("COS");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setText("INT");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setText("SAG");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setText("CAR");

        forza.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        forza.setEnabled(false);

        mod_forza.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        mod_forza.setEnabled(false);

        destrezza.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        destrezza.setEnabled(false);

        mod_destrezza.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        mod_destrezza.setEnabled(false);
        mod_destrezza.setMinimumSize(new java.awt.Dimension(60, 20));

        costituzione.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        costituzione.setEnabled(false);

        mod_costituzione.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        mod_costituzione.setEnabled(false);
        mod_costituzione.setMinimumSize(new java.awt.Dimension(60, 20));

        intelligenza.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        intelligenza.setEnabled(false);

        mod_intelligenza.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        mod_intelligenza.setEnabled(false);
        mod_intelligenza.setMinimumSize(new java.awt.Dimension(60, 20));
        mod_intelligenza.setPreferredSize(new java.awt.Dimension(60, 20));

        saggezza.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        saggezza.setEnabled(false);

        carisma.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        carisma.setEnabled(false);

        mod_saggezza.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        mod_saggezza.setEnabled(false);
        mod_saggezza.setMinimumSize(new java.awt.Dimension(60, 20));
        mod_saggezza.setPreferredSize(new java.awt.Dimension(60, 20));

        mod_carisma.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        mod_carisma.setEnabled(false);
        mod_carisma.setMinimumSize(new java.awt.Dimension(60, 20));
        mod_carisma.setPreferredSize(new java.awt.Dimension(60, 20));

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel13.setText("Valore");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel14.setText("Modificatore");

        javax.swing.GroupLayout AttributiLayout = new javax.swing.GroupLayout(Attributi);
        Attributi.setLayout(AttributiLayout);
        AttributiLayout.setHorizontalGroup(
            AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AttributiLayout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(AttributiLayout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(80, 80, 80)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel14))
                    .addGroup(AttributiLayout.createSequentialGroup()
                        .addGroup(AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12))
                        .addGap(69, 69, 69)
                        .addGroup(AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(forza, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                            .addComponent(destrezza)
                            .addComponent(costituzione)
                            .addComponent(intelligenza)
                            .addComponent(saggezza)
                            .addComponent(carisma))
                        .addGap(50, 50, 50)
                        .addGroup(AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(mod_costituzione, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(mod_destrezza, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(mod_intelligenza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(mod_forza, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mod_saggezza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mod_carisma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        AttributiLayout.setVerticalGroup(
            AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AttributiLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14))
                .addGap(30, 30, 30)
                .addGroup(AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(forza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_forza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(destrezza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_destrezza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(costituzione, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_costituzione, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(intelligenza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_intelligenza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(saggezza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_saggezza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(AttributiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(carisma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_carisma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        Resistenze.setBorder(javax.swing.BorderFactory.createTitledBorder("Resistenze"));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel15.setText("Riflessi");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setText("Tempra");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setText("Volontà");

        riflessi.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        riflessi.setEnabled(false);

        tempra.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        tempra.setEnabled(false);

        volonta.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        volonta.setEnabled(false);

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel18.setText("Classe Armatura");

        classeArmatura.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        classeArmatura.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        classeArmatura.setText("0");
        classeArmatura.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        classeArmatura.setEnabled(false);

        javax.swing.GroupLayout ResistenzeLayout = new javax.swing.GroupLayout(Resistenze);
        Resistenze.setLayout(ResistenzeLayout);
        ResistenzeLayout.setHorizontalGroup(
            ResistenzeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ResistenzeLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(ResistenzeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17))
                .addGap(55, 55, 55)
                .addGroup(ResistenzeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tempra, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                    .addComponent(riflessi)
                    .addComponent(volonta))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 221, Short.MAX_VALUE)
                .addGroup(ResistenzeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ResistenzeLayout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addGap(30, 30, 30))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ResistenzeLayout.createSequentialGroup()
                        .addComponent(classeArmatura, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(50, 50, 50))))
        );
        ResistenzeLayout.setVerticalGroup(
            ResistenzeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ResistenzeLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(ResistenzeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(riflessi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addGroup(ResistenzeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ResistenzeLayout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(ResistenzeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16)
                            .addComponent(tempra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(36, 36, 36)
                        .addGroup(ResistenzeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(volonta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(ResistenzeLayout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(classeArmatura, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout CaratteristicheLayout = new javax.swing.GroupLayout(Caratteristiche);
        Caratteristiche.setLayout(CaratteristicheLayout);
        CaratteristicheLayout.setHorizontalGroup(
            CaratteristicheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CaratteristicheLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CaratteristicheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Attributi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Resistenze, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        CaratteristicheLayout.setVerticalGroup(
            CaratteristicheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CaratteristicheLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Attributi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(Resistenze, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        SchedaTabbedPane.addTab("Caratteristiche", Caratteristiche);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 595, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 647, Short.MAX_VALUE)
        );

        SchedaTabbedPane.addTab("Abilità", jPanel3);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 595, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 647, Short.MAX_VALUE)
        );

        SchedaTabbedPane.addTab("Equipaggiamento", jPanel4);

        Salva.setText("Salva");
        Salva.setMaximumSize(new java.awt.Dimension(80, 40));
        Salva.setMinimumSize(new java.awt.Dimension(80, 40));
        Salva.setPreferredSize(new java.awt.Dimension(80, 40));
        Salva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalvaActionPerformed(evt);
            }
        });

        jButton1.setText("Annulla");
        jButton1.setMaximumSize(new java.awt.Dimension(80, 40));
        jButton1.setMinimumSize(new java.awt.Dimension(80, 40));
        jButton1.setPreferredSize(new java.awt.Dimension(80, 40));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Salva, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(SchedaTabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(SchedaTabbedPane, javax.swing.GroupLayout.PREFERRED_SIZE, 675, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Salva, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Allineamento1TextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Allineamento1TextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Allineamento1TextFieldActionPerformed

    private void SalvaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalvaActionPerformed
        
        try
        {

            RemoteInterface temp = Client.getRemoteInterface();
            scheda.addCampo( "nome" , temp.newCampoTestuale( "nome" , NomeTextField.getText() ) );
            scheda.addCampo( "razza" , temp.newCampoTestuale("razza" , (String) RazzaTextField.getText()) );
            scheda.addCampo( "classe" , temp.newCampoTestuale("classe" , (String) ClasseTextField.getText()) );
            scheda.addCampo( "taglia" , temp.newCampoTestuale("taglia" , TagliaTextField.getText() ) );
            scheda.addCampo( "allineamento" , temp.newCampoTestuale("allineamento" , (String) Allineamento1TextField.getText()  + " " + (String) Allineamento2TextField.getText() ) );
            
            /*
            scheda.addCampo( "nome" , new CampoTestuale( "nome" , NomeTextField.getText() ) );
            scheda.addCampo( "razza" , new CampoTestuale( "razza" , (String) RazzaTextField.getText()) );
            scheda.addCampo( "classe" , new CampoTestuale( "classe" , (String) ClasseTextField.getText()) );
            scheda.addCampo( "taglia" , new CampoTestuale( "taglia" , TagliaTextField.getText() ) );
            scheda.addCampo( "allineamento" , new CampoTestuale( "allineamento" , (String) Allineamento1TextField.getText()  + " " + (String) Allineamento2TextField.getText() ) );
            */
            /*
            for ( String key : scheda.getChiaviCampi() )
            {
				System.out.println( scheda.getCampo(key) );
            }*/
            
            //CO7 ValidaScheda
            try 
            {
                if( tavolo.ValidaScheda(scheda) )
                {
                    System.out.println("Scheda Validata Con successo");
                    mainWindow.MostraPannelloSessione(tavolo);
                    this.dispose();
                }
                else
                {
                    System.out.println("Scheda non Validata");
                }
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
        catch(RemoteException ex) {
			ex.printStackTrace();
		}
    }//GEN-LAST:event_SalvaActionPerformed



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Allineamento1TextField;
    private javax.swing.JTextField Allineamento2TextField;
    private javax.swing.JLabel AllineamentoLabel;
    private javax.swing.JPanel Attributi;
    private javax.swing.JPanel Caratteristiche;
    private javax.swing.JLabel ClasseLabel;
    private javax.swing.JTextField ClasseTextField;
    private javax.swing.JPanel InformazioniGeneraliPannello;
    private javax.swing.JLabel NomeLabel;
    private javax.swing.JTextField NomeTextField;
    private javax.swing.JLabel RazzaLabel;
    private javax.swing.JTextField RazzaTextField;
    private javax.swing.JPanel Resistenze;
    private javax.swing.JButton Salva;
    private javax.swing.JTabbedPane SchedaTabbedPane;
    private javax.swing.JLabel TagliaLabel;
    private javax.swing.JTextField TagliaTextField;
    private javax.swing.JTextField carisma;
    private javax.swing.JTextField classeArmatura;
    private javax.swing.JTextField costituzione;
    private javax.swing.JTextField destrezza;
    private javax.swing.JTextField forza;
    private javax.swing.JTextField intelligenza;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTextField mod_carisma;
    private javax.swing.JTextField mod_costituzione;
    private javax.swing.JTextField mod_destrezza;
    private javax.swing.JTextField mod_forza;
    private javax.swing.JTextField mod_intelligenza;
    private javax.swing.JTextField mod_saggezza;
    private javax.swing.JTextField riflessi;
    private javax.swing.JTextField saggezza;
    private javax.swing.JTextField tempra;
    private javax.swing.JTextField volonta;
    // End of variables declaration//GEN-END:variables
}
