package UI;

import ManaGDR_RemoteInterface.*;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import javax.swing.JFrame;


public class UCreazioneCreator
{
    public static UCreazioneCreator handle;     
    
    
    private UCreazioneCreator(){}
    
    
    public static UCreazioneCreator getInstance() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
        if (handle == null)
            handle = new UCreazioneCreator();
        return handle;
    }
    
    
    public JFrame Create( String c , ITavolo t , IScheda s , MainWindow mW ) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
        java.lang.reflect.Method method = UCreazioneCreator.class.getMethod("get" + c , ITavolo.class , IScheda.class , MainWindow.class );
        return (JFrame) method.invoke( UCreazioneCreator.class , t , s , mW );
    }
    
    
    public static JFrame getOpzioneCreazioneManuale( ITavolo t , IScheda s , MainWindow mainWindow )
    {
        return new UCreazioneManuale( t , s , mainWindow );
    }
    
    
    public static JFrame getOpzioneCreazioneRandom( ITavolo t , IScheda s , MainWindow mainWindow ) throws RemoteException
    {
        return new UCreazioneRandom( t , s , mainWindow );
    }
    
    
    public static JFrame getOpzioneCreazioneNPC( ITavolo t , IScheda s , MainWindow mainWindow )
    {
        return new UCreazioneNpc( t , s , mainWindow );
    }
    

}
