package domain;

import ManaGDR_RemoteInterface.*;


public interface File_Adapter extends RegolamentoUploader
{
	public IModuloDiRegolamento parse( Object o ) throws Exception;
}
