package domain;

import ManaGDR_Client.Client;
import java.io.File;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import java.lang.reflect.*;

import ManaGDR_RemoteInterface.*;


public class XMLFile_Adaptee implements File_Adapter
{
	private static DocumentBuilderFactory dbfactory;
	private static DocumentBuilder db;
	private static Document doc;


	public IModuloDiRegolamento parse( Object o ) throws Exception
	{
		IModuloDiRegolamento nuovoModulo;
		RegolaComponent setRegole;

		if( o.getClass() != File.class )
			throw new Exception("XMLFile_Adaptee: wrong class\nExpected: " + File.class.toString() + "\nFound: " + o.getClass().toString() );

		try {
			dbfactory = DocumentBuilderFactory.newInstance();
			db = dbfactory.newDocumentBuilder();
			doc = db.parse( (File)o );
		} catch( Exception e ) {
			e.printStackTrace();
			return null;
		}

		doc.getDocumentElement().normalize();
		// Crea il nuovo modulo e il suo set di regole
		nuovoModulo = Client.getRemoteInterface().newModuloRegolamento();
		setRegole = Client.getRemoteInterface().newRegolaComposite();
		NodeList listaNodi = doc.getElementsByTagName( "regolamento" );
		// Prendiamo il primo regolamento, ignorando i successivi
		Node regolamento = listaNodi.item(0);
		// Se è un Element...(quasi scontato)
		if (regolamento.getNodeType() == Node.ELEMENT_NODE) {
			Element e = (Element) regolamento;

			// Ricava i tag "regola"
			NodeList regole = e.getElementsByTagName("regola");
			for (int j = 0; j < regole.getLength(); j++) {

				// per ogni j-esima regola...
				Element regola = (Element) regole.item(j);

				Regola r;
				try {
					r = Client.getRemoteInterface().NuovaRegola(regola.getAttribute("tipo"));
				} catch( Exception ex )
				{
					System.err.println("Parsing XML: Impossibile creare regola \"" + regola.getAttribute("tipo") + "\"");
					ex.printStackTrace();
					continue; // Skippa in caso di errori
				}

				if( r == null ) {
					System.err.println("Parsing XML: Regola nulla!");
					continue;
				}

				// Imposta il nome del campo al quale la regola fa riferimento
				if( SetNomeRegola(regola,r) == false ) {
					System.err.println("Parsing XML: Impossibile assegnare il campo di riferimento della regola");
					continue; // se ci sono problemi, la salta
				}

				// Imposta i vari attributi della classe chiamando metodi appositi
				// WARNING - prestare attenzione a come si scrivono gli XML!
				if( SetAttributiRegola(regola,r) == false ) {
					System.err.println("Parsing XML: Errore nell'assegnazione attributi");
					continue; // Skippa se problemi
				}

				// Aggiungo la regola
				setRegole = AggiungiRegola( setRegole, r );
			}
		}
		// Assegnazione finale
		return Client.getRemoteInterface().AssociaRegolamento(nuovoModulo, setRegole);
	}


	private boolean SetNomeRegola(Element elemento, Regola r)
	{
		Method metodo;

		try {
			metodo = r.getClass().getMethod("setNomeCampo", String.class );
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		try {
			metodo.invoke(r, elemento.getAttribute("nome"));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}


	private boolean SetAttributiRegola(Element elemento, Regola r)
	{
		// WARNING - prestare attenzione a come si scrivono gli XML!
		NodeList figli = elemento.getElementsByTagName("*"); // "*" fa da wildcard

		// Scorriamo tutti i figli dell'elemento...
		for (int k = 0; k < figli.getLength(); k++) {
			// k-esimo figlio...
			Element figlio = (Element)figli.item(k);
			// Costruisco il nome del metodo setter (Warning! XML!)
			String nomeMetodo = "set" + figlio.getNodeName();
			// Ricavo il tipo dato da usare (Warning! XML!)
			ChiamaSetter( nomeMetodo, getClasse(figlio), figlio, r );
		}
		return true;
	}


	private Class getClasse(Element e)
	{
		Class result;

		if( e.getAttribute("tipo").equals("int") )
			return int.class;

		if( e.getAttribute("tipo").equals("String") )
			return String.class;

		try {
			result = Class.forName("domain." + e.getAttribute("tipo"));
		} catch( ClassNotFoundException ex ) {
			ex.printStackTrace();
			return String.class;
		}
		return result;
	}

	private boolean ChiamaSetter( String m, Class c, Element e, Regola r )
	{
		Method metodo;
		try {
			metodo = r.getClass().getMethod(m, c);
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

		if( c == int.class ) {
			int temp = 0;
			temp = Integer.parseInt( e.getFirstChild().getNodeValue().toString() );

			try {
				metodo.invoke(r, temp );
			} catch (Exception ex) {
				ex.printStackTrace();
				return false;
			}
		}

		if( c == String.class ) {
			try {
				metodo.invoke(r, e.getFirstChild().getNodeValue().toString() );
			} catch (Exception ex) {
				ex.printStackTrace();
				return false;
			}
		}

		return true;
	}

	// pattern GRASP - INDIRECTION
	private RegolaComponent AggiungiRegola( RegolaComponent component, Regola r )
	{
		// Indirezione per ovviare alla non-conoscenza da parte del client
		// della classe RegolaComposite
		RegolaComponent result = null;
		try {
			result = Client.getRemoteInterface().AggiungiRegola( component, r );
		} catch( Exception ex ) {
			ex.printStackTrace();
		}
		return result;
	}
}

