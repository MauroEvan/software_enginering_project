package domain;

import java.util.Date;
import java.rmi.RemoteException;

import ManaGDR_RemoteInterface.*;


// SINGLETON
public class DatiLogin
{
	private IGiocatore giocatore;
	private Date ultimoLogin;
	private static DatiLogin handle = null;


	protected DatiLogin() {};


	public static DatiLogin getInstance() throws RemoteException
	{
		if (handle == null)
			handle = new DatiLogin();
		return handle;
	}


	public IGiocatore getGiocatore() throws RemoteException {return giocatore;}
	public void setGiocatore(IGiocatore giocatore) throws RemoteException {this.giocatore = giocatore;}
	public Date getUltimoLogin() throws RemoteException {return ultimoLogin;}
	public void setUltimoLogin(Date ultimoLogin) throws RemoteException {this.ultimoLogin = ultimoLogin;}
}