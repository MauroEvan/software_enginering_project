package domain;

import ManaGDR_RemoteInterface.IModuloDiRegolamento;


public interface RegolamentoUploader
{
	public IModuloDiRegolamento parse( Object o )  throws Exception;
}
