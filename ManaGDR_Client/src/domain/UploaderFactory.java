package domain;

public class UploaderFactory
{
	private static UploaderFactory handle = null;


	protected UploaderFactory() {};

	public static UploaderFactory getInstance()
	{
		if( handle == null )
			handle = new UploaderFactory();
		return handle;
	}

	
	public static RegolamentoUploader newUploader(String classe)
	{
		Class c;
		RegolamentoUploader result = null;

		try {
			c = Class.forName(classe);
			result = (RegolamentoUploader) c.newInstance();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(-10);
		}

		return result;
	}
}
