/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import domain.*;

public class RegolaGenericaDAOImpl implements domain.dao.RegolaGenericaDAO {
	public RegolaGenerica loadRegolaGenericaByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaGenericaByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica getRegolaGenericaByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getRegolaGenericaByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica loadRegolaGenericaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaGenericaByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica getRegolaGenericaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getRegolaGenericaByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica loadRegolaGenericaByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaGenerica) session.load(domain.RegolaGenerica.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica getRegolaGenericaByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaGenerica) session.get(domain.RegolaGenerica.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica loadRegolaGenericaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaGenerica) session.load(domain.RegolaGenerica.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica getRegolaGenericaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaGenerica) session.get(domain.RegolaGenerica.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaGenerica(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryRegolaGenerica(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaGenerica(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryRegolaGenerica(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica[] listRegolaGenericaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listRegolaGenericaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica[] listRegolaGenericaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listRegolaGenericaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaGenerica(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaGenerica as RegolaGenerica");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaGenerica(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaGenerica as RegolaGenerica");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaGenerica", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica[] listRegolaGenericaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRegolaGenerica(session, condition, orderBy);
			return (RegolaGenerica[]) list.toArray(new RegolaGenerica[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica[] listRegolaGenericaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRegolaGenerica(session, condition, orderBy, lockMode);
			return (RegolaGenerica[]) list.toArray(new RegolaGenerica[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica loadRegolaGenericaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaGenericaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica loadRegolaGenericaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaGenericaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica loadRegolaGenericaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		RegolaGenerica[] regolaGenericas = listRegolaGenericaByQuery(session, condition, orderBy);
		if (regolaGenericas != null && regolaGenericas.length > 0)
			return regolaGenericas[0];
		else
			return null;
	}
	
	public RegolaGenerica loadRegolaGenericaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		RegolaGenerica[] regolaGenericas = listRegolaGenericaByQuery(session, condition, orderBy, lockMode);
		if (regolaGenericas != null && regolaGenericas.length > 0)
			return regolaGenericas[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateRegolaGenericaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaGenericaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaGenericaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaGenericaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaGenericaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaGenerica as RegolaGenerica");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaGenericaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaGenerica as RegolaGenerica");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaGenerica", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaGenerica createRegolaGenerica() {
		return new domain.RegolaGenerica();
	}
	
	public boolean save(domain.RegolaGenerica regolaGenerica) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().saveObject(regolaGenerica);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(domain.RegolaGenerica regolaGenerica) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().deleteObject(regolaGenerica);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.RegolaGenerica regolaGenerica)throws PersistentException {
		try {
			if(regolaGenerica.getRegolaComposite() != null) {
				regolaGenerica.getRegolaComposite().regolaGenerica.remove(regolaGenerica);
			}
			
			return delete(regolaGenerica);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.RegolaGenerica regolaGenerica, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(regolaGenerica.getRegolaComposite() != null) {
				regolaGenerica.getRegolaComposite().regolaGenerica.remove(regolaGenerica);
			}
			
			try {
				session.delete(regolaGenerica);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(domain.RegolaGenerica regolaGenerica) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().refresh(regolaGenerica);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(domain.RegolaGenerica regolaGenerica) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().evict(regolaGenerica);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
