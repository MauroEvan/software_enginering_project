/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import domain.*;

public class CampoAbilitàDAOImpl implements domain.dao.CampoAbilitàDAO {
	public CampoAbilità loadCampoAbilitàByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadCampoAbilitàByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità getCampoAbilitàByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getCampoAbilitàByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità loadCampoAbilitàByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadCampoAbilitàByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità getCampoAbilitàByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getCampoAbilitàByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità loadCampoAbilitàByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (CampoAbilità) session.load(domain.CampoAbilità.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità getCampoAbilitàByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (CampoAbilità) session.get(domain.CampoAbilità.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità loadCampoAbilitàByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (CampoAbilità) session.load(domain.CampoAbilità.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità getCampoAbilitàByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (CampoAbilità) session.get(domain.CampoAbilità.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoAbilità(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryCampoAbilità(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoAbilità(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryCampoAbilità(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità[] listCampoAbilitàByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listCampoAbilitàByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità[] listCampoAbilitàByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listCampoAbilitàByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoAbilità(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.CampoAbilità as CampoAbilità");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoAbilità(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.CampoAbilità as CampoAbilità");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("CampoAbilità", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità[] listCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryCampoAbilità(session, condition, orderBy);
			return (CampoAbilità[]) list.toArray(new CampoAbilità[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità[] listCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryCampoAbilità(session, condition, orderBy, lockMode);
			return (CampoAbilità[]) list.toArray(new CampoAbilità[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità loadCampoAbilitàByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadCampoAbilitàByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità loadCampoAbilitàByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadCampoAbilitàByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità loadCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		CampoAbilità[] campoAbilitàs = listCampoAbilitàByQuery(session, condition, orderBy);
		if (campoAbilitàs != null && campoAbilitàs.length > 0)
			return campoAbilitàs[0];
		else
			return null;
	}
	
	public CampoAbilità loadCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		CampoAbilità[] campoAbilitàs = listCampoAbilitàByQuery(session, condition, orderBy, lockMode);
		if (campoAbilitàs != null && campoAbilitàs.length > 0)
			return campoAbilitàs[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateCampoAbilitàByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateCampoAbilitàByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoAbilitàByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateCampoAbilitàByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.CampoAbilità as CampoAbilità");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.CampoAbilità as CampoAbilità");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("CampoAbilità", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilità createCampoAbilità() {
		return new domain.CampoAbilità();
	}
	
	public boolean save(domain.CampoAbilità campoAbilità) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().saveObject(campoAbilità);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(domain.CampoAbilità campoAbilità) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().deleteObject(campoAbilità);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.CampoAbilità campoAbilità)throws PersistentException {
		try {
			if(campoAbilità.getScheda() != null) {
				campoAbilità.getScheda().campoAbilità.remove(campoAbilità);
			}
			
			return delete(campoAbilità);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.CampoAbilità campoAbilità, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(campoAbilità.getScheda() != null) {
				campoAbilità.getScheda().campoAbilità.remove(campoAbilità);
			}
			
			try {
				session.delete(campoAbilità);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(domain.CampoAbilità campoAbilità) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().refresh(campoAbilità);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(domain.CampoAbilità campoAbilità) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().evict(campoAbilità);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
