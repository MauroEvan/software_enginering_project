/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

public class Scheda {
	public Scheda() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == domain.ORMConstants.KEY_SCHEDA_CAMPORESISTENZE) {
			return ORM_campoResistenze;
		}
		else if (key == domain.ORMConstants.KEY_SCHEDA_CAMPOABILITÀ) {
			return ORM_campoAbilità;
		}
		else if (key == domain.ORMConstants.KEY_SCHEDA_CAMPOTESTUALE) {
			return ORM_campoTestuale;
		}
		else if (key == domain.ORMConstants.KEY_SCHEDA_CAMPONUMERICO) {
			return ORM_campoNumerico;
		}
		else if (key == domain.ORMConstants.KEY_SCHEDA_CAMPOOGGETTO) {
			return ORM_campoOggetto;
		}
		else if (key == domain.ORMConstants.KEY_SCHEDA_CAMPOATTRIBUTO) {
			return ORM_campoAttributo;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int ID;
	
	private java.util.Set ORM_campoResistenze = new java.util.HashSet();
	
	private java.util.Set ORM_campoAbilità = new java.util.HashSet();
	
	private java.util.Set ORM_campoTestuale = new java.util.HashSet();
	
	private java.util.Set ORM_campoNumerico = new java.util.HashSet();
	
	private java.util.Set ORM_campoOggetto = new java.util.HashSet();
	
	private java.util.Set ORM_campoAttributo = new java.util.HashSet();
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	private void setORM_CampoResistenze(java.util.Set value) {
		this.ORM_campoResistenze = value;
	}
	
	private java.util.Set getORM_CampoResistenze() {
		return ORM_campoResistenze;
	}
	
	public final domain.CampoResistenzeSetCollection campoResistenze = new domain.CampoResistenzeSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_SCHEDA_CAMPORESISTENZE, domain.ORMConstants.KEY_CAMPORESISTENZE_SCHEDA, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_CampoAbilità(java.util.Set value) {
		this.ORM_campoAbilità = value;
	}
	
	private java.util.Set getORM_CampoAbilità() {
		return ORM_campoAbilità;
	}
	
	public final domain.CampoAbilitàSetCollection campoAbilità = new domain.CampoAbilitàSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_SCHEDA_CAMPOABILITÀ, domain.ORMConstants.KEY_CAMPOABILITÀ_SCHEDA, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_CampoTestuale(java.util.Set value) {
		this.ORM_campoTestuale = value;
	}
	
	private java.util.Set getORM_CampoTestuale() {
		return ORM_campoTestuale;
	}
	
	public final domain.CampoTestualeSetCollection campoTestuale = new domain.CampoTestualeSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_SCHEDA_CAMPOTESTUALE, domain.ORMConstants.KEY_CAMPOTESTUALE_SCHEDA, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_CampoNumerico(java.util.Set value) {
		this.ORM_campoNumerico = value;
	}
	
	private java.util.Set getORM_CampoNumerico() {
		return ORM_campoNumerico;
	}
	
	public final domain.CampoNumericoSetCollection campoNumerico = new domain.CampoNumericoSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_SCHEDA_CAMPONUMERICO, domain.ORMConstants.KEY_CAMPONUMERICO_SCHEDA, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_CampoOggetto(java.util.Set value) {
		this.ORM_campoOggetto = value;
	}
	
	private java.util.Set getORM_CampoOggetto() {
		return ORM_campoOggetto;
	}
	
	public final domain.CampoOggettoSetCollection campoOggetto = new domain.CampoOggettoSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_SCHEDA_CAMPOOGGETTO, domain.ORMConstants.KEY_CAMPOOGGETTO_SCHEDA, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_CampoAttributo(java.util.Set value) {
		this.ORM_campoAttributo = value;
	}
	
	private java.util.Set getORM_CampoAttributo() {
		return ORM_campoAttributo;
	}
	
	public final domain.CampoAttributoSetCollection campoAttributo = new domain.CampoAttributoSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_SCHEDA_CAMPOATTRIBUTO, domain.ORMConstants.KEY_CAMPOATTRIBUTO_SCHEDA, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public void addCampo(domain.Campo c) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public domain.Campo getCampo(String key) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public string[0..*] getChiaviCampi() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
