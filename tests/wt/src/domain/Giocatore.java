/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

public class Giocatore {
	public Giocatore() {
	}
	
	private int ID;
	
	private String nomeGiocatore;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setNomeGiocatore(String value) {
		this.nomeGiocatore = value;
	}
	
	public String getNomeGiocatore() {
		return nomeGiocatore;
	}
	
	public void aggiungiScheda(domain.Scheda s) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
