/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

public class RegolaCampoTestuale implements domain.RegolaCampo {
	public RegolaCampoTestuale() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == domain.ORMConstants.KEY_REGOLACAMPOTESTUALE_REGOLACOMPOSITE) {
			this.regolaComposite = (domain.RegolaComposite) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;
	
	private String token;
	
	private String nomeCampo;
	
	private domain.RegolaComposite regolaComposite;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setToken(String value) {
		this.token = value;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setNomeCampo(String value) {
		this.nomeCampo = value;
	}
	
	public String getNomeCampo() {
		return nomeCampo;
	}
	
	public void setRegolaComposite(domain.RegolaComposite value) {
		if (regolaComposite != null) {
			regolaComposite.regolaCampoTestuale.remove(this);
		}
		if (value != null) {
			value.regolaCampoTestuale.add(this);
		}
	}
	
	public domain.RegolaComposite getRegolaComposite() {
		return regolaComposite;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_RegolaComposite(domain.RegolaComposite value) {
		this.regolaComposite = value;
	}
	
	private domain.RegolaComposite getORM_RegolaComposite() {
		return regolaComposite;
	}
	
	public boolean ApplicaRegolaSuScheda(domain.Scheda s) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
