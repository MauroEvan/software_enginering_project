/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

public class CampoNumerico implements domain.Campo {
	public CampoNumerico() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == domain.ORMConstants.KEY_CAMPONUMERICO_SCHEDA) {
			this.scheda = (domain.Scheda) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;
	
	private String nome;
	
	private int valore;
	
	private domain.Scheda scheda;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setNome(String value) {
		this.nome = value;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setValore(int value) {
		this.valore = value;
	}
	
	public int getValore() {
		return valore;
	}
	
	public void setScheda(domain.Scheda value) {
		if (scheda != null) {
			scheda.campoNumerico.remove(this);
		}
		if (value != null) {
			value.campoNumerico.add(this);
		}
	}
	
	public domain.Scheda getScheda() {
		return scheda;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Scheda(domain.Scheda value) {
		this.scheda = value;
	}
	
	private domain.Scheda getORM_Scheda() {
		return scheda;
	}
	
	private int Randomizer() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
