/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

public class CampoAttributo implements domain.Campo {
	public CampoAttributo() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == domain.ORMConstants.KEY_CAMPOATTRIBUTO_SCHEDA) {
			this.scheda = (domain.Scheda) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;
	
	private Integer nome;
	
	private int valore;
	
	private int modificatore;
	
	private domain.Scheda scheda;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setNome(int value) {
		setNome(new Integer(value));
	}
	
	public void setNome(Integer value) {
		this.nome = value;
	}
	
	public Integer getNome() {
		return nome;
	}
	
	public void setValore(int value) {
		this.valore = value;
	}
	
	public int getValore() {
		return valore;
	}
	
	public void setModificatore(int value) {
		this.modificatore = value;
	}
	
	public int getModificatore() {
		return modificatore;
	}
	
	public void setScheda(domain.Scheda value) {
		if (scheda != null) {
			scheda.campoAttributo.remove(this);
		}
		if (value != null) {
			value.campoAttributo.add(this);
		}
	}
	
	public domain.Scheda getScheda() {
		return scheda;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Scheda(domain.Scheda value) {
		this.scheda = value;
	}
	
	private domain.Scheda getORM_Scheda() {
		return scheda;
	}
	
	private int Randomizer() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
