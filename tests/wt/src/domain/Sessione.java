/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

public class Sessione {
	public Sessione() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == domain.ORMConstants.KEY_SESSIONE_MODULOREGOLAMENTO) {
			return ORM_moduloRegolamento;
		}
		else if (key == domain.ORMConstants.KEY_SESSIONE_EVENTI) {
			return ORM_eventi;
		}
		else if (key == domain.ORMConstants.KEY_SESSIONE_SCHEDE) {
			return ORM_schede;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int ID;
	
	private domain.Giocatore gm;
	
	private domain.Mappa mappa;
	
	private String descr;
	
	private java.util.Set ORM_moduloRegolamento = new java.util.HashSet();
	
	private java.util.Set ORM_eventi = new java.util.HashSet();
	
	private java.util.Set ORM_schede = new java.util.HashSet();
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setDescr(String value) {
		this.descr = value;
	}
	
	public String getDescr() {
		return descr;
	}
	
	public void setMappa(domain.Mappa value) {
		this.mappa = value;
	}
	
	public domain.Mappa getMappa() {
		return mappa;
	}
	
	private void setORM_ModuloRegolamento(java.util.Set value) {
		this.ORM_moduloRegolamento = value;
	}
	
	private java.util.Set getORM_ModuloRegolamento() {
		return ORM_moduloRegolamento;
	}
	
	public final domain.ModuloDiRegolamentoSetCollection moduloRegolamento = new domain.ModuloDiRegolamentoSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_SESSIONE_MODULOREGOLAMENTO, domain.ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	private void setORM_Eventi(java.util.Set value) {
		this.ORM_eventi = value;
	}
	
	private java.util.Set getORM_Eventi() {
		return ORM_eventi;
	}
	
	public final domain.EventoSetCollection eventi = new domain.EventoSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_SESSIONE_EVENTI, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Schede(java.util.Set value) {
		this.ORM_schede = value;
	}
	
	private java.util.Set getORM_Schede() {
		return ORM_schede;
	}
	
	public final domain.SchedaSetCollection schede = new domain.SchedaSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_SESSIONE_SCHEDE, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public void setGm(domain.Giocatore value) {
		this.gm = value;
	}
	
	public domain.Giocatore getGm() {
		return gm;
	}
	
	public void ottieniITuoiDati() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public void associaSchedeLibere() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public Sessione(domain.Giocatore gm) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public domain.Scheda getScheda(int index) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public void setIdSessione(int i) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public boolean isGM(domain.Giocatore g) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public domain.Regola[0..*] ApplicaRegoleAScheda(domain.Scheda s) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public domain.Scheda AggiungiSchedaVuota() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
