/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

public class RegolaGenerica implements domain.Regola {
	public RegolaGenerica() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == domain.ORMConstants.KEY_REGOLAGENERICA_REGOLACOMPOSITE) {
			this.regolaComposite = (domain.RegolaComposite) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;
	
	private String nomeRegola;
	
	private String testoRegola;
	
	private domain.RegolaComposite regolaComposite;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setNomeRegola(String value) {
		this.nomeRegola = value;
	}
	
	public String getNomeRegola() {
		return nomeRegola;
	}
	
	public void setTestoRegola(String value) {
		this.testoRegola = value;
	}
	
	public String getTestoRegola() {
		return testoRegola;
	}
	
	public void setRegolaComposite(domain.RegolaComposite value) {
		if (regolaComposite != null) {
			regolaComposite.regolaGenerica.remove(this);
		}
		if (value != null) {
			value.regolaGenerica.add(this);
		}
	}
	
	public domain.RegolaComposite getRegolaComposite() {
		return regolaComposite;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_RegolaComposite(domain.RegolaComposite value) {
		this.regolaComposite = value;
	}
	
	private domain.RegolaComposite getORM_RegolaComposite() {
		return regolaComposite;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
