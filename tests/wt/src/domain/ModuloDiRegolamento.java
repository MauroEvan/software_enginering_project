/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

public class ModuloDiRegolamento {
	public ModuloDiRegolamento() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == domain.ORMConstants.KEY_MODULODIREGOLAMENTO_REGOLACOMPOSITE) {
			this.regolaComposite = (domain.RegolaComposite) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;
	
	private domain.RegolaComposite regolaComposite;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setRegolaComposite(domain.RegolaComposite value) {
		if (this.regolaComposite != value) {
			domain.RegolaComposite lregolaComposite = this.regolaComposite;
			this.regolaComposite = value;
			if (value != null) {
				regolaComposite.setModulo(this);
			}
			else {
				lregolaComposite.setModulo(null);
			}
		}
	}
	
	public domain.RegolaComposite getRegolaComposite() {
		return regolaComposite;
	}
	
	public domain.Regola[0..*] ApplicaSuScheda(domain.Scheda s) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
