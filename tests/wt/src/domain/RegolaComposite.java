/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

public class RegolaComposite implements domain.RegolaComponent {
	public RegolaComposite() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLAGENERICA) {
			return ORM_regolaGenerica;
		}
		else if (key == domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPONUMERICO) {
			return ORM_regolaCampoNumerico;
		}
		else if (key == domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPORESISTENZE) {
			return ORM_regolaCampoResistenze;
		}
		else if (key == domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOATTRIBUTO) {
			return ORM_regolaCampoAttributo;
		}
		else if (key == domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOABILITÀ) {
			return ORM_regolaCampoAbilità;
		}
		else if (key == domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOOGGETTO) {
			return ORM_regolaCampoOggetto;
		}
		else if (key == domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOTESTUALE) {
			return ORM_regolaCampoTestuale;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int ID;
	
	private String nome;
	
	private domain.ModuloDiRegolamento modulo;
	
	private java.util.Set ORM_regolaGenerica = new java.util.HashSet();
	
	private java.util.Set ORM_regolaCampoNumerico = new java.util.HashSet();
	
	private java.util.Set ORM_regolaCampoResistenze = new java.util.HashSet();
	
	private java.util.Set ORM_regolaCampoAttributo = new java.util.HashSet();
	
	private java.util.Set ORM_regolaCampoAbilità = new java.util.HashSet();
	
	private java.util.Set ORM_regolaCampoOggetto = new java.util.HashSet();
	
	private java.util.Set ORM_regolaCampoTestuale = new java.util.HashSet();
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setNome(String value) {
		this.nome = value;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setModulo(domain.ModuloDiRegolamento value) {
		if (this.modulo != value) {
			domain.ModuloDiRegolamento lmodulo = this.modulo;
			this.modulo = value;
			if (value != null) {
				modulo.setRegolaComposite(this);
			}
			else {
				lmodulo.setRegolaComposite(null);
			}
		}
	}
	
	public domain.ModuloDiRegolamento getModulo() {
		return modulo;
	}
	
	private void setORM_RegolaGenerica(java.util.Set value) {
		this.ORM_regolaGenerica = value;
	}
	
	private java.util.Set getORM_RegolaGenerica() {
		return ORM_regolaGenerica;
	}
	
	public final domain.RegolaGenericaSetCollection regolaGenerica = new domain.RegolaGenericaSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLAGENERICA, domain.ORMConstants.KEY_REGOLAGENERICA_REGOLACOMPOSITE, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_RegolaCampoNumerico(java.util.Set value) {
		this.ORM_regolaCampoNumerico = value;
	}
	
	private java.util.Set getORM_RegolaCampoNumerico() {
		return ORM_regolaCampoNumerico;
	}
	
	public final domain.RegolaCampoNumericoSetCollection regolaCampoNumerico = new domain.RegolaCampoNumericoSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPONUMERICO, domain.ORMConstants.KEY_REGOLACAMPONUMERICO_REGOLACOMPOSITE, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_RegolaCampoResistenze(java.util.Set value) {
		this.ORM_regolaCampoResistenze = value;
	}
	
	private java.util.Set getORM_RegolaCampoResistenze() {
		return ORM_regolaCampoResistenze;
	}
	
	public final domain.RegolaCampoResistenzeSetCollection regolaCampoResistenze = new domain.RegolaCampoResistenzeSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPORESISTENZE, domain.ORMConstants.KEY_REGOLACAMPORESISTENZE_REGOLACOMPOSITE, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_RegolaCampoAttributo(java.util.Set value) {
		this.ORM_regolaCampoAttributo = value;
	}
	
	private java.util.Set getORM_RegolaCampoAttributo() {
		return ORM_regolaCampoAttributo;
	}
	
	public final domain.RegolaCampoAttributoSetCollection regolaCampoAttributo = new domain.RegolaCampoAttributoSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOATTRIBUTO, domain.ORMConstants.KEY_REGOLACAMPOATTRIBUTO_REGOLACOMPOSITE, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_RegolaCampoAbilità(java.util.Set value) {
		this.ORM_regolaCampoAbilità = value;
	}
	
	private java.util.Set getORM_RegolaCampoAbilità() {
		return ORM_regolaCampoAbilità;
	}
	
	public final domain.RegolaCampoAbilitàSetCollection regolaCampoAbilità = new domain.RegolaCampoAbilitàSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOABILITÀ, domain.ORMConstants.KEY_REGOLACAMPOABILITÀ_REGOLACOMPOSITE, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_RegolaCampoOggetto(java.util.Set value) {
		this.ORM_regolaCampoOggetto = value;
	}
	
	private java.util.Set getORM_RegolaCampoOggetto() {
		return ORM_regolaCampoOggetto;
	}
	
	public final domain.RegolaCampoOggettoSetCollection regolaCampoOggetto = new domain.RegolaCampoOggettoSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOOGGETTO, domain.ORMConstants.KEY_REGOLACAMPOOGGETTO_REGOLACOMPOSITE, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_RegolaCampoTestuale(java.util.Set value) {
		this.ORM_regolaCampoTestuale = value;
	}
	
	private java.util.Set getORM_RegolaCampoTestuale() {
		return ORM_regolaCampoTestuale;
	}
	
	public final domain.RegolaCampoTestualeSetCollection regolaCampoTestuale = new domain.RegolaCampoTestualeSetCollection(this, _ormAdapter, domain.ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOTESTUALE, domain.ORMConstants.KEY_REGOLACAMPOTESTUALE_REGOLACOMPOSITE, domain.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public domain.RegolaComponent getChild(int index) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public void addChild(domain.RegolaComponent regola) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public void removeChild(int index) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public boolean Applica(int Object o) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
