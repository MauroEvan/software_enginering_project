/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import domain.*;

public interface SessioneDAO {
	public Sessione loadSessioneByORMID(int ID) throws PersistentException;
	public Sessione getSessioneByORMID(int ID) throws PersistentException;
	public Sessione loadSessioneByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Sessione getSessioneByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Sessione loadSessioneByORMID(PersistentSession session, int ID) throws PersistentException;
	public Sessione getSessioneByORMID(PersistentSession session, int ID) throws PersistentException;
	public Sessione loadSessioneByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Sessione getSessioneByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Sessione[] listSessioneByQuery(String condition, String orderBy) throws PersistentException;
	public Sessione[] listSessioneByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List querySessione(String condition, String orderBy) throws PersistentException;
	public java.util.List querySessione(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateSessioneByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateSessioneByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Sessione[] listSessioneByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public Sessione[] listSessioneByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List querySessione(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List querySessione(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateSessioneByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateSessioneByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Sessione loadSessioneByQuery(String condition, String orderBy) throws PersistentException;
	public Sessione loadSessioneByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Sessione loadSessioneByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public Sessione loadSessioneByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Sessione createSessione();
	public boolean save(domain.Sessione sessione) throws PersistentException;
	public boolean delete(domain.Sessione sessione) throws PersistentException;
	public boolean refresh(domain.Sessione sessione) throws PersistentException;
	public boolean evict(domain.Sessione sessione) throws PersistentException;
}
