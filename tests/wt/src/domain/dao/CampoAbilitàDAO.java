/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import domain.*;

public interface CampoAbilitàDAO {
	public CampoAbilità loadCampoAbilitàByORMID(int ID) throws PersistentException;
	public CampoAbilità getCampoAbilitàByORMID(int ID) throws PersistentException;
	public CampoAbilità loadCampoAbilitàByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilità getCampoAbilitàByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilità loadCampoAbilitàByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoAbilità getCampoAbilitàByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoAbilità loadCampoAbilitàByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilità getCampoAbilitàByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilità[] listCampoAbilitàByQuery(String condition, String orderBy) throws PersistentException;
	public CampoAbilità[] listCampoAbilitàByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoAbilità(String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoAbilità(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoAbilitàByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoAbilitàByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilità[] listCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoAbilità[] listCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoAbilità(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoAbilità(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilità loadCampoAbilitàByQuery(String condition, String orderBy) throws PersistentException;
	public CampoAbilità loadCampoAbilitàByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilità loadCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoAbilità loadCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilità createCampoAbilità();
	public boolean save(domain.CampoAbilità campoAbilità) throws PersistentException;
	public boolean delete(domain.CampoAbilità campoAbilità) throws PersistentException;
	public boolean deleteAndDissociate(domain.CampoAbilità campoAbilità) throws PersistentException;
	public boolean deleteAndDissociate(domain.CampoAbilità campoAbilità, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(domain.CampoAbilità campoAbilità) throws PersistentException;
	public boolean evict(domain.CampoAbilità campoAbilità) throws PersistentException;
}
