/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import domain.*;

public interface RegolaCampoAbilitàDAO {
	public RegolaCampoAbilità loadRegolaCampoAbilitàByORMID(int ID) throws PersistentException;
	public RegolaCampoAbilità getRegolaCampoAbilitàByORMID(int ID) throws PersistentException;
	public RegolaCampoAbilità loadRegolaCampoAbilitàByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilità getRegolaCampoAbilitàByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilità loadRegolaCampoAbilitàByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoAbilità getRegolaCampoAbilitàByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoAbilità loadRegolaCampoAbilitàByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilità getRegolaCampoAbilitàByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilità[] listRegolaCampoAbilitàByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoAbilità[] listRegolaCampoAbilitàByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoAbilità(String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoAbilità(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoAbilitàByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoAbilitàByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilità[] listRegolaCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoAbilità[] listRegolaCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoAbilità(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoAbilità(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilità loadRegolaCampoAbilitàByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoAbilità loadRegolaCampoAbilitàByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilità loadRegolaCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoAbilità loadRegolaCampoAbilitàByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilità createRegolaCampoAbilità();
	public boolean save(domain.RegolaCampoAbilità regolaCampoAbilità) throws PersistentException;
	public boolean delete(domain.RegolaCampoAbilità regolaCampoAbilità) throws PersistentException;
	public boolean deleteAndDissociate(domain.RegolaCampoAbilità regolaCampoAbilità) throws PersistentException;
	public boolean deleteAndDissociate(domain.RegolaCampoAbilità regolaCampoAbilità, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(domain.RegolaCampoAbilità regolaCampoAbilità) throws PersistentException;
	public boolean evict(domain.RegolaCampoAbilità regolaCampoAbilità) throws PersistentException;
}
