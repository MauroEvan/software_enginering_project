/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import domain.*;

public interface GiocatoreDAO {
	public Giocatore loadGiocatoreByORMID(int ID) throws PersistentException;
	public Giocatore getGiocatoreByORMID(int ID) throws PersistentException;
	public Giocatore loadGiocatoreByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Giocatore getGiocatoreByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Giocatore loadGiocatoreByORMID(PersistentSession session, int ID) throws PersistentException;
	public Giocatore getGiocatoreByORMID(PersistentSession session, int ID) throws PersistentException;
	public Giocatore loadGiocatoreByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Giocatore getGiocatoreByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Giocatore[] listGiocatoreByQuery(String condition, String orderBy) throws PersistentException;
	public Giocatore[] listGiocatoreByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryGiocatore(String condition, String orderBy) throws PersistentException;
	public java.util.List queryGiocatore(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateGiocatoreByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateGiocatoreByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Giocatore[] listGiocatoreByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public Giocatore[] listGiocatoreByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryGiocatore(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryGiocatore(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateGiocatoreByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateGiocatoreByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Giocatore loadGiocatoreByQuery(String condition, String orderBy) throws PersistentException;
	public Giocatore loadGiocatoreByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Giocatore loadGiocatoreByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public Giocatore loadGiocatoreByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Giocatore createGiocatore();
	public boolean save(domain.Giocatore giocatore) throws PersistentException;
	public boolean delete(domain.Giocatore giocatore) throws PersistentException;
	public boolean refresh(domain.Giocatore giocatore) throws PersistentException;
	public boolean evict(domain.Giocatore giocatore) throws PersistentException;
}
