/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import domain.*;

public interface MappaDAO {
	public Mappa loadMappaByORMID(int ID) throws PersistentException;
	public Mappa getMappaByORMID(int ID) throws PersistentException;
	public Mappa loadMappaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Mappa getMappaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Mappa loadMappaByORMID(PersistentSession session, int ID) throws PersistentException;
	public Mappa getMappaByORMID(PersistentSession session, int ID) throws PersistentException;
	public Mappa loadMappaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Mappa getMappaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Mappa[] listMappaByQuery(String condition, String orderBy) throws PersistentException;
	public Mappa[] listMappaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryMappa(String condition, String orderBy) throws PersistentException;
	public java.util.List queryMappa(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateMappaByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateMappaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Mappa[] listMappaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public Mappa[] listMappaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryMappa(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryMappa(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateMappaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateMappaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Mappa loadMappaByQuery(String condition, String orderBy) throws PersistentException;
	public Mappa loadMappaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Mappa loadMappaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public Mappa loadMappaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Mappa createMappa();
	public boolean save(domain.Mappa mappa) throws PersistentException;
	public boolean delete(domain.Mappa mappa) throws PersistentException;
	public boolean refresh(domain.Mappa mappa) throws PersistentException;
	public boolean evict(domain.Mappa mappa) throws PersistentException;
}
