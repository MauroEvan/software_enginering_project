/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import domain.*;

public interface ModuloDiRegolamentoDAO {
	public ModuloDiRegolamento loadModuloDiRegolamentoByORMID(int ID) throws PersistentException;
	public ModuloDiRegolamento getModuloDiRegolamentoByORMID(int ID) throws PersistentException;
	public ModuloDiRegolamento loadModuloDiRegolamentoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public ModuloDiRegolamento getModuloDiRegolamentoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public ModuloDiRegolamento loadModuloDiRegolamentoByORMID(PersistentSession session, int ID) throws PersistentException;
	public ModuloDiRegolamento getModuloDiRegolamentoByORMID(PersistentSession session, int ID) throws PersistentException;
	public ModuloDiRegolamento loadModuloDiRegolamentoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public ModuloDiRegolamento getModuloDiRegolamentoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public ModuloDiRegolamento[] listModuloDiRegolamentoByQuery(String condition, String orderBy) throws PersistentException;
	public ModuloDiRegolamento[] listModuloDiRegolamentoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryModuloDiRegolamento(String condition, String orderBy) throws PersistentException;
	public java.util.List queryModuloDiRegolamento(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateModuloDiRegolamentoByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateModuloDiRegolamentoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public ModuloDiRegolamento[] listModuloDiRegolamentoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public ModuloDiRegolamento[] listModuloDiRegolamentoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryModuloDiRegolamento(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryModuloDiRegolamento(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateModuloDiRegolamentoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateModuloDiRegolamentoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public ModuloDiRegolamento loadModuloDiRegolamentoByQuery(String condition, String orderBy) throws PersistentException;
	public ModuloDiRegolamento loadModuloDiRegolamentoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public ModuloDiRegolamento loadModuloDiRegolamentoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public ModuloDiRegolamento loadModuloDiRegolamentoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public ModuloDiRegolamento createModuloDiRegolamento();
	public boolean save(domain.ModuloDiRegolamento moduloDiRegolamento) throws PersistentException;
	public boolean delete(domain.ModuloDiRegolamento moduloDiRegolamento) throws PersistentException;
	public boolean deleteAndDissociate(domain.ModuloDiRegolamento moduloDiRegolamento) throws PersistentException;
	public boolean deleteAndDissociate(domain.ModuloDiRegolamento moduloDiRegolamento, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(domain.ModuloDiRegolamento moduloDiRegolamento) throws PersistentException;
	public boolean evict(domain.ModuloDiRegolamento moduloDiRegolamento) throws PersistentException;
}
