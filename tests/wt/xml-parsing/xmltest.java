import javax.xml.parsers.*;
import org.w3c.dom.*;
import java.io.File;

public class xmltest
{
	private static DocumentBuilderFactory dbfactory;
	private static DocumentBuilder db;
	private static Document doc;


	public static void scrivi( String s ) { System.out.println( s ); }


	public static void main( String args[] )
	{
		// test - da file a struttura Document
		File target = new File("/Users/wtiberti/Desktop/esempio.xml");
		try
		{
			dbfactory = DocumentBuilderFactory.newInstance();
			db = dbfactory.newDocumentBuilder();
			doc = db.parse(target);
		}
		catch( Exception e )
		{
			e.printStackTrace();
			System.exit(-1);
		}
		doc.getDocumentElement().normalize();
		//******************************

		NodeList listaNodi = doc.getElementsByTagName( "regolamento" );

		// Scorriamo i nodi top-level (regolamento)
		for( int i=0; i<listaNodi.getLength(); i++ )
		{
			Node n = listaNodi.item(i); // ricava la struttura Node
			scrivi( "\nElemento: " + n.getNodeName() );

			// Se è un Element...(quasi scontato)
			if( n.getNodeType() == Node.ELEMENT_NODE )
			{
				Element e = (Element)n;
				
				// Ricava i tag "regola"
				NodeList regole = e.getElementsByTagName("regola");
				for( int j=0; j<regole.getLength(); j++ )
				{
					// j-esima regola...
					Element regola = (Element) regole.item(j);

					scrivi( regola.getNodeName() + " : tipo=" + regola.getAttribute("tipo") + ", nome=" + regola.getAttribute("nome") );

					// ricava i tag figli della regola
					NodeList figli = regola.getElementsByTagName("*"); // "*" fa da wildcard
					for( int k=0; k<figli.getLength(); k++ )
					{
						// k-esimo figlio...
						Node figlio = figli.item(k);
						scrivi( "\tnome: " + figlio.getNodeName() + ", valore: " + figlio.getFirstChild().getNodeValue() ); 
					}
				}
			}
		}
	}
}
