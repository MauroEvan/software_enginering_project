PRAGMA foreign_keys = ON;

drop table if exists Mappa;
CREATE TABLE `Mappa` (
	`ID`	INTEGER(10),
	PRIMARY KEY(ID)
);
insert into Mappa VALUES( 1 );
insert into Mappa VALUES( 2 );
insert into Mappa VALUES( 3 );



drop table if exists Giocatore;
CREATE TABLE `Giocatore` (
	`ID`	INTEGER,
	`NomeGiocatore`	VARCHAR(255),
	PRIMARY KEY(ID)
);
insert into Giocatore VALUES( 1, "Player1" );
insert into Giocatore VALUES( 2, "Player2" );
insert into Giocatore VALUES( 3, "Player3" );
insert into Giocatore VALUES( 4, "Player4" );
insert into Giocatore VALUES( 5, "Player5" );



drop table if exists Sessione;
CREATE TABLE `Sessione` (
	`ID`	INTEGER(10) NOT NULL UNIQUE,
	`gm`	INTEGER(10),
	`MappaID`	INTEGER(10),
	`Descr`	VARCHAR(255),
	PRIMARY KEY(ID),
	FOREIGN KEY(`gm`) REFERENCES Giocatore(ID)
	FOREIGN KEY(`MappaID`) REFERENCES Mappa(ID)
);

insert into Sessione VALUES( 1, 1, 1, "Sessione Numero 1" );
insert into Sessione VALUES( 2, 5, 2, "Sessione Numero 2" );



drop table if exists Evento;
CREATE TABLE `Evento` (
	`ID`	INTEGER,
	`SessioneID`	INTEGER NOT NULL,
	PRIMARY KEY(ID),
	FOREIGN KEY(`SessioneID`) REFERENCES Sessione(ID)
);
insert into Evento VALUES( 1, 1 );
insert into Evento VALUES( 2, 1 );
insert into Evento VALUES( 3, 2 );
insert into Evento VALUES( 7, 2 );



drop table if exists Scheda;
CREATE TABLE `Scheda` (
	`ID`	INTEGER(10),
	`SessioneID`	INTEGER,
	PRIMARY KEY(ID),
	FOREIGN KEY(`SessioneID`) REFERENCES Sessione(ID)
);
insert into Scheda VALUES( 1, 1 );
insert into Scheda VALUES( 2, 2 );


drop table if exists CampoAttributo;
CREATE TABLE `CampoAttributo` (
	`ID`	INTEGER(10),
	`Nome`	VARCHAR(255),
	`Valore`	INTEGER(10),
	`Modificatore`	INTEGER(10),
	`SchedaID`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`SchedaID`) REFERENCES Scheda(ID)
);
insert into CampoAttributo VALUES( 1, "Forza", 10, 1, 1 );
insert into CampoAttributo VALUES( 2, "Agilita", 12, 2, 1 );
insert into CampoAttributo VALUES( 3, "Destrezza", 10, 1, 1 );
insert into CampoAttributo VALUES( 4, "Carisma", 14, 3, 1 );
insert into CampoAttributo VALUES( 5, "Intelligenza", 10, 1, 1 );
insert into CampoAttributo VALUES( 6, "Saggezza", 10, 1, 1 );



drop table if exists CampoOggetto;
CREATE TABLE `CampoOggetto` (
	`ID`	INTEGER(10),
	`Nome`	VARCHAR(255),
	`Quantita`	INTEGER(10),
	`SchedaID`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`SchedaID`) REFERENCES Scheda(ID)
);
insert into CampoOggetto VALUES( 1, "Spada Arrugginita", 1, 1 );
insert into CampoOggetto VALUES( 2, "Book Of Larman", 2, 1 );



drop table if exists CampoNumerico;
CREATE TABLE `CampoNumerico` (
	`ID`	INTEGER(10),
	`Nome`	VARCHAR(255),
	`Valore`	INTEGER(10),
	`SchedaID`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`SchedaID`) REFERENCES Scheda(ID)
);
insert into CampoNumerico VALUES( 1, "Esperienza", 768, 1 );
insert into CampoNumerico VALUES( 2, "Denaro", 1000, 1 );
insert into CampoNumerico VALUES( 3, "Esperienza", 555, 2 );
insert into CampoNumerico VALUES( 4, "Denaro", 0, 2 );


drop table if exists CampoTestuale;
CREATE TABLE `CampoTestuale` (
	`ID`	INTEGER(10),
	`Nome`	VARCHAR(255),
	`Valore`	VARCHAR(255),
	`SchedaID`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`SchedaID`) REFERENCES Scheda(ID)
);
insert into CampoTestuale VALUES( 1, "Nome Personaggio", "Craig Larman", 1 );
insert into CampoTestuale VALUES( 2, "Classe", "Ladro", 1 );


drop table if exists CampoAbilita;
CREATE TABLE `CampoAbilita` (
	`ID`	INTEGER(10),
	`Nome`	VARCHAR(255),
	`Valore`	INTEGER(10),
	`AttributoChiave`	VARCHAR(255),
	`Modificatore`	INTEGER(10),
	`DiClasse`	INTEGER(1),
	`SchedaID`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`SchedaID`) REFERENCES Scheda(ID)
);
insert into CampoAbilita VALUES( 1, "Furtivita", 10, "Agilita", 1, 1, 1 );
insert into CampoAbilita VALUES( 2, "Ascoltare", 9, "Forza", 1, 0, 1 );
insert into CampoAbilita VALUES( 3, "Osservare", 10, "Saggezza", 1, 0, 1 );
insert into CampoAbilita VALUES( 4, "TrovareTrappole", 10, "Intelligenza", 1, 1, 1 );



drop table if exists CampoResistenze;
CREATE TABLE `CampoResistenze` (
	`ID`	INTEGER(10),
	`Nome`	VARCHAR(255),
	`ClasseArmatura`	INTEGER(10),
	`Riflessi`	INTEGER(10),
	`Tempra`	INTEGER(10),
	`Volonta`	INTEGER(10),
	`SchedaID`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`SchedaID`) REFERENCES Scheda(ID)
);
insert into CampoResistenze VALUES( 1, "Resistenze", 10, 10, 10, 10, 1 );




drop table if exists ModuloDiRegolamento;
CREATE TABLE `ModuloDiRegolamento` (
	`ID`	INTEGER(10),
	PRIMARY KEY(ID)
);
insert into ModuloDiRegolamento VALUES( 1 );




drop table if exists Sessione_ModuloDiRegolamento;
CREATE TABLE `Sessione_ModuloDiRegolamento` (
	`SessioneID`	INTEGER(10),
	`ModuloDiRegolamentoID`	INTEGER(10),
	FOREIGN KEY(`SessioneID`) REFERENCES Sessione(ID),
	FOREIGN KEY(`ModuloDiRegolamentoID`) REFERENCES ModuloDiRegolamento(ID)
);
insert into Sessione_ModuloDiRegolamento VALUES( 1, 1 );
insert into Sessione_ModuloDiRegolamento VALUES( 2, 1 );




drop table if exists RegolaComposite;
CREATE TABLE `RegolaComposite` (
	`ID`	INTEGER(10),
	`nome`	VARCHAR(255),
	`Modulo`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`Modulo`) REFERENCES ModuloDiRegolamento(ID)
);
insert into RegolaComposite VALUES( 1, "RegoleBase1", 1 );




drop table if exists RegolaCampoTestuale;
CREATE TABLE `RegolaCampoTestuale` (
	`ID`	INTEGER(10),
	`Token`	VARCHAR(255),
	`NomeCampo`	VARCHAR(255),
	`RegolaCompositeID`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`RegolaCompositeID`) REFERENCES RegolaComposite(ID)
);
insert into RegolaCampoTestuale VALUES( 1, "Ladro", "Classe", 1 ); -- Per individuare la classe



drop table if exists RegolaCampoOggetto;
CREATE TABLE `RegolaCampoOggetto` (
	`ID`	INTEGER(10),
	`MaxQuantita`	VARCHAR(255),
	`NomeCampo`	VARCHAR(255),
	`RegolaCompositeID`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`RegolaCompositeID`) REFERENCES RegolaComposite(ID)
);
insert into RegolaCampoOggetto VALUES( 1, 1, "Book Of Larman", 1 );


drop table if exists RegolaCampoAbilita;
CREATE TABLE `RegolaCampoAbilita` (
	`ID`	INTEGER(10),
	`SogliaValore`	INTEGER(10),
	`SogliaModificatore`	INTEGER(10),
	`NomeCampo`	VARCHAR(255),
	`RegolaCompositeID`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`RegolaCompositeID`) REFERENCES RegolaComposite(ID)
);
insert into RegolaCampoAbilita VALUES( 1, 10, 0, "Furtivita", 1 );


drop table if exists RegolaCampoResistenze;
CREATE TABLE `RegolaCampoResistenze` (
	`ID`	INTEGER(10),
	`MinArmatura`	INTEGER(10),
	`MinVolonta`	INTEGER(10),
	`MinTempra`	INTEGER(10),
	`MinRiflessi`	INTEGER(10),
	`NomeCampo`	VARCHAR(255),
	`RegolaCompositeID`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`RegolaCompositeID`) REFERENCES RegolaComposite(ID)
);
insert into RegolaCampoResistenze VALUES( 1, 10, 10, 10, 12, "Resistenze", 1 );


drop table if exists RegolaCampoAttributo;
CREATE TABLE `RegolaCampoAttributo` (
	`ID`	INTEGER(10),
	`SogliaValore`	INTEGER(10),
	`SogliaModificatore`	INTEGER(10),
	`NomeCampo`	VARCHAR(255),
	`RegolaCompositeID`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`RegolaCompositeID`) REFERENCES RegolaComposite(ID)
);
insert into RegolaCampoAttributo VALUES( 1, 10, 0, "Forza", 1 );


drop table if exists RegolaCampoNumerico;
CREATE TABLE `RegolaCampoNumerico` (
	`ID`	INTEGER(10),
	`LimiteSuperiore`	INTEGER(10),
	`LimiteInferiore`	INTEGER(10),
	`NomeCampo`	VARCHAR(255),
	`RegolaCompositeID`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`RegolaCompositeID`) REFERENCES RegolaComposite(ID)
);
insert into RegolaCampoNumerico VALUES( 1, 20000, 0, "Denaro", 1 );


drop table if exists RegolaGenerica;
CREATE TABLE `RegolaGenerica` (
	`ID`	INTEGER(10),
	`NomeRegola`	VARCHAR(255),
	`TestoRegola`	VARCHAR(255),
	`RegolaCompositeID`	INTEGER(10),
	PRIMARY KEY(ID),
	FOREIGN KEY(`RegolaCompositeID`) REFERENCES RegolaComposite(ID)
);
insert into RegolaGenerica VALUES( 1, "Ingegneria del Software 1", "Applicare i GRASP ove sensato e possibile", 1 );


