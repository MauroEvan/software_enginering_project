package server;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import remote_interface.A;
import remote_interface.B;

public class B_RemoteImpl extends UnicastRemoteObject implements B , Serializable {

    private int bint;
    private String bstring;

    private String bout;

    protected B_RemoteImpl() throws RemoteException{
        this.bint = 10;
        this.bstring = "Hello World!";
    }
    
    protected B_RemoteImpl(int b1, String b2) throws RemoteException{
        this.bint = b1;
        this.bstring = b2;
    }

    public void ElaboraB() {
        this.bout = this.getBstring().concat(" ").concat( Integer.toString(this.getBint()) );
    }
    
    public int getBint() {
        return bint;
    }

    public void setBint(int bint) {
        this.bint = bint;
    }

    public String getBstring() {
        return bstring;
    }

    public void setBstring(String bstring) {
        this.bstring = bstring;
    }

    public String getBout() throws RemoteException {
        return bout;
    }

    public void setBout(String bout) {
        this.bout = bout;
    }
    
    public boolean aTask(String check) throws RemoteException {
        return false;
    }
    
}
