
package server;

import remote_interface.Test_Engine;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ATask_RemoteImpl extends UnicastRemoteObject implements Test_Engine{

    protected ATask_RemoteImpl() throws RemoteException{
        super();
    }

    @Override
    public boolean aTask(String check) throws RemoteException {
        if ( "ok".equals(check) ) {
            return true;
        }
        return false;
    }
    
}
