
package server;

import remote_interface.Costanti;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Test_Server {
    
    public static void main(String[] args) throws RemoteException, AlreadyBoundException, NotBoundException{

//        ATask_RemoteImpl impl = new ATask_RemoteImpl();  
        A_RemoteImpl impl = new A_RemoteImpl();
//        impl.OttieniDaC().ElaboraB();        
       
        LocateRegistry.createRegistry(Costanti.PORT_RMI);
        
        Registry registry = LocateRegistry.getRegistry(Costanti.PORT_RMI);
        registry.rebind(Costanti.ID_RMI, impl);
        
        System.out.println("Server started");
    }
   
}
