
package server;

import java.io.Serializable;
import java.rmi.RemoteException;

public class C_Class implements Serializable{


    private int C;

    private B_RemoteImpl B;
    
    public C_Class() throws RemoteException{
        C = 3;
        B = new B_RemoteImpl(100 , "Ciao Mondo!");
    }
    public int getC() {
        return C;
    }

    public void setC(int C) {
        this.C = C;
    }

    public B_RemoteImpl getB() {
        return B;
    }

    public void setB(B_RemoteImpl B) {
        this.B = B;
    }
}
