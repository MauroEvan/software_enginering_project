
package server;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import remote_interface.A;
import remote_interface.B;
import remote_interface.Costanti;

public class A_RemoteImpl extends UnicastRemoteObject implements A , Serializable{

    //oggetto di classe B conosciuto dal client
    private B bobj;
    
    //il client non conosce la classe C
    private C_Class cobj;

    protected A_RemoteImpl() throws RemoteException{
        super();
        cobj = new C_Class();
    }

    public B ElaboraA() throws RemoteException {
        if (bobj == null) {
            bobj = new B_RemoteImpl();
        }
        
        return bobj;
    }
        
    public B OttieniDaC() {
        return this.getCobj().getB();
    }
    
    public B getBobj() {
        return bobj;
    }

    public void setBobj(B bobj) {
        this.bobj = bobj;
    }

    @Override
    public boolean aTask(String check) throws RemoteException {
        return false;
    }
    
    public C_Class getCobj() {
        return cobj;
    }
    
    public void setCobj(C_Class cobj) {
        this.cobj = cobj;
    }
}
