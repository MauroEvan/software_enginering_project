
package client;

import remote_interface.Costanti;
import remote_interface.Test_Engine;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.Remote;
import remote_interface.A;
import remote_interface.B;

public class Test_Client {

    public static void main(String[] args) throws RemoteException, NotBoundException {
        
//        Registry registry = LocateRegistry.getRegistry("localhost",Costanti.PORT_RMI);
//        Test_Engine engine = (Test_Engine) registry.lookup(Costanti.ID_RMI);
//
//        System.out.println(engine.aTask("ok"));
//        System.out.println(engine.aTask("test"));
        
        Registry registry = LocateRegistry.getRegistry("localhost",Costanti.PORT_RMI);
        A engine = (A) registry.lookup(Costanti.ID_RMI);
        
        engine.ElaboraA().ElaboraB();
        B B_stub = engine.ElaboraA();
               
        System.out.println( B_stub.getBout() );
        
        engine.OttieniDaC().ElaboraB();
        System.out.println( engine.OttieniDaC().getBout() );
        
    }
    
}
