
package remote_interface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface A extends Test_Engine {
    public B ElaboraA() throws RemoteException;
    
    public B OttieniDaC() throws RemoteException;

}
