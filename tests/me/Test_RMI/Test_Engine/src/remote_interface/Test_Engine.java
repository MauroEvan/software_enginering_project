
package remote_interface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Test_Engine extends Remote {

    public boolean aTask(String check) throws RemoteException;
    
}
