package remote_interface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface B extends Test_Engine {
    public void ElaboraB() throws RemoteException;
    public String getBout() throws RemoteException;
}
