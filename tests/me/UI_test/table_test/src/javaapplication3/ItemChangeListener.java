package javaapplication3;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

class ItemChangeListener implements ItemListener {
    public void itemStateChanged(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
            Object item = event.getItem();
            test.memos.remove(item);
            System.out.println("rimosso elemento" + item.toString());
        }
    }   
}
