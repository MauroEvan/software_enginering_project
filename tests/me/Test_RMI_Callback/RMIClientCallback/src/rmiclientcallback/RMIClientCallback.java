package rmiclientcallback;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import remoteinterface.Costanti;
import remoteinterface.ServerInterface;

public class RMIClientCallback {

    public static void main(String[] args) throws RemoteException, NotBoundException {
       
        Registry registry = LocateRegistry.getRegistry("localhost",Costanti.PORT_RMI);
        ServerInterface server = (ServerInterface) registry.lookup(Costanti.ID_RMI);
        
        ClientImpl client = new ClientImpl("ClientTest", server);
        if (server.testMethod("test"))
            System.out.println("OK!!!");
    }
    
}
