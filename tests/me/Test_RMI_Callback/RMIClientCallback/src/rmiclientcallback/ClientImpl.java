package rmiclientcallback;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import remoteinterface.ClientInterface;
import remoteinterface.ServerInterface;

public class ClientImpl extends UnicastRemoteObject implements ClientInterface {
    
    private ServerInterface server;
    private String name;
    
    public ClientImpl(String name, ServerInterface server) throws RemoteException {
        this.name = name;
        this.server = server;
        server.registerClient(this);
    }
    
    public void CallBackMethod(String s) throws RemoteException {
        if(s.equals("test_from_server"))
            System.out.println("SERVER MESSAGE OK!!!");
        else System.out.println("SERVER MESSAGE OKOK!!!");
    }
    
}
