package remoteinterface;

import java.rmi.Remote;
import java.rmi.RemoteException;        

public interface ServerInterface extends Remote {
    void registerClient(ClientInterface client) throws RemoteException;
    boolean testMethod(String s) throws RemoteException;
}
