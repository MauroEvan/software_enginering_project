package remoteinterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ClientInterface extends Remote {
    void CallBackMethod(String s)  throws RemoteException;
}
