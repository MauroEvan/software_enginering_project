package rmiserver;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import remoteinterface.Costanti;

public class RMIServer {

    public static void main(String[] args) throws RemoteException {
        
        ServerImpl impl = new ServerImpl();
        LocateRegistry.createRegistry(Costanti.PORT_RMI);
        
        Registry registry = LocateRegistry.getRegistry(Costanti.PORT_RMI);
        registry.rebind(Costanti.ID_RMI, impl);
        
        System.out.println("Server started");
    }
    
}
