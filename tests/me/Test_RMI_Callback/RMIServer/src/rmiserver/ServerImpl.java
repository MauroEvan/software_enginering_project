package rmiserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import remoteinterface.ClientInterface;
import remoteinterface.ServerInterface;

public class ServerImpl extends UnicastRemoteObject implements ServerInterface{
    
    private ClientInterface clientLogged;
    
    public ServerImpl() throws RemoteException {
        super();
    }

    public void registerClient(ClientInterface client) {
        clientLogged = client;
    }

    public boolean testMethod(String s) throws RemoteException {
        if(s.equals("test"))
        {
            this.clientLogged.CallBackMethod(s);
            System.out.println("CLIENT MESSAGE OK!!");
            return true;
        }
        this.clientLogged.CallBackMethod("asd");
        System.out.println("CLIENT MESSAGE OKOK!!");
        return false;
    }
    
}
