package UI;

import Domain.*;
import java.awt.event.MouseEvent;

public class UCreazioneManuale extends javax.swing.JFrame{
    
    private Scheda scheda;
    private Tavolo tavolo;
    
    private javax.swing.JPanel AbilitaPannello;
    private javax.swing.JComboBox Allineamento1ComboBox;
    private javax.swing.JComboBox Allineamento2ComboBox;
    private javax.swing.JLabel AllineamentoLabel;
    private javax.swing.JButton AnnullaButton;
    private javax.swing.JPanel AttributiPannello;
    private javax.swing.JPanel CaratteristichePannello;
    private javax.swing.JComboBox ClasseComboBox;
    private javax.swing.JLabel ClasseLabel;
    private javax.swing.JPanel EquipaggiamentoPannello;
    private javax.swing.JPanel InformazioniGeneraliPannello;
    private javax.swing.JLabel NomeLabel;
    private javax.swing.JTextField NomeTextField;   
    private javax.swing.JComboBox RazzaComboBox;
    private javax.swing.JLabel RazzaLabel;
    private javax.swing.JPanel ResistenzePannello;
    private javax.swing.JButton SalvaButton;
    private javax.swing.JTabbedPane SchedaTabbedPane;
    private javax.swing.JLabel TagliaLabel;
    private javax.swing.JTextField TagliaTextField;
    private javax.swing.JTextField carisma;
    private javax.swing.JTextField costituzione;
    private javax.swing.JTextField destrezza;
    private javax.swing.JTextField forza;
    private javax.swing.JTextField intelligenza;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField jTextField13;
    private javax.swing.JTextField jTextField14;
    private javax.swing.JTextField jTextField15;
    private javax.swing.JTextField jTextField16;
    private javax.swing.JTextField mod_carisma;
    private javax.swing.JTextField mod_costituzione;
    private javax.swing.JTextField mod_destrezza;
    private javax.swing.JTextField mod_forza;
    private javax.swing.JTextField mod_intelligenza;
    private javax.swing.JTextField mod_saggezza;
    private javax.swing.JTextField saggezza;
    
    public UCreazioneManuale( Tavolo t , Scheda s )
    {   
        scheda = s;
        tavolo = t;
        
        SchedaTabbedPane = new javax.swing.JTabbedPane();
        InformazioniGeneraliPannello = new javax.swing.JPanel();
        NomeTextField = new javax.swing.JTextField();
        RazzaLabel = new javax.swing.JLabel();
        ClasseLabel = new javax.swing.JLabel();
        AllineamentoLabel = new javax.swing.JLabel();
        TagliaLabel = new javax.swing.JLabel();
        TagliaTextField = new javax.swing.JTextField();
        NomeLabel = new javax.swing.JLabel();
        RazzaComboBox = new javax.swing.JComboBox();
        ClasseComboBox = new javax.swing.JComboBox();
        Allineamento1ComboBox = new javax.swing.JComboBox();
        Allineamento2ComboBox = new javax.swing.JComboBox();
        CaratteristichePannello = new javax.swing.JPanel();
        AttributiPannello = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        forza = new javax.swing.JTextField();
        destrezza = new javax.swing.JTextField();
        costituzione = new javax.swing.JTextField();
        intelligenza = new javax.swing.JTextField();
        saggezza = new javax.swing.JTextField();
        carisma = new javax.swing.JTextField();
        mod_forza = new javax.swing.JTextField();
        mod_destrezza = new javax.swing.JTextField();
        mod_costituzione = new javax.swing.JTextField();
        mod_intelligenza = new javax.swing.JTextField();
        mod_saggezza = new javax.swing.JTextField();
        mod_carisma = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        ResistenzePannello = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jTextField13 = new javax.swing.JTextField();
        jTextField14 = new javax.swing.JTextField();
        jTextField15 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jTextField16 = new javax.swing.JTextField();
        AbilitaPannello = new javax.swing.JPanel();
        EquipaggiamentoPannello = new javax.swing.JPanel();
        AnnullaButton = new javax.swing.JButton();
        SalvaButton = new javax.swing.JButton();
        
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setMinimumSize(new java.awt.Dimension(600, 820));
        this.setResizable(false);
        this.setTitle("Creazione manuale della scheda");

        NomeTextField.setText("inserire nome...");

        RazzaLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        RazzaLabel.setText("Razza");

        ClasseLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        ClasseLabel.setText("Classe");

        AllineamentoLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        AllineamentoLabel.setText("Allineamento");

        TagliaLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        TagliaLabel.setText("Taglia");
        
        TagliaTextField.setText("Media");
        TagliaTextField.setEnabled(false);

        NomeLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        NomeLabel.setText("Nome Personaggio");
        NomeLabel.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseEvent( java.awt.event.MouseEvent evt){
                NomeTextFieldMouseClicked(evt);
            }

        });

        RazzaComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Umano", "Elfo", "Gnomo", "Nano" }));
        RazzaComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RazzaComboBoxActionPerformed(evt);
            }
        });

        ClasseComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Guerriero", "Ladro", "Mago" }));
        Allineamento1ComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Legale", "Neutrale", "Caotico" }));
        Allineamento2ComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Buono", "Neutrale", "Malvagio" }));

        javax.swing.GroupLayout InformazioniGeneraliPannelloLayout = new javax.swing.GroupLayout(InformazioniGeneraliPannello);
        InformazioniGeneraliPannello.setLayout(InformazioniGeneraliPannelloLayout);
        InformazioniGeneraliPannelloLayout.setHorizontalGroup(
            InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InformazioniGeneraliPannelloLayout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InformazioniGeneraliPannelloLayout.createSequentialGroup()
                        .addComponent(NomeLabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(InformazioniGeneraliPannelloLayout.createSequentialGroup()
                        .addGroup(InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, InformazioniGeneraliPannelloLayout.createSequentialGroup()
                                .addComponent(Allineamento1ComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(Allineamento2ComboBox, 0, 125, Short.MAX_VALUE))
                            .addComponent(ClasseComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(AllineamentoLabel, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ClasseLabel, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(RazzaLabel, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(NomeTextField, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(RazzaComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(InformazioniGeneraliPannelloLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                                .addComponent(TagliaLabel)
                                .addGap(238, 238, 238))
                            .addGroup(InformazioniGeneraliPannelloLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(TagliaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
        );
        InformazioniGeneraliPannelloLayout.setVerticalGroup(
            InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InformazioniGeneraliPannelloLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(NomeLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(NomeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RazzaLabel)
                    .addComponent(TagliaLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TagliaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(RazzaComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(ClasseLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ClasseComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(AllineamentoLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InformazioniGeneraliPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Allineamento1ComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Allineamento2ComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(446, Short.MAX_VALUE))
        );

        SchedaTabbedPane.addTab("Informazioni Generali", InformazioniGeneraliPannello);

        AttributiPannello.setBorder(javax.swing.BorderFactory.createTitledBorder("Attributi"));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("FOR");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("DES");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("COS");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("INT");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("SAG");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("CAR");
        
        SalvaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalvaActionPerformed(evt);
            }
        });

        
        forza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                forzaActionPerformed(evt);
            }
        });

        destrezza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                destrezzaActionPerformed(evt);
            }
        });

        costituzione.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                costituzioneActionPerformed(evt);
            }
        });

        intelligenza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                intelligenzaActionPerformed(evt);
            }
        });

        saggezza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saggezzaActionPerformed(evt);
            }
        });

        carisma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carismaActionPerformed(evt);
            }
        });

        mod_forza.setEnabled(false);

        mod_destrezza.setEnabled(false);

        mod_costituzione.setEnabled(false);

        mod_intelligenza.setEnabled(false);

        mod_saggezza.setEnabled(false);

        mod_carisma.setEnabled(false);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel7.setText("Valore");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel8.setText("Modificatore");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel9.setText("Attributo");

        javax.swing.GroupLayout AttributiPannelloLayout = new javax.swing.GroupLayout(AttributiPannello);
        AttributiPannello.setLayout(AttributiPannelloLayout);
        AttributiPannelloLayout.setHorizontalGroup(
            AttributiPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AttributiPannelloLayout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(AttributiPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel9))
                .addGap(65, 65, 65)
                .addGroup(AttributiPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(costituzione, javax.swing.GroupLayout.DEFAULT_SIZE, 59, Short.MAX_VALUE)
                    .addComponent(destrezza)
                    .addComponent(forza)
                    .addComponent(intelligenza)
                    .addComponent(saggezza)
                    .addComponent(carisma)
                    .addGroup(AttributiPannelloLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(15, 15, 15)))
                .addGap(40, 40, 40)
                .addGroup(AttributiPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mod_carisma, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_saggezza, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_intelligenza, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_destrezza, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(mod_forza, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_costituzione, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        AttributiPannelloLayout.setVerticalGroup(
            AttributiPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AttributiPannelloLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(AttributiPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addGap(27, 27, 27)
                .addGroup(AttributiPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(forza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_forza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(AttributiPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(destrezza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_destrezza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(AttributiPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(costituzione, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_costituzione, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(AttributiPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(intelligenza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_intelligenza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(AttributiPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(saggezza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_saggezza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(AttributiPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(carisma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mod_carisma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(92, Short.MAX_VALUE))
        );

        ResistenzePannello.setBorder(javax.swing.BorderFactory.createTitledBorder("Resistenze"));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setText("Riflessi");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setText("Tempra");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setText("Volontà");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setText("Classe Armatura");

        jTextField16.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jTextField16.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField16.setText("0");

        javax.swing.GroupLayout ResistenzePannelloLayout = new javax.swing.GroupLayout(ResistenzePannello);
        ResistenzePannello.setLayout(ResistenzePannelloLayout);
        ResistenzePannelloLayout.setHorizontalGroup(
            ResistenzePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ResistenzePannelloLayout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(ResistenzePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(jLabel10)
                    .addComponent(jLabel12))
                .addGap(59, 59, 59)
                .addGroup(ResistenzePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField13, javax.swing.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE)
                    .addComponent(jTextField15)
                    .addComponent(jTextField14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 190, Short.MAX_VALUE)
                .addGroup(ResistenzePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ResistenzePannelloLayout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addGap(41, 41, 41))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ResistenzePannelloLayout.createSequentialGroup()
                        .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(66, 66, 66))))
        );
        ResistenzePannelloLayout.setVerticalGroup(
            ResistenzePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ResistenzePannelloLayout.createSequentialGroup()
                .addGap(75, 75, 75)
                .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(ResistenzePannelloLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(ResistenzePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addGap(36, 36, 36)
                .addGroup(ResistenzePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addGroup(ResistenzePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24))
        );

        javax.swing.GroupLayout CaratteristichePannelloLayout = new javax.swing.GroupLayout(CaratteristichePannello);
        CaratteristichePannello.setLayout(CaratteristichePannelloLayout);
        CaratteristichePannelloLayout.setHorizontalGroup(
            CaratteristichePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CaratteristichePannelloLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CaratteristichePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(ResistenzePannello, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(AttributiPannello, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        CaratteristichePannelloLayout.setVerticalGroup(
            CaratteristichePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CaratteristichePannelloLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(AttributiPannello, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ResistenzePannello, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        SchedaTabbedPane.addTab("Caratteristiche", CaratteristichePannello);

        javax.swing.GroupLayout AbilitaPannelloLayout = new javax.swing.GroupLayout(AbilitaPannello);
        AbilitaPannello.setLayout(AbilitaPannelloLayout);
        AbilitaPannelloLayout.setHorizontalGroup(
            AbilitaPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 595, Short.MAX_VALUE)
        );
        AbilitaPannelloLayout.setVerticalGroup(
            AbilitaPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 730, Short.MAX_VALUE)
        );

        SchedaTabbedPane.addTab("Abilità", AbilitaPannello);

        EquipaggiamentoPannello.setPreferredSize(new java.awt.Dimension(600, 560));

        javax.swing.GroupLayout EquipaggiamentoPannelloLayout = new javax.swing.GroupLayout(EquipaggiamentoPannello);
        EquipaggiamentoPannello.setLayout(EquipaggiamentoPannelloLayout);
        EquipaggiamentoPannelloLayout.setHorizontalGroup(
            EquipaggiamentoPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 595, Short.MAX_VALUE)
        );
        EquipaggiamentoPannelloLayout.setVerticalGroup(
            EquipaggiamentoPannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 730, Short.MAX_VALUE)
        );

        SchedaTabbedPane.addTab("Equipaggiamento", EquipaggiamentoPannello);

        AnnullaButton.setText("Annulla");
        AnnullaButton.setPreferredSize(new java.awt.Dimension(80, 40));

        SalvaButton.setText("Salva");
        SalvaButton.setPreferredSize(new java.awt.Dimension(80, 40));

        javax.swing.GroupLayout thisLayout = new javax.swing.GroupLayout(this.getContentPane());
        this.getContentPane().setLayout(thisLayout);
        thisLayout.setHorizontalGroup(
            thisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(thisLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(AnnullaButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(SalvaButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(SchedaTabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
        );
        thisLayout.setVerticalGroup(
            thisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(thisLayout.createSequentialGroup()
                .addComponent(SchedaTabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(thisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(thisLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(SalvaButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(AnnullaButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }
    
    private void SalvaActionPerformed(java.awt.event.ActionEvent evt) {
        scheda.addCampo( "nome" , new CampoTestuale( "nome" , NomeTextField.getText() ) );
        scheda.addCampo( "razza" , new CampoTestuale( "razza" , (String) RazzaComboBox.getSelectedItem()) );
        scheda.addCampo( "classe" , new CampoTestuale( "classe" , (String) ClasseComboBox.getSelectedItem()) );
        scheda.addCampo( "taglia" , new CampoTestuale( "taglia" , TagliaTextField.getText() ) );
        scheda.addCampo( "allineamento" , new CampoTestuale( "allineamento" , (String) Allineamento1ComboBox.getSelectedItem()  + " " + (String) Allineamento2ComboBox.getSelectedItem() ) );
        
        //da valutare prima di concludere se la persistenza è necessaria o meno
        
        for ( String key : scheda.getChiaviCampi() )
        {
            System.out.println ( scheda.getCampo(key) );
        }
        
        //CO7 ValidaScheda
        
        if( tavolo.ValidaScheda(scheda) )
            System.out.println("Scheda Validata Con successo");
        else 
        {
            System.out.println("Scheda non Validata");
        }
    }
    
    
    private void NomeTextFieldMouseClicked(MouseEvent evt) {
        this.NomeTextField.setText("");
    }
    
    private void forzaActionPerformed(java.awt.event.ActionEvent evt) {                                      
        int valore = Integer.parseInt( forza.getText() );
        int modificatore = 0;
        if( (valore - 10) >= 0 )
            modificatore = (valore - 10)/2;
        else 
            modificatore = ( (valore - 10)/2 ) - 1;
        mod_forza.setText( Integer.toString(modificatore) );
        mod_forza.repaint();
    }                                     

    private void RazzaComboBoxActionPerformed(java.awt.event.ActionEvent evt) {                                              
        if( RazzaComboBox.getSelectedItem() == "Umano" || RazzaComboBox.getSelectedItem() == "Elfo")
        {
            TagliaTextField.setText("Media");
            TagliaTextField.repaint();
        }
        else
        {
            if(RazzaComboBox.getSelectedItem() == "Gnomo" || RazzaComboBox.getSelectedItem() == "Nano")
            {
                TagliaTextField.setText("Piccola");
                TagliaTextField.repaint();
            }
        }
    }                                             

    private void destrezzaActionPerformed(java.awt.event.ActionEvent evt) {                                          
        int valore = Integer.parseInt( destrezza.getText() );
        int modificatore = 0;
        if( (valore - 10) >= 0 )
            modificatore = (valore - 10)/2;
        else 
            modificatore = ( (valore - 10)/2 ) - 1;
        mod_destrezza.setText( Integer.toString(modificatore) );
        mod_destrezza.repaint();
    }                                         

    private void costituzioneActionPerformed(java.awt.event.ActionEvent evt) {                                             
        int valore = Integer.parseInt( costituzione.getText() );
        int modificatore = 0;
        if( (valore - 10) >= 0 )
            modificatore = (valore - 10)/2;
        else 
            modificatore = ( (valore - 10)/2 ) - 1;
        mod_costituzione.setText( Integer.toString(modificatore) );
        mod_costituzione.repaint();
    }                                            

    private void intelligenzaActionPerformed(java.awt.event.ActionEvent evt) {                                             
        int valore = Integer.parseInt( intelligenza.getText() );
        int modificatore = 0;
        if( (valore - 10) >= 0 )
            modificatore = (valore - 10)/2;
        else 
            modificatore = ( (valore - 10)/2 ) - 1;
        mod_intelligenza.setText( Integer.toString(modificatore) );
        mod_intelligenza.repaint();
    }                                            

    private void saggezzaActionPerformed(java.awt.event.ActionEvent evt) {                                         
        int valore = Integer.parseInt( saggezza.getText() );
        int modificatore = 0;
        if( (valore - 10) >= 0 )
            modificatore = (valore - 10)/2;
        else 
            modificatore = ( (valore - 10)/2 ) - 1;
        mod_saggezza.setText( Integer.toString(modificatore) );
        mod_saggezza.repaint();
    }                                        

    private void carismaActionPerformed(java.awt.event.ActionEvent evt) {                                        
        int valore = Integer.parseInt( carisma.getText() );
        int modificatore = 0;
        if( (valore - 10) >= 0 )
            modificatore = (valore - 10)/2;
        else 
            modificatore = ( (valore - 10)/2 ) - 1;
        mod_carisma.setText( Integer.toString(modificatore) );
        mod_carisma.repaint();
    }      
    
}
