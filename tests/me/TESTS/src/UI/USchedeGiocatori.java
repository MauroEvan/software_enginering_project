package UI;

import java.awt.FlowLayout;
import Domain.*;
import java.awt.event.ActionEvent;
import java.util.HashSet;
import javax.swing.*;

public class USchedeGiocatori extends JPanel {
    
    private javax.swing.JLabel NomeScheda;
    private javax.swing.JComboBox GiocatoreComboBox;
    private MainWindow parent;    
    private Scheda schedaAttuale;
    private Tavolo tavoloAttuale;
    
    // Contratto Operazione CO3: AssociaScheda
    private void GiocatoreComboBoxActionPerformed(ActionEvent evt) {        
        if (GiocatoreComboBox.getSelectedItem() instanceof Giocatore)
        {
            tavoloAttuale.AssociaScheda( (Giocatore)this.GiocatoreComboBox.getSelectedItem(), this.schedaAttuale);
        }
    }
    
    public USchedeGiocatori(Scheda scheda, Tavolo t, MainWindow parentWindow)
    {    
        NomeScheda = new javax.swing.JLabel();
        GiocatoreComboBox = new javax.swing.JComboBox();
        schedaAttuale = scheda;
        tavoloAttuale = t;
        
        this.setMinimumSize(new java.awt.Dimension(320, 60));
        this.setPreferredSize(new java.awt.Dimension(320, 60));

        NomeScheda.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        NomeScheda.setText(scheda.toString());
        GiocatoreComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] {}));
        GiocatoreComboBox.addItem("Non associato");
        
        System.out.println(t.getGiocatoriSeduti());
        
        for (Giocatore giocatore : t.getGiocatoriSeduti())
        {
            GiocatoreComboBox.addItem(giocatore);
            System.out.println( "Aggiunto alla combo il giocatore " + giocatore.getNomeGiocatore() );
        }
        
        GiocatoreComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GiocatoreComboBoxActionPerformed(evt);
            }
        });
        
        javax.swing.GroupLayout SchedaGiocatorePannelloLayout = new javax.swing.GroupLayout(this);
        this.setLayout(SchedaGiocatorePannelloLayout);
        SchedaGiocatorePannelloLayout.setHorizontalGroup(SchedaGiocatorePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SchedaGiocatorePannelloLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(NomeScheda)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 124, Short.MAX_VALUE)
                .addComponent(GiocatoreComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
        );
        SchedaGiocatorePannelloLayout.setVerticalGroup(SchedaGiocatorePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SchedaGiocatorePannelloLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(SchedaGiocatorePannelloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NomeScheda)
                    .addComponent(GiocatoreComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        
        this.parent = parentWindow;
        
    }
    
}
