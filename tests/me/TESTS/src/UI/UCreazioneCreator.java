package UI;

import Domain.*;
import javax.swing.JFrame;

public class UCreazioneCreator
{
    public static JFrame Create(String tipo , Tavolo t , Scheda s)
    {        
        if(tipo == "manuale") return new UCreazioneManuale( t , s );
        
        if(tipo == "random") return new UCreazioneRandom( t , s );
        
        if(tipo == "npc") return new UCreazioneNpc( t , s );
        
        return null;
    }
}
