/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

import static java.lang.Math.floor;
import java.util.Random;

public class CampoAbilita implements Campo {
	public CampoAbilita() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == Domain.ORMConstants.KEY_CAMPOABILITA_SCHEDA) {
			this.scheda = (Scheda) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;	
	private String nomeAbilita;	
	private int valoreGrado;	
	private CampoAttributo attributoChiave;	
	private int modificatore;	
	private boolean diClasse;
        
	private Scheda scheda;
        
        public CampoAbilita(String nomeAbilita, int valoreGrado, CampoAttributo attributoChiave, boolean diClasse)
        {
            this.attributoChiave = attributoChiave;
            this.diClasse = diClasse;
            this.valoreGrado = valoreGrado;
            this.nomeAbilita = nomeAbilita;
            this.modificatore = this.attributoChiave.getModificatore();
        }

        public CampoAbilita(String nomeAbilita, CampoAttributo attributoChiave)
        {
            this.attributoChiave = attributoChiave;
            this.nomeAbilita = nomeAbilita;
            this.diClasse = RandomBoolean();
            this.modificatore = attributoChiave.getModificatore();
            if(diClasse)
                this.valoreGrado = Randomizer();
            else
                this.valoreGrado = (int) floor(Randomizer()/2);
        }
        
        public String getNome() { return nomeAbilita; }
        public void setNome(String nomeAbilita) { this.nomeAbilita = nomeAbilita; }
	public void setValoreGrado(int valoreGrado) { this.valoreGrado = valoreGrado; }	
	public int getValoreGrado() { return valoreGrado; }
        public CampoAttributo getAttributoChiave() { return attributoChiave; }
        public void setAttributoChiave(CampoAttributo attributoChiave) { this.attributoChiave = attributoChiave; }
        public int getModificatore() { return modificatore; }
        public void setModificatore(int modificatore) { this.modificatore = modificatore; }
        public boolean isDiClasse() { return diClasse; }
        public void setDiClasse(boolean diClasse) { this.diClasse = diClasse; }
	
	private void setID(int value) { this.ID = value; }
	
	public int getID() { return ID; }
	
	public int getORMID() { return getID();	}	
	
	public void setScheda(Scheda value) {
		if (scheda != null) {
			scheda.campoAbilita.remove(this);
		}
		if (value != null) {
			value.campoAbilita.add(this);
		}
	}
	
	public Scheda getScheda() {
		return scheda;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Scheda(Scheda value) {this.scheda = value;}
	
	private Scheda getORM_Scheda() {return scheda;}
	
        private int Randomizer()
        {
            int min = 0;
            int max = 10;
            Random rand = new Random();
            return rand.nextInt((max - min) + 1) + min;
        }
	
        private boolean RandomBoolean()
        {
            Random rand = new Random();
            return rand.nextBoolean();
        }

        public String toString() { return this.getNome() + " " + this.getValoreGrado(); }
        
        public Object getValore() 
        {
            throw new UnsupportedOperationException("Not supported yet.");
        }
	
}
