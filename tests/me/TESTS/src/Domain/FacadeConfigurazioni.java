package Domain;
import java.util.ArrayList;

//import java.util.HashMap;

public class FacadeConfigurazioni
{
	static FacadeConfigurazioni handle = null;
	
	public static FacadeConfigurazioni getIstance()
	{
		if( handle == null )
			handle = new FacadeConfigurazioni();
		return handle;
	}
	
	public ArrayList<OpzioneCreazioneScheda> getOpzioniCreazioneScheda( Giocatore g, Sessione s )
	{
		ArrayList<OpzioneCreazioneScheda> opz = new ArrayList<OpzioneCreazioneScheda>();
		
		opz.add( new OpzioneCreazioneManuale() );
		opz.add( new OpzioneCreazioneRandom() );
		
		if( s.isGM(g) )
                    opz.add( new OpzioneCreazioneNPC() );
		
		return opz;
	}

//-----------------------------Una possibilita potrebbe essere usare una map--------------------------------
//----------------------------------------------------------------------------------------------------------        
//        public HashMap<OpzioneCreazioneScheda> getOpzioniCreazioneScheda( Giocatore g, Sessione s)
//        {
//            HashMap<String , OpzioneCreazioneScheda> opz = new HashMap<String , OpzioneCreazioneScheda>();
//            
//            OpzioneCreazioneManuale opz1 = new OpzioneCreazioneManuale();
//            OpzioneCreazioneRandom opz2 = new OpzioneCreazioneRandom();
//            opz.put(opz1.getTipo(), opz1);
//            opz.put(opz2.getTipo(), opz2);
//            
//            if( s.isGM(g) )
//            {
//                OpzioneCreazioneNPC opz3 = new OpzioneCreazioneNPC();
//                opz.put(opz3.getTipo(), opz3);
//            }    
//        }
//----------------------------------------------------------------------------------------------------------        
}
