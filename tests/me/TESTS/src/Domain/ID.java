/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

/**
 *
 * @author Mauro_Notebook
 */
public class ID 
{
        private int id;
    
        public int getId() 
        {
                return id;
        }

        public void setId(int id)
        {
                this.id = id;
        }

	public ID( int x )
	{
		this.id = x;
	}
        
        // @voerride
        public String toString()
        {
            return Integer.toUnsignedString( id );
        }
}
