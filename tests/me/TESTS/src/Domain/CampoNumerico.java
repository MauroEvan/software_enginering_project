/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

import java.util.Random;

public class CampoNumerico implements Campo {
	public CampoNumerico() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_CAMPONUMERICO_SCHEDA) {
			this.scheda = (Scheda) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;	
	private String nomeCampo;	
	private int valore;
        
	private Scheda scheda;
        
        public CampoNumerico(String nomeCampo, int valore)
        {
            this.nomeCampo = nomeCampo;
            this.valore = valore;
        }

        public CampoNumerico(String nomeCampo)
        {
            this.nomeCampo = nomeCampo;
            this.valore = Randomizer(nomeCampo);
        }
        
        public String getNome() { return nomeCampo; }
        public void setNome(String nomeCampo) { this.nomeCampo = nomeCampo; }
        public Integer getValore() { return valore; }
        public void setValore(int valore) { this.valore = valore; }

        public String toString() { return this.getNome() + " " + this.getValore(); }
        public int getModificatore() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}	
	
	public void setScheda(Scheda value) {
		if (scheda != null) {
			scheda.campoNumerico.remove(this);
		}
		if (value != null) {
			value.campoNumerico.add(this);
		}
	}
	
	public Scheda getScheda() {
		return scheda;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Scheda(Scheda value) {
		this.scheda = value;
	}
	
	private Scheda getORM_Scheda() {
		return scheda;
	}
	
        private int Randomizer(String nomeCampo){
            int min = 0;
            int max = 200000;
            int maxLvl = 20;
            Random rand = new Random();

            if(nomeCampo == "livello")
                return rand.nextInt((maxLvl - min) + 1) + min;
            return rand.nextInt((max - min) + 1) + min;
        }	
}
