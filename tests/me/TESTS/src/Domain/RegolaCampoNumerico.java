/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

public class RegolaCampoNumerico extends RegolaCampo {
	public RegolaCampoNumerico() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_REGOLACAMPONUMERICO_REGOLACOMPOSITE) {
			this.regolaComposite = (RegolaComposite) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;	
        private int limiteSuperiore; // limite superiore (inclusivo)
        private int limiteInferiore; // limite inferiore (inclusivo)	
	private String nomeCampo;
	
	private RegolaComposite regolaComposite;
        
        public RegolaCampoNumerico( String nomeCampo, int inf, int sup )
	{
		limiteInferiore = inf;
		limiteSuperiore = sup;
		this.nomeCampo = nomeCampo;
	}
	
	
        public boolean ApplicaRegolaSuScheda(Scheda s) 
        {
            if( s.getChiaviCampi().contains(nomeCampo) )
                    {
                            Campo c = s.getCampo(nomeCampo);

                            if( c instanceof CampoNumerico ) // Se è della classe giusta..
                            {
                                    CampoNumerico cn = (CampoNumerico)c;

                                    return (cn.getValore()<=limiteInferiore && cn.getValore()>=limiteSuperiore );
                            }
                    }
                    return false;
        }

	public int getLimiteSuperiore() {
		return limiteSuperiore;
	}

	public void setLimiteSuperiore(int limiteSuperiore) {
		this.limiteSuperiore = limiteSuperiore;
	}

	public int getLimiteInferiore() {
		return limiteInferiore;
	}

	public void setLimiteInferiore(int limiteInferiore) {
		this.limiteInferiore = limiteInferiore;
	}

	public String getNomeCampo() {
		return nomeCampo;
	}

	public void setNomeCampo(String nomeCampo) {
		this.nomeCampo = nomeCampo;
	}
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setRegolaComposite(RegolaComposite value) {
		if (regolaComposite != null) {
			regolaComposite.regolaCampoNumerico.remove(this);
		}
		if (value != null) {
			value.regolaCampoNumerico.add(this);
		}
	}
	
	public RegolaComposite getRegolaComposite() {
		return regolaComposite;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_RegolaComposite(RegolaComposite value) {
		this.regolaComposite = value;
	}
	
	private RegolaComposite getORM_RegolaComposite() {
		return regolaComposite;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
