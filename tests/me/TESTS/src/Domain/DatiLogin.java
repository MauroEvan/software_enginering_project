package Domain;
import java.util.Date;


public class DatiLogin
{
    private Giocatore giocatore;
    private Date ultimoLogin;
    private static DatiLogin handle = null;
    
    public static DatiLogin getInstance()
    {
        if( handle == null )
            handle = new DatiLogin();
        return handle;
    }

    public Giocatore getGiocatore() { return giocatore; }
    public void setGiocatore(Giocatore giocatore) { this.giocatore = giocatore; }
    public Date getUltimoLogin() { return ultimoLogin; }
    public void setUltimoLogin(Date ultimoLogin) { this.ultimoLogin = ultimoLogin; }
}
