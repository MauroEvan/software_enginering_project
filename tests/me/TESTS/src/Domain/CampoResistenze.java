/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

import java.util.ArrayList;
import java.util.Random;

public class CampoResistenze implements Campo {
	public CampoResistenze() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_CAMPORESISTENZE_SCHEDA) {
			this.scheda = (Scheda) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;	
	private int classeArmatura;	
	private int riflessi;	
	private int tempra;	
	private int volonta;
	
	private Scheda scheda;
        
        public CampoResistenze(int classeArmatura, int riflessi, int tempra, int volonta)
        {
            this.classeArmatura = 10 + classeArmatura;
            this.riflessi = riflessi;
            this.tempra = tempra;
            this.volonta = volonta;
        }

        public CampoResistenze(boolean random)
        {
                    if( random )
                    {
                            this.classeArmatura = 10 + this.Randomizer(0, 10);
                            this.riflessi = this.Randomizer(0, 6);
                            this.tempra = this.Randomizer(0, 6);
                            this.volonta = this.Randomizer(0, 6);
                    }
        }
        
        public String getNome() { return "resistenze"; }
        public int getClasseArmatura() { return classeArmatura; }
        public void setClasseArmatura(int classeArmatura) { this.classeArmatura = classeArmatura; }
        public int getRiflessi() { return riflessi; }
        public void setRiflessi(int riflessi) { this.riflessi = riflessi; }
        public int getTempra() { return tempra; }
        public void setTempra(int tempra) { this.tempra = tempra; }
        public int getVolonta() { return volonta; }
        public void setVolonta(int volonta) { this.volonta = volonta; }

        public String toString() { return "Classe armatura " + this.getClasseArmatura()
                                        + "\nRiflessi " + this.getRiflessi()
                                        + "\nTempra " + this.getTempra()
                                        + "\nVolontà " + this.getVolonta(); }

        public Object getValore() {
            ArrayList<Integer> valori = new ArrayList<>();
            valori.add(this.tempra);
            valori.add(this.riflessi);
            valori.add(this.volonta);
            valori.add(this.classeArmatura);
            return valori;
        }

        public int getModificatore() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
		
	public void setScheda(Scheda value) {
		if (scheda != null) {
			scheda.campoResistenze.remove(this);
		}
		if (value != null) {
			value.campoResistenze.add(this);
		}
	}
	
	public Scheda getScheda() {
		return scheda;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Scheda(Scheda value) {
		this.scheda = value;
	}
	
	private Scheda getORM_Scheda() {
		return scheda;
	}
	
        private int Randomizer(int min, int max)
        {
            Random rand = new Random();
            return rand.nextInt((max - min) + 1) + min;  
        }
	
}
