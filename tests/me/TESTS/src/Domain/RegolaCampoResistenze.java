/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

public class RegolaCampoResistenze extends RegolaCampo {
	public RegolaCampoResistenze() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_REGOLACAMPORESISTENZE_REGOLACOMPOSITE) {
			this.regolaComposite = (RegolaComposite) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;
	
	private int minArmatura;	
	private int minVolonta;	
	private int minTempra;	
	private int minRiflessi;	
	private String nomeCampo;
	
	private RegolaComposite regolaComposite;
        
	public RegolaCampoResistenze( String nomeCampo, int volonta, int tempra, int riflessi, int armatura )
	{
		this.nomeCampo = nomeCampo;
		minArmatura = armatura;
		minVolonta = volonta;
		minRiflessi = riflessi;
		minTempra = tempra;
	}
	
	
        public boolean ApplicaRegolaSuScheda(Scheda s) 
        {
            //Verifichiamo la presenza del campo
            if( s.getChiaviCampi().contains(nomeCampo) )
            {
                Campo c = s.getCampo(nomeCampo);

                if( c instanceof CampoResistenze )
                {
                    CampoResistenze cr = (CampoResistenze)c;

                    return( cr.getVolonta() >= minVolonta &&
                            cr.getClasseArmatura() >= minArmatura &&
                            cr.getRiflessi() >= minRiflessi &&
                            cr.getTempra() >= minTempra );
                }
            }
            return false;
        }

	public String getNomeCampo() {
		return nomeCampo;
	}

	public void setNomeCampo(String nomeCampo) {
		this.nomeCampo = nomeCampo;
	}

	public int getMinVolonta() {
		return minVolonta;
	}

	public void setMinVolonta(int minVolonta) {
		this.minVolonta = minVolonta;
	}

	public int getMinTempra() {
		return minTempra;
	}

	public void setMinTempra(int minTempra) {
		this.minTempra = minTempra;
	}

	public int getMinRiflessi() {
		return minRiflessi;
	}

	public void setMinRiflessi(int minRiflessi) {
		this.minRiflessi = minRiflessi;
	}

	public int getMinArmatura() {
		return minArmatura;
	}

	public void setMinArmatura(int minArmatura) {
		this.minArmatura = minArmatura;
	}
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setRegolaComposite(RegolaComposite value) {
		if (regolaComposite != null) {
			regolaComposite.regolaCampoResistenze.remove(this);
		}
		if (value != null) {
			value.regolaCampoResistenze.add(this);
		}
	}
	
	public RegolaComposite getRegolaComposite() {
		return regolaComposite;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_RegolaComposite(RegolaComposite value) {
		this.regolaComposite = value;
	}
	
	private RegolaComposite getORM_RegolaComposite() {
		return regolaComposite;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
