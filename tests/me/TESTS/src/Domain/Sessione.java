/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

import java.util.ArrayList;

public class Sessione {
	public Sessione() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_SESSIONE_MODULOREGOLAMENTO) {
			return ORM_moduloRegolamento;
		}
		else if (key == ORMConstants.KEY_SESSIONE_EVENTI) {
			return ORM_eventi;
		}
		else if (key == ORMConstants.KEY_SESSIONE_SCHEDE) {
			return ORM_schede;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int ID;
	
	private Giocatore gm;	
	private Mappa mappa;	
	private String descr;
	
	private java.util.Set ORM_moduloRegolamento = new java.util.HashSet();	
	private java.util.Set ORM_eventi = new java.util.HashSet();	
	private java.util.Set ORM_schede = new java.util.HashSet();
        
        private ID idSessione;
	private ArrayList<ModuloDiRegolamento> setRegole = new ArrayList<ModuloDiRegolamento>();
	private ArrayList<Evento> eventi = new ArrayList<Evento>();
	private ArrayList<Scheda> schede = new ArrayList<Scheda>();
	private StatoSessione stato;
        
	public Sessione( Giocatore gm )
	{
		this.stato = StatoSessione.sessioneInattiva;
		this.gm = gm;
	}

        
	public String getDescr() 
	{
			return descr;
	}


	public void setDescr(String descr) 
	{
			this.descr = descr;
	}

        
	public ID getIdSessione()
	{
		return this.idSessione;
	}
	
	
	public void setIdSessione( ID nuovaIdSessione )
	{
		this.idSessione = nuovaIdSessione;
	}
	

	public Giocatore getGm()
	{
		return gm;
	}


	public void setGm(Giocatore gm)
	{
		this.gm = gm;
	}


	public Mappa getMappa()
	{
		return mappa;
	}


	public void setMappa(Mappa mappa)
	{
		this.mappa = mappa;
	}


	public StatoSessione getStato()
	{
		return stato;
	}


	public void setStato(StatoSessione stato)
	{
		this.stato = stato;
	}


	public void ottieniITuoiDati()
	{
		// Caso d'uso di avviamento----------
		Evento e;
		ModuloDiRegolamento m;
		Scheda s;

		this.mappa = new Mappa();
		for( int i = 0; i<3; i++ )
		{
			e = new Evento();
			m = new ModuloDiRegolamento();
			s = new Scheda();
			e.setIdEvento( new ID(i) );
			m.setIdModulo( new ID(i) );
			s.setIdScheda( new ID(i) );
			this.eventi.add( e );
			this.setRegole.add( m );
			this.schede.add( s );
		}
		// -----------------------------------
	}


	public void associaSchedeLibere()
	{
		for( Scheda schedaAttuale : this.schede )
		{
			if( schedaAttuale.getGiocatore() == null )
			{
				schedaAttuale.setGiocatore( this.gm );
				this.gm.aggiungiScheda( schedaAttuale );
                System.out.println(schedaAttuale.toString() + " non era associata. E' stata associata con " + this.gm.toString());
			}
		}
	}


	public ArrayList<Scheda> getSchede()
	{
		return schede;
	}

	
	void setIdSessione(int i) 
	{
		this.idSessione = new ID( i );
	}

	
	public String toString()
	{
		return "Sessione#"+idSessione.toString();
	}
	
	
	public boolean isGM( Giocatore g )
	{
		return g==gm;
	}
	
	public Scheda AggiungiSchedaVuota()
	{
		Scheda s = new Scheda();
		schede.add(s);
		return s;
	}
	
	
	// Ritorna le regole fallenti di tutti i moduli
	public ArrayList<Regola> ApplicaRegoleAScheda( Scheda s )
	{
		ArrayList<Regola> blacklist_totale = new ArrayList<Regola>();
		
		for( ModuloDiRegolamento m : setRegole )
		{
			blacklist_totale.addAll( m.ApplicaSuScheda(s) );
		}
		
		return blacklist_totale;
	}
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
		
	private void setORM_ModuloRegolamento(java.util.Set value) {
		this.ORM_moduloRegolamento = value;
	}
	
	private java.util.Set getORM_ModuloRegolamento() {
		return ORM_moduloRegolamento;
	}
	
	public final ModuloDiRegolamentoSetCollection moduloRegolamento = new ModuloDiRegolamentoSetCollection(this, _ormAdapter, ORMConstants.KEY_SESSIONE_MODULOREGOLAMENTO, ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	private void setORM_Eventi(java.util.Set value) {
		this.ORM_eventi = value;
	}
	
	private java.util.Set getORM_Eventi() {
		return ORM_eventi;
	}
	
	public final EventoSetCollection eventiSetCollection = new EventoSetCollection(this, _ormAdapter, ORMConstants.KEY_SESSIONE_EVENTI, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Schede(java.util.Set value) {
		this.ORM_schede = value;
	}
	
	private java.util.Set getORM_Schede() {
		return ORM_schede;
	}
	
	public final SchedaSetCollection schedeSetCollection = new SchedaSetCollection(this, _ormAdapter, ORMConstants.KEY_SESSIONE_SCHEDE, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public Scheda getScheda(int index) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
}
