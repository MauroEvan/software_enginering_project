/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

import java.util.ArrayList;
import java.util.Iterator;

public class RegolaComposite implements RegolaComponent {
	public RegolaComposite() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_REGOLACOMPOSITE_REGOLAGENERICA) {
			return ORM_regolaGenerica;
		}
		else if (key == ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPONUMERICO) {
			return ORM_regolaCampoNumerico;
		}
		else if (key == ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPORESISTENZE) {
			return ORM_regolaCampoResistenze;
		}
		else if (key == ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOATTRIBUTO) {
			return ORM_regolaCampoAttributo;
		}
		else if (key == ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOABILITA) {
			return ORM_regolaCampoAbilita;
		}
		else if (key == ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOOGGETTO) {
			return ORM_regolaCampoOggetto;
		}
		else if (key == ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOTESTUALE) {
			return ORM_regolaCampoTestuale;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int ID;	
	private String nome;	
	private ModuloDiRegolamento modulo;
        
        private ArrayList<RegolaComponent> children = new ArrayList<RegolaComponent>();
	
	private java.util.Set ORM_regolaGenerica = new java.util.HashSet();	
	private java.util.Set ORM_regolaCampoNumerico = new java.util.HashSet();	
	private java.util.Set ORM_regolaCampoResistenze = new java.util.HashSet();	
	private java.util.Set ORM_regolaCampoAttributo = new java.util.HashSet();	
	private java.util.Set ORM_regolaCampoAbilita = new java.util.HashSet();	
	private java.util.Set ORM_regolaCampoOggetto = new java.util.HashSet();	
	private java.util.Set ORM_regolaCampoTestuale = new java.util.HashSet();
        
	public boolean Applica( Object o )
	{	
		// Scorre tutti i figli e applica
		for( RegolaComponent c : children )
		{
			if( c.Applica(o) == false )
			{
				// TODO - Notifica all'utente?
				return false; // Se anche una sola figlia fallisce...
			}
		}		
		return true;
	}
        
        public Iterator iterator()
        {
            return new RegolaIterator(children.iterator());
        }	
	
	public RegolaComponent GetChild( int index ) throws IndexOutOfBoundsException
	{
		if( index >= children.size() )
			throw new IndexOutOfBoundsException();
		return children.get(index);
	}	
	
	public void AddChild( RegolaComponent r )
	{
		children.add(r);
	}	
	
	public void RemChild( int index ) throws IndexOutOfBoundsException
	{
		if( index >= children.size() )
			throw new IndexOutOfBoundsException();
		children.remove(index);
	}
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setNome(String value) {
		this.nome = value;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setModulo(ModuloDiRegolamento value) {
		if (this.modulo != value) {
			ModuloDiRegolamento lmodulo = this.modulo;
			this.modulo = value;
			if (value != null) {
				modulo.setRegolaComposite(this);
			}
			else {
				lmodulo.setRegolaComposite(null);
			}
		}
	}
	
	public ModuloDiRegolamento getModulo() {
		return modulo;
	}
	
	private void setORM_RegolaGenerica(java.util.Set value) {
		this.ORM_regolaGenerica = value;
	}
	
	private java.util.Set getORM_RegolaGenerica() {
		return ORM_regolaGenerica;
	}
	
	public final RegolaGenericaSetCollection regolaGenerica = new RegolaGenericaSetCollection(this, _ormAdapter, ORMConstants.KEY_REGOLACOMPOSITE_REGOLAGENERICA, ORMConstants.KEY_REGOLAGENERICA_REGOLACOMPOSITE, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_RegolaCampoNumerico(java.util.Set value) {
		this.ORM_regolaCampoNumerico = value;
	}
	
	private java.util.Set getORM_RegolaCampoNumerico() {
		return ORM_regolaCampoNumerico;
	}
	
	public final RegolaCampoNumericoSetCollection regolaCampoNumerico = new RegolaCampoNumericoSetCollection(this, _ormAdapter, ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPONUMERICO, ORMConstants.KEY_REGOLACAMPONUMERICO_REGOLACOMPOSITE, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_RegolaCampoResistenze(java.util.Set value) {
		this.ORM_regolaCampoResistenze = value;
	}
	
	private java.util.Set getORM_RegolaCampoResistenze() {
		return ORM_regolaCampoResistenze;
	}
	
	public final RegolaCampoResistenzeSetCollection regolaCampoResistenze = new RegolaCampoResistenzeSetCollection(this, _ormAdapter, ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPORESISTENZE, ORMConstants.KEY_REGOLACAMPORESISTENZE_REGOLACOMPOSITE, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_RegolaCampoAttributo(java.util.Set value) {
		this.ORM_regolaCampoAttributo = value;
	}
	
	private java.util.Set getORM_RegolaCampoAttributo() {
		return ORM_regolaCampoAttributo;
	}
	
	public final RegolaCampoAttributoSetCollection regolaCampoAttributo = new RegolaCampoAttributoSetCollection(this, _ormAdapter, ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOATTRIBUTO, ORMConstants.KEY_REGOLACAMPOATTRIBUTO_REGOLACOMPOSITE, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_RegolaCampoAbilità(java.util.Set value) {
		this.ORM_regolaCampoAbilita = value;
	}
	
	private java.util.Set getORM_RegolaCampoAbilità() {
		return ORM_regolaCampoAbilita;
	}
	
	public final RegolaCampoAbilitaSetCollection regolaCampoAbilita = new RegolaCampoAbilitaSetCollection(this, _ormAdapter, ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOABILITA, ORMConstants.KEY_REGOLACAMPOABILITA_REGOLACOMPOSITE, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_RegolaCampoOggetto(java.util.Set value) {
		this.ORM_regolaCampoOggetto = value;
	}
	
	private java.util.Set getORM_RegolaCampoOggetto() {
		return ORM_regolaCampoOggetto;
	}
	
	public final RegolaCampoOggettoSetCollection regolaCampoOggetto = new RegolaCampoOggettoSetCollection(this, _ormAdapter, ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOOGGETTO, ORMConstants.KEY_REGOLACAMPOOGGETTO_REGOLACOMPOSITE, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_RegolaCampoTestuale(java.util.Set value) {
		this.ORM_regolaCampoTestuale = value;
	}
	
	private java.util.Set getORM_RegolaCampoTestuale() {
		return ORM_regolaCampoTestuale;
	}
	
	public final RegolaCampoTestualeSetCollection regolaCampoTestuale = new RegolaCampoTestualeSetCollection(this, _ormAdapter, ORMConstants.KEY_REGOLACOMPOSITE_REGOLACAMPOTESTUALE, ORMConstants.KEY_REGOLACAMPOTESTUALE_REGOLACOMPOSITE, ORMConstants.KEY_MUL_ONE_TO_MANY);
		
	public String toString() {
		return String.valueOf(getID());
	}
	
}
