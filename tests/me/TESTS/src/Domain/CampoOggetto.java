/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

import java.util.Random;

public class CampoOggetto implements Campo {
	public CampoOggetto() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_CAMPOOGGETTO_SCHEDA) {
			this.scheda = (Scheda) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;	
	private String nomeOggetto;	
	private int quantita;
	
	private Scheda scheda;
        
        public CampoOggetto(String nomeOggetto, int quantita)
        {
            this.nomeOggetto = nomeOggetto;
            this.quantita = quantita;
        }
    
        public CampoOggetto( String nomeOggetto, boolean random )
	{
		this.nomeOggetto = nomeOggetto;
		if( random )
			this.quantita = Randomizer();
	}
        
        public String getNome() { return nomeOggetto; }
        public void setNome(String nomeOggetto) { this.nomeOggetto = nomeOggetto; }
        public int getQuantita() { return quantita; }
        public void setQuantita(int quantita) { this.quantita = quantita; }

        public String toString() { return this.getNome() + " " + this.getQuantita(); }

        public Object getValore() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public int getModificatore() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setScheda(Scheda value) {
		if (scheda != null) {
			scheda.campoOggetto.remove(this);
		}
		if (value != null) {
			value.campoOggetto.add(this);
		}
	}
	
	public Scheda getScheda() {
		return scheda;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Scheda(Scheda value) {
		this.scheda = value;
	}
	
	private Scheda getORM_Scheda() {
		return scheda;
	}
	
        private int Randomizer()
        {
            int min = 1;
            int max = 6;
            Random rand = new Random();
            return rand.nextInt((max - min) + 1) + min;
        }
        
}
