/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

import java.util.ArrayList;

public class Giocatore {
	public Giocatore() {
	}
	
	private int ID;
	
	private String nomeGiocatore;
        private ArrayList<Scheda> schedeGiocatore = new ArrayList<Scheda>();
        
        public String getNomeGiocatore() 
	{
		return nomeGiocatore;
	}


	public void setNomeGiocatore(String nomeGiocatore) 
	{
		this.nomeGiocatore = nomeGiocatore;
	}
	
	public void aggiungiScheda( Scheda s )
	{
		schedeGiocatore.add( s );
	}

        public String toString()
        {
            if( nomeGiocatore == null )
                return "SenzaNome";
            return nomeGiocatore;
        }
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}

}
