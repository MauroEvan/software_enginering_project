/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Scheda {
	public Scheda() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_SCHEDA_CAMPORESISTENZE) {
			return ORM_campoResistenze;
		}
		else if (key == ORMConstants.KEY_SCHEDA_CAMPOABILITA) {
			return ORM_campoAbilita;
		}
		else if (key == ORMConstants.KEY_SCHEDA_CAMPOTESTUALE) {
			return ORM_campoTestuale;
		}
		else if (key == ORMConstants.KEY_SCHEDA_CAMPONUMERICO) {
			return ORM_campoNumerico;
		}
		else if (key == ORMConstants.KEY_SCHEDA_CAMPOOGGETTO) {
			return ORM_campoOggetto;
		}
		else if (key == ORMConstants.KEY_SCHEDA_CAMPOATTRIBUTO) {
			return ORM_campoAttributo;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int ID;
	private ID idScheda;
	private Giocatore giocatore;
	private HashMap<String,Campo> campi = new HashMap<String,Campo>();
	
	private java.util.Set ORM_campoResistenze = new java.util.HashSet();	
	private java.util.Set ORM_campoAbilita = new java.util.HashSet();	
	private java.util.Set ORM_campoTestuale = new java.util.HashSet();	
	private java.util.Set ORM_campoNumerico = new java.util.HashSet();	
	private java.util.Set ORM_campoOggetto = new java.util.HashSet();	
	private java.util.Set ORM_campoAttributo = new java.util.HashSet();
        
	public ID getIdScheda() { return idScheda; }
	public void setIdScheda(ID idScheda) { this.idScheda = idScheda; }
	public Giocatore getGiocatore()	{ return giocatore; }
	public void setGiocatore(Giocatore giocatore) {	this.giocatore = giocatore; }

	public String toString()
	{
		return "Scheda#"+idScheda.toString();
	}
	
	public void addCampo( String key, Campo c )
	{
		// Nota: sovrascrive se la chiave già c'è
		campi.put( key, c );
	}
	
	public Campo getCampo( String key )
	{
		return campi.get( key );
	}
	
	public Set<String> getChiaviCampi()
	{
		return campi.keySet();
	}
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	private void setORM_CampoResistenze(java.util.Set value) {
		this.ORM_campoResistenze = value;
	}
	
	private java.util.Set getORM_CampoResistenze() {
		return ORM_campoResistenze;
	}
	
	public final CampoResistenzeSetCollection campoResistenze = new CampoResistenzeSetCollection(this, _ormAdapter, ORMConstants.KEY_SCHEDA_CAMPORESISTENZE, ORMConstants.KEY_CAMPORESISTENZE_SCHEDA, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_CampoAbilità(java.util.Set value) {
		this.ORM_campoAbilita = value;
	}
	
	private java.util.Set getORM_CampoAbilità() {
		return ORM_campoAbilita;
	}
	
	public final CampoAbilitaSetCollection campoAbilita = new CampoAbilitaSetCollection(this, _ormAdapter, ORMConstants.KEY_SCHEDA_CAMPOABILITA, ORMConstants.KEY_CAMPOABILITA_SCHEDA, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_CampoTestuale(java.util.Set value) {
		this.ORM_campoTestuale = value;
	}
	
	private java.util.Set getORM_CampoTestuale() {
		return ORM_campoTestuale;
	}
	
	public final CampoTestualeSetCollection campoTestuale = new CampoTestualeSetCollection(this, _ormAdapter, ORMConstants.KEY_SCHEDA_CAMPOTESTUALE, ORMConstants.KEY_CAMPOTESTUALE_SCHEDA, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_CampoNumerico(java.util.Set value) {
		this.ORM_campoNumerico = value;
	}
	
	private java.util.Set getORM_CampoNumerico() {
		return ORM_campoNumerico;
	}
	
	public final CampoNumericoSetCollection campoNumerico = new CampoNumericoSetCollection(this, _ormAdapter, ORMConstants.KEY_SCHEDA_CAMPONUMERICO, ORMConstants.KEY_CAMPONUMERICO_SCHEDA, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_CampoOggetto(java.util.Set value) {
		this.ORM_campoOggetto = value;
	}
	
	private java.util.Set getORM_CampoOggetto() {
		return ORM_campoOggetto;
	}
	
	public final CampoOggettoSetCollection campoOggetto = new CampoOggettoSetCollection(this, _ormAdapter, ORMConstants.KEY_SCHEDA_CAMPOOGGETTO, ORMConstants.KEY_CAMPOOGGETTO_SCHEDA, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_CampoAttributo(java.util.Set value) {
		this.ORM_campoAttributo = value;
	}
	
	private java.util.Set getORM_CampoAttributo() {
		return ORM_campoAttributo;
	}
	
	public final CampoAttributoSetCollection campoAttributo = new CampoAttributoSetCollection(this, _ormAdapter, ORMConstants.KEY_SCHEDA_CAMPOATTRIBUTO, ORMConstants.KEY_CAMPOATTRIBUTO_SCHEDA, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public void addCampo(Campo c) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
		
}
