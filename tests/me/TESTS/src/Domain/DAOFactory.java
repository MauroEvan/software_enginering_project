/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

import Domain.dao.*;

public abstract class DAOFactory {
	private static DAOFactory _factory = new DAOFactoryImpl();
	
	public static DAOFactory getDAOFactory() {
		return _factory;
	}
	
	public abstract GiocatoreDAO getGiocatoreDAO();
	public abstract SessioneDAO getSessioneDAO();
	public abstract MappaDAO getMappaDAO();
	public abstract EventoDAO getEventoDAO();
	public abstract ModuloDiRegolamentoDAO getModuloDiRegolamentoDAO();
	public abstract SchedaDAO getSchedaDAO();
	public abstract CampoNumericoDAO getCampoNumericoDAO();
	public abstract CampoTestualeDAO getCampoTestualeDAO();
	public abstract CampoAttributoDAO getCampoAttributoDAO();
	public abstract CampoAbilitaDAO getCampoAbilitàDAO();
	public abstract CampoOggettoDAO getCampoOggettoDAO();
	public abstract CampoResistenzeDAO getCampoResistenzeDAO();
	public abstract RegolaCompositeDAO getRegolaCompositeDAO();
	public abstract RegolaGenericaDAO getRegolaGenericaDAO();
	public abstract RegolaCampoTestualeDAO getRegolaCampoTestualeDAO();
	public abstract RegolaCampoAttributoDAO getRegolaCampoAttributoDAO();
	public abstract RegolaCampoOggettoDAO getRegolaCampoOggettoDAO();
	public abstract RegolaCampoAbilitaDAO getRegolaCampoAbilitaDAO();
	public abstract RegolaCampoNumericoDAO getRegolaCampoNumericoDAO();
	public abstract RegolaCampoResistenzeDAO getRegolaCampoResistenzeDAO();
}

