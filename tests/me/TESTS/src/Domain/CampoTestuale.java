/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

import java.util.Random;

public class CampoTestuale implements Campo {
	public CampoTestuale() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_CAMPOTESTUALE_SCHEDA) {
			this.scheda = (Scheda) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
        
        public CampoTestuale(String nomeCampo, String valore)
        {
            this.nomeCampo = nomeCampo;
            this.valore = valore;
        }

        public CampoTestuale(String nomeCampo)
        {
            this.nomeCampo = nomeCampo;
            this.valore = Randomizer(nomeCampo);
        }
	
	private int ID;	
	private String nomeCampo;	
	private String valore;
	
	private Scheda scheda;
        

        public String getNome() { return nomeCampo; }
        public void setNome(String nomeCampo) { this.nomeCampo = nomeCampo; }
        public String getValore() { return valore; }
        public void setValore(String valore) { this.valore = valore; }

        public String toString() { return this.getNome() + " " + this.getValore(); }    

        public int getModificatore() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setScheda(Scheda value) {
		if (scheda != null) {
			scheda.campoTestuale.remove(this);
		}
		if (value != null) {
			value.campoTestuale.add(this);
		}
	}
	
	public Scheda getScheda() {
		return scheda;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Scheda(Scheda value) {
		this.scheda = value;
	}
	
	private Scheda getORM_Scheda() {
		return scheda;
	}
	
        private String Randomizer(String nomeCampo)
        {
            if(nomeCampo.equals("razza")) return RandomizerRazza();
            if(nomeCampo.equals("classe")) return RandomizerClasse();
            if(nomeCampo.equals("allineamento")) return RandomizerAllineamento();

            return RandomizerNome();
        }

        private String RandomizerNome(){
            char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
            StringBuilder sb = new StringBuilder();
            Random rand = new Random();
            for (int i = 0; i < 7; i++) {
                char c = chars[rand.nextInt(chars.length)];
                sb.append(c);
            }
            sb.append(' ');
            for (int i = 0; i < 7; i++) {
                char c = chars[rand.nextInt(chars.length)];
                sb.append(c);
            }
            return sb.toString();
        }

        private String RandomizerRazza(){
            String[] razze = new String[4];
            razze[0] = "Umano";
            razze[1] = "Elfo";
            razze[2] = "Gnomo";
            razze[3] = "Nano";

            Random rand = new Random();
            return razze[ rand.nextInt(3) ];
        }

        private String RandomizerAllineamento(){
            String[] first = new String[3];
            first[0] = "Legale";
            first[1] = "Neutrale";
            first[2] = "Caotico";

            String[] second = new String[3];
            second[0] = "Buono";
            second[1] = "Neutrale";
            second[2] = "Malvagio";

            Random rand = new Random();
            StringBuilder sb = new StringBuilder();


            sb.append( first[ rand.nextInt(3) ] ).append( " " ).append( second[ rand.nextInt(3) ] );
            return sb.toString();
        }

        private String RandomizerClasse(){
            String[] razze = new String[3];
            razze[0] = "Guerriero";
            razze[1] = "Mago";
            razze[2] = "Ladro";

            Random rand = new Random();
            return razze[ rand.nextInt(3) ];
        }
		
}
