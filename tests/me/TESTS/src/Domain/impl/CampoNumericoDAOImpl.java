/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import Domain.*;

public class CampoNumericoDAOImpl implements Domain.dao.CampoNumericoDAO {
	public CampoNumerico loadCampoNumericoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadCampoNumericoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico getCampoNumericoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return getCampoNumericoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico loadCampoNumericoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadCampoNumericoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico getCampoNumericoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return getCampoNumericoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico loadCampoNumericoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (CampoNumerico) session.load(CampoNumerico.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico getCampoNumericoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (CampoNumerico) session.get(CampoNumerico.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico loadCampoNumericoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (CampoNumerico) session.load(CampoNumerico.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico getCampoNumericoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (CampoNumerico) session.get(CampoNumerico.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoNumerico(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return queryCampoNumerico(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoNumerico(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return queryCampoNumerico(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico[] listCampoNumericoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return listCampoNumericoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico[] listCampoNumericoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return listCampoNumericoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoNumerico(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Domain.CampoNumerico as CampoNumerico");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoNumerico(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Domain.CampoNumerico as CampoNumerico");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("CampoNumerico", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico[] listCampoNumericoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryCampoNumerico(session, condition, orderBy);
			return (CampoNumerico[]) list.toArray(new CampoNumerico[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico[] listCampoNumericoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryCampoNumerico(session, condition, orderBy, lockMode);
			return (CampoNumerico[]) list.toArray(new CampoNumerico[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico loadCampoNumericoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadCampoNumericoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico loadCampoNumericoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadCampoNumericoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico loadCampoNumericoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		CampoNumerico[] campoNumericos = listCampoNumericoByQuery(session, condition, orderBy);
		if (campoNumericos != null && campoNumericos.length > 0)
			return campoNumericos[0];
		else
			return null;
	}
	
	public CampoNumerico loadCampoNumericoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		CampoNumerico[] campoNumericos = listCampoNumericoByQuery(session, condition, orderBy, lockMode);
		if (campoNumericos != null && campoNumericos.length > 0)
			return campoNumericos[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateCampoNumericoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return iterateCampoNumericoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoNumericoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return iterateCampoNumericoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoNumericoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Domain.CampoNumerico as CampoNumerico");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoNumericoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Domain.CampoNumerico as CampoNumerico");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("CampoNumerico", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoNumerico createCampoNumerico() {
		return new CampoNumerico();
	}
	
	public boolean save(CampoNumerico campoNumerico) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().saveObject(campoNumerico);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(CampoNumerico campoNumerico) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().deleteObject(campoNumerico);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(CampoNumerico campoNumerico)throws PersistentException {
		try {
			if(campoNumerico.getScheda() != null) {
				campoNumerico.getScheda().campoNumerico.remove(campoNumerico);
			}
			
			return delete(campoNumerico);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(CampoNumerico campoNumerico, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(campoNumerico.getScheda() != null) {
				campoNumerico.getScheda().campoNumerico.remove(campoNumerico);
			}
			
			try {
				session.delete(campoNumerico);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(CampoNumerico campoNumerico) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().refresh(campoNumerico);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(CampoNumerico campoNumerico) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().evict(campoNumerico);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
