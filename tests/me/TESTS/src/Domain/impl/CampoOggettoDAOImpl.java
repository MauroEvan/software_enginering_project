/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import Domain.*;

public class CampoOggettoDAOImpl implements Domain.dao.CampoOggettoDAO {
	public CampoOggetto loadCampoOggettoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadCampoOggettoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto getCampoOggettoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return getCampoOggettoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto loadCampoOggettoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadCampoOggettoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto getCampoOggettoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return getCampoOggettoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto loadCampoOggettoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (CampoOggetto) session.load(CampoOggetto.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto getCampoOggettoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (CampoOggetto) session.get(CampoOggetto.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto loadCampoOggettoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (CampoOggetto) session.load(CampoOggetto.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto getCampoOggettoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (CampoOggetto) session.get(CampoOggetto.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoOggetto(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return queryCampoOggetto(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoOggetto(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return queryCampoOggetto(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto[] listCampoOggettoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return listCampoOggettoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto[] listCampoOggettoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return listCampoOggettoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoOggetto(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Domain.CampoOggetto as CampoOggetto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoOggetto(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Domain.CampoOggetto as CampoOggetto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("CampoOggetto", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto[] listCampoOggettoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryCampoOggetto(session, condition, orderBy);
			return (CampoOggetto[]) list.toArray(new CampoOggetto[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto[] listCampoOggettoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryCampoOggetto(session, condition, orderBy, lockMode);
			return (CampoOggetto[]) list.toArray(new CampoOggetto[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto loadCampoOggettoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadCampoOggettoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto loadCampoOggettoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadCampoOggettoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto loadCampoOggettoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		CampoOggetto[] campoOggettos = listCampoOggettoByQuery(session, condition, orderBy);
		if (campoOggettos != null && campoOggettos.length > 0)
			return campoOggettos[0];
		else
			return null;
	}
	
	public CampoOggetto loadCampoOggettoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		CampoOggetto[] campoOggettos = listCampoOggettoByQuery(session, condition, orderBy, lockMode);
		if (campoOggettos != null && campoOggettos.length > 0)
			return campoOggettos[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateCampoOggettoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return iterateCampoOggettoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoOggettoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return iterateCampoOggettoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoOggettoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From CampoOggetto as CampoOggetto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoOggettoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From CampoOggetto as CampoOggetto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("CampoOggetto", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoOggetto createCampoOggetto() {
		return new CampoOggetto();
	}
	
	public boolean save(CampoOggetto campoOggetto) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().saveObject(campoOggetto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(CampoOggetto campoOggetto) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().deleteObject(campoOggetto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(CampoOggetto campoOggetto)throws PersistentException {
		try {
			if(campoOggetto.getScheda() != null) {
				campoOggetto.getScheda().campoOggetto.remove(campoOggetto);
			}
			
			return delete(campoOggetto);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(CampoOggetto campoOggetto, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(campoOggetto.getScheda() != null) {
				campoOggetto.getScheda().campoOggetto.remove(campoOggetto);
			}
			
			try {
				session.delete(campoOggetto);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(CampoOggetto campoOggetto) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().refresh(campoOggetto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(CampoOggetto campoOggetto) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().evict(campoOggetto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
