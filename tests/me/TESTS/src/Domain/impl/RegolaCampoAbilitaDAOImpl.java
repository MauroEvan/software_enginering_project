/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import Domain.*;

public class RegolaCampoAbilitaDAOImpl implements Domain.dao.RegolaCampoAbilitaDAO {
	public RegolaCampoAbilita loadRegolaCampoAbilitaByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return RegolaCampoAbilitaDAOImpl.this.loadRegolaCampoAbilitaByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita getRegolaCampoAbilitaByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return RegolaCampoAbilitaDAOImpl.this.getRegolaCampoAbilitaByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita loadRegolaCampoAbilitaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoAbilitaByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita getRegolaCampoAbilitaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return getRegolaCampoAbilitaByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita loadRegolaCampoAbilitaByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaCampoAbilita) session.load(RegolaCampoAbilita.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita getRegolaCampoAbilitaByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaCampoAbilita) session.get(RegolaCampoAbilita.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita loadRegolaCampoAbilitaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaCampoAbilita) session.load(RegolaCampoAbilita.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita getRegolaCampoAbilitaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaCampoAbilita) session.get(RegolaCampoAbilita.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoAbilita(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return RegolaCampoAbilitaDAOImpl.this.queryRegolaCampoAbilita(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoAbilita(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return queryRegolaCampoAbilita(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita[] listRegolaCampoAbilitaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return RegolaCampoAbilitaDAOImpl.this.listRegolaCampoAbilitaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita[] listRegolaCampoAbilitaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return listRegolaCampoAbilitaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoAbilita(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaCampoAbilità as RegolaCampoAbilità");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoAbilita(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaCampoAbilità as RegolaCampoAbilità");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaCampoAbilità", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita[] listRegolaCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = RegolaCampoAbilitaDAOImpl.this.queryRegolaCampoAbilita(session, condition, orderBy);
			return (RegolaCampoAbilita[]) list.toArray(new RegolaCampoAbilita[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita[] listRegolaCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRegolaCampoAbilita(session, condition, orderBy, lockMode);
			return (RegolaCampoAbilita[]) list.toArray(new RegolaCampoAbilita[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita loadRegolaCampoAbilitaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return RegolaCampoAbilitaDAOImpl.this.loadRegolaCampoAbilitaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita loadRegolaCampoAbilitaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoAbilitaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita loadRegolaCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		RegolaCampoAbilita[] regolaCampoAbilitas = RegolaCampoAbilitaDAOImpl.this.listRegolaCampoAbilitaByQuery(session, condition, orderBy);
		if (regolaCampoAbilitas != null && regolaCampoAbilitas.length > 0)
			return regolaCampoAbilitas[0];
		else
			return null;
	}
	
	public RegolaCampoAbilita loadRegolaCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		RegolaCampoAbilita[] regolaCampoAbilitas = listRegolaCampoAbilitaByQuery(session, condition, orderBy, lockMode);
		if (regolaCampoAbilitas != null && regolaCampoAbilitas.length > 0)
			return regolaCampoAbilitas[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateRegolaCampoAbilitaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return RegolaCampoAbilitaDAOImpl.this.iterateRegolaCampoAbilitaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoAbilitaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCampoAbilitaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaCampoAbilità as RegolaCampoAbilità");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaCampoAbilità as RegolaCampoAbilità");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaCampoAbilità", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAbilita createRegolaCampoAbilita() {
		return new RegolaCampoAbilita();
	}
	
	public boolean save(RegolaCampoAbilita regolaCampoAbilità) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().saveObject(regolaCampoAbilità);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(RegolaCampoAbilita regolaCampoAbilita) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().deleteObject(regolaCampoAbilita);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(RegolaCampoAbilita regolaCampoAbilita)throws PersistentException {
		try {
			if(regolaCampoAbilita.getRegolaComposite() != null) {
				//regolaCampoAbilita.getRegolaComposite().RegolaComposite.this.regolaCampoAbilita.remove(regolaCampoAbilita);
				regolaCampoAbilita.getRegolaComposite().regolaCampoAbilita.remove(regolaCampoAbilita);
			}
			
			return delete(regolaCampoAbilita);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(RegolaCampoAbilita regolaCampoAbilita, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(regolaCampoAbilita.getRegolaComposite() != null) {
				regolaCampoAbilita.getRegolaComposite().regolaCampoAbilita.remove(regolaCampoAbilita);
			}
			
			try {
				session.delete(regolaCampoAbilita);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(RegolaCampoAbilita regolaCampoAbilita) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().refresh(regolaCampoAbilita);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(RegolaCampoAbilita regolaCampoAbilita) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().evict(regolaCampoAbilita);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
