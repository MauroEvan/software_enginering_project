/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import Domain.*;

public class RegolaCompositeDAOImpl implements Domain.dao.RegolaCompositeDAO {
	public RegolaComposite loadRegolaCompositeByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCompositeByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite getRegolaCompositeByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return getRegolaCompositeByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite loadRegolaCompositeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCompositeByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite getRegolaCompositeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return getRegolaCompositeByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite loadRegolaCompositeByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaComposite) session.load(RegolaComposite.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite getRegolaCompositeByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaComposite) session.get(RegolaComposite.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite loadRegolaCompositeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaComposite) session.load(RegolaComposite.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite getRegolaCompositeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaComposite) session.get(RegolaComposite.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaComposite(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return queryRegolaComposite(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaComposite(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return queryRegolaComposite(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite[] listRegolaCompositeByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return listRegolaCompositeByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite[] listRegolaCompositeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return listRegolaCompositeByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaComposite(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaComposite as RegolaComposite");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaComposite(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaComposite as RegolaComposite");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaComposite", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite[] listRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRegolaComposite(session, condition, orderBy);
			return (RegolaComposite[]) list.toArray(new RegolaComposite[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite[] listRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRegolaComposite(session, condition, orderBy, lockMode);
			return (RegolaComposite[]) list.toArray(new RegolaComposite[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite loadRegolaCompositeByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCompositeByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite loadRegolaCompositeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCompositeByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite loadRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		RegolaComposite[] regolaComposites = listRegolaCompositeByQuery(session, condition, orderBy);
		if (regolaComposites != null && regolaComposites.length > 0)
			return regolaComposites[0];
		else
			return null;
	}
	
	public RegolaComposite loadRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		RegolaComposite[] regolaComposites = listRegolaCompositeByQuery(session, condition, orderBy, lockMode);
		if (regolaComposites != null && regolaComposites.length > 0)
			return regolaComposites[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateRegolaCompositeByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCompositeByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCompositeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCompositeByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaComposite as RegolaComposite");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaComposite as RegolaComposite");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaComposite", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite createRegolaComposite() {
		return new RegolaComposite();
	}
	
	public boolean save(RegolaComposite regolaComposite) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().saveObject(regolaComposite);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(RegolaComposite regolaComposite) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().deleteObject(regolaComposite);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(RegolaComposite regolaComposite)throws PersistentException {
		try {
			if(regolaComposite.getModulo() != null) {
				regolaComposite.getModulo().setRegolaComposite(null);
			}
			
			RegolaGenerica[] lRegolaGenericas = regolaComposite.regolaGenerica.toArray();
			for(int i = 0; i < lRegolaGenericas.length; i++) {
				lRegolaGenericas[i].setRegolaComposite(null);
			}
			RegolaCampoNumerico[] lRegolaCampoNumericos = regolaComposite.regolaCampoNumerico.toArray();
			for(int i = 0; i < lRegolaCampoNumericos.length; i++) {
				lRegolaCampoNumericos[i].setRegolaComposite(null);
			}
			RegolaCampoResistenze[] lRegolaCampoResistenzes = regolaComposite.regolaCampoResistenze.toArray();
			for(int i = 0; i < lRegolaCampoResistenzes.length; i++) {
				lRegolaCampoResistenzes[i].setRegolaComposite(null);
			}
			RegolaCampoAttributo[] lRegolaCampoAttributos = regolaComposite.regolaCampoAttributo.toArray();
			for(int i = 0; i < lRegolaCampoAttributos.length; i++) {
				lRegolaCampoAttributos[i].setRegolaComposite(null);
			}
			RegolaCampoAbilita[] lRegolaCampoAbilitas = regolaComposite.regolaCampoAbilita.toArray();
			for(int i = 0; i < lRegolaCampoAbilitas.length; i++) {
				lRegolaCampoAbilitas[i].setRegolaComposite(null);
			}
			RegolaCampoOggetto[] lRegolaCampoOggettos = regolaComposite.regolaCampoOggetto.toArray();
			for(int i = 0; i < lRegolaCampoOggettos.length; i++) {
				lRegolaCampoOggettos[i].setRegolaComposite(null);
			}
			RegolaCampoTestuale[] lRegolaCampoTestuales = regolaComposite.regolaCampoTestuale.toArray();
			for(int i = 0; i < lRegolaCampoTestuales.length; i++) {
				lRegolaCampoTestuales[i].setRegolaComposite(null);
			}
			return delete(regolaComposite);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(RegolaComposite regolaComposite, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(regolaComposite.getModulo() != null) {
				regolaComposite.getModulo().setRegolaComposite(null);
			}
			
			RegolaGenerica[] lRegolaGenericas = regolaComposite.regolaGenerica.toArray();
			for(int i = 0; i < lRegolaGenericas.length; i++) {
				lRegolaGenericas[i].setRegolaComposite(null);
			}
			RegolaCampoNumerico[] lRegolaCampoNumericos = regolaComposite.regolaCampoNumerico.toArray();
			for(int i = 0; i < lRegolaCampoNumericos.length; i++) {
				lRegolaCampoNumericos[i].setRegolaComposite(null);
			}
			RegolaCampoResistenze[] lRegolaCampoResistenzes = regolaComposite.regolaCampoResistenze.toArray();
			for(int i = 0; i < lRegolaCampoResistenzes.length; i++) {
				lRegolaCampoResistenzes[i].setRegolaComposite(null);
			}
			RegolaCampoAttributo[] lRegolaCampoAttributos = regolaComposite.regolaCampoAttributo.toArray();
			for(int i = 0; i < lRegolaCampoAttributos.length; i++) {
				lRegolaCampoAttributos[i].setRegolaComposite(null);
			}
			RegolaCampoAbilita[] lRegolaCampoAbilitas = regolaComposite.regolaCampoAbilita.toArray();
			for(int i = 0; i < lRegolaCampoAbilitas.length; i++) {
				lRegolaCampoAbilitas[i].setRegolaComposite(null);
			}
			RegolaCampoOggetto[] lRegolaCampoOggettos = regolaComposite.regolaCampoOggetto.toArray();
			for(int i = 0; i < lRegolaCampoOggettos.length; i++) {
				lRegolaCampoOggettos[i].setRegolaComposite(null);
			}
			RegolaCampoTestuale[] lRegolaCampoTestuales = regolaComposite.regolaCampoTestuale.toArray();
			for(int i = 0; i < lRegolaCampoTestuales.length; i++) {
				lRegolaCampoTestuales[i].setRegolaComposite(null);
			}
			try {
				session.delete(regolaComposite);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(RegolaComposite regolaComposite) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().refresh(regolaComposite);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(RegolaComposite regolaComposite) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().evict(regolaComposite);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
