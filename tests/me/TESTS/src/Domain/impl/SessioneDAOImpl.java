/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import Domain.*;

public class SessioneDAOImpl implements Domain.dao.SessioneDAO {
	public Sessione loadSessioneByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = Domain.ManaGDRPersistentManager.instance().getSession();
			return loadSessioneByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione getSessioneByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = Domain.ManaGDRPersistentManager.instance().getSession();
			return getSessioneByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione loadSessioneByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = Domain.ManaGDRPersistentManager.instance().getSession();
			return loadSessioneByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione getSessioneByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = Domain.ManaGDRPersistentManager.instance().getSession();
			return getSessioneByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione loadSessioneByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Sessione) session.load(Domain.Sessione.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione getSessioneByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Sessione) session.get(Domain.Sessione.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione loadSessioneByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Sessione) session.load(Domain.Sessione.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione getSessioneByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Sessione) session.get(Domain.Sessione.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List querySessione(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = Domain.ManaGDRPersistentManager.instance().getSession();
			return querySessione(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List querySessione(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = Domain.ManaGDRPersistentManager.instance().getSession();
			return querySessione(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione[] listSessioneByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = Domain.ManaGDRPersistentManager.instance().getSession();
			return listSessioneByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione[] listSessioneByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = Domain.ManaGDRPersistentManager.instance().getSession();
			return listSessioneByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List querySessione(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Domain.Sessione as Sessione");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List querySessione(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Domain.Sessione as Sessione");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Sessione", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione[] listSessioneByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = querySessione(session, condition, orderBy);
			return (Sessione[]) list.toArray(new Sessione[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione[] listSessioneByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = querySessione(session, condition, orderBy, lockMode);
			return (Sessione[]) list.toArray(new Sessione[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione loadSessioneByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = Domain.ManaGDRPersistentManager.instance().getSession();
			return loadSessioneByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione loadSessioneByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = Domain.ManaGDRPersistentManager.instance().getSession();
			return loadSessioneByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione loadSessioneByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Sessione[] sessiones = listSessioneByQuery(session, condition, orderBy);
		if (sessiones != null && sessiones.length > 0)
			return sessiones[0];
		else
			return null;
	}
	
	public Sessione loadSessioneByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Sessione[] sessiones = listSessioneByQuery(session, condition, orderBy, lockMode);
		if (sessiones != null && sessiones.length > 0)
			return sessiones[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateSessioneByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = Domain.ManaGDRPersistentManager.instance().getSession();
			return iterateSessioneByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateSessioneByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = Domain.ManaGDRPersistentManager.instance().getSession();
			return iterateSessioneByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateSessioneByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Domain.Sessione as Sessione");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateSessioneByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Domain.Sessione as Sessione");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Sessione", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Sessione createSessione() {
		return new Domain.Sessione();
	}
	
	public boolean save(Domain.Sessione sessione) throws PersistentException {
		try {
			Domain.ManaGDRPersistentManager.instance().saveObject(sessione);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(Domain.Sessione sessione) throws PersistentException {
		try {
			Domain.ManaGDRPersistentManager.instance().deleteObject(sessione);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(Domain.Sessione sessione) throws PersistentException {
		try {
			Domain.ManaGDRPersistentManager.instance().getSession().refresh(sessione);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(Domain.Sessione sessione) throws PersistentException {
		try {
			Domain.ManaGDRPersistentManager.instance().getSession().evict(sessione);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
