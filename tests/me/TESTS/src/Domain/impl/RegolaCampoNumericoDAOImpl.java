/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import Domain.*;

public class RegolaCampoNumericoDAOImpl implements Domain.dao.RegolaCampoNumericoDAO {
	public RegolaCampoNumerico loadRegolaCampoNumericoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoNumericoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico getRegolaCampoNumericoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return getRegolaCampoNumericoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico loadRegolaCampoNumericoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoNumericoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico getRegolaCampoNumericoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return getRegolaCampoNumericoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico loadRegolaCampoNumericoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaCampoNumerico) session.load(RegolaCampoNumerico.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico getRegolaCampoNumericoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaCampoNumerico) session.get(RegolaCampoNumerico.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico loadRegolaCampoNumericoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaCampoNumerico) session.load(RegolaCampoNumerico.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico getRegolaCampoNumericoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaCampoNumerico) session.get(RegolaCampoNumerico.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoNumerico(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return queryRegolaCampoNumerico(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoNumerico(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return queryRegolaCampoNumerico(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico[] listRegolaCampoNumericoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return listRegolaCampoNumericoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico[] listRegolaCampoNumericoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return listRegolaCampoNumericoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoNumerico(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaCampoNumerico as RegolaCampoNumerico");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoNumerico(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaCampoNumerico as RegolaCampoNumerico");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaCampoNumerico", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico[] listRegolaCampoNumericoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRegolaCampoNumerico(session, condition, orderBy);
			return (RegolaCampoNumerico[]) list.toArray(new RegolaCampoNumerico[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico[] listRegolaCampoNumericoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRegolaCampoNumerico(session, condition, orderBy, lockMode);
			return (RegolaCampoNumerico[]) list.toArray(new RegolaCampoNumerico[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico loadRegolaCampoNumericoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoNumericoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico loadRegolaCampoNumericoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoNumericoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico loadRegolaCampoNumericoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		RegolaCampoNumerico[] regolaCampoNumericos = listRegolaCampoNumericoByQuery(session, condition, orderBy);
		if (regolaCampoNumericos != null && regolaCampoNumericos.length > 0)
			return regolaCampoNumericos[0];
		else
			return null;
	}
	
	public RegolaCampoNumerico loadRegolaCampoNumericoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		RegolaCampoNumerico[] regolaCampoNumericos = listRegolaCampoNumericoByQuery(session, condition, orderBy, lockMode);
		if (regolaCampoNumericos != null && regolaCampoNumericos.length > 0)
			return regolaCampoNumericos[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateRegolaCampoNumericoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCampoNumericoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoNumericoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCampoNumericoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoNumericoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaCampoNumerico as RegolaCampoNumerico");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoNumericoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaCampoNumerico as RegolaCampoNumerico");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaCampoNumerico", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoNumerico createRegolaCampoNumerico() {
		return new RegolaCampoNumerico();
	}
	
	public boolean save(RegolaCampoNumerico regolaCampoNumerico) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().saveObject(regolaCampoNumerico);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(RegolaCampoNumerico regolaCampoNumerico) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().deleteObject(regolaCampoNumerico);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(RegolaCampoNumerico regolaCampoNumerico)throws PersistentException {
		try {
			if(regolaCampoNumerico.getRegolaComposite() != null) {
				regolaCampoNumerico.getRegolaComposite().regolaCampoNumerico.remove(regolaCampoNumerico);
			}
			
			return delete(regolaCampoNumerico);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(RegolaCampoNumerico regolaCampoNumerico, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(regolaCampoNumerico.getRegolaComposite() != null) {
				regolaCampoNumerico.getRegolaComposite().regolaCampoNumerico.remove(regolaCampoNumerico);
			}
			
			try {
				session.delete(regolaCampoNumerico);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(RegolaCampoNumerico regolaCampoNumerico) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().refresh(regolaCampoNumerico);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(RegolaCampoNumerico regolaCampoNumerico) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().evict(regolaCampoNumerico);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
