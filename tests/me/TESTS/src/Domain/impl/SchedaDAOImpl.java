/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import Domain.*;

public class SchedaDAOImpl implements Domain.dao.SchedaDAO {
	public Scheda loadSchedaByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadSchedaByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda getSchedaByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return getSchedaByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda loadSchedaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadSchedaByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda getSchedaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return getSchedaByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda loadSchedaByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Scheda) session.load(Scheda.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda getSchedaByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Scheda) session.get(Scheda.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda loadSchedaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Scheda) session.load(Scheda.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda getSchedaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Scheda) session.get(Scheda.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryScheda(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return queryScheda(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryScheda(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return queryScheda(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda[] listSchedaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return listSchedaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda[] listSchedaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return listSchedaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryScheda(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Scheda as Scheda");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryScheda(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Scheda as Scheda");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Scheda", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda[] listSchedaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryScheda(session, condition, orderBy);
			return (Scheda[]) list.toArray(new Scheda[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda[] listSchedaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryScheda(session, condition, orderBy, lockMode);
			return (Scheda[]) list.toArray(new Scheda[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda loadSchedaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadSchedaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda loadSchedaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadSchedaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda loadSchedaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Scheda[] schedas = listSchedaByQuery(session, condition, orderBy);
		if (schedas != null && schedas.length > 0)
			return schedas[0];
		else
			return null;
	}
	
	public Scheda loadSchedaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Scheda[] schedas = listSchedaByQuery(session, condition, orderBy, lockMode);
		if (schedas != null && schedas.length > 0)
			return schedas[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateSchedaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return iterateSchedaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateSchedaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return iterateSchedaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateSchedaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Scheda as Scheda");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateSchedaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Scheda as Scheda");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Scheda", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public Scheda createScheda() {
		return new Scheda();
	}
	
	public boolean save(Scheda scheda) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().saveObject(scheda);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(Scheda scheda) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().deleteObject(scheda);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(Scheda scheda)throws PersistentException {
		try {
			CampoResistenze[] lCampoResistenzes = scheda.campoResistenze.toArray();
			for(int i = 0; i < lCampoResistenzes.length; i++) {
				lCampoResistenzes[i].setScheda(null);
			}
			CampoAbilita[] lCampoAbilitàs = scheda.campoAbilita.toArray();
			for(int i = 0; i < lCampoAbilitàs.length; i++) {
				lCampoAbilitàs[i].setScheda(null);
			}
			CampoTestuale[] lCampoTestuales = scheda.campoTestuale.toArray();
			for(int i = 0; i < lCampoTestuales.length; i++) {
				lCampoTestuales[i].setScheda(null);
			}
			CampoNumerico[] lCampoNumericos = scheda.campoNumerico.toArray();
			for(int i = 0; i < lCampoNumericos.length; i++) {
				lCampoNumericos[i].setScheda(null);
			}
			CampoOggetto[] lCampoOggettos = scheda.campoOggetto.toArray();
			for(int i = 0; i < lCampoOggettos.length; i++) {
				lCampoOggettos[i].setScheda(null);
			}
			CampoAttributo[] lCampoAttributos = scheda.campoAttributo.toArray();
			for(int i = 0; i < lCampoAttributos.length; i++) {
				lCampoAttributos[i].setScheda(null);
			}
			return delete(scheda);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(Scheda scheda, org.orm.PersistentSession session)throws PersistentException {
		try {
			CampoResistenze[] lCampoResistenzes = scheda.campoResistenze.toArray();
			for(int i = 0; i < lCampoResistenzes.length; i++) {
				lCampoResistenzes[i].setScheda(null);
			}
			CampoAbilita[] lCampoAbilitàs = scheda.campoAbilita.toArray();
			for(int i = 0; i < lCampoAbilitàs.length; i++) {
				lCampoAbilitàs[i].setScheda(null);
			}
			CampoTestuale[] lCampoTestuales = scheda.campoTestuale.toArray();
			for(int i = 0; i < lCampoTestuales.length; i++) {
				lCampoTestuales[i].setScheda(null);
			}
			CampoNumerico[] lCampoNumericos = scheda.campoNumerico.toArray();
			for(int i = 0; i < lCampoNumericos.length; i++) {
				lCampoNumericos[i].setScheda(null);
			}
			CampoOggetto[] lCampoOggettos = scheda.campoOggetto.toArray();
			for(int i = 0; i < lCampoOggettos.length; i++) {
				lCampoOggettos[i].setScheda(null);
			}
			CampoAttributo[] lCampoAttributos = scheda.campoAttributo.toArray();
			for(int i = 0; i < lCampoAttributos.length; i++) {
				lCampoAttributos[i].setScheda(null);
			}
			try {
				session.delete(scheda);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(Scheda scheda) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().refresh(scheda);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(Scheda scheda) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().evict(scheda);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
