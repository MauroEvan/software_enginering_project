/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import Domain.*;

public class RegolaCampoAttributoDAOImpl implements Domain.dao.RegolaCampoAttributoDAO {
	public RegolaCampoAttributo loadRegolaCampoAttributoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoAttributoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo getRegolaCampoAttributoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return getRegolaCampoAttributoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo loadRegolaCampoAttributoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoAttributoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo getRegolaCampoAttributoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return getRegolaCampoAttributoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo loadRegolaCampoAttributoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaCampoAttributo) session.load(RegolaCampoAttributo.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo getRegolaCampoAttributoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaCampoAttributo) session.get(RegolaCampoAttributo.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo loadRegolaCampoAttributoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaCampoAttributo) session.load(RegolaCampoAttributo.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo getRegolaCampoAttributoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaCampoAttributo) session.get(RegolaCampoAttributo.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoAttributo(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return queryRegolaCampoAttributo(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoAttributo(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return queryRegolaCampoAttributo(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo[] listRegolaCampoAttributoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return listRegolaCampoAttributoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo[] listRegolaCampoAttributoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return listRegolaCampoAttributoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoAttributo(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaCampoAttributo as RegolaCampoAttributo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoAttributo(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaCampoAttributo as RegolaCampoAttributo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaCampoAttributo", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo[] listRegolaCampoAttributoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRegolaCampoAttributo(session, condition, orderBy);
			return (RegolaCampoAttributo[]) list.toArray(new RegolaCampoAttributo[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo[] listRegolaCampoAttributoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRegolaCampoAttributo(session, condition, orderBy, lockMode);
			return (RegolaCampoAttributo[]) list.toArray(new RegolaCampoAttributo[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo loadRegolaCampoAttributoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoAttributoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo loadRegolaCampoAttributoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoAttributoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo loadRegolaCampoAttributoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		RegolaCampoAttributo[] regolaCampoAttributos = listRegolaCampoAttributoByQuery(session, condition, orderBy);
		if (regolaCampoAttributos != null && regolaCampoAttributos.length > 0)
			return regolaCampoAttributos[0];
		else
			return null;
	}
	
	public RegolaCampoAttributo loadRegolaCampoAttributoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		RegolaCampoAttributo[] regolaCampoAttributos = listRegolaCampoAttributoByQuery(session, condition, orderBy, lockMode);
		if (regolaCampoAttributos != null && regolaCampoAttributos.length > 0)
			return regolaCampoAttributos[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateRegolaCampoAttributoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCampoAttributoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoAttributoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCampoAttributoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoAttributoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaCampoAttributo as RegolaCampoAttributo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoAttributoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From RegolaCampoAttributo as RegolaCampoAttributo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaCampoAttributo", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoAttributo createRegolaCampoAttributo() {
		return new RegolaCampoAttributo();
	}
	
	public boolean save(RegolaCampoAttributo regolaCampoAttributo) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().saveObject(regolaCampoAttributo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(RegolaCampoAttributo regolaCampoAttributo) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().deleteObject(regolaCampoAttributo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(RegolaCampoAttributo regolaCampoAttributo)throws PersistentException {
		try {
			if(regolaCampoAttributo.getRegolaComposite() != null) {
				regolaCampoAttributo.getRegolaComposite().regolaCampoAttributo.remove(regolaCampoAttributo);
			}
			
			return delete(regolaCampoAttributo);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(RegolaCampoAttributo regolaCampoAttributo, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(regolaCampoAttributo.getRegolaComposite() != null) {
				regolaCampoAttributo.getRegolaComposite().regolaCampoAttributo.remove(regolaCampoAttributo);
			}
			
			try {
				session.delete(regolaCampoAttributo);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(RegolaCampoAttributo regolaCampoAttributo) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().refresh(regolaCampoAttributo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(RegolaCampoAttributo regolaCampoAttributo) throws PersistentException {
		try {
			ManaGDRPersistentManager.instance().getSession().evict(regolaCampoAttributo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
