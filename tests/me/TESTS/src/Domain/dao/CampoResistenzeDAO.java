/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import Domain.*;

public interface CampoResistenzeDAO {
	public CampoResistenze loadCampoResistenzeByORMID(int ID) throws PersistentException;
	public CampoResistenze getCampoResistenzeByORMID(int ID) throws PersistentException;
	public CampoResistenze loadCampoResistenzeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoResistenze getCampoResistenzeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoResistenze loadCampoResistenzeByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoResistenze getCampoResistenzeByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoResistenze loadCampoResistenzeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoResistenze getCampoResistenzeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoResistenze[] listCampoResistenzeByQuery(String condition, String orderBy) throws PersistentException;
	public CampoResistenze[] listCampoResistenzeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoResistenze(String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoResistenze(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoResistenzeByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoResistenzeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoResistenze[] listCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoResistenze[] listCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoResistenze(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoResistenze(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoResistenze loadCampoResistenzeByQuery(String condition, String orderBy) throws PersistentException;
	public CampoResistenze loadCampoResistenzeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoResistenze loadCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoResistenze loadCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoResistenze createCampoResistenze();
	public boolean save(CampoResistenze campoResistenze) throws PersistentException;
	public boolean delete(CampoResistenze campoResistenze) throws PersistentException;
	public boolean deleteAndDissociate(CampoResistenze campoResistenze) throws PersistentException;
	public boolean deleteAndDissociate(CampoResistenze campoResistenze, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(CampoResistenze campoResistenze) throws PersistentException;
	public boolean evict(CampoResistenze campoResistenze) throws PersistentException;
}
