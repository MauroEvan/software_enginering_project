/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import Domain.*;

public interface EventoDAO {
	public Evento loadEventoByORMID(int ID) throws PersistentException;
	public Evento getEventoByORMID(int ID) throws PersistentException;
	public Evento loadEventoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Evento getEventoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Evento loadEventoByORMID(PersistentSession session, int ID) throws PersistentException;
	public Evento getEventoByORMID(PersistentSession session, int ID) throws PersistentException;
	public Evento loadEventoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Evento getEventoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Evento[] listEventoByQuery(String condition, String orderBy) throws PersistentException;
	public Evento[] listEventoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryEvento(String condition, String orderBy) throws PersistentException;
	public java.util.List queryEvento(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateEventoByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateEventoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Evento[] listEventoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public Evento[] listEventoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryEvento(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryEvento(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateEventoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateEventoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Evento loadEventoByQuery(String condition, String orderBy) throws PersistentException;
	public Evento loadEventoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Evento loadEventoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public Evento loadEventoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Evento createEvento();
	public boolean save(Evento evento) throws PersistentException;
	public boolean delete(Evento evento) throws PersistentException;
	public boolean refresh(Evento evento) throws PersistentException;
	public boolean evict(Evento evento) throws PersistentException;
}
