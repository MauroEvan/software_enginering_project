/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import Domain.*;

public interface RegolaCampoAbilitaDAO {
	public RegolaCampoAbilita loadRegolaCampoAbilitaByORMID(int ID) throws PersistentException;
	public RegolaCampoAbilita getRegolaCampoAbilitaByORMID(int ID) throws PersistentException;
	public RegolaCampoAbilita loadRegolaCampoAbilitaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilita getRegolaCampoAbilitaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilita loadRegolaCampoAbilitaByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoAbilita getRegolaCampoAbilitaByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoAbilita loadRegolaCampoAbilitaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilita getRegolaCampoAbilitaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilita[] listRegolaCampoAbilitaByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoAbilita[] listRegolaCampoAbilitaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoAbilita(String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoAbilita(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoAbilitaByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoAbilitaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilita[] listRegolaCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoAbilita[] listRegolaCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoAbilita(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoAbilita(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilita loadRegolaCampoAbilitaByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoAbilita loadRegolaCampoAbilitaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilita loadRegolaCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoAbilita loadRegolaCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAbilita createRegolaCampoAbilita();
	public boolean save(RegolaCampoAbilita regolaCampoAbilita) throws PersistentException;
	public boolean delete(RegolaCampoAbilita regolaCampoAbilita) throws PersistentException;
	public boolean deleteAndDissociate(RegolaCampoAbilita regolaCampoAbilita) throws PersistentException;
	public boolean deleteAndDissociate(RegolaCampoAbilita regolaCampoAbilita, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(RegolaCampoAbilita regolaCampoAbilita) throws PersistentException;
	public boolean evict(RegolaCampoAbilita regolaCampoAbilita) throws PersistentException;
}
