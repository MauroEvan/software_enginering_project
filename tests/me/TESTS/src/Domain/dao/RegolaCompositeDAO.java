/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import Domain.*;

public interface RegolaCompositeDAO {
	public RegolaComposite loadRegolaCompositeByORMID(int ID) throws PersistentException;
	public RegolaComposite getRegolaCompositeByORMID(int ID) throws PersistentException;
	public RegolaComposite loadRegolaCompositeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaComposite getRegolaCompositeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaComposite loadRegolaCompositeByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaComposite getRegolaCompositeByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaComposite loadRegolaCompositeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaComposite getRegolaCompositeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaComposite[] listRegolaCompositeByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaComposite[] listRegolaCompositeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaComposite(String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaComposite(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCompositeByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCompositeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaComposite[] listRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaComposite[] listRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaComposite(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaComposite(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaComposite loadRegolaCompositeByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaComposite loadRegolaCompositeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaComposite loadRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaComposite loadRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaComposite createRegolaComposite();
	public boolean save(RegolaComposite regolaComposite) throws PersistentException;
	public boolean delete(RegolaComposite regolaComposite) throws PersistentException;
	public boolean deleteAndDissociate(RegolaComposite regolaComposite) throws PersistentException;
	public boolean deleteAndDissociate(RegolaComposite regolaComposite, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(RegolaComposite regolaComposite) throws PersistentException;
	public boolean evict(RegolaComposite regolaComposite) throws PersistentException;
}
