/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import Domain.*;

public interface CampoNumericoDAO {
	public CampoNumerico loadCampoNumericoByORMID(int ID) throws PersistentException;
	public CampoNumerico getCampoNumericoByORMID(int ID) throws PersistentException;
	public CampoNumerico loadCampoNumericoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoNumerico getCampoNumericoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoNumerico loadCampoNumericoByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoNumerico getCampoNumericoByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoNumerico loadCampoNumericoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoNumerico getCampoNumericoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoNumerico[] listCampoNumericoByQuery(String condition, String orderBy) throws PersistentException;
	public CampoNumerico[] listCampoNumericoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoNumerico(String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoNumerico(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoNumericoByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoNumericoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoNumerico[] listCampoNumericoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoNumerico[] listCampoNumericoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoNumerico(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoNumerico(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoNumericoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoNumericoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoNumerico loadCampoNumericoByQuery(String condition, String orderBy) throws PersistentException;
	public CampoNumerico loadCampoNumericoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoNumerico loadCampoNumericoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoNumerico loadCampoNumericoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoNumerico createCampoNumerico();
	public boolean save(CampoNumerico campoNumerico) throws PersistentException;
	public boolean delete(CampoNumerico campoNumerico) throws PersistentException;
	public boolean deleteAndDissociate(CampoNumerico campoNumerico) throws PersistentException;
	public boolean deleteAndDissociate(CampoNumerico campoNumerico, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(CampoNumerico campoNumerico) throws PersistentException;
	public boolean evict(CampoNumerico campoNumerico) throws PersistentException;
}
