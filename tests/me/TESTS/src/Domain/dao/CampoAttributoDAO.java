/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import Domain.*;

public interface CampoAttributoDAO {
	public CampoAttributo loadCampoAttributoByORMID(int ID) throws PersistentException;
	public CampoAttributo getCampoAttributoByORMID(int ID) throws PersistentException;
	public CampoAttributo loadCampoAttributoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAttributo getCampoAttributoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAttributo loadCampoAttributoByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoAttributo getCampoAttributoByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoAttributo loadCampoAttributoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAttributo getCampoAttributoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAttributo[] listCampoAttributoByQuery(String condition, String orderBy) throws PersistentException;
	public CampoAttributo[] listCampoAttributoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoAttributo(String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoAttributo(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoAttributoByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoAttributoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAttributo[] listCampoAttributoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoAttributo[] listCampoAttributoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoAttributo(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoAttributo(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoAttributoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoAttributoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAttributo loadCampoAttributoByQuery(String condition, String orderBy) throws PersistentException;
	public CampoAttributo loadCampoAttributoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAttributo loadCampoAttributoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoAttributo loadCampoAttributoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAttributo createCampoAttributo();
	public boolean save(CampoAttributo campoAttributo) throws PersistentException;
	public boolean delete(CampoAttributo campoAttributo) throws PersistentException;
	public boolean deleteAndDissociate(CampoAttributo campoAttributo) throws PersistentException;
	public boolean deleteAndDissociate(CampoAttributo campoAttributo, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(CampoAttributo campoAttributo) throws PersistentException;
	public boolean evict(CampoAttributo campoAttributo) throws PersistentException;
}
