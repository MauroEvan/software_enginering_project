/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import Domain.*;

public interface SchedaDAO {
	public Scheda loadSchedaByORMID(int ID) throws PersistentException;
	public Scheda getSchedaByORMID(int ID) throws PersistentException;
	public Scheda loadSchedaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Scheda getSchedaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Scheda loadSchedaByORMID(PersistentSession session, int ID) throws PersistentException;
	public Scheda getSchedaByORMID(PersistentSession session, int ID) throws PersistentException;
	public Scheda loadSchedaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Scheda getSchedaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public Scheda[] listSchedaByQuery(String condition, String orderBy) throws PersistentException;
	public Scheda[] listSchedaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryScheda(String condition, String orderBy) throws PersistentException;
	public java.util.List queryScheda(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateSchedaByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateSchedaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Scheda[] listSchedaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public Scheda[] listSchedaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryScheda(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryScheda(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateSchedaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateSchedaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Scheda loadSchedaByQuery(String condition, String orderBy) throws PersistentException;
	public Scheda loadSchedaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Scheda loadSchedaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public Scheda loadSchedaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public Scheda createScheda();
	public boolean save(Scheda scheda) throws PersistentException;
	public boolean delete(Scheda scheda) throws PersistentException;
	public boolean deleteAndDissociate(Scheda scheda) throws PersistentException;
	public boolean deleteAndDissociate(Scheda scheda, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(Scheda scheda) throws PersistentException;
	public boolean evict(Scheda scheda) throws PersistentException;
}
