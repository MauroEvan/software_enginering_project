/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import Domain.*;

public interface RegolaCampoOggettoDAO {
	public RegolaCampoOggetto loadRegolaCampoOggettoByORMID(int ID) throws PersistentException;
	public RegolaCampoOggetto getRegolaCampoOggettoByORMID(int ID) throws PersistentException;
	public RegolaCampoOggetto loadRegolaCampoOggettoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoOggetto getRegolaCampoOggettoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoOggetto loadRegolaCampoOggettoByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoOggetto getRegolaCampoOggettoByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoOggetto loadRegolaCampoOggettoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoOggetto getRegolaCampoOggettoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoOggetto[] listRegolaCampoOggettoByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoOggetto[] listRegolaCampoOggettoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoOggetto(String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoOggetto(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoOggettoByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoOggettoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoOggetto[] listRegolaCampoOggettoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoOggetto[] listRegolaCampoOggettoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoOggetto(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoOggetto(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoOggettoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoOggettoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoOggetto loadRegolaCampoOggettoByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoOggetto loadRegolaCampoOggettoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoOggetto loadRegolaCampoOggettoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoOggetto loadRegolaCampoOggettoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoOggetto createRegolaCampoOggetto();
	public boolean save(RegolaCampoOggetto regolaCampoOggetto) throws PersistentException;
	public boolean delete(RegolaCampoOggetto regolaCampoOggetto) throws PersistentException;
	public boolean deleteAndDissociate(RegolaCampoOggetto regolaCampoOggetto) throws PersistentException;
	public boolean deleteAndDissociate(RegolaCampoOggetto regolaCampoOggetto, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(RegolaCampoOggetto regolaCampoOggetto) throws PersistentException;
	public boolean evict(RegolaCampoOggetto regolaCampoOggetto) throws PersistentException;
}
