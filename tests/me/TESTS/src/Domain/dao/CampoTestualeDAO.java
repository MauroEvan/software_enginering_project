/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import Domain.*;

public interface CampoTestualeDAO {
	public CampoTestuale loadCampoTestualeByORMID(int ID) throws PersistentException;
	public CampoTestuale getCampoTestualeByORMID(int ID) throws PersistentException;
	public CampoTestuale loadCampoTestualeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoTestuale getCampoTestualeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoTestuale loadCampoTestualeByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoTestuale getCampoTestualeByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoTestuale loadCampoTestualeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoTestuale getCampoTestualeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoTestuale[] listCampoTestualeByQuery(String condition, String orderBy) throws PersistentException;
	public CampoTestuale[] listCampoTestualeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoTestuale(String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoTestuale(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoTestualeByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoTestualeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoTestuale[] listCampoTestualeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoTestuale[] listCampoTestualeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoTestuale(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoTestuale(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoTestualeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoTestualeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoTestuale loadCampoTestualeByQuery(String condition, String orderBy) throws PersistentException;
	public CampoTestuale loadCampoTestualeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoTestuale loadCampoTestualeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoTestuale loadCampoTestualeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoTestuale createCampoTestuale();
	public boolean save(CampoTestuale campoTestuale) throws PersistentException;
	public boolean delete(CampoTestuale campoTestuale) throws PersistentException;
	public boolean deleteAndDissociate(CampoTestuale campoTestuale) throws PersistentException;
	public boolean deleteAndDissociate(CampoTestuale campoTestuale, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(CampoTestuale campoTestuale) throws PersistentException;
	public boolean evict(CampoTestuale campoTestuale) throws PersistentException;
}
