package Domain;
import java.util.ArrayList;

public class Scaffale
{
	private static Scaffale handle;
	
	// Caso d'uso di avviamento
	private ArrayList<Sessione> database = new ArrayList<Sessione>();
	// -----------------------------------------
	
	
	public Scaffale()
	{
		/* Caso d'uso di avviamento */
		Sessione s;
		for( int i=0; i<Lobby.getIstance().getGiocatoriConnessi().size(); i++ )
		{
			s = new Sessione( Lobby.getIstance().getGiocatoriConnessi().get(i) );
			s.setIdSessione( i );
            s.setDescr("test  " + s.toString());
			database.add( s );
		}
	}
	
	public static Scaffale getIstance()
	{
		// Lazy Initialization
		if( handle == null )
			handle = new Scaffale();
		return handle;
	}
	
	public ElencoSessioni RecuperaSessioni(Giocatore gmSessione)
	{
		ElencoSessioni e = new ElencoSessioni();
		
		// Caso d'uso di avviamento
		for( int i=0; i<database.size(); i++ )
		{
			e.add( database.get(i) );
		}
			
		return e;
	}


	public Sessione SelezionaSessione( ID idSessione ) throws NullPointerException
	{
		Sessione s = getSessioneDaId( idSessione );
		s.ottieniITuoiDati();
		return s;
	}
	
	
	public Sessione getSessioneDaId( ID idSessione )
	{
		for( int i=0; i<database.size(); i++ )
		{
			if( database.get( i ).getIdSessione() == idSessione )
				return database.get( i );
		}

		throw new NullPointerException();
	}
}
