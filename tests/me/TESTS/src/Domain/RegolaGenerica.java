/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

public class RegolaGenerica implements Regola {
	public RegolaGenerica() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_REGOLAGENERICA_REGOLACOMPOSITE) {
			this.regolaComposite = (RegolaComposite) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;	
	private String nomeRegola;	
	private String testoRegola;
	
	private RegolaComposite regolaComposite;
	
	private void setID(int value) {
		this.ID = value;
	}
        
	public boolean Applica( Object o )
	{
		// TODO: mostriamo solo qualcosa all'utente?
		return true;
	}	

	public String getNomeRegola() {	return nomeRegola; }
	public String getTestoRegola() { return testoRegola; }
	public void setNomeRegola(String nomeRegola) { this.nomeRegola = nomeRegola; }
	public void setTestoRegola(String testoRegola) { this.testoRegola = testoRegola; }
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setRegolaComposite(RegolaComposite value) {
		if (regolaComposite != null) {
			regolaComposite.regolaGenerica.remove(this);
		}
		if (value != null) {
			value.regolaGenerica.add(this);
		}
	}
	
	public RegolaComposite getRegolaComposite() {
		return regolaComposite;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_RegolaComposite(RegolaComposite value) {
		this.regolaComposite = value;
	}
	
	private RegolaComposite getORM_RegolaComposite() {
		return regolaComposite;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
