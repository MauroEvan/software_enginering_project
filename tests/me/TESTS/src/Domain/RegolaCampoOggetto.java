/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

public class RegolaCampoOggetto extends RegolaCampo {
	public RegolaCampoOggetto() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_REGOLACAMPOOGGETTO_REGOLACOMPOSITE) {
			this.regolaComposite = (RegolaComposite) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;	
	private int maxQuantita;	
	private String nomeCampo;
	
	private RegolaComposite regolaComposite;
        
	public RegolaCampoOggetto( String nomeCampo, int quantita )
	{
		maxQuantita = quantita;
		this.nomeCampo = nomeCampo;
	}	
	
        public boolean ApplicaRegolaSuScheda(Scheda s) 
        {
            if( s.getChiaviCampi().contains(nomeCampo) )
                    {
                            Campo c = s.getCampo(nomeCampo);

                            if( c instanceof CampoOggetto ) // Se è della classe giusta..
                            {
                                    CampoOggetto co = (CampoOggetto)c;
                                    return (co.getQuantita() < maxQuantita);
                            }
                    }
                    return false;
        }

	public int getMaxQuantita() {
		return maxQuantita;
	}

	public void setMaxQuantita(int maxQuantita) {
		this.maxQuantita = maxQuantita;
	}

	public String getNomeCampo() {
		return nomeCampo;
	}

	public void setNomeCampo(String nomeCampo) {
		this.nomeCampo = nomeCampo;
	}
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setMassimaQuantità(int value) {
		this.maxQuantita = value;
	}
	
	public int getMassimaQuantità() {
		return maxQuantita;
	}
	
	public void setRegolaComposite(RegolaComposite value) {
		if (regolaComposite != null) {
			regolaComposite.regolaCampoOggetto.remove(this);
		}
		if (value != null) {
			value.regolaCampoOggetto.add(this);
		}
	}
	
	public RegolaComposite getRegolaComposite() {
		return regolaComposite;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_RegolaComposite(RegolaComposite value) {
		this.regolaComposite = value;
	}
	
	private RegolaComposite getORM_RegolaComposite() {
		return regolaComposite;
	}

	public String toString() {
		return String.valueOf(getID());
	}
	
}
