package Domain;

import java.util.HashSet;
import java.util.ArrayList;

public class Tavolo
{
	private ID idTavolo;
	private Sessione sessione;
	private HashSet<Giocatore> giocatoriSeduti;


	public Tavolo()
	{
		giocatoriSeduti = new HashSet<Giocatore>();
	}


	public ID getIdTavolo()
	{
		return idTavolo;
	}


	public void setIdTavolo(ID idTavolo)
	{
		this.idTavolo = idTavolo;
	}


	public Sessione getSessione()
	{
		return sessione;
	}


	public void setSessione(Sessione sessione)
	{
		this.sessione = sessione;
	}       


	public void AssociaScheda(Giocatore giocatore, Scheda scheda)
	{
		scheda.setGiocatore( giocatore );
		giocatore.aggiungiScheda( scheda );
                
                System.out.println(giocatore.toString() + " associato con " + scheda.toString());
	}


	public void mettiSessioneSulTavolo(Sessione nuovaSessione)
	{
		this.sessione = nuovaSessione;

		// Assicuriamoci che non ci siano altri giocatori seduti
		this.giocatoriSeduti.clear();
		// Aggiungiamo il GM ai giocatori seduti della sessione messa sul tavolo
		this.giocatoriSeduti.add( nuovaSessione.getGm() );
	}


	public void AttivaSessione()
	{
		this.sessione.associaSchedeLibere();
		this.sessione.setStato( StatoSessione.sessioneAttiva );
		System.out.println( "SESSIONE ATTIVATA\n" + "Tavolo:" + this.toString() + "\nSessione:" + this.sessione.toString() );
	}


	public void AggiungiGiocatore( Giocatore g )
	{
		// TODO: controllare se il giocatore non è già seduto
		giocatoriSeduti.add( g );
	}


	public HashSet<Giocatore> getGiocatoriSeduti()
	{
		return giocatoriSeduti;
	}
	
        
        public ArrayList<OpzioneCreazioneScheda> AvviaCreazioneScheda()
        {
            Giocatore g = DatiLogin.getInstance().getGiocatore();
            return FacadeConfigurazioni.getIstance().getOpzioniCreazioneScheda( g, sessione );
        }
	
	public Scheda CreaNuovaScheda( OpzioneCreazioneScheda opz )
	{
		Scheda s = sessione.AggiungiSchedaVuota();
		opz.RiempiScheda( s );
		return s;
	}
	
	public boolean ValidaScheda( Scheda s )
	{
		ArrayList<Regola> blacklist_totale = new ArrayList<Regola>();
		blacklist_totale = sessione.ApplicaRegoleAScheda( s );
		
// TODO : mostrare all'utente le regole fallenti
		
		if( blacklist_totale.size() != 0 )
			return false;
		return true;
	}
}