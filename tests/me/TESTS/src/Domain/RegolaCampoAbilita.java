/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

public class RegolaCampoAbilita extends RegolaCampo {
	public RegolaCampoAbilita() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_REGOLACAMPOABILITA_REGOLACOMPOSITE) {
			this.regolaComposite = (RegolaComposite) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;	
	private String nomeCampo; //chiave (String) nell'Hashmap dei campi in Scheda	
	private int sogliaValore = 0;
	private int sogliaModificatore = 0;
	
	private RegolaComposite regolaComposite;        
	
	public RegolaCampoAbilita( String nomeCampo, int sogliaValore, int sogliaModificatore )
	{
		this.sogliaValore = sogliaValore;
		this.nomeCampo = nomeCampo;
		this.sogliaModificatore = sogliaModificatore;
	}
	

	
	private boolean ControllaValore( CampoAbilita campo )
	{
		int val = campo.getValoreGrado();
		
		// (se è uguale a 0 lo ignoriamo
		if( val >= sogliaValore || val == 0 )
			return true;
		return false;
	}
	
	
	private boolean ControllaModificatore( CampoAbilita campo )
	{
		int mod = campo.getModificatore();
		
		// (se è uguale a 0 lo ignoriamo
		if( mod >= sogliaModificatore || mod == 0 )
			return true;
		return false;
	}	
	
        public boolean ApplicaRegolaSuScheda(Scheda s) 
        {
                    //Verifichiamo la presenza del campo
                    if( s.getChiaviCampi().contains(nomeCampo) )
                    {
                            Campo c = s.getCampo(nomeCampo);

                            if( c instanceof CampoAbilita ) // Se è della classe giusta..
                            {
                                    CampoAbilita ca = (CampoAbilita)c;

                                    return ControllaModificatore(ca) &&
                                               ControllaValore(ca);
                            }
                    }
                    return false;
        }
	
	public int getSogliaValore() { return sogliaValore; }
	public void setSogliaValore(int sogliaValore) {this.sogliaValore = sogliaValore;}
	public int getSogliaModificatore() {return sogliaModificatore;}
	public void setSogliaModificatore(int sogliaModificatore) {this.sogliaModificatore = sogliaModificatore;}
	public String getNomeCampo() {return nomeCampo;}
	public void setNomeCampo(String nomeCampo) {this.nomeCampo = nomeCampo;}
        
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
		
	public void setRegolaComposite(RegolaComposite value) {
		if (regolaComposite != null) {
			regolaComposite.regolaCampoAbilita.remove(this);
		}
		if (value != null) {
			value.regolaCampoAbilita.add(this);
		}
	}
	
	public RegolaComposite getRegolaComposite() {
		return regolaComposite;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_RegolaComposite(RegolaComposite value) {
		this.regolaComposite = value;
	}
	
	private RegolaComposite getORM_RegolaComposite() {
		return regolaComposite;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
