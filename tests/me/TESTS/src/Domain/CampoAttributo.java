/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

import java.util.LinkedList;
import java.util.Random;

public class CampoAttributo implements Campo {
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_CAMPOATTRIBUTO_SCHEDA) {
			this.scheda = (Scheda) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;	
	private String nomeAttributo;	
	private int valore;	
	private int modificatore;	
	private Scheda scheda;
        
        public CampoAttributo()
        {

        }

        public CampoAttributo(String nomeAttributo, int valore)
        {
            this.nomeAttributo = nomeAttributo;
            this.valore = valore;        
            if( (valore - 10) > 0 )
                this.modificatore = (valore - 10)/2;
            else 
                this.modificatore = ( (valore - 10)/2 ) - 1;
        }

        public CampoAttributo(String nomeAttributo)
        {
            this.nomeAttributo = nomeAttributo;
            this.valore = Randomizer();

            if( ((Integer) this.getValore() - 10) > 0 )
                this.modificatore = ((Integer) this.getValore() - 10)/2;
            else 
                this.modificatore = ( ((Integer) this.getValore() - 10)/2 ) - 1;        
        }
        
        public String getNome() { return nomeAttributo; }
        public void setNome(String nomeAttributo) { this.nomeAttributo = nomeAttributo; }
        public Object getValore() { return valore; }
        public void setValore(int valore) { this.valore = valore; }
        public int getModificatore() { return modificatore; }
        public void setModificatore(int modificatore) { this.modificatore = modificatore; }

        public String toString() { return this.getNome() + " - " + this.getValore() + " - " + this.getModificatore(); }
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setNome(int value) {
		setNome(new Integer(value));
	}
	
	public void setScheda(Scheda value) {
		if (scheda != null) {
			scheda.campoAttributo.remove(this);
		}
		if (value != null) {
			value.campoAttributo.add(this);
		}
	}
	
	public Scheda getScheda() {
		return scheda;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Scheda(Scheda value) {
		this.scheda = value;
	}
	
	private Scheda getORM_Scheda() {
		return scheda;
	}
	
        private int Randomizer()
        {
            LinkedList<Integer> tiri= new LinkedList<Integer>();

            for(int i=0; i<4; i++)
            {
                int min = 1;
                int max = 6;
                Random rand = new Random();
                int tiroRandom = rand.nextInt((max - min) + 1) + min;
                tiri.add(tiroRandom);
            }
            int totale = 0;
            //tiri.sort( null ); //TODO - fare un check
            for(int i=0; i<3; i++)
            {
                totale = totale + tiri.pollLast();
            }
            return totale;
        }
}
