/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

public class Mappa {
	public Mappa() {
            
	}
	
	private int ID;
        
        private ID idMappa;

	public ID getIdMappa()
	{
		return idMappa;
	}

	public void setIdMappa(ID idMappa)
	{
		this.idMappa = idMappa;
	}
	
	public void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
