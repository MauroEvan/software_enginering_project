/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

import Domain.impl.*;
import Domain.dao.*;

public class DAOFactoryImpl extends DAOFactory {
	private GiocatoreDAO _giocatoreDAO = new GiocatoreDAOImpl();
	public GiocatoreDAO getGiocatoreDAO() {
		return _giocatoreDAO;
	}
	
	private SessioneDAO _sessioneDAO = new SessioneDAOImpl();
	public SessioneDAO getSessioneDAO() {
		return _sessioneDAO;
	}
	
	private MappaDAO _mappaDAO = new MappaDAOImpl();
	public MappaDAO getMappaDAO() {
		return _mappaDAO;
	}
	
	private EventoDAO _eventoDAO = new EventoDAOImpl();
	public EventoDAO getEventoDAO() {
		return _eventoDAO;
	}
	
	private ModuloDiRegolamentoDAO _moduloDiRegolamentoDAO = new ModuloDiRegolamentoDAOImpl();
	public ModuloDiRegolamentoDAO getModuloDiRegolamentoDAO() {
		return _moduloDiRegolamentoDAO;
	}
	
	private SchedaDAO _schedaDAO = new SchedaDAOImpl();
	public SchedaDAO getSchedaDAO() {
		return _schedaDAO;
	}
	
	private CampoNumericoDAO _campoNumericoDAO = new CampoNumericoDAOImpl();
	public CampoNumericoDAO getCampoNumericoDAO() {
		return _campoNumericoDAO;
	}
	
	private CampoTestualeDAO _campoTestualeDAO = new CampoTestualeDAOImpl();
	public CampoTestualeDAO getCampoTestualeDAO() {
		return _campoTestualeDAO;
	}
	
	private CampoAttributoDAO _campoAttributoDAO = new CampoAttributoDAOImpl();
	public CampoAttributoDAO getCampoAttributoDAO() {
		return _campoAttributoDAO;
	}
	
	private CampoAbilitaDAO _campoAbilitàDAO = new CampoAbilitaDAOImpl();
	public CampoAbilitaDAO getCampoAbilitàDAO() {
		return _campoAbilitàDAO;
	}
	
	private CampoOggettoDAO _campoOggettoDAO = new CampoOggettoDAOImpl();
	public CampoOggettoDAO getCampoOggettoDAO() {
		return _campoOggettoDAO;
	}
	
	private CampoResistenzeDAO _campoResistenzeDAO = new CampoResistenzeDAOImpl();
	public CampoResistenzeDAO getCampoResistenzeDAO() {
		return _campoResistenzeDAO;
	}
	
	private RegolaCompositeDAO _regolaCompositeDAO = new RegolaCompositeDAOImpl();
	public RegolaCompositeDAO getRegolaCompositeDAO() {
		return _regolaCompositeDAO;
	}
	
	private RegolaGenericaDAO _regolaGenericaDAO = new RegolaGenericaDAOImpl();
	public RegolaGenericaDAO getRegolaGenericaDAO() {
		return _regolaGenericaDAO;
	}
	
	private RegolaCampoTestualeDAO _regolaCampoTestualeDAO = new RegolaCampoTestualeDAOImpl();
	public RegolaCampoTestualeDAO getRegolaCampoTestualeDAO() {
		return _regolaCampoTestualeDAO;
	}
	
	private RegolaCampoAttributoDAO _regolaCampoAttributoDAO = new RegolaCampoAttributoDAOImpl();
	public RegolaCampoAttributoDAO getRegolaCampoAttributoDAO() {
		return _regolaCampoAttributoDAO;
	}
	
	private RegolaCampoOggettoDAO _regolaCampoOggettoDAO = new RegolaCampoOggettoDAOImpl();
	public RegolaCampoOggettoDAO getRegolaCampoOggettoDAO() {
		return _regolaCampoOggettoDAO;
	}
	
	private RegolaCampoAbilitaDAO _regolaCampoAbilitaDAO = new RegolaCampoAbilitaDAOImpl();
	public RegolaCampoAbilitaDAO getRegolaCampoAbilitaDAO() {
		return _regolaCampoAbilitaDAO;
	}
	
	private RegolaCampoNumericoDAO _regolaCampoNumericoDAO = new RegolaCampoNumericoDAOImpl();
	public RegolaCampoNumericoDAO getRegolaCampoNumericoDAO() {
		return _regolaCampoNumericoDAO;
	}
	
	private RegolaCampoResistenzeDAO _regolaCampoResistenzeDAO = new RegolaCampoResistenzeDAOImpl();
	public RegolaCampoResistenzeDAO getRegolaCampoResistenzeDAO() {
		return _regolaCampoResistenzeDAO;
	}
	
}

