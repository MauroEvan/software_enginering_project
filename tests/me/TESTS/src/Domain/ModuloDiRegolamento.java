/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package Domain;

import java.util.ArrayList;

public class ModuloDiRegolamento {
	public ModuloDiRegolamento() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_MODULODIREGOLAMENTO_REGOLACOMPOSITE) {
			this.regolamento = (RegolaComposite) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;	
	private ID idModulo;
	private RegolaComposite regolamento = new RegolaComposite();
        
	public ArrayList<Regola> ApplicaSuScheda( Scheda s )
	{
		ArrayList<Regola> blacklist = new ArrayList<Regola>();
        
		RegolaIterator it = new RegolaIterator(regolamento.iterator());
		while( it.hasNext() )
		{
			RegolaComponent a = it.next();

			if( a instanceof Regola )
			{
				if( a.Applica(s) == false )
					blacklist.add( (Regola) a );
			}
		}
                
		return blacklist;
	}
	
	
	public ID getIdModulo() { return idModulo; }
	public void setIdModulo(ID idModulo) { this.idModulo = idModulo; }
	public RegolaComposite getRegolamento() { return regolamento; }
	public void setRegolamento(RegolaComposite regolamento) { this.regolamento = regolamento; }
        
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setRegolaComposite(RegolaComposite value) {
		if (this.regolamento != value) {
			RegolaComposite lregolaComposite = this.regolamento;
			this.regolamento = value;
			if (value != null) {
				regolamento.setModulo(this);
			}
			else {
				lregolaComposite.setModulo(null);
			}
		}
	}
	
	public RegolaComposite getRegolaComposite() {
		return regolamento;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
