package engine;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import compute.Compute;
import compute.Task;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;

public class ComputeEngine implements Compute {

    public ComputeEngine() {
        super();
    }

    public <T> T executeTask(Task<T> t) {
        return t.execute();
    }

    public static void main(String[] args) {
        System.setProperty("java.security.policy", "D:/Università/Ingegneria del Software/Software_Engineering_Project/software_engeneering_project/tests/me/Oracle_Test_RMI/Oracle_Test_RMI/src/client.policy");
        System.setProperty("java.rmi.server.codebase", "D:/Università/Ingegneria del Software/Software_Engineering_Project/software_engeneering_project/tests/me/Oracle_Test_RMI/Oracle_Test_RMI/src/");
        System.setProperty("java.rmi.server.hostname", "127.0.0.1");
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            String name = "Compute";
            Compute engine = new ComputeEngine();
            Compute stub =
                (Compute) UnicastRemoteObject.exportObject(engine, 0);
            LocateRegistry.createRegistry(1099);
            Registry registry = LocateRegistry.getRegistry();            
            registry.rebind(name, stub);
            System.out.println("ComputeEngine bound");
        } catch (Exception e) {
            System.err.println("ComputeEngine exception:");
            e.printStackTrace();
        }
    }
}