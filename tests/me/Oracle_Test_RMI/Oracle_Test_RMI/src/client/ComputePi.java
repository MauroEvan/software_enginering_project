package client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.math.BigDecimal;
import compute.Compute;

public class ComputePi {
    
    public static void main(String args[]) {
        System.setProperty("java.security.policy", "D:/Università/Ingegneria del Software/Software_Engineering_Project/software_engeneering_project/tests/me/Oracle_Test_RMI/Oracle_Test_RMI/src/client.policy");
        System.setProperty("java.rmi.server.codebase", "D:/Università/Ingegneria del Software/Software_Engineering_Project/software_engeneering_project/tests/me/Oracle_Test_RMI/Oracle_Test_RMI/src/");
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            String name = "ComputeC";
            LocateRegistry.createRegistry(1099);
            Registry registry = LocateRegistry.getRegistry(args[0]);
            Compute comp = (Compute) registry.lookup(name);
            Pi task = new Pi(Integer.parseInt(args[1]));
            BigDecimal pi = comp.executeTask(task);
            System.out.println(pi);
        } catch (Exception e) {
            System.err.println("ComputePi exception:");
            e.printStackTrace();
        }
    }    
}