package domain;

public class RegolaCampoOggetto extends RegolaCampo
{
    private int maxQuantita;
	private String nomeCampo;
    
	
	public RegolaCampoOggetto( String nomeCampo, int quantita )
	{
		maxQuantita = quantita;
		this.nomeCampo = nomeCampo;
	}
	
	
    public boolean ApplicaRegolaSuScheda(Scheda s) 
    {
        if( s.getChiaviCampi().contains(nomeCampo) )
		{
			Campo c = s.getCampo(nomeCampo);
			
			if( c instanceof CampoOggetto ) // Se è della classe giusta..
			{
				CampoOggetto co = (CampoOggetto)c;
				return (co.getQuantita() < maxQuantita);
			}
		}
		return false;
    }

	public int getMaxQuantita() {
		return maxQuantita;
	}

	public void setMaxQuantita(int maxQuantita) {
		this.maxQuantita = maxQuantita;
	}

	public String getNomeCampo() {
		return nomeCampo;
	}

	public void setNomeCampo(String nomeCampo) {
		this.nomeCampo = nomeCampo;
	}
	
	
	
}
