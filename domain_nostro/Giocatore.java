package domain;

import java.util.ArrayList;
import domain.Scheda;

public class Giocatore 
{
	private String nomeGiocatore;
	private ArrayList<Scheda> schedeGiocatore = new ArrayList<Scheda>();


	public String getNomeGiocatore() 
	{
		return nomeGiocatore;
	}


	public void setNomeGiocatore(String nomeGiocatore) 
	{
		this.nomeGiocatore = nomeGiocatore;
	}
	
	public void aggiungiScheda( Scheda s )
	{
		schedeGiocatore.add( s );
	}
        
        //@override
        public String toString()
        {
            if( nomeGiocatore == null )
                return "SenzaNome";
            return nomeGiocatore;
        }
}
