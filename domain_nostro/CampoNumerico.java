
package domain;

import java.util.Random;

public class CampoNumerico implements Campo  {

    private String nomeCampo;
    private int valore;
    
    public CampoNumerico(String nomeCampo, int valore)
    {
        this.nomeCampo = nomeCampo;
        this.valore = valore;
    }
    
    public CampoNumerico(String nomeCampo)
    {
        this.nomeCampo = nomeCampo;
        this.valore = Randomizer(nomeCampo);
    }
    
    private int Randomizer(String nomeCampo){
        int min = 0;
        int max = 200000;
        int maxLvl = 20;
        Random rand = new Random();
        
        if(nomeCampo == "livello")
            return rand.nextInt((maxLvl - min) + 1) + min;
        return rand.nextInt((max - min) + 1) + min;
    }
    
    public String getNome() { return nomeCampo; }
    public void setNome(String nomeCampo) { this.nomeCampo = nomeCampo; }
    public int getValore() { return valore; }
    public void setValore(int valore) { this.valore = valore; }
    
    public String toString() { return this.getNome() + " " + this.getValore(); }
}
