package domain;

import java.util.ArrayList;
import java.util.Iterator;

public class RegolaComposite implements RegolaComponent
{
	private ArrayList<RegolaComponent> children = new ArrayList<RegolaComponent>();
	
	
	public boolean Applica( Object o )
	{	
		// Scorre tutti i figli e applica
		for( RegolaComponent c : children )
		{
			if( c.Applica(o) == false )
			{
				// TODO - Notifica all'utente?
				return false; // Se anche una sola figlia fallisce...
			}
		}
		
		return true;
	}
        
        public Iterator iterator()
        {
            return new RegolaIterator(children.iterator());
        }
	
	
	public RegolaComponent GetChild( int index ) throws IndexOutOfBoundsException
	{
		if( index >= children.size() )
			throw new IndexOutOfBoundsException();
		return children.get(index);
	}
	
	
	public void AddChild( RegolaComponent r )
	{
		children.add(r);
	}
	
	
	public void RemChild( int index ) throws IndexOutOfBoundsException
	{
		if( index >= children.size() )
			throw new IndexOutOfBoundsException();
		children.remove(index);
	}
}
