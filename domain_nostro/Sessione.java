package domain;

import java.util.ArrayList;

public class Sessione
{
	private ID idSessione;
	private Giocatore gm;
	private Mappa mappa;
	private ArrayList<ModuloDiRegolamento> setRegole = new ArrayList<ModuloDiRegolamento>();
	private ArrayList<Evento> eventi = new ArrayList<Evento>();
	private ArrayList<Scheda> schede = new ArrayList<Scheda>();
	private StatoSessione stato;
    private String descr;
        

	public Sessione( Giocatore gm )
	{
		this.stato = StatoSessione.sessioneInattiva;
		this.gm = gm;
	}

        
	public String getDescr() 
	{
			return descr;
	}


	public void setDescr(String descr) 
	{
			this.descr = descr;
	}

        
	public ID getIdSessione()
	{
		return this.idSessione;
	}
	
	
	public void setIdSessione( ID nuovaIdSessione )
	{
		this.idSessione = nuovaIdSessione;
	}
	

	public Giocatore getGm()
	{
		return gm;
	}


	public void setGm(Giocatore gm)
	{
		this.gm = gm;
	}


	public Mappa getMappa()
	{
		return mappa;
	}


	public void setMappa(Mappa mappa)
	{
		this.mappa = mappa;
	}


	public StatoSessione getStato()
	{
		return stato;
	}


	public void setStato(StatoSessione stato)
	{
		this.stato = stato;
	}


	public void ottieniITuoiDati()
	{
		// Caso d'uso di avviamento----------
		Evento e;
		ModuloDiRegolamento m;
		Scheda s;

		this.mappa = new Mappa();
		for( int i = 0; i<3; i++ )
		{
			e = new Evento();
			m = new ModuloDiRegolamento();
			s = new Scheda();
			e.setIdEvento( new ID(i) );
			m.setIdModulo( new ID(i) );
			s.setIdScheda( new ID(i) );
			this.eventi.add( e );
			this.setRegole.add( m );
			this.schede.add( s );
		}
		// -----------------------------------
	}


	public void associaSchedeLibere()
	{
		for( Scheda schedaAttuale : this.schede )
		{
			if( schedaAttuale.getGiocatore() == null )
			{
				schedaAttuale.setGiocatore( this.gm );
				this.gm.aggiungiScheda( schedaAttuale );
                System.out.println(schedaAttuale.toString() + " non era associata. E' stata associata con " + this.gm.toString());
			}
		}
	}


	public ArrayList<Scheda> getSchede()
	{
		return schede;
	}

	
	void setIdSessione(int i) 
	{
		this.idSessione = new ID( i );
	}

	
	public String toString()
	{
		return "Sessione#"+idSessione.toString();
	}
	
	
	public boolean isGM( Giocatore g )
	{
		return g==gm;
	}
	
	public Scheda AggiungiSchedaVuota()
	{
		Scheda s = new Scheda();
		schede.add(s);
		return s;
	}
	
	
	// Ritorna le regole fallenti di tutti i moduli
	public ArrayList<Regola> ApplicaRegoleAScheda( Scheda s )
	{
		ArrayList<Regola> blacklist_totale = new ArrayList<Regola>();
		
		for( ModuloDiRegolamento m : setRegole )
		{
			blacklist_totale.addAll( m.ApplicaSuScheda(s) );
		}
		
		return blacklist_totale;
	}
}
