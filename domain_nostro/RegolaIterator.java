package domain;

import java.util.Iterator;
import java.util.Stack;

public class RegolaIterator implements Iterator
{
    Stack<Iterator<RegolaComponent>> stack = new Stack<Iterator<RegolaComponent>>();
    
    
    public RegolaIterator( Iterator<RegolaComponent> i )
    {
        stack.push( i );
    }
    
    public boolean hasNext()
    {
        if( stack.empty() )
            return false;
        
        Iterator<RegolaComponent> t = stack.peek();
        if( t.hasNext() )
            return true;
        stack.pop();
        return this.hasNext();
    }

    public RegolaComponent next()
    {
        if( hasNext() == false )
            return null;
        
        Iterator<RegolaComponent> t = stack.peek();
        RegolaComponent rc = t.next();
        
        if( rc instanceof RegolaComposite )
            stack.push( ((RegolaComposite)rc).iterator() );
        return rc;
    }
    
}
