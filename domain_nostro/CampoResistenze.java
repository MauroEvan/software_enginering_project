
package domain;

import java.util.Random;

public class CampoResistenze implements Campo  {
    
    private int classeArmatura;
    private int riflessi;
    private int tempra;
    private int volonta;
    
    public CampoResistenze(int classeArmatura, int riflessi, int tempra, int volonta)
    {
        this.classeArmatura = 10 + classeArmatura;
        this.riflessi = riflessi;
        this.tempra = tempra;
        this.volonta = volonta;
    }
    
    public CampoResistenze(boolean random)
    {
		if( random )
		{
			this.classeArmatura = 10 + this.Randomizer(0, 10);
			this.riflessi = this.Randomizer(0, 6);
			this.tempra = this.Randomizer(0, 6);
			this.volonta = this.Randomizer(0, 6);
		}
    }
    
    private int Randomizer(int min, int max)
    {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;  
    }

	public String getNome() { return "resistenze"; }
    public int getClasseArmatura() { return classeArmatura; }
    public void setClasseArmatura(int classeArmatura) { this.classeArmatura = classeArmatura; }
    public int getRiflessi() { return riflessi; }
    public void setRiflessi(int riflessi) { this.riflessi = riflessi; }
    public int getTempra() { return tempra; }
    public void setTempra(int tempra) { this.tempra = tempra; }
    public int getVolonta() { return volonta; }
    public void setVolonta(int volonta) { this.volonta = volonta; }
    
    public String toString() { return "Classe armatura " + this.getClasseArmatura()
                                    + "\nRiflessi " + this.getRiflessi()
                                    + "\nTempra " + this.getTempra()
                                    + "\nVolontà " + this.getVolonta(); }
}
