package domain;

public class RegolaCampoAttributo extends RegolaCampo
{
	private int sogliaValore = 0;
	private int sogliaModificatore = 0;
	private String nomeCampo; //chiave (String) nell'Hashmap dei campi in Scheda
	
	
	public RegolaCampoAttributo( String nomeCampo, int sogliaValore, int sogliaModificatore )
	{
		this.sogliaValore = sogliaValore;
		this.nomeCampo = nomeCampo;
		this.sogliaModificatore = sogliaModificatore;
	}
	
	
	private boolean ControllaValore( CampoAttributo campo )
	{
		int val = campo.getValore();
		
		// (se è uguale a 0 lo ignoriamo
		if( val >= sogliaValore || val == 0 )
			return true;
		return false;
	}
	
	
	private boolean ControllaModificatore( CampoAttributo campo )
	{
		int mod = campo.getModificatore();
                int val = campo.getValore();
		
		// (se è uguale a 0 lo ignoriamo
//		if( mod >= sogliaModificatore || mod == 0 )
//			return true;
//		return false;
                
                if( mod == (val - 10)/2 || mod == ( ( (val - 10)/2 ) - 1 ) ) 
                    return true;
                return false;
	}
	
	
    public boolean ApplicaRegolaSuScheda(Scheda s) 
    {
		//Verifichiamo la presenza del campo
		if( s.getChiaviCampi().contains(nomeCampo) )
		{
			Campo c = s.getCampo(nomeCampo);
			
			if( c instanceof CampoAttributo ) // Se è della classe giusta..
			{
				CampoAttributo ca = (CampoAttributo)c;
				
				return ControllaModificatore(ca) &&
					   ControllaValore(ca);
			}
		}
		return false;
    }
	
	public int getSogliaValore() { return sogliaValore; }
	public void setSogliaValore(int sogliaValore) {this.sogliaValore = sogliaValore;}
	public int getSogliaModificatore() {return sogliaModificatore;}
	public void setSogliaModificatore(int sogliaModificatore) {this.sogliaModificatore = sogliaModificatore;}
	public String getNomeCampo() {return nomeCampo;}
	public void setNomeCampo(String nomeCampo) {this.nomeCampo = nomeCampo;}
}
