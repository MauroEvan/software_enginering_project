package domain;

import java.util.ArrayList;

public class ModuloDiRegolamento
{
	private ID idModulo;
	private RegolaComposite regolamento = new RegolaComposite();

	// Ritorna la lista delle regole fallenti
	public ArrayList<Regola> ApplicaSuScheda( Scheda s )
	{
		ArrayList<Regola> blacklist = new ArrayList<Regola>();
        
		RegolaIterator it = new RegolaIterator(regolamento.iterator());
		while( it.hasNext() )
		{
			RegolaComponent a = it.next();

			if( a instanceof Regola )
			{
				if( a.Applica(s) == false )
					blacklist.add( (Regola) a );
			}
		}
                
		return blacklist;
	}
	
	
	public ID getIdModulo() { return idModulo; }
	public void setIdModulo(ID idModulo) { this.idModulo = idModulo; }
	public RegolaComposite getRegolamento() { return regolamento; }
	public void setRegolamento(RegolaComposite regolamento) { this.regolamento = regolamento; }
}