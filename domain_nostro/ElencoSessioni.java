package domain;

import java.util.ArrayList;
import domain.Sessione;


public class ElencoSessioni
{
	private ArrayList<Sessione> sessioni = new ArrayList<Sessione>();

	
	public boolean add( Sessione s )
	{
		return sessioni.add( s );
	}
	
	
	public Sessione at( int index ) throws IndexOutOfBoundsException
	{
		if( index >= sessioni.size() )
			throw new IndexOutOfBoundsException();
		return this.sessioni.get( index );
	}

	
	public int getSize()
	{
		return sessioni.size();
	}
}
