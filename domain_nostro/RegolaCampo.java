package domain;


public abstract class RegolaCampo implements Regola
{
	public boolean Applica( Object o )
	{
            if( o.getClass() == Scheda.class )
                return ApplicaRegolaSuScheda( (Scheda) o );
            return true;
	}
	
	public abstract boolean ApplicaRegolaSuScheda( Scheda s );
}
