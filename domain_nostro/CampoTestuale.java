
package domain;

import java.util.Random;

public class CampoTestuale implements Campo {
    
    private String nomeCampo;
    private String valore;
    
    public CampoTestuale(String nomeCampo, String valore)
    {
        this.nomeCampo = nomeCampo;
        this.valore = valore;
    }
    
    public CampoTestuale(String nomeCampo)
    {
        this.nomeCampo = nomeCampo;
        this.valore = Randomizer();
    }
    
    private String Randomizer()
    {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random rand = new Random();
        for (int i = 0; i < 7; i++) {
            char c = chars[rand.nextInt(chars.length)];
            sb.append(c);
        }
        sb.append(' ');
        for (int i = 0; i < 7; i++) {
            char c = chars[rand.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public String getNome() { return nomeCampo; }
    public void setNome(String nomeCampo) { this.nomeCampo = nomeCampo; }
    public String getValore() { return valore; }
    public void setValore(String valore) { this.valore = valore; }
    
    public String toString() { return this.getNome() + " " + this.getValore(); }    
}
