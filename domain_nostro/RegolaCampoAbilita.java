package domain;

public class RegolaCampoAbilita extends RegolaCampo
{
	private int sogliaValore = 0;
	private int sogliaModificatore = 0;
	private String nomeCampo; //chiave (String) nell'Hashmap dei campi in Scheda
	
	public RegolaCampoAbilita( String nomeCampo, int sogliaValore, int sogliaModificatore )
	{
		this.sogliaValore = sogliaValore;
		this.nomeCampo = nomeCampo;
		this.sogliaModificatore = sogliaModificatore;
	}

	
	private boolean ControllaValore( CampoAbilita campo )
	{
		int val = campo.getValoreGrado();
		
		// (se è uguale a 0 lo ignoriamo
		if( val >= sogliaValore || val == 0 )
			return true;
		return false;
	}
	
	
	private boolean ControllaModificatore( CampoAbilita campo )
	{
		int mod = campo.getModificatore();
		
		// (se è uguale a 0 lo ignoriamo
		if( mod >= sogliaModificatore || mod == 0 )
			return true;
		return false;
	}
	
	
    public boolean ApplicaRegolaSuScheda(Scheda s) 
    {
		//Verifichiamo la presenza del campo
		if( s.getChiaviCampi().contains(nomeCampo) )
		{
			Campo c = s.getCampo(nomeCampo);
			
			if( c instanceof CampoAbilita ) // Se è della classe giusta..
			{
				CampoAbilita ca = (CampoAbilita)c;
				
				return ControllaModificatore(ca) &&
					   ControllaValore(ca);
			}
		}
		return false;
    }
	
	public int getSogliaValore() { return sogliaValore; }
	public void setSogliaValore(int sogliaValore) {this.sogliaValore = sogliaValore;}
	public int getSogliaModificatore() {return sogliaModificatore;}
	public void setSogliaModificatore(int sogliaModificatore) {this.sogliaModificatore = sogliaModificatore;}
	public String getNomeCampo() {return nomeCampo;}
	public void setNomeCampo(String nomeCampo) {this.nomeCampo = nomeCampo;}
}
