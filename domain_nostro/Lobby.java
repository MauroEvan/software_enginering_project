package domain;
import java.util.ArrayList;

public class Lobby
{
	private static Lobby handle;
	
	private ArrayList<Giocatore> lista = new ArrayList<Giocatore>();
	private ArrayList<Tavolo> tavoli = new ArrayList<Tavolo>();
	
	public static Lobby getIstance()
	{
            // Lazy Initialization
            if( handle == null )
                    handle = new Lobby();
            return handle;
	}
	
	
	public Lobby()
	{
		// Caso d'uso di avviamento
		Giocatore testPlayer = new Giocatore();
		testPlayer.setNomeGiocatore( "EsempioGM" );
		lista.add( testPlayer );
		Giocatore testPlayer2 = new Giocatore();
		testPlayer2.setNomeGiocatore( "EsempioGiocatore2" );
		lista.add( testPlayer2 );
		Giocatore testPlayer3 = new Giocatore();
		testPlayer3.setNomeGiocatore( "EsempioGiocatore3" );
		lista.add( testPlayer3 );
	}
	
	public ArrayList<Giocatore> getGiocatoriConnessi()
	{
		return lista;
	}
	
	public void AggiungiGiocatore( Giocatore g )
	{
		lista.add(g);
	}
	
	public Tavolo CreaNuovoTavolo()
	{
		Tavolo t = new Tavolo();
		tavoli.add(t);
		return t;
	}
}
