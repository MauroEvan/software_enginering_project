package domain;
import java.util.HashMap;
import java.util.Set;

public class Scheda
{
	private ID idScheda;
	private Giocatore giocatore;
	private HashMap<String,Campo> campi = new HashMap<String,Campo>();


	public ID getIdScheda() { return idScheda; }
	public void setIdScheda(ID idScheda) { this.idScheda = idScheda; }
	public Giocatore getGiocatore()	{ return giocatore; }
	public void setGiocatore(Giocatore giocatore) {	this.giocatore = giocatore; }
        
	//@override
	public String toString()
	{
		return "Scheda#"+idScheda.toString();
	}
	
	public void addCampo( String key, Campo c )
	{
		// Nota: sovrascrive se la chiave già c'è
		campi.put( key, c );
	}
	
	public Campo getCampo( String key )
	{
		return campi.get( key );
	}
	
	public Set<String> getChiaviCampi()
	{
		return campi.keySet();
	}
}
