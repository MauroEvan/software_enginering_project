
package domain;

public class RegolaCampoResistenze extends RegolaCampo 
{
	private String nomeCampo;
	private int minVolonta;
	private int minTempra;
	private int minRiflessi;
	private int minArmatura;
	
	public RegolaCampoResistenze( String nomeCampo, int volonta, int tempra, int riflessi, int armatura )
	{
		this.nomeCampo = nomeCampo;
		minArmatura = armatura;
		minVolonta = volonta;
		minRiflessi = riflessi;
		minTempra = tempra;
	}
	
	
    public boolean ApplicaRegolaSuScheda(Scheda s) 
    {
        //Verifichiamo la presenza del campo
		if( s.getChiaviCampi().contains(nomeCampo) )
		{
			Campo c = s.getCampo(nomeCampo);
			
			if( c instanceof CampoResistenze )
			{
				CampoResistenze cr = (CampoResistenze)c;
				
				return( cr.getVolonta() >= minVolonta &&
						cr.getClasseArmatura() >= minArmatura &&
						cr.getRiflessi() >= minRiflessi &&
						cr.getTempra() >= minTempra );
			}
		}
		return false;
    }

	public String getNomeCampo() {
		return nomeCampo;
	}

	public void setNomeCampo(String nomeCampo) {
		this.nomeCampo = nomeCampo;
	}

	public int getMinVolonta() {
		return minVolonta;
	}

	public void setMinVolonta(int minVolonta) {
		this.minVolonta = minVolonta;
	}

	public int getMinTempra() {
		return minTempra;
	}

	public void setMinTempra(int minTempra) {
		this.minTempra = minTempra;
	}

	public int getMinRiflessi() {
		return minRiflessi;
	}

	public void setMinRiflessi(int minRiflessi) {
		this.minRiflessi = minRiflessi;
	}

	public int getMinArmatura() {
		return minArmatura;
	}

	public void setMinArmatura(int minArmatura) {
		this.minArmatura = minArmatura;
	}
	
	
}
