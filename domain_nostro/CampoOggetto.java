
package domain;

import java.util.Random;

public class CampoOggetto implements Campo  {
    
    private String nomeOggetto;
    private int quantita;
    
	
    public CampoOggetto(String nomeOggetto, int quantita)
    {
        this.nomeOggetto = nomeOggetto;
        this.quantita = quantita;
    }
    
    public CampoOggetto( String nomeOggetto, boolean random )
	{
		this.nomeOggetto = nomeOggetto;
		if( random )
			this.quantita = Randomizer();
	}
	
    private int Randomizer()
    {
        int min = 1;
        int max = 6;
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    public String getNome() { return nomeOggetto; }
    public void setNome(String nomeOggetto) { this.nomeOggetto = nomeOggetto; }
    public int getQuantita() { return quantita; }
    public void setQuantita(int quantita) { this.quantita = quantita; }
    
    public String toString() { return this.getNome() + " " + this.getQuantita(); }
}
