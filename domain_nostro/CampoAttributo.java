
package domain;

import java.util.LinkedList;
import java.util.Random;

public class CampoAttributo implements Campo  {
    
    private String nomeAttributo;
    private int valore;
    private int modificatore;
    
    public CampoAttributo()
    {
        
    }
    
    public CampoAttributo(String nomeAttributo, int valore)
    {
        this.nomeAttributo = nomeAttributo;
        this.valore = valore;        
        if( (valore - 10) > 0 )
            this.modificatore = (valore - 10)/2;
        else 
            this.modificatore = ( (valore - 10)/2 ) - 1;
    }
    
    public CampoAttributo(String nomeAttributo)
    {
        this.nomeAttributo = nomeAttributo;
        this.valore = Randomizer();
                   
        if( (this.getValore() - 10) > 0 )
            this.modificatore = (this.getValore() - 10)/2;
        else 
            this.modificatore = ( (this.getValore() - 10)/2 ) - 1;        
    }
    
    private int Randomizer()
    {
        LinkedList<Integer> tiri= new LinkedList<Integer>();
        
        for(int i=0; i<4; i++)
        {
            int min = 1;
            int max = 6;
            Random rand = new Random();
            int tiroRandom = rand.nextInt((max - min) + 1) + min;
            tiri.add(tiroRandom);
        }
        int totale = 0;
        tiri.sort(null);
        for(int i=0; i<3; i++)
        {
            totale = totale + tiri.pollLast();
        }
        return totale;
    }

    public String getNome() { return nomeAttributo; }
    public void setNome(String nomeAttributo) { this.nomeAttributo = nomeAttributo; }
    public int getValore() { return valore; }
    public void setValore(int valore) { this.valore = valore; }
    public int getModificatore() { return modificatore; }
    public void setModificatore(int modificatore) { this.modificatore = modificatore; }
    
    public String toString() { return this.getNome() + " - " + this.getValore() + " - " + this.getModificatore(); }
}
