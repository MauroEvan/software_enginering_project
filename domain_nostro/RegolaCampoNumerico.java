package domain;


public class RegolaCampoNumerico extends RegolaCampo
{
    private int limiteSuperiore; // limite superiore (inclusivo)
    private int limiteInferiore; // limite inferiore (inclusivo)
	private String nomeCampo;
    
	
	public RegolaCampoNumerico( String nomeCampo, int inf, int sup )
	{
		limiteInferiore = inf;
		limiteSuperiore = sup;
		this.nomeCampo = nomeCampo;
	}
	
	
    public boolean ApplicaRegolaSuScheda(Scheda s) 
    {
        if( s.getChiaviCampi().contains(nomeCampo) )
		{
			Campo c = s.getCampo(nomeCampo);
			
			if( c instanceof CampoNumerico ) // Se è della classe giusta..
			{
				CampoNumerico cn = (CampoNumerico)c;
				
				return (cn.getValore()<=limiteInferiore && cn.getValore()>=limiteSuperiore );
			}
		}
		return false;
    }

	public int getLimiteSuperiore() {
		return limiteSuperiore;
	}

	public void setLimiteSuperiore(int limiteSuperiore) {
		this.limiteSuperiore = limiteSuperiore;
	}

	public int getLimiteInferiore() {
		return limiteInferiore;
	}

	public void setLimiteInferiore(int limiteInferiore) {
		this.limiteInferiore = limiteInferiore;
	}

	public String getNomeCampo() {
		return nomeCampo;
	}

	public void setNomeCampo(String nomeCampo) {
		this.nomeCampo = nomeCampo;
	}
}
