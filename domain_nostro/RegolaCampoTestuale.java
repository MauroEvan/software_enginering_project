
package domain;

public class RegolaCampoTestuale extends RegolaCampo
{
	private String token;
	private String nomeCampo;
	
	public RegolaCampoTestuale( String nomeCampo, String token )
	{
		this.nomeCampo = nomeCampo;
		this.token = token;
	}
	
	
    public boolean ApplicaRegolaSuScheda(Scheda s) 
    {
        if( s.getChiaviCampi().contains(nomeCampo) )
		{
			Campo c = s.getCampo(nomeCampo);
			
			if( c instanceof CampoTestuale )
			{
				CampoTestuale ct = (CampoTestuale)c;
				return ct.getValore().contains(token);
			}
		}
		return false;
    }

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getNomeCampo() {
		return nomeCampo;
	}

	public void setNomeCampo(String nomeCampo) {
		this.nomeCampo = nomeCampo;
	}
	
	
	
}
