
package domain;

import static java.lang.Math.floor;
import java.util.Random;

public class CampoAbilita implements Campo {
    
    private String nomeAbilita;
    private int valoreGrado = 0;
    private CampoAttributo attributoChiave;
    private int modificatore = 0;
    private boolean diClasse = false;
    

    public CampoAbilita(String nomeAbilita, int valoreGrado, CampoAttributo attributoChiave, boolean diClasse)
    {
        this.attributoChiave = attributoChiave;
        this.diClasse = diClasse;
        this.valoreGrado = valoreGrado;
        this.nomeAbilita = nomeAbilita;
        this.modificatore = this.attributoChiave.getModificatore();
    }
    
    public CampoAbilita(String nomeAbilita, CampoAttributo attributoChiave)
    {
        this.attributoChiave = attributoChiave;
        this.nomeAbilita = nomeAbilita;
        this.diClasse = RandomBoolean();
        this.modificatore = attributoChiave.getModificatore();
        if(diClasse)
            this.valoreGrado = Randomizer();
        else
            this.valoreGrado = (int) floor(Randomizer()/2);
    }
    
    private int Randomizer()
    {
        int min = 0;
        int max = 10;
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }
    
    private boolean RandomBoolean()
    {
        Random rand = new Random();
        return rand.nextBoolean();
    }

    public String getNome() { return nomeAbilita; }
    public void setNome(String nomeAbilita) { this.nomeAbilita = nomeAbilita; }
    public int getValoreGrado() { return valoreGrado; }
    public void setValoreGrado(int valoreGrado) { this.valoreGrado = valoreGrado; }
    public CampoAttributo getAttributoChiave() { return attributoChiave; }
    public void setAttributoChiave(CampoAttributo attributoChiave) { this.attributoChiave = attributoChiave; }
    public int getModificatore() { return modificatore; }
    public void setModificatore(int modificatore) { this.modificatore = modificatore; }
    public boolean isDiClasse() { return diClasse; }
    public void setDiClasse(boolean diClasse) { this.diClasse = diClasse; }
    
    public String toString() { return this.getNome() + " " + this.getValoreGrado(); }
}
