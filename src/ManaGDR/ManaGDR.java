package ManaGDR;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.orm.PersistentException;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import domain.*;
import ManaGDR_RemoteInterface.*;

public class ManaGDR
{
	private static String port;
	private static String id;

	public static void main( String[] args ) throws RemoteException, AlreadyBoundException, NotBoundException
	{
		// Recupero opzioni base per bindare il server RMI
		port = FacadeConfigurazioni.getInstance().get("serverPort");
		id = FacadeConfigurazioni.getInstance().get("serverID");

		// Creazione del registro
		LocateRegistry.createRegistry( Integer.parseInt(port) );
		Registry registry = LocateRegistry.getRegistry(Integer.parseInt(port));


		// Binding sulla porta specificata
		try {
			registry.rebind( id, ManaGDRServerFacade.getInstance() );
		}
		catch( Exception e ) {
			System.err.println( "[SERVER] Errore: Impossibile bindare il server" );
			e.printStackTrace();
			System.exit(-1);
		}
		System.out.println("Server " + id + " partito sulla porta " + port + " :)\n\n" );
	}
}