/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import domain.*;

public class CampoAbilitaDAOImpl implements domain.dao.CampoAbilitaDAO {
	public CampoAbilita loadCampoAbilitaByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadCampoAbilitaByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita getCampoAbilitaByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getCampoAbilitaByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita loadCampoAbilitaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadCampoAbilitaByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita getCampoAbilitaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getCampoAbilitaByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita loadCampoAbilitaByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (CampoAbilita) session.load(domain.CampoAbilita.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita getCampoAbilitaByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (CampoAbilita) session.get(domain.CampoAbilita.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita loadCampoAbilitaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (CampoAbilita) session.load(domain.CampoAbilita.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita getCampoAbilitaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (CampoAbilita) session.get(domain.CampoAbilita.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoAbilita(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryCampoAbilita(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoAbilita(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryCampoAbilita(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita[] listCampoAbilitaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listCampoAbilitaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita[] listCampoAbilitaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listCampoAbilitaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoAbilita(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.CampoAbilita as CampoAbilita");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoAbilita(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.CampoAbilita as CampoAbilita");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("CampoAbilita", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita[] listCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryCampoAbilita(session, condition, orderBy);
			return (CampoAbilita[]) list.toArray(new CampoAbilita[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita[] listCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryCampoAbilita(session, condition, orderBy, lockMode);
			return (CampoAbilita[]) list.toArray(new CampoAbilita[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita loadCampoAbilitaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadCampoAbilitaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita loadCampoAbilitaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadCampoAbilitaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita loadCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		CampoAbilita[] campoAbilitas = listCampoAbilitaByQuery(session, condition, orderBy);
		if (campoAbilitas != null && campoAbilitas.length > 0)
			return campoAbilitas[0];
		else
			return null;
	}
	
	public CampoAbilita loadCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		CampoAbilita[] campoAbilitas = listCampoAbilitaByQuery(session, condition, orderBy, lockMode);
		if (campoAbilitas != null && campoAbilitas.length > 0)
			return campoAbilitas[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateCampoAbilitaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateCampoAbilitaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoAbilitaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateCampoAbilitaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.CampoAbilita as CampoAbilita");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.CampoAbilita as CampoAbilita");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("CampoAbilita", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita createCampoAbilita() {
		return new domain.CampoAbilita( null, null );
	}
	
	public boolean save(domain.CampoAbilita campoAbilita) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().saveObject(campoAbilita);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(domain.CampoAbilita campoAbilita) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().deleteObject(campoAbilita);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.CampoAbilita campoAbilita)throws PersistentException {
		try {
			if(campoAbilita.getScheda() != null) {
				campoAbilita.getScheda().getCampoAbilità().remove(campoAbilita);
			}
			
			return delete(campoAbilita);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.CampoAbilita campoAbilita, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(campoAbilita.getScheda() != null) {
				campoAbilita.getScheda().getCampoAbilità().remove(campoAbilita);
			}
			
			try {
				session.delete(campoAbilita);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(domain.CampoAbilita campoAbilita) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().refresh(campoAbilita);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(domain.CampoAbilita campoAbilita) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().evict(campoAbilita);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAbilita loadCampoAbilitaByCriteria(CampoAbilitaCriteria campoAbilitaCriteria) {
		CampoAbilita[] campoAbilitas = listCampoAbilitaByCriteria(campoAbilitaCriteria);
		if(campoAbilitas == null || campoAbilitas.length == 0) {
			return null;
		}
		return campoAbilitas[0];
	}
	
	public CampoAbilita[] listCampoAbilitaByCriteria(CampoAbilitaCriteria campoAbilitaCriteria) {
		return campoAbilitaCriteria.listCampoAbilita();
	}
}
