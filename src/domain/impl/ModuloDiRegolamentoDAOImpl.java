/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import domain.*;

public class ModuloDiRegolamentoDAOImpl implements domain.dao.ModuloDiRegolamentoDAO {
	public ModuloDiRegolamento loadModuloDiRegolamentoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadModuloDiRegolamentoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento getModuloDiRegolamentoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getModuloDiRegolamentoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento loadModuloDiRegolamentoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadModuloDiRegolamentoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento getModuloDiRegolamentoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getModuloDiRegolamentoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento loadModuloDiRegolamentoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (ModuloDiRegolamento) session.load(domain.ModuloDiRegolamento.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento getModuloDiRegolamentoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (ModuloDiRegolamento) session.get(domain.ModuloDiRegolamento.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento loadModuloDiRegolamentoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (ModuloDiRegolamento) session.load(domain.ModuloDiRegolamento.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento getModuloDiRegolamentoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (ModuloDiRegolamento) session.get(domain.ModuloDiRegolamento.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryModuloDiRegolamento(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryModuloDiRegolamento(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryModuloDiRegolamento(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryModuloDiRegolamento(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento[] listModuloDiRegolamentoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listModuloDiRegolamentoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento[] listModuloDiRegolamentoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listModuloDiRegolamentoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryModuloDiRegolamento(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.ModuloDiRegolamento as ModuloDiRegolamento");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryModuloDiRegolamento(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.ModuloDiRegolamento as ModuloDiRegolamento");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("ModuloDiRegolamento", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento[] listModuloDiRegolamentoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryModuloDiRegolamento(session, condition, orderBy);
			return (ModuloDiRegolamento[]) list.toArray(new ModuloDiRegolamento[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento[] listModuloDiRegolamentoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryModuloDiRegolamento(session, condition, orderBy, lockMode);
			return (ModuloDiRegolamento[]) list.toArray(new ModuloDiRegolamento[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento loadModuloDiRegolamentoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadModuloDiRegolamentoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento loadModuloDiRegolamentoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadModuloDiRegolamentoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento loadModuloDiRegolamentoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		ModuloDiRegolamento[] moduloDiRegolamentos = listModuloDiRegolamentoByQuery(session, condition, orderBy);
		if (moduloDiRegolamentos != null && moduloDiRegolamentos.length > 0)
			return moduloDiRegolamentos[0];
		else
			return null;
	}
	
	public ModuloDiRegolamento loadModuloDiRegolamentoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		ModuloDiRegolamento[] moduloDiRegolamentos = listModuloDiRegolamentoByQuery(session, condition, orderBy, lockMode);
		if (moduloDiRegolamentos != null && moduloDiRegolamentos.length > 0)
			return moduloDiRegolamentos[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateModuloDiRegolamentoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateModuloDiRegolamentoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateModuloDiRegolamentoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateModuloDiRegolamentoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateModuloDiRegolamentoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.ModuloDiRegolamento as ModuloDiRegolamento");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateModuloDiRegolamentoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.ModuloDiRegolamento as ModuloDiRegolamento");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("ModuloDiRegolamento", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento createModuloDiRegolamento() {
		return new domain.ModuloDiRegolamento();
	}
	
	public boolean save(domain.ModuloDiRegolamento moduloDiRegolamento) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().saveObject(moduloDiRegolamento);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(domain.ModuloDiRegolamento moduloDiRegolamento) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().deleteObject(moduloDiRegolamento);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.ModuloDiRegolamento moduloDiRegolamento)throws PersistentException {
		try {
			if(moduloDiRegolamento.getRegolamento() != null) {
				//moduloDiRegolamento.getRegolamento().setModulo(null);
			}
			
			return delete(moduloDiRegolamento);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.ModuloDiRegolamento moduloDiRegolamento, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(moduloDiRegolamento.getRegolamento() != null) {
				//moduloDiRegolamento.getRegolamento().setModulo(null);
			}
			
			try {
				session.delete(moduloDiRegolamento);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(domain.ModuloDiRegolamento moduloDiRegolamento) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().refresh(moduloDiRegolamento);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(domain.ModuloDiRegolamento moduloDiRegolamento) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().evict(moduloDiRegolamento);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public ModuloDiRegolamento loadModuloDiRegolamentoByCriteria(ModuloDiRegolamentoCriteria moduloDiRegolamentoCriteria) {
		ModuloDiRegolamento[] moduloDiRegolamentos = listModuloDiRegolamentoByCriteria(moduloDiRegolamentoCriteria);
		if(moduloDiRegolamentos == null || moduloDiRegolamentos.length == 0) {
			return null;
		}
		return moduloDiRegolamentos[0];
	}
	
	public ModuloDiRegolamento[] listModuloDiRegolamentoByCriteria(ModuloDiRegolamentoCriteria moduloDiRegolamentoCriteria) {
		return moduloDiRegolamentoCriteria.listModuloDiRegolamento();
	}
}
