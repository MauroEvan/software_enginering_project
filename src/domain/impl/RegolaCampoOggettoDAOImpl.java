/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import domain.*;

public class RegolaCampoOggettoDAOImpl implements domain.dao.RegolaCampoOggettoDAO {
	public RegolaCampoOggetto loadRegolaCampoOggettoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoOggettoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto getRegolaCampoOggettoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getRegolaCampoOggettoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto loadRegolaCampoOggettoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoOggettoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto getRegolaCampoOggettoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getRegolaCampoOggettoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto loadRegolaCampoOggettoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaCampoOggetto) session.load(domain.RegolaCampoOggetto.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto getRegolaCampoOggettoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaCampoOggetto) session.get(domain.RegolaCampoOggetto.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto loadRegolaCampoOggettoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaCampoOggetto) session.load(domain.RegolaCampoOggetto.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto getRegolaCampoOggettoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaCampoOggetto) session.get(domain.RegolaCampoOggetto.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoOggetto(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryRegolaCampoOggetto(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoOggetto(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryRegolaCampoOggetto(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto[] listRegolaCampoOggettoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listRegolaCampoOggettoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto[] listRegolaCampoOggettoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listRegolaCampoOggettoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoOggetto(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaCampoOggetto as RegolaCampoOggetto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoOggetto(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaCampoOggetto as RegolaCampoOggetto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaCampoOggetto", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto[] listRegolaCampoOggettoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRegolaCampoOggetto(session, condition, orderBy);
			return (RegolaCampoOggetto[]) list.toArray(new RegolaCampoOggetto[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto[] listRegolaCampoOggettoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRegolaCampoOggetto(session, condition, orderBy, lockMode);
			return (RegolaCampoOggetto[]) list.toArray(new RegolaCampoOggetto[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto loadRegolaCampoOggettoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoOggettoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto loadRegolaCampoOggettoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoOggettoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto loadRegolaCampoOggettoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		RegolaCampoOggetto[] regolaCampoOggettos = listRegolaCampoOggettoByQuery(session, condition, orderBy);
		if (regolaCampoOggettos != null && regolaCampoOggettos.length > 0)
			return regolaCampoOggettos[0];
		else
			return null;
	}
	
	public RegolaCampoOggetto loadRegolaCampoOggettoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		RegolaCampoOggetto[] regolaCampoOggettos = listRegolaCampoOggettoByQuery(session, condition, orderBy, lockMode);
		if (regolaCampoOggettos != null && regolaCampoOggettos.length > 0)
			return regolaCampoOggettos[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateRegolaCampoOggettoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCampoOggettoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoOggettoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCampoOggettoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoOggettoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaCampoOggetto as RegolaCampoOggetto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoOggettoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaCampoOggetto as RegolaCampoOggetto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaCampoOggetto", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto createRegolaCampoOggetto() {
		return new domain.RegolaCampoOggetto();
	}
	
	public boolean save(domain.RegolaCampoOggetto regolaCampoOggetto) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().saveObject(regolaCampoOggetto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(domain.RegolaCampoOggetto regolaCampoOggetto) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().deleteObject(regolaCampoOggetto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.RegolaCampoOggetto regolaCampoOggetto)throws PersistentException {
		try {
			if(regolaCampoOggetto.getRegolaComposite() != null) {
				regolaCampoOggetto.getRegolaComposite().getRegolaCampoOggetto().remove(regolaCampoOggetto);
			}
			
			return delete(regolaCampoOggetto);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.RegolaCampoOggetto regolaCampoOggetto, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(regolaCampoOggetto.getRegolaComposite() != null) {
				regolaCampoOggetto.getRegolaComposite().getRegolaCampoOggetto().remove(regolaCampoOggetto);
			}
			
			try {
				session.delete(regolaCampoOggetto);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(domain.RegolaCampoOggetto regolaCampoOggetto) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().refresh(regolaCampoOggetto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(domain.RegolaCampoOggetto regolaCampoOggetto) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().evict(regolaCampoOggetto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoOggetto loadRegolaCampoOggettoByCriteria(RegolaCampoOggettoCriteria regolaCampoOggettoCriteria) {
		RegolaCampoOggetto[] regolaCampoOggettos = listRegolaCampoOggettoByCriteria(regolaCampoOggettoCriteria);
		if(regolaCampoOggettos == null || regolaCampoOggettos.length == 0) {
			return null;
		}
		return regolaCampoOggettos[0];
	}
	
	public RegolaCampoOggetto[] listRegolaCampoOggettoByCriteria(RegolaCampoOggettoCriteria regolaCampoOggettoCriteria) {
		return regolaCampoOggettoCriteria.listRegolaCampoOggetto();
	}
}
