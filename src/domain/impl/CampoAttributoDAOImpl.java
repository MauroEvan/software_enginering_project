/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import domain.*;

public class CampoAttributoDAOImpl implements domain.dao.CampoAttributoDAO {
	public CampoAttributo loadCampoAttributoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadCampoAttributoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo getCampoAttributoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getCampoAttributoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo loadCampoAttributoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadCampoAttributoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo getCampoAttributoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getCampoAttributoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo loadCampoAttributoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (CampoAttributo) session.load(domain.CampoAttributo.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo getCampoAttributoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (CampoAttributo) session.get(domain.CampoAttributo.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo loadCampoAttributoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (CampoAttributo) session.load(domain.CampoAttributo.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo getCampoAttributoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (CampoAttributo) session.get(domain.CampoAttributo.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoAttributo(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryCampoAttributo(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoAttributo(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryCampoAttributo(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo[] listCampoAttributoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listCampoAttributoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo[] listCampoAttributoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listCampoAttributoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoAttributo(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.CampoAttributo as CampoAttributo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryCampoAttributo(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.CampoAttributo as CampoAttributo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("CampoAttributo", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo[] listCampoAttributoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryCampoAttributo(session, condition, orderBy);
			return (CampoAttributo[]) list.toArray(new CampoAttributo[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo[] listCampoAttributoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryCampoAttributo(session, condition, orderBy, lockMode);
			return (CampoAttributo[]) list.toArray(new CampoAttributo[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo loadCampoAttributoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadCampoAttributoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo loadCampoAttributoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadCampoAttributoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo loadCampoAttributoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		CampoAttributo[] campoAttributos = listCampoAttributoByQuery(session, condition, orderBy);
		if (campoAttributos != null && campoAttributos.length > 0)
			return campoAttributos[0];
		else
			return null;
	}
	
	public CampoAttributo loadCampoAttributoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		CampoAttributo[] campoAttributos = listCampoAttributoByQuery(session, condition, orderBy, lockMode);
		if (campoAttributos != null && campoAttributos.length > 0)
			return campoAttributos[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateCampoAttributoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateCampoAttributoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoAttributoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateCampoAttributoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoAttributoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.CampoAttributo as CampoAttributo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateCampoAttributoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.CampoAttributo as CampoAttributo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("CampoAttributo", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo createCampoAttributo() {
		return new domain.CampoAttributo( null );
	}
	
	public boolean save(domain.CampoAttributo campoAttributo) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().saveObject(campoAttributo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(domain.CampoAttributo campoAttributo) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().deleteObject(campoAttributo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.CampoAttributo campoAttributo)throws PersistentException {
		try {
			if(campoAttributo.getScheda() != null) {
				campoAttributo.getScheda().getCampoAttributo().remove(campoAttributo);
			}
			
			return delete(campoAttributo);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.CampoAttributo campoAttributo, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(campoAttributo.getScheda() != null) {
				campoAttributo.getScheda().getCampoAttributo().remove(campoAttributo);
			}
			
			try {
				session.delete(campoAttributo);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(domain.CampoAttributo campoAttributo) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().refresh(campoAttributo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(domain.CampoAttributo campoAttributo) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().evict(campoAttributo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public CampoAttributo loadCampoAttributoByCriteria(CampoAttributoCriteria campoAttributoCriteria) {
		CampoAttributo[] campoAttributos = listCampoAttributoByCriteria(campoAttributoCriteria);
		if(campoAttributos == null || campoAttributos.length == 0) {
			return null;
		}
		return campoAttributos[0];
	}
	
	public CampoAttributo[] listCampoAttributoByCriteria(CampoAttributoCriteria campoAttributoCriteria) {
		return campoAttributoCriteria.listCampoAttributo();
	}
}
