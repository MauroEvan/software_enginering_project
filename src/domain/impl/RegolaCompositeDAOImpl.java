/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import domain.*;

public class RegolaCompositeDAOImpl implements domain.dao.RegolaCompositeDAO {
	public RegolaComposite loadRegolaCompositeByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCompositeByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite getRegolaCompositeByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getRegolaCompositeByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite loadRegolaCompositeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCompositeByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite getRegolaCompositeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getRegolaCompositeByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite loadRegolaCompositeByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaComposite) session.load(domain.RegolaComposite.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite getRegolaCompositeByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaComposite) session.get(domain.RegolaComposite.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite loadRegolaCompositeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaComposite) session.load(domain.RegolaComposite.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite getRegolaCompositeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaComposite) session.get(domain.RegolaComposite.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaComposite(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryRegolaComposite(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaComposite(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryRegolaComposite(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite[] listRegolaCompositeByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listRegolaCompositeByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite[] listRegolaCompositeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listRegolaCompositeByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaComposite(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaComposite as RegolaComposite");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaComposite(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaComposite as RegolaComposite");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaComposite", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite[] listRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRegolaComposite(session, condition, orderBy);
			return (RegolaComposite[]) list.toArray(new RegolaComposite[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite[] listRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRegolaComposite(session, condition, orderBy, lockMode);
			return (RegolaComposite[]) list.toArray(new RegolaComposite[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite loadRegolaCompositeByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCompositeByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite loadRegolaCompositeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCompositeByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite loadRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		RegolaComposite[] regolaComposites = listRegolaCompositeByQuery(session, condition, orderBy);
		if (regolaComposites != null && regolaComposites.length > 0)
			return regolaComposites[0];
		else
			return null;
	}
	
	public RegolaComposite loadRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		RegolaComposite[] regolaComposites = listRegolaCompositeByQuery(session, condition, orderBy, lockMode);
		if (regolaComposites != null && regolaComposites.length > 0)
			return regolaComposites[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateRegolaCompositeByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCompositeByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCompositeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCompositeByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaComposite as RegolaComposite");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCompositeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaComposite as RegolaComposite");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaComposite", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite createRegolaComposite() {
		return new domain.RegolaComposite();
	}
	
	public boolean save(domain.RegolaComposite regolaComposite) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().saveObject(regolaComposite);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(domain.RegolaComposite regolaComposite) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().deleteObject(regolaComposite);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.RegolaComposite regolaComposite)throws PersistentException {
		try {
			if(regolaComposite.getModulo() != null) {
				regolaComposite.getModulo().setRegolamento(null);
			}
			
			domain.RegolaGenerica[] lRegolaGenericas = (domain.RegolaGenerica[])regolaComposite.getRegolaGenerica().toArray(new domain.RegolaGenerica[regolaComposite.getRegolaGenerica().size()]);
			for(int i = 0; i < lRegolaGenericas.length; i++) {
				lRegolaGenericas[i].setRegolaComposite(null);
			}
			domain.RegolaCampoNumerico[] lRegolaCampoNumericos = (domain.RegolaCampoNumerico[])regolaComposite.getRegolaCampoNumerico().toArray(new domain.RegolaCampoNumerico[regolaComposite.getRegolaCampoNumerico().size()]);
			for(int i = 0; i < lRegolaCampoNumericos.length; i++) {
				lRegolaCampoNumericos[i].setRegolaComposite(null);
			}
			domain.RegolaCampoResistenze[] lRegolaCampoResistenzes = (domain.RegolaCampoResistenze[])regolaComposite.getRegolaCampoResistenze().toArray(new domain.RegolaCampoResistenze[regolaComposite.getRegolaCampoResistenze().size()]);
			for(int i = 0; i < lRegolaCampoResistenzes.length; i++) {
				lRegolaCampoResistenzes[i].setRegolaComposite(null);
			}
			domain.RegolaCampoAttributo[] lRegolaCampoAttributos = (domain.RegolaCampoAttributo[])regolaComposite.getRegolaCampoAttributo().toArray(new domain.RegolaCampoAttributo[regolaComposite.getRegolaCampoAttributo().size()]);
			for(int i = 0; i < lRegolaCampoAttributos.length; i++) {
				lRegolaCampoAttributos[i].setRegolaComposite(null);
			}
			domain.RegolaCampoAbilita[] lRegolaCampoAbilitàs = (domain.RegolaCampoAbilita[])regolaComposite.getRegolaCampoAbilità().toArray(new domain.RegolaCampoAbilita[regolaComposite.getRegolaCampoAbilità().size()]);
			for(int i = 0; i < lRegolaCampoAbilitàs.length; i++) {
				lRegolaCampoAbilitàs[i].setRegolaComposite(null);
			}
			domain.RegolaCampoOggetto[] lRegolaCampoOggettos = (domain.RegolaCampoOggetto[])regolaComposite.getRegolaCampoOggetto().toArray(new domain.RegolaCampoOggetto[regolaComposite.getRegolaCampoOggetto().size()]);
			for(int i = 0; i < lRegolaCampoOggettos.length; i++) {
				lRegolaCampoOggettos[i].setRegolaComposite(null);
			}
			domain.RegolaCampoTestuale[] lRegolaCampoTestuales = (domain.RegolaCampoTestuale[])regolaComposite.getRegolaCampoTestuale().toArray(new domain.RegolaCampoTestuale[regolaComposite.getRegolaCampoTestuale().size()]);
			for(int i = 0; i < lRegolaCampoTestuales.length; i++) {
				lRegolaCampoTestuales[i].setRegolaComposite(null);
			}
			return delete(regolaComposite);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.RegolaComposite regolaComposite, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(regolaComposite.getModulo() != null) {
				regolaComposite.getModulo().setRegolamento(null);
			}
			
			domain.RegolaGenerica[] lRegolaGenericas = (domain.RegolaGenerica[])regolaComposite.getRegolaGenerica().toArray(new domain.RegolaGenerica[regolaComposite.getRegolaGenerica().size()]);
			for(int i = 0; i < lRegolaGenericas.length; i++) {
				lRegolaGenericas[i].setRegolaComposite(null);
			}
			domain.RegolaCampoNumerico[] lRegolaCampoNumericos = (domain.RegolaCampoNumerico[])regolaComposite.getRegolaCampoNumerico().toArray(new domain.RegolaCampoNumerico[regolaComposite.getRegolaCampoNumerico().size()]);
			for(int i = 0; i < lRegolaCampoNumericos.length; i++) {
				lRegolaCampoNumericos[i].setRegolaComposite(null);
			}
			domain.RegolaCampoResistenze[] lRegolaCampoResistenzes = (domain.RegolaCampoResistenze[])regolaComposite.getRegolaCampoResistenze().toArray(new domain.RegolaCampoResistenze[regolaComposite.getRegolaCampoResistenze().size()]);
			for(int i = 0; i < lRegolaCampoResistenzes.length; i++) {
				lRegolaCampoResistenzes[i].setRegolaComposite(null);
			}
			domain.RegolaCampoAttributo[] lRegolaCampoAttributos = (domain.RegolaCampoAttributo[])regolaComposite.getRegolaCampoAttributo().toArray(new domain.RegolaCampoAttributo[regolaComposite.getRegolaCampoAttributo().size()]);
			for(int i = 0; i < lRegolaCampoAttributos.length; i++) {
				lRegolaCampoAttributos[i].setRegolaComposite(null);
			}
			domain.RegolaCampoAbilita[] lRegolaCampoAbilitàs = (domain.RegolaCampoAbilita[])regolaComposite.getRegolaCampoAbilità().toArray(new domain.RegolaCampoAbilita[regolaComposite.getRegolaCampoAbilità().size()]);
			for(int i = 0; i < lRegolaCampoAbilitàs.length; i++) {
				lRegolaCampoAbilitàs[i].setRegolaComposite(null);
			}
			domain.RegolaCampoOggetto[] lRegolaCampoOggettos = (domain.RegolaCampoOggetto[])regolaComposite.getRegolaCampoOggetto().toArray(new domain.RegolaCampoOggetto[regolaComposite.getRegolaCampoOggetto().size()]);
			for(int i = 0; i < lRegolaCampoOggettos.length; i++) {
				lRegolaCampoOggettos[i].setRegolaComposite(null);
			}
			domain.RegolaCampoTestuale[] lRegolaCampoTestuales = (domain.RegolaCampoTestuale[])regolaComposite.getRegolaCampoTestuale().toArray(new domain.RegolaCampoTestuale[regolaComposite.getRegolaCampoTestuale().size()]);
			for(int i = 0; i < lRegolaCampoTestuales.length; i++) {
				lRegolaCampoTestuales[i].setRegolaComposite(null);
			}
			try {
				session.delete(regolaComposite);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(domain.RegolaComposite regolaComposite) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().refresh(regolaComposite);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(domain.RegolaComposite regolaComposite) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().evict(regolaComposite);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaComposite loadRegolaCompositeByCriteria(RegolaCompositeCriteria regolaCompositeCriteria) {
		RegolaComposite[] regolaComposites = listRegolaCompositeByCriteria(regolaCompositeCriteria);
		if(regolaComposites == null || regolaComposites.length == 0) {
			return null;
		}
		return regolaComposites[0];
	}
	
	public RegolaComposite[] listRegolaCompositeByCriteria(RegolaCompositeCriteria regolaCompositeCriteria) {
		return regolaCompositeCriteria.listRegolaComposite();
	}
}
