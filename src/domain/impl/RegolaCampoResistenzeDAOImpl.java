/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import domain.*;

public class RegolaCampoResistenzeDAOImpl implements domain.dao.RegolaCampoResistenzeDAO {
	public RegolaCampoResistenze loadRegolaCampoResistenzeByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoResistenzeByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze getRegolaCampoResistenzeByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getRegolaCampoResistenzeByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze loadRegolaCampoResistenzeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoResistenzeByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze getRegolaCampoResistenzeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getRegolaCampoResistenzeByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze loadRegolaCampoResistenzeByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaCampoResistenze) session.load(domain.RegolaCampoResistenze.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze getRegolaCampoResistenzeByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaCampoResistenze) session.get(domain.RegolaCampoResistenze.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze loadRegolaCampoResistenzeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaCampoResistenze) session.load(domain.RegolaCampoResistenze.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze getRegolaCampoResistenzeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaCampoResistenze) session.get(domain.RegolaCampoResistenze.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoResistenze(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryRegolaCampoResistenze(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoResistenze(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryRegolaCampoResistenze(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze[] listRegolaCampoResistenzeByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listRegolaCampoResistenzeByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze[] listRegolaCampoResistenzeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listRegolaCampoResistenzeByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoResistenze(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaCampoResistenze as RegolaCampoResistenze");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoResistenze(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaCampoResistenze as RegolaCampoResistenze");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaCampoResistenze", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze[] listRegolaCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRegolaCampoResistenze(session, condition, orderBy);
			return (RegolaCampoResistenze[]) list.toArray(new RegolaCampoResistenze[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze[] listRegolaCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRegolaCampoResistenze(session, condition, orderBy, lockMode);
			return (RegolaCampoResistenze[]) list.toArray(new RegolaCampoResistenze[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze loadRegolaCampoResistenzeByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoResistenzeByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze loadRegolaCampoResistenzeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoResistenzeByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze loadRegolaCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		RegolaCampoResistenze[] regolaCampoResistenzes = listRegolaCampoResistenzeByQuery(session, condition, orderBy);
		if (regolaCampoResistenzes != null && regolaCampoResistenzes.length > 0)
			return regolaCampoResistenzes[0];
		else
			return null;
	}
	
	public RegolaCampoResistenze loadRegolaCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		RegolaCampoResistenze[] regolaCampoResistenzes = listRegolaCampoResistenzeByQuery(session, condition, orderBy, lockMode);
		if (regolaCampoResistenzes != null && regolaCampoResistenzes.length > 0)
			return regolaCampoResistenzes[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateRegolaCampoResistenzeByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCampoResistenzeByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoResistenzeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCampoResistenzeByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaCampoResistenze as RegolaCampoResistenze");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaCampoResistenze as RegolaCampoResistenze");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaCampoResistenze", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze createRegolaCampoResistenze() {
		return new domain.RegolaCampoResistenze();
	}
	
	public boolean save(domain.RegolaCampoResistenze regolaCampoResistenze) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().saveObject(regolaCampoResistenze);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(domain.RegolaCampoResistenze regolaCampoResistenze) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().deleteObject(regolaCampoResistenze);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.RegolaCampoResistenze regolaCampoResistenze)throws PersistentException {
		try {
			if(regolaCampoResistenze.getRegolaComposite() != null) {
				regolaCampoResistenze.getRegolaComposite().getRegolaCampoResistenze().remove(regolaCampoResistenze);
			}
			
			return delete(regolaCampoResistenze);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.RegolaCampoResistenze regolaCampoResistenze, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(regolaCampoResistenze.getRegolaComposite() != null) {
				regolaCampoResistenze.getRegolaComposite().getRegolaCampoResistenze().remove(regolaCampoResistenze);
			}
			
			try {
				session.delete(regolaCampoResistenze);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(domain.RegolaCampoResistenze regolaCampoResistenze) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().refresh(regolaCampoResistenze);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(domain.RegolaCampoResistenze regolaCampoResistenze) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().evict(regolaCampoResistenze);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoResistenze loadRegolaCampoResistenzeByCriteria(RegolaCampoResistenzeCriteria regolaCampoResistenzeCriteria) {
		RegolaCampoResistenze[] regolaCampoResistenzes = listRegolaCampoResistenzeByCriteria(regolaCampoResistenzeCriteria);
		if(regolaCampoResistenzes == null || regolaCampoResistenzes.length == 0) {
			return null;
		}
		return regolaCampoResistenzes[0];
	}
	
	public RegolaCampoResistenze[] listRegolaCampoResistenzeByCriteria(RegolaCampoResistenzeCriteria regolaCampoResistenzeCriteria) {
		return regolaCampoResistenzeCriteria.listRegolaCampoResistenze();
	}
}
