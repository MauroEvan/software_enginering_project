/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.impl;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;
import domain.*;

public class RegolaCampoTestualeDAOImpl implements domain.dao.RegolaCampoTestualeDAO {
	public RegolaCampoTestuale loadRegolaCampoTestualeByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoTestualeByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale getRegolaCampoTestualeByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getRegolaCampoTestualeByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale loadRegolaCampoTestualeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoTestualeByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale getRegolaCampoTestualeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return getRegolaCampoTestualeByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale loadRegolaCampoTestualeByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaCampoTestuale) session.load(domain.RegolaCampoTestuale.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale getRegolaCampoTestualeByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (RegolaCampoTestuale) session.get(domain.RegolaCampoTestuale.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale loadRegolaCampoTestualeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaCampoTestuale) session.load(domain.RegolaCampoTestuale.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale getRegolaCampoTestualeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (RegolaCampoTestuale) session.get(domain.RegolaCampoTestuale.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoTestuale(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryRegolaCampoTestuale(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoTestuale(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return queryRegolaCampoTestuale(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale[] listRegolaCampoTestualeByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listRegolaCampoTestualeByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale[] listRegolaCampoTestualeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return listRegolaCampoTestualeByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoTestuale(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaCampoTestuale as RegolaCampoTestuale");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public List queryRegolaCampoTestuale(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaCampoTestuale as RegolaCampoTestuale");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaCampoTestuale", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale[] listRegolaCampoTestualeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRegolaCampoTestuale(session, condition, orderBy);
			return (RegolaCampoTestuale[]) list.toArray(new RegolaCampoTestuale[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale[] listRegolaCampoTestualeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRegolaCampoTestuale(session, condition, orderBy, lockMode);
			return (RegolaCampoTestuale[]) list.toArray(new RegolaCampoTestuale[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale loadRegolaCampoTestualeByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoTestualeByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale loadRegolaCampoTestualeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return loadRegolaCampoTestualeByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale loadRegolaCampoTestualeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		RegolaCampoTestuale[] regolaCampoTestuales = listRegolaCampoTestualeByQuery(session, condition, orderBy);
		if (regolaCampoTestuales != null && regolaCampoTestuales.length > 0)
			return regolaCampoTestuales[0];
		else
			return null;
	}
	
	public RegolaCampoTestuale loadRegolaCampoTestualeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		RegolaCampoTestuale[] regolaCampoTestuales = listRegolaCampoTestualeByQuery(session, condition, orderBy, lockMode);
		if (regolaCampoTestuales != null && regolaCampoTestuales.length > 0)
			return regolaCampoTestuales[0];
		else
			return null;
	}
	
	public java.util.Iterator iterateRegolaCampoTestualeByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCampoTestualeByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoTestualeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = domain.ManaGDRPersistentManager.instance().getSession();
			return iterateRegolaCampoTestualeByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoTestualeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaCampoTestuale as RegolaCampoTestuale");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public java.util.Iterator iterateRegolaCampoTestualeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From domain.RegolaCampoTestuale as RegolaCampoTestuale");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("RegolaCampoTestuale", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale createRegolaCampoTestuale() {
		return new domain.RegolaCampoTestuale();
	}
	
	public boolean save(domain.RegolaCampoTestuale regolaCampoTestuale) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().saveObject(regolaCampoTestuale);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean delete(domain.RegolaCampoTestuale regolaCampoTestuale) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().deleteObject(regolaCampoTestuale);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.RegolaCampoTestuale regolaCampoTestuale)throws PersistentException {
		try {
			if(regolaCampoTestuale.getRegolaComposite() != null) {
				regolaCampoTestuale.getRegolaComposite().getRegolaCampoTestuale().remove(regolaCampoTestuale);
			}
			
			return delete(regolaCampoTestuale);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean deleteAndDissociate(domain.RegolaCampoTestuale regolaCampoTestuale, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(regolaCampoTestuale.getRegolaComposite() != null) {
				regolaCampoTestuale.getRegolaComposite().getRegolaCampoTestuale().remove(regolaCampoTestuale);
			}
			
			try {
				session.delete(regolaCampoTestuale);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean refresh(domain.RegolaCampoTestuale regolaCampoTestuale) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().refresh(regolaCampoTestuale);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public boolean evict(domain.RegolaCampoTestuale regolaCampoTestuale) throws PersistentException {
		try {
			domain.ManaGDRPersistentManager.instance().getSession().evict(regolaCampoTestuale);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public RegolaCampoTestuale loadRegolaCampoTestualeByCriteria(RegolaCampoTestualeCriteria regolaCampoTestualeCriteria) {
		RegolaCampoTestuale[] regolaCampoTestuales = listRegolaCampoTestualeByCriteria(regolaCampoTestualeCriteria);
		if(regolaCampoTestuales == null || regolaCampoTestuales.length == 0) {
			return null;
		}
		return regolaCampoTestuales[0];
	}
	
	public RegolaCampoTestuale[] listRegolaCampoTestualeByCriteria(RegolaCampoTestualeCriteria regolaCampoTestualeCriteria) {
		return regolaCampoTestualeCriteria.listRegolaCampoTestuale();
	}
}
