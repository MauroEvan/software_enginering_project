package domain;

import ManaGDR_RemoteInterface.*;


public class RegolaCampoResistenze extends domain.RegolaCampo
{
	private int ID;
	private int minArmatura;
	private int minVolonta;
	private int minTempra;
	private int minRiflessi;
	private String nomeCampo;
	private RegolaComposite regolaComposite;


	public RegolaCampoResistenze() {}


	public RegolaCampoResistenze( String nomeCampo, int volonta, int tempra, int riflessi, int armatura )
	{
		this.nomeCampo = nomeCampo;
		minArmatura = armatura;
		minVolonta = volonta;
		minRiflessi = riflessi;
		minTempra = tempra;
	}


	public boolean ApplicaRegolaSuScheda(Scheda s)
	{
		//Verifichiamo la presenza del campo
		if (s.getChiaviCampi().contains(nomeCampo)) {
			Campo c = s.getCampo(nomeCampo);
			if (c instanceof CampoResistenze) {
				CampoResistenze cr = (CampoResistenze) c;
				return (cr.getVolonta() >= minVolonta
					&& cr.getClasseArmatura() >= minArmatura
					&& cr.getRiflessi() >= minRiflessi
					&& cr.getTempra() >= minTempra);
			}
		}
		return false;
	}


	public String toString()
	{
		return "RegolaCampoResistenze#" + String.valueOf(getID());
	}


	public void setID(int value) {this.ID = value;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setMinArmatura(int value) {this.minArmatura = value;}
	public int getMinArmatura() {return minArmatura;}
	public void setMinVolonta(int value) {this.minVolonta = value;}
	public int getMinVolonta() {return minVolonta;}
	public void setMinTempra(int value) {this.minTempra = value;}
	public int getMinTempra() {return minTempra;}
	public void setMinRiflessi(int value) {this.minRiflessi = value;}
	public int getMinRiflessi() {return minRiflessi;}
	public void setNomeCampo(String value) {this.nomeCampo = value;}
	public String getNomeCampo() {return nomeCampo;}
	public void setRegolaComposite(RegolaComposite value) {this.regolaComposite = value;}
	public RegolaComposite getRegolaComposite() {return regolaComposite;}
}
