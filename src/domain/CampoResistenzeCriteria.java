/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CampoResistenzeCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression classeArmatura;
	public final IntegerExpression riflessi;
	public final IntegerExpression tempra;
	public final IntegerExpression volonta;
	public final StringExpression nome;
	public final IntegerExpression schedaId;
	public final AssociationExpression scheda;
	
	public CampoResistenzeCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		classeArmatura = new IntegerExpression("classeArmatura", this);
		riflessi = new IntegerExpression("riflessi", this);
		tempra = new IntegerExpression("tempra", this);
		volonta = new IntegerExpression("volonta", this);
		nome = new StringExpression("nome", this);
		schedaId = new IntegerExpression("scheda.ID", this);
		scheda = new AssociationExpression("scheda", this);
	}
	
	public CampoResistenzeCriteria(PersistentSession session) {
		this(session.createCriteria(CampoResistenze.class));
	}
	
	public CampoResistenzeCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public SchedaCriteria createSchedaCriteria() {
		return new SchedaCriteria(createCriteria("scheda"));
	}
	
	public CampoResistenze uniqueCampoResistenze() {
		return (CampoResistenze) super.uniqueResult();
	}
	
	public CampoResistenze[] listCampoResistenze() {
		java.util.List list = super.list();
		return (CampoResistenze[]) list.toArray(new CampoResistenze[list.size()]);
	}
}

