package domain;

import ManaGDR_RemoteInterface.*;


public class RegolaCampoNumerico extends RegolaCampo
{
	private int ID;	
	private int limiteSuperiore;	
	private int limiteInferiore;	
	private String nomeCampo;	
	private RegolaComposite regolaComposite;
	
        
	public RegolaCampoNumerico() {}
	
        
	public RegolaCampoNumerico( String nomeCampo, int inf, int sup )
	{
		limiteInferiore = inf;
		limiteSuperiore = sup;
		this.nomeCampo = nomeCampo;
	}

	
	public boolean ApplicaRegolaSuScheda(Scheda s)
	{
		if (s.getChiaviCampi().contains(nomeCampo)) {
			Campo c = s.getCampo(nomeCampo);
			if (c instanceof CampoNumerico) {// Se è della classe giusta..
				CampoNumerico cn = (CampoNumerico) c;
				return (cn.getValore() <= limiteInferiore && cn.getValore() >= limiteSuperiore);
			}
		}
		return false;
	}
	
	
	public String toString()
	{
		return "RegolaCampoNumerico#" + String.valueOf(getID());
	}
	
	
	public void setID(int value) {this.ID = value;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setLimiteSuperiore(int value) {this.limiteSuperiore = value;}
	public int getLimiteSuperiore() {return limiteSuperiore;}
	public void setLimiteInferiore(int value) {this.limiteInferiore = value;}
	public int getLimiteInferiore() {return limiteInferiore;}
	public void setNomeCampo(String value) {this.nomeCampo = value;}
	public String getNomeCampo() {return nomeCampo;}
	public void setRegolaComposite(RegolaComposite value) {this.regolaComposite = value;}
	public RegolaComposite getRegolaComposite() {return regolaComposite;}
}
