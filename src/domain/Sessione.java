package domain;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.rmi.RemoteException;

import ManaGDR_RemoteInterface.*;


public class Sessione implements ISessione
{
	private int ID;
	private Giocatore gm;
	private Mappa mappa;
	private String descr;
	private StatoSessione stato;
	private ID idSessione;
	private ArrayList<ModuloDiRegolamento> moduloRegolamento = new java.util.ArrayList<>();
	private ArrayList<Evento> eventi = new java.util.ArrayList<>();
	private ArrayList<Scheda> schede = new java.util.ArrayList<>();


	public Sessione()
	{
		idSessione = new ID(0);
	}


	public Sessione(Giocatore gm)
	{
		super();
		this.stato = StatoSessione.sessioneInattiva;
		this.gm = gm;
	}


	public void setID(int value)
	{
		this.ID = value;
		this.idSessione.setId(value);
	}


	public Set<IModuloDiRegolamento> getModuloRegolamento()
	{
		HashSet<IModuloDiRegolamento> tmp = new HashSet<>();
		tmp.addAll(this.moduloRegolamento);
		return tmp;
	}


	public Set<IEvento> getEventi()
	{
		HashSet<IEvento> tmp = new HashSet<>();
		tmp.addAll(this.eventi);
		return tmp;
	}


	public Set<IScheda> getSchede()
	{
		HashSet<IScheda> tmp = new HashSet<>();
		tmp.addAll(this.schede);
		return tmp;
	}


	public void ottieniITuoiDati()
	{
		// Caso d'uso di avviamento
		/*  Possibilità di inserimento del pattern proxy per recuperare solo
			un proxy delle sessioni e in questo metodo recuperare l'oggetto
			reale.
		*/
	}


	public void associaSchedeLibere() throws RemoteException
	{
		for (IScheda schedaAttuale : this.schede)
		{
			if (schedaAttuale.getGiocatore() == null)
			{
				schedaAttuale.setGiocatore(this.gm);
				this.gm.aggiungiScheda(schedaAttuale);
				System.out.println(schedaAttuale.toString() + " non era associata. E' stata associata con " + this.gm.toString());
			}
		}
	}



	public ArrayList<Regola> ApplicaRegoleAScheda(IScheda s) throws RemoteException
	{
		ArrayList<Regola> blacklist_totale = new ArrayList<>();

		for (IModuloDiRegolamento m : moduloRegolamento)
			blacklist_totale.addAll(m.ApplicaSuScheda(s));

		return blacklist_totale;
	}


	// pattern GRASP - CREATOR
	public IScheda AggiungiSchedaVuota()
	{
		Scheda s = new Scheda();
		schede.add(s);
		return s;
	}


	public boolean RimuoviScheda(IScheda scheda)
	{
		return schede.remove(scheda);
	}


	public String toString()
	{
		return "Sessione#" + String.valueOf(getID());
	}

	public StatoSessione getStato() {return stato;}
	public void setStato(StatoSessione stato) {this.stato = stato;}
	public void setIdSessione(IID idScheda) {this.idSessione = (ID)idScheda;}
	public int getID() {return ID;}
	public int getIdSessione() {return getID();}
	public int getORMID() {return getID();}
	public void setDescr(String value) {this.descr = value;}
	public String getDescr() {return descr;}
	public void setMappa(IMappa value) {this.mappa = (Mappa)value;}
	public IMappa getMappa() {return mappa;}

	public void setModuloRegolamento(Set<IModuloDiRegolamento> value)
	{
		for( IModuloDiRegolamento im : value ) {
			moduloRegolamento.add( (ModuloDiRegolamento)im );
		}
	}

	public ArrayList<IModuloDiRegolamento> getModuloRegolamento_byList()
	{
		ArrayList<IModuloDiRegolamento> result = new ArrayList<>();
		for( IModuloDiRegolamento im : moduloRegolamento )
			result.add( im );
		return result;
	}

	public ArrayList<IEvento> getEventi_byList()
	{
		ArrayList<IEvento> result = new ArrayList<>();
		for( IEvento im : eventi )
			result.add( im );
		return result;
	}

	public void setEventi(Set<IEvento> value)
	{
		for( IEvento im : value ) {
			eventi.add( (Evento)im );
		}
	}

	public void setSchede(Set<IScheda> value)
	{
		for( IScheda im : value ){
			schede.add( (Scheda)im );
		}
	}

	public ArrayList<IScheda> getSchede_byList()
	{
		ArrayList<IScheda> result = new ArrayList<>();
		for( IScheda im : schede )
			result.add( im );
		return result;
	}

	public void setGm(IGiocatore value) {this.gm = (Giocatore)value;}
	public IGiocatore getGm() {return gm;}
	public IScheda getScheda(int index) {return schede.get(index);}
	public boolean isGM(IGiocatore g) {return g == gm;}
}
