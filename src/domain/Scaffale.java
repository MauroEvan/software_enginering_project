package domain;

import java.util.ArrayList;
import org.orm.PersistentException;
import java.io.Serializable;
import java.rmi.RemoteException;

import ManaGDR_RemoteInterface.*;


// pattern GRASP - CONTROLLER
// pattern SINGLETON
public class Scaffale implements IScaffale
{
	private static Scaffale handle;
	private ArrayList<Sessione> database = new ArrayList<>();


	protected Scaffale() {}


	public static IScaffale getInstance() throws RemoteException
	{
		if (handle == null)
			handle = new Scaffale();
		return handle;
	}


	public IElencoSessioni RecuperaSessioni(IGiocatore gmSessione) throws RemoteException
	{
		ElencoSessioni e = new ElencoSessioni();

		try {
			for (Sessione s : PersistenceFacade.getInstance().getSessioneDaGM((Giocatore)gmSessione) ) {
				s.setStato(StatoSessione.sessioneInattiva);
				e.add(s);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return e;
	}


	public ISessione SelezionaSessione(IID idSessione) throws RemoteException
	{
		ISessione s = getSessioneDaId(idSessione);
		try {
			s.ottieniITuoiDati();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}


	public ISessione getSessioneDaId(IID idSessione) throws RemoteException
	{
		Sessione s = null;
		try {
			s = PersistenceFacade.getInstance().getSessioneDaId((ID)idSessione);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
}
