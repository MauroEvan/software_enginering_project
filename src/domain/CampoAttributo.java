package domain;

import java.util.LinkedList;
import java.util.Random;
import ManaGDR_RemoteInterface.*;

public class CampoAttributo implements Campo
{
	private int ID;
	private String nome;
	private int valore;
	private int modificatore;
	private domain.Scheda scheda;

	
	public CampoAttributo() {}

	
	public CampoAttributo(String nomeAttributo, int valore)
	{
		this.nome = nomeAttributo;
		this.valore = valore;
		if ((valore - 10) > 0)
			this.modificatore = (valore - 10) / 2;
		else
			this.modificatore = ((valore - 10) / 2) - 1;
	}

	
	public CampoAttributo(String nomeAttributo)
	{
		this.nome = nomeAttributo;
		this.valore = Randomizer();

		if ((this.getValore() - 10) > 0)
			this.modificatore = (this.getValore() - 10) / 2;
		else
			this.modificatore = ((this.getValore() - 10) / 2) - 1;
	}


	private int Randomizer()
	{
		final int min = 1;
		final int max = 6;
		LinkedList<Integer> tiri = new LinkedList<Integer>();

		for (int i=0; i<4; i++)
		{
			Random rand = new Random();
			int tiroRandom = rand.nextInt((max - min) + 1) + min;
			tiri.add( tiroRandom );
		}
		
		tiri.sort(null);
		
		int totale = 0;
		for (int i = 0; i < 3; i++) {
			totale += tiri.pollLast();
		}
		return totale;
	}

	
	public String toString()
	{
		return this.getNome() + " - " + this.getValore() + " - " + this.getModificatore();
	}
	
	
	public void setID(int value) {this.ID = value;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setNome(String value) {this.nome = value;}
	public String getNome() {return nome;}
	public void setValore(int value) {this.valore = value;}
	public Integer getValore() {return valore;}
	public void setModificatore(int value) {this.modificatore = value;}
	public Integer getModificatore() {return modificatore;}
	public void setScheda(IScheda value) {this.scheda = (Scheda)value;}
	public Scheda getScheda() {return scheda;}
}
