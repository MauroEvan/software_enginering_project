package domain;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.orm.PersistentException;
import java.lang.reflect.*;

import ManaGDR_RemoteInterface.*;


// SINGLETON + FACADE
public class ManaGDRServerFacade extends UnicastRemoteObject implements RemoteInterface, Serializable
{
	private static ManaGDRServerFacade handle = null;


	public static ManaGDRServerFacade getInstance() throws RemoteException
	{
		if( handle==null )
			handle = new ManaGDRServerFacade();
		return handle;
	}


	protected ManaGDRServerFacade() throws RemoteException
	{
			super();
	}


	public String Test() throws RemoteException
	{
		return "Hello World!";
	}


	public IGiocatore getGiocatoreFromName(String nome) throws RemoteException
	{
		Giocatore g = null;

		try {
			g = PersistenceFacade.getInstance().getGiocatoreDalNome(nome);
		}
		catch (PersistentException ex) {
			System.err.println( "[SERVER] ERRORE: Richiesta per giocatore " + nome + " fallita." );
			ex.printStackTrace();
		}

		return g;
	}


	public Campo newCampoTestuale(String nome, String valore)
	{
		return new CampoTestuale(nome, valore);
	}


	public IID newID(int valore)
	{
		return new ID(valore);
	}


	public IScaffale getScaffale() throws RemoteException
	{
		return Scaffale.getInstance();
	}


	public ILobby getLobby() throws RemoteException
	{
		try  {
			return Lobby.getInstance();
		} catch (PersistentException ex) { ex.printStackTrace(); }

		return null;
	}


	public IModuloDiRegolamento newModuloRegolamento() throws RemoteException
	{
		return new ModuloDiRegolamento();
	}


	public RegolaComponent newRegolaComposite() throws RemoteException
	{
		RegolaComposite result = new RegolaComposite();
		result.setNome("NuovoSetRegole");
		return result;
	}


	public RegolaComponent AggiungiRegola( RegolaComponent c, Regola r ) throws RemoteException
	{
		RegolaComposite composite = (RegolaComposite)c;

		composite.addChild(r);

		Method m = null;
		try {
			m = composite.getClass().getMethod("get"+r.getClass().getSimpleName());
			java.util.Set set = (java.util.Set)m.invoke(composite);
			set.add(r);
			m = r.getClass().getMethod("setRegolaComposite", RegolaComposite.class );
			m.invoke(r, composite);
		} catch( Exception ex )
		{
			ex.printStackTrace();
			System.err.println( "Impossibile aggiungere regola al set giusto" );
		}
		return composite;
	}


	public Regola NuovaRegola( String classe ) throws RemoteException
	{
		// Tenta di ricavare la classe giusta
		Class classeRegola;
		try {
			classeRegola = Class.forName("domain."+classe);
			// Crea nuovo oggetto di quel tipo
			return (Regola)classeRegola.newInstance();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}


	public void InviaRegolamento( IModuloDiRegolamento m ) throws RemoteException
	{
		LibreriaRegolamenti.getInstance().InviaModulo((ModuloDiRegolamento)m);
	}


	public IModuloDiRegolamento AssociaRegolamento( IModuloDiRegolamento m, RegolaComponent r ) throws RemoteException
	{
		RegolaComposite temp = (RegolaComposite)r;
		temp.setModulo((ModuloDiRegolamento)m);
		m.setRegolamento(temp);
		return m;
	}
}
