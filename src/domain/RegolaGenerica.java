package domain;

import ManaGDR_RemoteInterface.*;


public class RegolaGenerica implements Regola
{
	private int ID;
	private String nomeRegola;
	private String testoRegola;
	private domain.RegolaComposite regolaComposite;

	public RegolaGenerica() {}


	public void setID(int value) {
		this.ID = value;
	}

	public int getID() {
		return ID;
	}

	public int getORMID() {
		return getID();
	}

	public void setNomeRegola(String value) {
		this.nomeRegola = value;
	}

	public String getNomeRegola() {
		return nomeRegola;
	}

	public void setTestoRegola(String value) {
		this.testoRegola = value;
	}

	public String getTestoRegola() {
		return testoRegola;
	}

	public void setRegolaComposite(domain.RegolaComposite value) {
		this.regolaComposite = value;
	}

	public domain.RegolaComposite getRegolaComposite() {
		return regolaComposite;
	}

	public String toString() {
		return String.valueOf(getID());
	}

	public boolean Applica(Object o)
	{
		return true;
	}

}
