/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ModuloDiRegolamentoCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression regolamentoId;
	public final AssociationExpression regolamento;
	
	public ModuloDiRegolamentoCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		regolamentoId = new IntegerExpression("regolamento.ID", this);
		regolamento = new AssociationExpression("regolamento", this);
	}
	
	public ModuloDiRegolamentoCriteria(PersistentSession session) {
		this(session.createCriteria(ModuloDiRegolamento.class));
	}
	
	public ModuloDiRegolamentoCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public RegolaCompositeCriteria createRegolamentoCriteria() {
		return new RegolaCompositeCriteria(createCriteria("regolamento"));
	}
	
	public ModuloDiRegolamento uniqueModuloDiRegolamento() {
		return (ModuloDiRegolamento) super.uniqueResult();
	}
	
	public ModuloDiRegolamento[] listModuloDiRegolamento() {
		java.util.List list = super.list();
		return (ModuloDiRegolamento[]) list.toArray(new ModuloDiRegolamento[list.size()]);
	}
}

