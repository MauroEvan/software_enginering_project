package domain;

public class AmministratoreRegolamento extends Giocatore
{
    private String accessLevel; // Tipo di credenziali


    public AmministratoreRegolamento()
    {
        accessLevel = "default";
    }


    public String getAccessLevel() {return accessLevel;}
    public void setAccessLevel(String accessLevel) {this.accessLevel = accessLevel;}
}
