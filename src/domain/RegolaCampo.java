package domain;

import ManaGDR_RemoteInterface.*;


public abstract class RegolaCampo implements Regola
{
	public boolean Applica(Object o)
	{
		if (o.getClass() == Scheda.class)
			return ApplicaRegolaSuScheda((Scheda) o);
		return true;
	}

	// Metodo specializzato dalle classi implementatrici
	public abstract boolean ApplicaRegolaSuScheda(Scheda s);
}
