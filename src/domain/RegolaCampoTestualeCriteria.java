/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegolaCampoTestualeCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final StringExpression token;
	public final StringExpression nomeCampo;
	public final IntegerExpression regolaCompositeId;
	public final AssociationExpression regolaComposite;
	
	public RegolaCampoTestualeCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		token = new StringExpression("token", this);
		nomeCampo = new StringExpression("nomeCampo", this);
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this);
		regolaComposite = new AssociationExpression("regolaComposite", this);
	}
	
	public RegolaCampoTestualeCriteria(PersistentSession session) {
		this(session.createCriteria(RegolaCampoTestuale.class));
	}
	
	public RegolaCampoTestualeCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public RegolaCompositeCriteria createRegolaCompositeCriteria() {
		return new RegolaCompositeCriteria(createCriteria("regolaComposite"));
	}
	
	public RegolaCampoTestuale uniqueRegolaCampoTestuale() {
		return (RegolaCampoTestuale) super.uniqueResult();
	}
	
	public RegolaCampoTestuale[] listRegolaCampoTestuale() {
		java.util.List list = super.list();
		return (RegolaCampoTestuale[]) list.toArray(new RegolaCampoTestuale[list.size()]);
	}
}

