/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegolaCampoResistenzeDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression minArmatura;
	public final IntegerExpression minVolonta;
	public final IntegerExpression minTempra;
	public final IntegerExpression minRiflessi;
	public final StringExpression nomeCampo;
	public final IntegerExpression regolaCompositeId;
	public final AssociationExpression regolaComposite;
	
	public RegolaCampoResistenzeDetachedCriteria() {
		super(domain.RegolaCampoResistenze.class, domain.RegolaCampoResistenzeCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		minArmatura = new IntegerExpression("minArmatura", this.getDetachedCriteria());
		minVolonta = new IntegerExpression("minVolonta", this.getDetachedCriteria());
		minTempra = new IntegerExpression("minTempra", this.getDetachedCriteria());
		minRiflessi = new IntegerExpression("minRiflessi", this.getDetachedCriteria());
		nomeCampo = new StringExpression("nomeCampo", this.getDetachedCriteria());
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this.getDetachedCriteria());
		regolaComposite = new AssociationExpression("regolaComposite", this.getDetachedCriteria());
	}
	
	public RegolaCampoResistenzeDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.RegolaCampoResistenzeCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		minArmatura = new IntegerExpression("minArmatura", this.getDetachedCriteria());
		minVolonta = new IntegerExpression("minVolonta", this.getDetachedCriteria());
		minTempra = new IntegerExpression("minTempra", this.getDetachedCriteria());
		minRiflessi = new IntegerExpression("minRiflessi", this.getDetachedCriteria());
		nomeCampo = new StringExpression("nomeCampo", this.getDetachedCriteria());
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this.getDetachedCriteria());
		regolaComposite = new AssociationExpression("regolaComposite", this.getDetachedCriteria());
	}
	
	public RegolaCompositeDetachedCriteria createRegolaCompositeCriteria() {
		return new RegolaCompositeDetachedCriteria(createCriteria("regolaComposite"));
	}
	
	public RegolaCampoResistenze uniqueRegolaCampoResistenze(PersistentSession session) {
		return (RegolaCampoResistenze) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public RegolaCampoResistenze[] listRegolaCampoResistenze(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (RegolaCampoResistenze[]) list.toArray(new RegolaCampoResistenze[list.size()]);
	}
}

