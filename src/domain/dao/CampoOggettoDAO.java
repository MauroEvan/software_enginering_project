/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import domain.*;

public interface CampoOggettoDAO {
	public CampoOggetto loadCampoOggettoByORMID(int ID) throws PersistentException;
	public CampoOggetto getCampoOggettoByORMID(int ID) throws PersistentException;
	public CampoOggetto loadCampoOggettoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoOggetto getCampoOggettoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoOggetto loadCampoOggettoByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoOggetto getCampoOggettoByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoOggetto loadCampoOggettoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoOggetto getCampoOggettoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoOggetto[] listCampoOggettoByQuery(String condition, String orderBy) throws PersistentException;
	public CampoOggetto[] listCampoOggettoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoOggetto(String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoOggetto(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoOggettoByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoOggettoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoOggetto[] listCampoOggettoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoOggetto[] listCampoOggettoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoOggetto(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoOggetto(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoOggettoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoOggettoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoOggetto loadCampoOggettoByQuery(String condition, String orderBy) throws PersistentException;
	public CampoOggetto loadCampoOggettoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoOggetto loadCampoOggettoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoOggetto loadCampoOggettoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoOggetto createCampoOggetto();
	public boolean save(domain.CampoOggetto campoOggetto) throws PersistentException;
	public boolean delete(domain.CampoOggetto campoOggetto) throws PersistentException;
	public boolean deleteAndDissociate(domain.CampoOggetto campoOggetto) throws PersistentException;
	public boolean deleteAndDissociate(domain.CampoOggetto campoOggetto, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(domain.CampoOggetto campoOggetto) throws PersistentException;
	public boolean evict(domain.CampoOggetto campoOggetto) throws PersistentException;
	public CampoOggetto loadCampoOggettoByCriteria(CampoOggettoCriteria campoOggettoCriteria);
	public CampoOggetto[] listCampoOggettoByCriteria(CampoOggettoCriteria campoOggettoCriteria);
}
