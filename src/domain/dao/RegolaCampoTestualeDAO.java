/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import domain.*;

public interface RegolaCampoTestualeDAO {
	public RegolaCampoTestuale loadRegolaCampoTestualeByORMID(int ID) throws PersistentException;
	public RegolaCampoTestuale getRegolaCampoTestualeByORMID(int ID) throws PersistentException;
	public RegolaCampoTestuale loadRegolaCampoTestualeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoTestuale getRegolaCampoTestualeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoTestuale loadRegolaCampoTestualeByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoTestuale getRegolaCampoTestualeByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoTestuale loadRegolaCampoTestualeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoTestuale getRegolaCampoTestualeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoTestuale[] listRegolaCampoTestualeByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoTestuale[] listRegolaCampoTestualeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoTestuale(String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoTestuale(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoTestualeByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoTestualeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoTestuale[] listRegolaCampoTestualeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoTestuale[] listRegolaCampoTestualeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoTestuale(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoTestuale(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoTestualeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoTestualeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoTestuale loadRegolaCampoTestualeByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoTestuale loadRegolaCampoTestualeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoTestuale loadRegolaCampoTestualeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoTestuale loadRegolaCampoTestualeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoTestuale createRegolaCampoTestuale();
	public boolean save(domain.RegolaCampoTestuale regolaCampoTestuale) throws PersistentException;
	public boolean delete(domain.RegolaCampoTestuale regolaCampoTestuale) throws PersistentException;
	public boolean deleteAndDissociate(domain.RegolaCampoTestuale regolaCampoTestuale) throws PersistentException;
	public boolean deleteAndDissociate(domain.RegolaCampoTestuale regolaCampoTestuale, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(domain.RegolaCampoTestuale regolaCampoTestuale) throws PersistentException;
	public boolean evict(domain.RegolaCampoTestuale regolaCampoTestuale) throws PersistentException;
	public RegolaCampoTestuale loadRegolaCampoTestualeByCriteria(RegolaCampoTestualeCriteria regolaCampoTestualeCriteria);
	public RegolaCampoTestuale[] listRegolaCampoTestualeByCriteria(RegolaCampoTestualeCriteria regolaCampoTestualeCriteria);
}
