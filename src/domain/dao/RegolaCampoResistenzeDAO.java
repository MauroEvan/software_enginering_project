/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import domain.*;

public interface RegolaCampoResistenzeDAO {
	public RegolaCampoResistenze loadRegolaCampoResistenzeByORMID(int ID) throws PersistentException;
	public RegolaCampoResistenze getRegolaCampoResistenzeByORMID(int ID) throws PersistentException;
	public RegolaCampoResistenze loadRegolaCampoResistenzeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoResistenze getRegolaCampoResistenzeByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoResistenze loadRegolaCampoResistenzeByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoResistenze getRegolaCampoResistenzeByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoResistenze loadRegolaCampoResistenzeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoResistenze getRegolaCampoResistenzeByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoResistenze[] listRegolaCampoResistenzeByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoResistenze[] listRegolaCampoResistenzeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoResistenze(String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoResistenze(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoResistenzeByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoResistenzeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoResistenze[] listRegolaCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoResistenze[] listRegolaCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoResistenze(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoResistenze(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoResistenze loadRegolaCampoResistenzeByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoResistenze loadRegolaCampoResistenzeByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoResistenze loadRegolaCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoResistenze loadRegolaCampoResistenzeByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoResistenze createRegolaCampoResistenze();
	public boolean save(domain.RegolaCampoResistenze regolaCampoResistenze) throws PersistentException;
	public boolean delete(domain.RegolaCampoResistenze regolaCampoResistenze) throws PersistentException;
	public boolean deleteAndDissociate(domain.RegolaCampoResistenze regolaCampoResistenze) throws PersistentException;
	public boolean deleteAndDissociate(domain.RegolaCampoResistenze regolaCampoResistenze, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(domain.RegolaCampoResistenze regolaCampoResistenze) throws PersistentException;
	public boolean evict(domain.RegolaCampoResistenze regolaCampoResistenze) throws PersistentException;
	public RegolaCampoResistenze loadRegolaCampoResistenzeByCriteria(RegolaCampoResistenzeCriteria regolaCampoResistenzeCriteria);
	public RegolaCampoResistenze[] listRegolaCampoResistenzeByCriteria(RegolaCampoResistenzeCriteria regolaCampoResistenzeCriteria);
}
