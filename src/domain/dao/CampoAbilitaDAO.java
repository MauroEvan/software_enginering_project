/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import domain.*;

public interface CampoAbilitaDAO {
	public CampoAbilita loadCampoAbilitaByORMID(int ID) throws PersistentException;
	public CampoAbilita getCampoAbilitaByORMID(int ID) throws PersistentException;
	public CampoAbilita loadCampoAbilitaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilita getCampoAbilitaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilita loadCampoAbilitaByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoAbilita getCampoAbilitaByORMID(PersistentSession session, int ID) throws PersistentException;
	public CampoAbilita loadCampoAbilitaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilita getCampoAbilitaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilita[] listCampoAbilitaByQuery(String condition, String orderBy) throws PersistentException;
	public CampoAbilita[] listCampoAbilitaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoAbilita(String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoAbilita(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoAbilitaByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoAbilitaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilita[] listCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoAbilita[] listCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryCampoAbilita(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryCampoAbilita(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilita loadCampoAbilitaByQuery(String condition, String orderBy) throws PersistentException;
	public CampoAbilita loadCampoAbilitaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilita loadCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public CampoAbilita loadCampoAbilitaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public CampoAbilita createCampoAbilita();
	public boolean save(domain.CampoAbilita campoAbilita) throws PersistentException;
	public boolean delete(domain.CampoAbilita campoAbilita) throws PersistentException;
	public boolean deleteAndDissociate(domain.CampoAbilita campoAbilita) throws PersistentException;
	public boolean deleteAndDissociate(domain.CampoAbilita campoAbilita, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(domain.CampoAbilita campoAbilita) throws PersistentException;
	public boolean evict(domain.CampoAbilita campoAbilita) throws PersistentException;
	public CampoAbilita loadCampoAbilitaByCriteria(CampoAbilitaCriteria campoAbilitaCriteria);
	public CampoAbilita[] listCampoAbilitaByCriteria(CampoAbilitaCriteria campoAbilitaCriteria);
}
