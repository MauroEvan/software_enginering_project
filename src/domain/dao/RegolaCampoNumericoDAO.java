/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import domain.*;

public interface RegolaCampoNumericoDAO {
	public RegolaCampoNumerico loadRegolaCampoNumericoByORMID(int ID) throws PersistentException;
	public RegolaCampoNumerico getRegolaCampoNumericoByORMID(int ID) throws PersistentException;
	public RegolaCampoNumerico loadRegolaCampoNumericoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoNumerico getRegolaCampoNumericoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoNumerico loadRegolaCampoNumericoByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoNumerico getRegolaCampoNumericoByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoNumerico loadRegolaCampoNumericoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoNumerico getRegolaCampoNumericoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoNumerico[] listRegolaCampoNumericoByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoNumerico[] listRegolaCampoNumericoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoNumerico(String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoNumerico(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoNumericoByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoNumericoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoNumerico[] listRegolaCampoNumericoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoNumerico[] listRegolaCampoNumericoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoNumerico(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoNumerico(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoNumericoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoNumericoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoNumerico loadRegolaCampoNumericoByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoNumerico loadRegolaCampoNumericoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoNumerico loadRegolaCampoNumericoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoNumerico loadRegolaCampoNumericoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoNumerico createRegolaCampoNumerico();
	public boolean save(domain.RegolaCampoNumerico regolaCampoNumerico) throws PersistentException;
	public boolean delete(domain.RegolaCampoNumerico regolaCampoNumerico) throws PersistentException;
	public boolean deleteAndDissociate(domain.RegolaCampoNumerico regolaCampoNumerico) throws PersistentException;
	public boolean deleteAndDissociate(domain.RegolaCampoNumerico regolaCampoNumerico, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(domain.RegolaCampoNumerico regolaCampoNumerico) throws PersistentException;
	public boolean evict(domain.RegolaCampoNumerico regolaCampoNumerico) throws PersistentException;
	public RegolaCampoNumerico loadRegolaCampoNumericoByCriteria(RegolaCampoNumericoCriteria regolaCampoNumericoCriteria);
	public RegolaCampoNumerico[] listRegolaCampoNumericoByCriteria(RegolaCampoNumericoCriteria regolaCampoNumericoCriteria);
}
