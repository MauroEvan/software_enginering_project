/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import domain.*;

public interface RegolaGenericaDAO {
	public RegolaGenerica loadRegolaGenericaByORMID(int ID) throws PersistentException;
	public RegolaGenerica getRegolaGenericaByORMID(int ID) throws PersistentException;
	public RegolaGenerica loadRegolaGenericaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaGenerica getRegolaGenericaByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaGenerica loadRegolaGenericaByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaGenerica getRegolaGenericaByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaGenerica loadRegolaGenericaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaGenerica getRegolaGenericaByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaGenerica[] listRegolaGenericaByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaGenerica[] listRegolaGenericaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaGenerica(String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaGenerica(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaGenericaByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaGenericaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaGenerica[] listRegolaGenericaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaGenerica[] listRegolaGenericaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaGenerica(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaGenerica(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaGenericaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaGenericaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaGenerica loadRegolaGenericaByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaGenerica loadRegolaGenericaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaGenerica loadRegolaGenericaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaGenerica loadRegolaGenericaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaGenerica createRegolaGenerica();
	public boolean save(domain.RegolaGenerica regolaGenerica) throws PersistentException;
	public boolean delete(domain.RegolaGenerica regolaGenerica) throws PersistentException;
	public boolean deleteAndDissociate(domain.RegolaGenerica regolaGenerica) throws PersistentException;
	public boolean deleteAndDissociate(domain.RegolaGenerica regolaGenerica, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(domain.RegolaGenerica regolaGenerica) throws PersistentException;
	public boolean evict(domain.RegolaGenerica regolaGenerica) throws PersistentException;
	public RegolaGenerica loadRegolaGenericaByCriteria(RegolaGenericaCriteria regolaGenericaCriteria);
	public RegolaGenerica[] listRegolaGenericaByCriteria(RegolaGenericaCriteria regolaGenericaCriteria);
}
