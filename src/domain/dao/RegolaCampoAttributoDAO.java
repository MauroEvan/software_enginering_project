/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain.dao;

import org.orm.*;
import org.hibernate.LockMode;
import domain.*;

public interface RegolaCampoAttributoDAO {
	public RegolaCampoAttributo loadRegolaCampoAttributoByORMID(int ID) throws PersistentException;
	public RegolaCampoAttributo getRegolaCampoAttributoByORMID(int ID) throws PersistentException;
	public RegolaCampoAttributo loadRegolaCampoAttributoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAttributo getRegolaCampoAttributoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAttributo loadRegolaCampoAttributoByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoAttributo getRegolaCampoAttributoByORMID(PersistentSession session, int ID) throws PersistentException;
	public RegolaCampoAttributo loadRegolaCampoAttributoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAttributo getRegolaCampoAttributoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAttributo[] listRegolaCampoAttributoByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoAttributo[] listRegolaCampoAttributoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoAttributo(String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoAttributo(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoAttributoByQuery(String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoAttributoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAttributo[] listRegolaCampoAttributoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoAttributo[] listRegolaCampoAttributoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.List queryRegolaCampoAttributo(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.List queryRegolaCampoAttributo(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoAttributoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public java.util.Iterator iterateRegolaCampoAttributoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAttributo loadRegolaCampoAttributoByQuery(String condition, String orderBy) throws PersistentException;
	public RegolaCampoAttributo loadRegolaCampoAttributoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAttributo loadRegolaCampoAttributoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException;
	public RegolaCampoAttributo loadRegolaCampoAttributoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException;
	public RegolaCampoAttributo createRegolaCampoAttributo();
	public boolean save(domain.RegolaCampoAttributo regolaCampoAttributo) throws PersistentException;
	public boolean delete(domain.RegolaCampoAttributo regolaCampoAttributo) throws PersistentException;
	public boolean deleteAndDissociate(domain.RegolaCampoAttributo regolaCampoAttributo) throws PersistentException;
	public boolean deleteAndDissociate(domain.RegolaCampoAttributo regolaCampoAttributo, org.orm.PersistentSession session) throws PersistentException;
	public boolean refresh(domain.RegolaCampoAttributo regolaCampoAttributo) throws PersistentException;
	public boolean evict(domain.RegolaCampoAttributo regolaCampoAttributo) throws PersistentException;
	public RegolaCampoAttributo loadRegolaCampoAttributoByCriteria(RegolaCampoAttributoCriteria regolaCampoAttributoCriteria);
	public RegolaCampoAttributo[] listRegolaCampoAttributoByCriteria(RegolaCampoAttributoCriteria regolaCampoAttributoCriteria);
}
