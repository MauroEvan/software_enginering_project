package domain;

import ManaGDR_RemoteInterface.*;


public class RegolaCampoAbilita extends RegolaCampo
{
	private int ID;	
	private String nomeCampo;
	private int sogliaValore;
	private int sogliaModificatore;
	private RegolaComposite regolaComposite;
	
        
	public RegolaCampoAbilita() {}
	
       
	public RegolaCampoAbilita( String nomeCampo, int sogliaValore, int sogliaModificatore )
	{
		this.sogliaValore = sogliaValore;
		this.nomeCampo = nomeCampo;
		this.sogliaModificatore = sogliaModificatore;
	}


	private boolean ControllaValore( CampoAbilita campo )
	{
		int val = campo.getValore();
		
		// (se è uguale a 0 lo ignoriamo
		if( val >= sogliaValore || val == 0 )
			return true;
		return false;
	}
	
	
	private boolean ControllaModificatore( CampoAbilita campo )
	{
		int mod = campo.getModificatore();
		
		// (se è uguale a 0 lo ignoriamo
		if( mod >= sogliaModificatore || mod == 0 )
			return true;
		return false;
	}
	
	
	public boolean ApplicaRegolaSuScheda(domain.Scheda s)
	{
		//Verifichiamo la presenza del campo
		if( s.getChiaviCampi().contains(nomeCampo) ) {
			Campo c = s.getCampo(nomeCampo);
			if( c instanceof CampoAbilita ) {// Se è della classe giusta..
				CampoAbilita ca = (CampoAbilita) c;
				return ControllaModificatore(ca) && ControllaValore(ca);
			}
		}
		return false;
	}
	
	
	public String toString()
	{
		return "RegolaAbilità#" + String.valueOf(getID());
	}
	
	
	public void setID(int value) {this.ID = value;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setNomeCampo(String value) {this.nomeCampo = value;}
	public String getNomeCampo() {return nomeCampo;}
	public void setSogliaValore(int value) {this.sogliaValore = value;}
	public int getSogliaValore() {return sogliaValore;}
	public void setSogliaModificatore(int value) {this.sogliaModificatore = value;}
	public int getSogliaModificatore() {return sogliaModificatore;}
	public void setRegolaComposite(domain.RegolaComposite value) {this.regolaComposite = value;	}
	public RegolaComposite getRegolaComposite() {return regolaComposite;}
}
