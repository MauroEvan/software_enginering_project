/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegolaCampoAbilitaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final StringExpression nomeCampo;
	public final IntegerExpression sogliaValore;
	public final IntegerExpression sogliaModificatore;
	public final IntegerExpression regolaCompositeId;
	public final AssociationExpression regolaComposite;
	
	public RegolaCampoAbilitaDetachedCriteria() {
		super(domain.RegolaCampoAbilita.class, domain.RegolaCampoAbilitaCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nomeCampo = new StringExpression("nomeCampo", this.getDetachedCriteria());
		sogliaValore = new IntegerExpression("sogliaValore", this.getDetachedCriteria());
		sogliaModificatore = new IntegerExpression("sogliaModificatore", this.getDetachedCriteria());
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this.getDetachedCriteria());
		regolaComposite = new AssociationExpression("regolaComposite", this.getDetachedCriteria());
	}
	
	public RegolaCampoAbilitaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.RegolaCampoAbilitaCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nomeCampo = new StringExpression("nomeCampo", this.getDetachedCriteria());
		sogliaValore = new IntegerExpression("sogliaValore", this.getDetachedCriteria());
		sogliaModificatore = new IntegerExpression("sogliaModificatore", this.getDetachedCriteria());
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this.getDetachedCriteria());
		regolaComposite = new AssociationExpression("regolaComposite", this.getDetachedCriteria());
	}
	
	public RegolaCompositeDetachedCriteria createRegolaCompositeCriteria() {
		return new RegolaCompositeDetachedCriteria(createCriteria("regolaComposite"));
	}
	
	public RegolaCampoAbilita uniqueRegolaCampoAbilita(PersistentSession session) {
		return (RegolaCampoAbilita) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public RegolaCampoAbilita[] listRegolaCampoAbilita(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (RegolaCampoAbilita[]) list.toArray(new RegolaCampoAbilita[list.size()]);
	}
}

