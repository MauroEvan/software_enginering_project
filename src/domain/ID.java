package domain;

public class ID implements ManaGDR_RemoteInterface.IID
{
	private int id;

	public int getId() {return id;}
	public void setId(int id) {this.id = id;}
	public ID() {this.id = 0;}
	public ID(int x) {this.id = x;}

	public String toString()
	{
		return "ID#" + id;
	}
}
