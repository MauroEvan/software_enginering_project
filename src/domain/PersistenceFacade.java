package domain;

import java.util.List;
import javax.persistence.PersistenceException;
import org.orm.PersistentException;


// SINGLETON + FACADE
public class PersistenceFacade
{
	private static PersistenceFacade handle;


	protected PersistenceFacade() {};


	public static PersistenceFacade getInstance()
	{
		if (handle == null)
			handle = new PersistenceFacade();
		return handle;
	}


	public void SalvaScheda( Scheda s ) throws PersistentException
	{
		DAOFactory.getDAOFactory().getSchedaDAO().save(s);
	}


	public void SalvaModulo( ModuloDiRegolamento m ) throws PersistentException
	{
		RegolaComposite setregole = (RegolaComposite)m.getRegolamento();

		DAOFactory.getDAOFactory().getModuloDiRegolamentoDAO().save(m);
		DAOFactory.getDAOFactory().getRegolaCompositeDAO().save(setregole);

		// Per ovviare a ciò che Hibernate ha tirato fuori...
		for( Object r : setregole.getRegolaCampoAbilità() ) {
			ManaGDRPersistentManager.instance().saveObject(r);
		}

		for( Object r : setregole.getRegolaCampoAttributo() ) {
			ManaGDRPersistentManager.instance().saveObject(r);
		}

		for( Object r : setregole.getRegolaCampoNumerico() ) {
			ManaGDRPersistentManager.instance().saveObject(r);
		}

		for( Object r : setregole.getRegolaCampoOggetto() ) {
			ManaGDRPersistentManager.instance().saveObject(r);
		}

		for( Object r : setregole.getRegolaCampoResistenze() ) {
			ManaGDRPersistentManager.instance().saveObject(r);
		}

		for( Object r : setregole.getRegolaCampoTestuale() ) {
			ManaGDRPersistentManager.instance().saveObject(r);
		}

		for( Object r : setregole.getRegolaGenerica() ) {
			ManaGDRPersistentManager.instance().saveObject(r);
		}
	}


	public Sessione[] getSessioneDaGM( Giocatore gm ) throws PersistentException
	{
		return DAOFactory.getDAOFactory().getSessioneDAO().listSessioneByQuery("gm=" + gm.getORMID(), null);
	}


	public Sessione getSessioneDaId( ID sessid ) throws PersistentException
	{
		return DAOFactory.getDAOFactory().getSessioneDAO().getSessioneByORMID(sessid.getId());
	}


	public Giocatore[] getGiocatoriLoggati() throws PersistentException
	{
		return DAOFactory.getDAOFactory().getGiocatoreDAO().listGiocatoreByQuery(null, null);
	}


	public Giocatore getGiocatoreDalNome(String nome) throws PersistentException
	{
		List<Giocatore> g = DAOFactory.getDAOFactory().getGiocatoreDAO().queryGiocatore(null, null);
		for( Giocatore i : g )
			if( i.getNomeGiocatore().equals(nome))
				return i;
		throw new PersistenceException("Giocatore non trovato");
		// Hibernate funziona bene... dicevano...
		//return (Giocatore)(DAOFactory.getDAOFactory().getGiocatoreDAO().queryGiocatore("NomeGiocatore=" + nome, null));
	}
}
