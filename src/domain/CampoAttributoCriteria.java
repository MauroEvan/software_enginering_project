/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CampoAttributoCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final StringExpression nome;
	public final IntegerExpression valore;
	public final IntegerExpression modificatore;
	public final IntegerExpression schedaId;
	public final AssociationExpression scheda;
	
	public CampoAttributoCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		nome = new StringExpression("nome", this);
		valore = new IntegerExpression("valore", this);
		modificatore = new IntegerExpression("modificatore", this);
		schedaId = new IntegerExpression("scheda.ID", this);
		scheda = new AssociationExpression("scheda", this);
	}
	
	public CampoAttributoCriteria(PersistentSession session) {
		this(session.createCriteria(CampoAttributo.class));
	}
	
	public CampoAttributoCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public SchedaCriteria createSchedaCriteria() {
		return new SchedaCriteria(createCriteria("scheda"));
	}
	
	public CampoAttributo uniqueCampoAttributo() {
		return (CampoAttributo) super.uniqueResult();
	}
	
	public CampoAttributo[] listCampoAttributo() {
		java.util.List list = super.list();
		return (CampoAttributo[]) list.toArray(new CampoAttributo[list.size()]);
	}
}

