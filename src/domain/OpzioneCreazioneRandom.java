package domain;

import java.util.ArrayList;
import ManaGDR_RemoteInterface.*;
import java.rmi.RemoteException;

public class OpzioneCreazioneRandom implements OpzioneCreazioneScheda
{
	private final String tipo = "random";


	public void RiempiScheda(IScheda s) throws RemoteException
	{
		ArrayList<Campo> campi = new ArrayList<Campo>();

		campi.add(new CampoTestuale("nome"));
		campi.add(new CampoTestuale("razza"));
		campi.add(new CampoTestuale("classe"));
		campi.add(new CampoTestuale("allineamento"));
		campi.add(new CampoTestuale("taglia"));
		campi.add(new CampoNumerico("ricchezza"));
		campi.add(new CampoNumerico("livello"));
		campi.add(new CampoNumerico("esperienza"));
		campi.add(new CampoAttributo("forza"));
		campi.add(new CampoAttributo("destrezza"));
		campi.add(new CampoAttributo("costituzione"));
		campi.add(new CampoAttributo("intelligenza"));
		campi.add(new CampoAttributo("saggezza"));
		campi.add(new CampoAttributo("carisma"));
		campi.add(new CampoResistenze(true));
		campi.add(new CampoOggetto("oggetto1", true));

		for (Campo c : campi)
			s.addCampo(c.getNome(), c);

		campi.clear();
		campi.add(new CampoAbilita("ab1", (CampoAttributo) s.getCampo("forza")));
		campi.add(new CampoAbilita("ab2", (CampoAttributo) s.getCampo("destrezza")));
		campi.add(new CampoAbilita("ab3", (CampoAttributo) s.getCampo("costituzione")));
		campi.add(new CampoAbilita("ab4", (CampoAttributo) s.getCampo("intelligenza")));
		campi.add(new CampoAbilita("ab5", (CampoAttributo) s.getCampo("saggezza")));
		campi.add(new CampoAbilita("ab6", (CampoAttributo) s.getCampo("carisma")));

		for (Campo c : campi)
			s.addCampo(c.getNome(), c);
	}


	public String getTipo() {return this.tipo;}
}
