package domain;

import java.lang.reflect.*;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Set;
import org.orm.PersistentException;
import ManaGDR_RemoteInterface.*;
import java.io.Serializable;
import java.rmi.RemoteException;
import org.hibernate.FlushMode;


// pattern GRASP - CONTROLLER
public class Tavolo implements ITavolo
{
	private ID idTavolo;
	private Sessione sessione;
	private HashSet<IGiocatore> giocatoriSeduti;


	public Tavolo() {giocatoriSeduti = new HashSet<>();}
	public IID getIdTavolo() throws RemoteException {return idTavolo;}
	public void setIdTavolo(IID idTavolo) throws RemoteException {this.idTavolo = (ID)idTavolo;}
	public ISessione getSessione() throws RemoteException {return sessione;}
	public void setSessione(ISessione sessione) throws RemoteException {this.sessione = (Sessione)sessione;}
	public HashSet<IGiocatore> getGiocatoriSeduti() throws RemoteException {return giocatoriSeduti;}


	// pattern GRASP - Information Expert
	public void AssociaScheda(IGiocatore giocatore, IScheda scheda) throws RemoteException
	{
		scheda.setGiocatore( giocatore );
		giocatore.aggiungiScheda( scheda );
	}


	public void mettiSessioneSulTavolo(ISessione nuovaSessione) throws RemoteException
	{
		this.sessione = (Sessione)nuovaSessione;

		// Assicuriamoci che non ci siano altri giocatori seduti
		this.giocatoriSeduti.clear();
		// Aggiungiamo il GM ai giocatori seduti della sessione messa sul tavolo
		this.giocatoriSeduti.add( nuovaSessione.getGm() );
	}


	public void AttivaSessione() throws RemoteException
	{
		this.sessione.associaSchedeLibere();
		this.sessione.setStato( StatoSessione.sessioneAttiva );
		System.out.println( "SESSIONE ATTIVATA\n" + "Tavolo:" + this.toString() + "\nSessione:" + this.sessione.toString() );
	}


	public void AggiungiGiocatore( IGiocatore g )
	{
		if( ! giocatoriSeduti.contains(g) )
			giocatoriSeduti.add( g );
	}


	public ArrayList<OpzioneCreazioneScheda> AvviaCreazioneScheda( IGiocatore g ) throws RemoteException
	{
		return FacadeConfigurazioni.getOpzioniCreazioneScheda( g, sessione );
	}


	// pattern GRASP - CREATOR
	public IScheda CreaNuovaScheda( OpzioneCreazioneScheda opz ) throws RemoteException
	{
		IScheda s = sessione.AggiungiSchedaVuota();
		opz.RiempiScheda( (Scheda)s );
		return s;
	}


	public boolean ValidaScheda( IScheda s ) throws RemoteException
	{
		ArrayList<Regola> blacklist_totale = new ArrayList<>();

		blacklist_totale = sessione.ApplicaRegoleAScheda( s );
		if (blacklist_totale.size() != 0) {
			sessione.RimuoviScheda(s);
			return false;
		}

		Set<String> chiavi = s.getChiaviCampi();

		Set<Campo> campi = new HashSet();
		for (String k : chiavi) {
			Campo c = s.getCampo(k);
			campi.add(c);
		}

		for (Campo c : campi) {
			c.setScheda(s);
			try {
				Method method = s.getClass().getMethod("add" + c.getClass().getName().substring(7), Campo.class);
				method.invoke(s, c);
			}
			catch (NoSuchMethodException ex) {
				System.out.println("NO METHOD!!" + ": add" + c.getClass().getName().substring(7));
			} catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {}
		}

		try {
			PersistenceFacade.getInstance().SalvaScheda((Scheda)s);
		} catch (PersistentException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
