/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class GiocatoreDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final StringExpression nomeGiocatore;
	
	public GiocatoreDetachedCriteria() {
		super(domain.Giocatore.class, domain.GiocatoreCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nomeGiocatore = new StringExpression("nomeGiocatore", this.getDetachedCriteria());
	}
	
	public GiocatoreDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.GiocatoreCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nomeGiocatore = new StringExpression("nomeGiocatore", this.getDetachedCriteria());
	}
	
	public Giocatore uniqueGiocatore(PersistentSession session) {
		return (Giocatore) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Giocatore[] listGiocatore(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Giocatore[]) list.toArray(new Giocatore[list.size()]);
	}
}

