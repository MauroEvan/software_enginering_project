package domain;

import java.util.ArrayList;
import ManaGDR_RemoteInterface.*;


public class ElencoSessioni implements IElencoSessioni
{
	private ArrayList<Sessione> sessioni = new ArrayList<>();

	
	public ISessione at( int index ) throws IndexOutOfBoundsException
	{
		if( index >= sessioni.size() )
			throw new IndexOutOfBoundsException();
		return this.sessioni.get( index );
	}

	
	public boolean add( ISessione s ) {return sessioni.add( (Sessione)s );}
	public int getSize() {return sessioni.size();}
}
