package domain;

import java.util.ArrayList;
import ManaGDR_RemoteInterface.*;

import java.rmi.RemoteException;


public class ModuloDiRegolamento implements IModuloDiRegolamento
{
	private int ID;
	private RegolaComposite regolamento;
	private ID idModulo;


	public ModuloDiRegolamento() {}


	public void setID(int value)
	{
		this.ID = value;
		this.setIdModulo(new ID(value));
	}


	public ArrayList<Regola> ApplicaSuScheda(IScheda s) throws RemoteException
	{
		ArrayList<Regola> blacklist = new ArrayList<>(); // Regole che falliscono

		// PATTERN ITERATOR
		RegolaIterator it = new RegolaIterator(regolamento.iterator());
		while( it.hasNext() ) {
			RegolaComponent a = it.next();
			if (a instanceof Regola) {
				if (a.Applica(s) == false)
					blacklist.add((Regola) a);
			}
		}

		return blacklist;
	}


	public String toString()
	{
		String infoModulo = "Modulo[ID]: " + getID() +
							"Modulo[Regolamento]: " + this.regolamento.toString();
		return infoModulo;

	}

	public boolean Verifica()
	{
		// Verifica la conformità del modulo di regolamento
		// Caso d'uso di avviamento: accetta tutto
		return true;
	}


	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setRegolamento(RegolaComponent value) {this.regolamento = (RegolaComposite)value;}
	public RegolaComponent getRegolamento() {return regolamento;}
	public IID getIdModulo() {return idModulo;}
	public void setIdModulo(IID idModulo) {this.idModulo = (ID)idModulo;}
}
