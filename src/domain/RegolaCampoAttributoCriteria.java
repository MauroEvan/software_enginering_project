/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegolaCampoAttributoCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final StringExpression nomeCampo;
	public final IntegerExpression sogliaValore;
	public final IntegerExpression sogliaModificatore;
	public final IntegerExpression regolaCompositeId;
	public final AssociationExpression regolaComposite;
	
	public RegolaCampoAttributoCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		nomeCampo = new StringExpression("nomeCampo", this);
		sogliaValore = new IntegerExpression("sogliaValore", this);
		sogliaModificatore = new IntegerExpression("sogliaModificatore", this);
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this);
		regolaComposite = new AssociationExpression("regolaComposite", this);
	}
	
	public RegolaCampoAttributoCriteria(PersistentSession session) {
		this(session.createCriteria(RegolaCampoAttributo.class));
	}
	
	public RegolaCampoAttributoCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public RegolaCompositeCriteria createRegolaCompositeCriteria() {
		return new RegolaCompositeCriteria(createCriteria("regolaComposite"));
	}
	
	public RegolaCampoAttributo uniqueRegolaCampoAttributo() {
		return (RegolaCampoAttributo) super.uniqueResult();
	}
	
	public RegolaCampoAttributo[] listRegolaCampoAttributo() {
		java.util.List list = super.list();
		return (RegolaCampoAttributo[]) list.toArray(new RegolaCampoAttributo[list.size()]);
	}
}

