/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CampoTestualeDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final StringExpression nome;
	public final StringExpression valore;
	public final IntegerExpression schedaId;
	public final AssociationExpression scheda;
	
	public CampoTestualeDetachedCriteria() {
		super(domain.CampoTestuale.class, domain.CampoTestualeCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		valore = new StringExpression("valore", this.getDetachedCriteria());
		schedaId = new IntegerExpression("scheda.ID", this.getDetachedCriteria());
		scheda = new AssociationExpression("scheda", this.getDetachedCriteria());
	}
	
	public CampoTestualeDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.CampoTestualeCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		valore = new StringExpression("valore", this.getDetachedCriteria());
		schedaId = new IntegerExpression("scheda.ID", this.getDetachedCriteria());
		scheda = new AssociationExpression("scheda", this.getDetachedCriteria());
	}
	
	public SchedaDetachedCriteria createSchedaCriteria() {
		return new SchedaDetachedCriteria(createCriteria("scheda"));
	}
	
	public CampoTestuale uniqueCampoTestuale(PersistentSession session) {
		return (CampoTestuale) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public CampoTestuale[] listCampoTestuale(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (CampoTestuale[]) list.toArray(new CampoTestuale[list.size()]);
	}
}

