/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegolaCompositeCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final StringExpression nome;
	public final IntegerExpression moduloId;
	public final AssociationExpression modulo;
	public final CollectionExpression regolaGenerica;
	public final CollectionExpression regolaCampoNumerico;
	public final CollectionExpression regolaCampoResistenze;
	public final CollectionExpression regolaCampoAttributo;
	public final CollectionExpression regolaCampoAbilità;
	public final CollectionExpression regolaCampoOggetto;
	public final CollectionExpression regolaCampoTestuale;
	
	public RegolaCompositeCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		nome = new StringExpression("nome", this);
		moduloId = new IntegerExpression("modulo.ID", this);
		modulo = new AssociationExpression("modulo", this);
		regolaGenerica = new CollectionExpression("regolaGenerica", this);
		regolaCampoNumerico = new CollectionExpression("regolaCampoNumerico", this);
		regolaCampoResistenze = new CollectionExpression("regolaCampoResistenze", this);
		regolaCampoAttributo = new CollectionExpression("regolaCampoAttributo", this);
		regolaCampoAbilità = new CollectionExpression("regolaCampoAbilità", this);
		regolaCampoOggetto = new CollectionExpression("regolaCampoOggetto", this);
		regolaCampoTestuale = new CollectionExpression("regolaCampoTestuale", this);
	}
	
	public RegolaCompositeCriteria(PersistentSession session) {
		this(session.createCriteria(RegolaComposite.class));
	}
	
	public RegolaCompositeCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public ModuloDiRegolamentoCriteria createModuloCriteria() {
		return new ModuloDiRegolamentoCriteria(createCriteria("modulo"));
	}
	
	public RegolaGenericaCriteria createRegolaGenericaCriteria() {
		return new RegolaGenericaCriteria(createCriteria("regolaGenerica"));
	}
	
	public RegolaCampoNumericoCriteria createRegolaCampoNumericoCriteria() {
		return new RegolaCampoNumericoCriteria(createCriteria("regolaCampoNumerico"));
	}
	
	public RegolaCampoResistenzeCriteria createRegolaCampoResistenzeCriteria() {
		return new RegolaCampoResistenzeCriteria(createCriteria("regolaCampoResistenze"));
	}
	
	public RegolaCampoAttributoCriteria createRegolaCampoAttributoCriteria() {
		return new RegolaCampoAttributoCriteria(createCriteria("regolaCampoAttributo"));
	}
	
	public RegolaCampoAbilitaCriteria createRegolaCampoAbilitàCriteria() {
		return new RegolaCampoAbilitaCriteria(createCriteria("regolaCampoAbilità"));
	}
	
	public RegolaCampoOggettoCriteria createRegolaCampoOggettoCriteria() {
		return new RegolaCampoOggettoCriteria(createCriteria("regolaCampoOggetto"));
	}
	
	public RegolaCampoTestualeCriteria createRegolaCampoTestualeCriteria() {
		return new RegolaCampoTestualeCriteria(createCriteria("regolaCampoTestuale"));
	}
	
	public RegolaComposite uniqueRegolaComposite() {
		return (RegolaComposite) super.uniqueResult();
	}
	
	public RegolaComposite[] listRegolaComposite() {
		java.util.List list = super.list();
		return (RegolaComposite[]) list.toArray(new RegolaComposite[list.size()]);
	}
}

