package domain;

import ManaGDR_RemoteInterface.*;

public class Evento implements IEvento
{
	private int ID;
	private ID idEvento;

	
	public Evento() {}

	
	public void setID(int value)
	{
		this.ID = value;
		this.setIdEvento(new ID(value));
	}

	
	public String toString()
	{
		return "Evento#" + String.valueOf(getID());
	}

	
	public IID getIdEvento() {return this.idEvento;}
	public void setIdEvento(IID idEvento) {this.idEvento = (ID)idEvento;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
}
