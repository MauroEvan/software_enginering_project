/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegolaGenericaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final StringExpression nomeRegola;
	public final StringExpression testoRegola;
	public final IntegerExpression regolaCompositeId;
	public final AssociationExpression regolaComposite;
	
	public RegolaGenericaDetachedCriteria() {
		super(domain.RegolaGenerica.class, domain.RegolaGenericaCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nomeRegola = new StringExpression("nomeRegola", this.getDetachedCriteria());
		testoRegola = new StringExpression("testoRegola", this.getDetachedCriteria());
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this.getDetachedCriteria());
		regolaComposite = new AssociationExpression("regolaComposite", this.getDetachedCriteria());
	}
	
	public RegolaGenericaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.RegolaGenericaCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nomeRegola = new StringExpression("nomeRegola", this.getDetachedCriteria());
		testoRegola = new StringExpression("testoRegola", this.getDetachedCriteria());
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this.getDetachedCriteria());
		regolaComposite = new AssociationExpression("regolaComposite", this.getDetachedCriteria());
	}
	
	public RegolaCompositeDetachedCriteria createRegolaCompositeCriteria() {
		return new RegolaCompositeDetachedCriteria(createCriteria("regolaComposite"));
	}
	
	public RegolaGenerica uniqueRegolaGenerica(PersistentSession session) {
		return (RegolaGenerica) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public RegolaGenerica[] listRegolaGenerica(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (RegolaGenerica[]) list.toArray(new RegolaGenerica[list.size()]);
	}
}

