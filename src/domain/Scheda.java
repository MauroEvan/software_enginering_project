package domain;

import java.util.HashMap;
import java.util.Set;

import ManaGDR_RemoteInterface.*;


public class Scheda implements IScheda
{
	private int ID;
	private Giocatore giocatore;
	private ID idScheda;
	private HashMap<String, Campo> campi = new HashMap<>();
	private Set campoResistenze = new java.util.HashSet();
	private Set campoAbilità = new java.util.HashSet();
	private Set campoTestuale = new java.util.HashSet();
	private Set campoNumerico = new java.util.HashSet();
	private Set campoOggetto = new java.util.HashSet();
	private Set campoAttributo = new java.util.HashSet();


	public Scheda()
	{
		this.idScheda = new ID();
	}


	public void setID(int value)
	{
		this.ID = value;
		this.idScheda.setId(value);
	}


	public String toString()
	{
		return "Scheda#" + idScheda.toString();
	}

	public ID getIdScheda() {return idScheda;}
	public void setIdScheda(IID idScheda) {this.idScheda = (ID)idScheda;}
	public IGiocatore getGiocatore() {return giocatore;}
	public void setGiocatore(IGiocatore giocatore) {this.giocatore = (Giocatore)giocatore;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setCampoResistenze(Set value) {this.campoResistenze = value;}
	public Set getCampoResistenze() {return campoResistenze;}
	public void setCampoAbilità(Set value) {this.campoAbilità = value;}
	public Set getCampoAbilità() {return campoAbilità;}
	public void setCampoTestuale(Set value) {this.campoTestuale = value;}
	public Set getCampoTestuale() {return campoTestuale;}
	public void setCampoNumerico(Set value) {this.campoNumerico = value;}
	public Set getCampoNumerico() {return campoNumerico;}
	public void setCampoOggetto(Set value) {this.campoOggetto = value;}
	public Set getCampoOggetto() {return campoOggetto;}
	public void setCampoAttributo(Set value) {this.campoAttributo = value;}
	public Set getCampoAttributo() {return campoAttributo;}
	public void addCampo(String key, Campo c) {campi.put(key, c);}

	public Campo getCampo(String key)
	{
		Campo c = null;

		try {
			c = campi.get(key);
		} catch( Exception ex ) {
			System.err.println( "[Scheda] Errore recumpero campo con chiave " + key );
			ex.printStackTrace();
		}
		return c;
	}

	public Set<String> getChiaviCampi() {return campi.keySet();}
	public void addCampoResistenze(Campo value) {this.campoResistenze.add(value);}
	public void addCampoAbilita(Campo value) {this.campoAbilità.add(value);}
	public void addCampoTestuale(Campo value) {this.campoTestuale.add(value);}
	public void addCampoNumerico(Campo value) {this.campoNumerico.add(value);}
	public void addCampoOggetto(Campo value) {this.campoOggetto.add(value);}
	public void addCampoAttributo(Campo value) {this.campoAttributo.add(value);}
}
