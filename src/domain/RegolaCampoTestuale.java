package domain;

import ManaGDR_RemoteInterface.*;


public class RegolaCampoTestuale extends RegolaCampo
{
	private int ID;
	private String token;
	private String nomeCampo;
	private domain.RegolaComposite regolaComposite;


	public RegolaCampoTestuale() {}


	public RegolaCampoTestuale( String nomeCampo, String token )
	{
		this.nomeCampo = nomeCampo;
		this.token = token;
	}

	public boolean ApplicaRegolaSuScheda(domain.Scheda s) {
            if( s.getChiaviCampi().contains(nomeCampo) )
		{
			Campo c = s.getCampo(nomeCampo);

			if( c instanceof CampoTestuale )
			{
				CampoTestuale ct = (CampoTestuale)c;
				return ct.getValore().contains(token);
			}
		}
		return false;
	}


	public String toString()
	{
		return "RegolaCampoTestuale#" + String.valueOf(getID());
	}


	public void setID(int value) {this.ID = value;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setToken(String value) {this.token = value;}
	public String getToken() {return token;	}
	public void setNomeCampo(String value) {this.nomeCampo = value;}
	public String getNomeCampo() {return nomeCampo;	}
	public void setRegolaComposite(domain.RegolaComposite value) {this.regolaComposite = value;}
	public domain.RegolaComposite getRegolaComposite() {return regolaComposite;}
}
