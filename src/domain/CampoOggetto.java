package domain;

import java.util.Random;
import ManaGDR_RemoteInterface.*;

public class CampoOggetto implements Campo
{
	private int ID;
	private String nome;
	private int quantità;
	private Scheda scheda;
	

	public CampoOggetto() {}

	
	public CampoOggetto(String nomeOggetto, boolean random)
	{
		this.nome = nomeOggetto;
		if (random)
			this.quantità = Randomizer();
		else
			this.quantità = 0;
	}

	
	public CampoOggetto(String nomeOggetto, int quantita)
	{
		this.nome = nomeOggetto;
		this.quantità = quantita;
	}

	
	private int Randomizer()
	{
		final int min = 1;
		final int max = 6;
		
		Random rand = new Random();
		
		return rand.nextInt((max - min) + 1) + min;
	}

	
	public String toString()
	{
		return this.getNome() + " " + this.getQuantità();
	}
	
	
	public void setID(int value) {this.ID = value;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setNome(String value) {this.nome = value;}
	public String getNome() {return nome;}
	public void setQuantità(int value) {this.quantità = value;}
	public int getQuantità() {return quantità;}
	public void setScheda(IScheda value) {this.scheda = (Scheda)value;}
	public Scheda getScheda() {return scheda;}
	public Object getValore() {return null;}
	public Integer getModificatore() {return null;}
}
