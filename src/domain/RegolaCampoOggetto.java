package domain;

import ManaGDR_RemoteInterface.*;


public class RegolaCampoOggetto extends RegolaCampo
{
	private int ID;
	private int maxQuantità;
	private String nomeCampo;
	private RegolaComposite regolaComposite;

	
	public RegolaCampoOggetto() {}

	
	public RegolaCampoOggetto( String nomeCampo, int quantita )
	{
		maxQuantità = quantita;
		this.nomeCampo = nomeCampo;
	} 
	
	
	public boolean ApplicaRegolaSuScheda(Scheda s)
	{
		if (s.getChiaviCampi().contains(nomeCampo)) {
			Campo c = s.getCampo(nomeCampo);
			if (c instanceof CampoOggetto) {// Se è della classe giusta..
				CampoOggetto co = (CampoOggetto) c;
				return (co.getQuantità() < maxQuantità);
			}
		}
		return false;
	}
	
	
	public String toString()
	{
		return "RegolaCampoOggetto#" + String.valueOf(getID());
	}
	
	
	public void setID(int value) {this.ID = value;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setMaxQuantità(int value) {this.maxQuantità = value;}
	public int getMaxQuantità() {return maxQuantità;}
	public void setNomeCampo(String value) {this.nomeCampo = value;}
	public String getNomeCampo() {return nomeCampo;}
	public void setRegolaComposite(RegolaComposite value) {this.regolaComposite = value;}
	public RegolaComposite getRegolaComposite() {return regolaComposite;}
}
