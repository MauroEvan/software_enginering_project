/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class SchedaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final CollectionExpression campoResistenze;
	public final CollectionExpression campoAbilità;
	public final CollectionExpression campoTestuale;
	public final CollectionExpression campoNumerico;
	public final CollectionExpression campoOggetto;
	public final CollectionExpression campoAttributo;
	
	public SchedaDetachedCriteria() {
		super(domain.Scheda.class, domain.SchedaCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		campoResistenze = new CollectionExpression("campoResistenze", this.getDetachedCriteria());
		campoAbilità = new CollectionExpression("campoAbilità", this.getDetachedCriteria());
		campoTestuale = new CollectionExpression("campoTestuale", this.getDetachedCriteria());
		campoNumerico = new CollectionExpression("campoNumerico", this.getDetachedCriteria());
		campoOggetto = new CollectionExpression("campoOggetto", this.getDetachedCriteria());
		campoAttributo = new CollectionExpression("campoAttributo", this.getDetachedCriteria());
	}
	
	public SchedaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.SchedaCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		campoResistenze = new CollectionExpression("campoResistenze", this.getDetachedCriteria());
		campoAbilità = new CollectionExpression("campoAbilità", this.getDetachedCriteria());
		campoTestuale = new CollectionExpression("campoTestuale", this.getDetachedCriteria());
		campoNumerico = new CollectionExpression("campoNumerico", this.getDetachedCriteria());
		campoOggetto = new CollectionExpression("campoOggetto", this.getDetachedCriteria());
		campoAttributo = new CollectionExpression("campoAttributo", this.getDetachedCriteria());
	}
	
	public CampoResistenzeDetachedCriteria createCampoResistenzeCriteria() {
		return new CampoResistenzeDetachedCriteria(createCriteria("campoResistenze"));
	}
	
	public CampoAbilitaDetachedCriteria createCampoAbilitàCriteria() {
		return new CampoAbilitaDetachedCriteria(createCriteria("campoAbilità"));
	}
	
	public CampoTestualeDetachedCriteria createCampoTestualeCriteria() {
		return new CampoTestualeDetachedCriteria(createCriteria("campoTestuale"));
	}
	
	public CampoNumericoDetachedCriteria createCampoNumericoCriteria() {
		return new CampoNumericoDetachedCriteria(createCriteria("campoNumerico"));
	}
	
	public CampoOggettoDetachedCriteria createCampoOggettoCriteria() {
		return new CampoOggettoDetachedCriteria(createCriteria("campoOggetto"));
	}
	
	public CampoAttributoDetachedCriteria createCampoAttributoCriteria() {
		return new CampoAttributoDetachedCriteria(createCriteria("campoAttributo"));
	}
	
	public Scheda uniqueScheda(PersistentSession session) {
		return (Scheda) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Scheda[] listScheda(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Scheda[]) list.toArray(new Scheda[list.size()]);
	}
}

