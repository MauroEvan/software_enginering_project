/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ModuloDiRegolamentoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression regolamentoId;
	public final AssociationExpression regolamento;
	
	public ModuloDiRegolamentoDetachedCriteria() {
		super(domain.ModuloDiRegolamento.class, domain.ModuloDiRegolamentoCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		regolamentoId = new IntegerExpression("regolamento.ID", this.getDetachedCriteria());
		regolamento = new AssociationExpression("regolamento", this.getDetachedCriteria());
	}
	
	public ModuloDiRegolamentoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.ModuloDiRegolamentoCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		regolamentoId = new IntegerExpression("regolamento.ID", this.getDetachedCriteria());
		regolamento = new AssociationExpression("regolamento", this.getDetachedCriteria());
	}
	
	public RegolaCompositeDetachedCriteria createRegolamentoCriteria() {
		return new RegolaCompositeDetachedCriteria(createCriteria("regolamento"));
	}
	
	public ModuloDiRegolamento uniqueModuloDiRegolamento(PersistentSession session) {
		return (ModuloDiRegolamento) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public ModuloDiRegolamento[] listModuloDiRegolamento(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (ModuloDiRegolamento[]) list.toArray(new ModuloDiRegolamento[list.size()]);
	}
}

