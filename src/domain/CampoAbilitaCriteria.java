/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CampoAbilitaCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final StringExpression nome;
	public final IntegerExpression valore;
	public final StringExpression attributoChiave;
	public final IntegerExpression modificatore;
	public final BooleanExpression diClasse;
	public final IntegerExpression schedaId;
	public final AssociationExpression scheda;
	
	public CampoAbilitaCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		nome = new StringExpression("nome", this);
		valore = new IntegerExpression("valore", this);
		attributoChiave = new StringExpression("attributoChiave", this);
		modificatore = new IntegerExpression("modificatore", this);
		diClasse = new BooleanExpression("diClasse", this);
		schedaId = new IntegerExpression("scheda.ID", this);
		scheda = new AssociationExpression("scheda", this);
	}
	
	public CampoAbilitaCriteria(PersistentSession session) {
		this(session.createCriteria(CampoAbilita.class));
	}
	
	public CampoAbilitaCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public SchedaCriteria createSchedaCriteria() {
		return new SchedaCriteria(createCriteria("scheda"));
	}
	
	public CampoAbilita uniqueCampoAbilita() {
		return (CampoAbilita) super.uniqueResult();
	}
	
	public CampoAbilita[] listCampoAbilita() {
		java.util.List list = super.list();
		return (CampoAbilita[]) list.toArray(new CampoAbilita[list.size()]);
	}
}

