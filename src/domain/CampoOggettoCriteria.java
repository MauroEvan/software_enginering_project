/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CampoOggettoCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final StringExpression nome;
	public final IntegerExpression quantità;
	public final IntegerExpression schedaId;
	public final AssociationExpression scheda;
	
	public CampoOggettoCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		nome = new StringExpression("nome", this);
		quantità = new IntegerExpression("quantità", this);
		schedaId = new IntegerExpression("scheda.ID", this);
		scheda = new AssociationExpression("scheda", this);
	}
	
	public CampoOggettoCriteria(PersistentSession session) {
		this(session.createCriteria(CampoOggetto.class));
	}
	
	public CampoOggettoCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public SchedaCriteria createSchedaCriteria() {
		return new SchedaCriteria(createCriteria("scheda"));
	}
	
	public CampoOggetto uniqueCampoOggetto() {
		return (CampoOggetto) super.uniqueResult();
	}
	
	public CampoOggetto[] listCampoOggetto() {
		java.util.List list = super.list();
		return (CampoOggetto[]) list.toArray(new CampoOggetto[list.size()]);
	}
}

