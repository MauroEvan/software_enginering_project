/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CampoAttributoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final StringExpression nome;
	public final IntegerExpression valore;
	public final IntegerExpression modificatore;
	public final IntegerExpression schedaId;
	public final AssociationExpression scheda;
	
	public CampoAttributoDetachedCriteria() {
		super(domain.CampoAttributo.class, domain.CampoAttributoCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		valore = new IntegerExpression("valore", this.getDetachedCriteria());
		modificatore = new IntegerExpression("modificatore", this.getDetachedCriteria());
		schedaId = new IntegerExpression("scheda.ID", this.getDetachedCriteria());
		scheda = new AssociationExpression("scheda", this.getDetachedCriteria());
	}
	
	public CampoAttributoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.CampoAttributoCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		valore = new IntegerExpression("valore", this.getDetachedCriteria());
		modificatore = new IntegerExpression("modificatore", this.getDetachedCriteria());
		schedaId = new IntegerExpression("scheda.ID", this.getDetachedCriteria());
		scheda = new AssociationExpression("scheda", this.getDetachedCriteria());
	}
	
	public SchedaDetachedCriteria createSchedaCriteria() {
		return new SchedaDetachedCriteria(createCriteria("scheda"));
	}
	
	public CampoAttributo uniqueCampoAttributo(PersistentSession session) {
		return (CampoAttributo) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public CampoAttributo[] listCampoAttributo(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (CampoAttributo[]) list.toArray(new CampoAttributo[list.size()]);
	}
}

