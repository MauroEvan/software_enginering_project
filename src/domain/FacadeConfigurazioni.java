package domain;

import java.util.ArrayList;
import java.util.HashMap;

import ManaGDR_RemoteInterface.*;

// FACADE + SINGLETON
public class FacadeConfigurazioni
{
	private static FacadeConfigurazioni handle = null;
	private static HashMap<String, String> opzioniServer;
	private static final String serverConfigFile = "ManaGDR_server.conf";


	public static FacadeConfigurazioni getInstance()
	{
		if (handle == null) {
			handle = new FacadeConfigurazioni();
			opzioniServer = new HashMap<>();
			Populate( serverConfigFile );
		}

		return handle;
	}


	public static ArrayList<OpzioneCreazioneScheda> getOpzioniCreazioneScheda(IGiocatore g, Sessione s)
	{
		ArrayList<OpzioneCreazioneScheda> opz = new ArrayList<>();

		opz.add(new OpzioneCreazioneManuale());
		opz.add(new OpzioneCreazioneRandom());

		if (s.isGM(g))
			opz.add(new OpzioneCreazioneNPC());

		return opz;
	}


	public static int Populate(String filename)
	{
		opzioniServer.clear();
		
		// [TODO] Per futuri casi d'uso: lettura da file!
		opzioniServer.put( "serverPort", "1099" );
		opzioniServer.put( "serverID", "ManaGDR_Server1" );

		return opzioniServer.size();
	}


	public String get(String opz)
	{
		if (opzioniServer.containsKey(opz))
			return opzioniServer.get(opz);
		return null;
	}


	public void set(String key, String value)
	{
		opzioniServer.put(key, value);
	}
}
