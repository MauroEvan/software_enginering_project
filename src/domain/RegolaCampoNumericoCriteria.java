/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegolaCampoNumericoCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression limiteSuperiore;
	public final IntegerExpression limiteInferiore;
	public final StringExpression nomeCampo;
	public final IntegerExpression regolaCompositeId;
	public final AssociationExpression regolaComposite;
	
	public RegolaCampoNumericoCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		limiteSuperiore = new IntegerExpression("limiteSuperiore", this);
		limiteInferiore = new IntegerExpression("limiteInferiore", this);
		nomeCampo = new StringExpression("nomeCampo", this);
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this);
		regolaComposite = new AssociationExpression("regolaComposite", this);
	}
	
	public RegolaCampoNumericoCriteria(PersistentSession session) {
		this(session.createCriteria(RegolaCampoNumerico.class));
	}
	
	public RegolaCampoNumericoCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public RegolaCompositeCriteria createRegolaCompositeCriteria() {
		return new RegolaCompositeCriteria(createCriteria("regolaComposite"));
	}
	
	public RegolaCampoNumerico uniqueRegolaCampoNumerico() {
		return (RegolaCampoNumerico) super.uniqueResult();
	}
	
	public RegolaCampoNumerico[] listRegolaCampoNumerico() {
		java.util.List list = super.list();
		return (RegolaCampoNumerico[]) list.toArray(new RegolaCampoNumerico[list.size()]);
	}
}

