/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CampoResistenzeDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression classeArmatura;
	public final IntegerExpression riflessi;
	public final IntegerExpression tempra;
	public final IntegerExpression volonta;
	public final StringExpression nome;
	public final IntegerExpression schedaId;
	public final AssociationExpression scheda;
	
	public CampoResistenzeDetachedCriteria() {
		super(domain.CampoResistenze.class, domain.CampoResistenzeCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		classeArmatura = new IntegerExpression("classeArmatura", this.getDetachedCriteria());
		riflessi = new IntegerExpression("riflessi", this.getDetachedCriteria());
		tempra = new IntegerExpression("tempra", this.getDetachedCriteria());
		volonta = new IntegerExpression("volonta", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		schedaId = new IntegerExpression("scheda.ID", this.getDetachedCriteria());
		scheda = new AssociationExpression("scheda", this.getDetachedCriteria());
	}
	
	public CampoResistenzeDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.CampoResistenzeCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		classeArmatura = new IntegerExpression("classeArmatura", this.getDetachedCriteria());
		riflessi = new IntegerExpression("riflessi", this.getDetachedCriteria());
		tempra = new IntegerExpression("tempra", this.getDetachedCriteria());
		volonta = new IntegerExpression("volonta", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		schedaId = new IntegerExpression("scheda.ID", this.getDetachedCriteria());
		scheda = new AssociationExpression("scheda", this.getDetachedCriteria());
	}
	
	public SchedaDetachedCriteria createSchedaCriteria() {
		return new SchedaDetachedCriteria(createCriteria("scheda"));
	}
	
	public CampoResistenze uniqueCampoResistenze(PersistentSession session) {
		return (CampoResistenze) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public CampoResistenze[] listCampoResistenze(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (CampoResistenze[]) list.toArray(new CampoResistenze[list.size()]);
	}
}

