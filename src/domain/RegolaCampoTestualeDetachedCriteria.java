/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegolaCampoTestualeDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final StringExpression token;
	public final StringExpression nomeCampo;
	public final IntegerExpression regolaCompositeId;
	public final AssociationExpression regolaComposite;
	
	public RegolaCampoTestualeDetachedCriteria() {
		super(domain.RegolaCampoTestuale.class, domain.RegolaCampoTestualeCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		token = new StringExpression("token", this.getDetachedCriteria());
		nomeCampo = new StringExpression("nomeCampo", this.getDetachedCriteria());
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this.getDetachedCriteria());
		regolaComposite = new AssociationExpression("regolaComposite", this.getDetachedCriteria());
	}
	
	public RegolaCampoTestualeDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.RegolaCampoTestualeCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		token = new StringExpression("token", this.getDetachedCriteria());
		nomeCampo = new StringExpression("nomeCampo", this.getDetachedCriteria());
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this.getDetachedCriteria());
		regolaComposite = new AssociationExpression("regolaComposite", this.getDetachedCriteria());
	}
	
	public RegolaCompositeDetachedCriteria createRegolaCompositeCriteria() {
		return new RegolaCompositeDetachedCriteria(createCriteria("regolaComposite"));
	}
	
	public RegolaCampoTestuale uniqueRegolaCampoTestuale(PersistentSession session) {
		return (RegolaCampoTestuale) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public RegolaCampoTestuale[] listRegolaCampoTestuale(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (RegolaCampoTestuale[]) list.toArray(new RegolaCampoTestuale[list.size()]);
	}
}

