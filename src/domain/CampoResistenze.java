package domain;

import java.util.ArrayList;
import java.util.Random;

import ManaGDR_RemoteInterface.*;


public class CampoResistenze implements Campo {

	private int ID;
	private int classeArmatura;
	private int riflessi;
	private int tempra;
	private int volonta;
	private String nome = "resistenze";
	private Scheda scheda;

	
	public CampoResistenze() {}

	
	public CampoResistenze(int ca, int t, int v, int r)
	{
		this.classeArmatura = 10 + ca;
		this.riflessi = r;
		this.tempra = t;
		this.volonta = v;
	}

	
	public CampoResistenze(boolean random)
	{
		if (random)
		{
			this.classeArmatura = 10 + this.Randomize(0, 10);
			this.riflessi = this.Randomize(0, 6);
			this.tempra = this.Randomize(0, 6);
			this.volonta = this.Randomize(0, 6);
		}
	}


	private int Randomize(int min, int max)
	{
		Random rand = new Random();
		return rand.nextInt((max - min) + 1) + min;
	}

	
	public String toString()
	{
		return "Classe armatura " + this.getClasseArmatura()
			+ "\nRiflessi " + this.getRiflessi()
			+ "\nTempra " + this.getTempra()
			+ "\nVolontÃ  " + this.getVolonta();
	}

	
	public void setID(int value) {this.ID = value;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setClasseArmatura(int value) {this.classeArmatura = value;}
	public int getClasseArmatura() {return classeArmatura;}
	public void setRiflessi(int value) {this.riflessi = value;}
	public int getRiflessi() {return riflessi;}
	public void setTempra(int value) {this.tempra = value;}
	public int getTempra() {return tempra;}
	public void setVolonta(int value) {this.volonta = value;}
	public int getVolonta() {return volonta;}
	public void setNome(String value) {this.nome = value;}
	public String getNome() {return nome;}
	public void setScheda(IScheda value) {this.scheda = (Scheda)value;}
	public IScheda getScheda() {return scheda;}
	
	
	public Object getValore()
	{
		ArrayList<Integer> valori = new ArrayList<>();
		valori.add(this.tempra);
		valori.add(this.riflessi);
		valori.add(this.volonta);
		valori.add(this.classeArmatura);
		return valori;
	}
	
	
	public Integer getModificatore() {return null;}
}
