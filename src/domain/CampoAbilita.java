package domain;

import static java.lang.Math.floor;
import java.util.Random;

import ManaGDR_RemoteInterface.*;


public class CampoAbilita implements Campo
{
	private int ID;
	private String nome;
	private int valore;
	private String attributoChiave;
	private int modificatore;
	private boolean diClasse;
	private Scheda scheda;

	
	public CampoAbilita() {}
	
	
	public CampoAbilita(String nomeAbilita, int valoreGrado, CampoAttributo attributoChiave, boolean diClasse)
	{
		this.attributoChiave = attributoChiave.getNome();
		this.diClasse = diClasse;
		this.valore = valoreGrado;
		this.nome = nomeAbilita;
		this.modificatore = attributoChiave.getModificatore();
	}

	
	public CampoAbilita(String nomeAbilita, CampoAttributo attributoChiave)
	{
		this.attributoChiave = attributoChiave.toString();
		this.nome = nomeAbilita;
		this.diClasse = RandomBoolean();
		this.modificatore = attributoChiave.getModificatore();

		if (diClasse)
			this.valore = Randomizer();
		else
			this.valore = (int) floor(Randomizer() / 2);
	}

	
	private int Randomizer()
	{
		final int min = 0;
		final int max = 10;
		
		Random rand = new Random();
		return rand.nextInt((max - min) + 1) + min;
	}

	
	private boolean RandomBoolean()
	{
		Random rand = new Random();
		return rand.nextBoolean();
	}

	
	public String toString()
	{
		return this.getNome() + " " + this.getValore();
	}
	
	
	public void setID(int value) {this.ID = value;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setNome(String value) {this.nome = value;}
	public String getNome() {return nome;}
	public void setValore(int value) {this.valore = value;}
	public Integer getValore() {return valore;}
	public void setAttributoChiave(String value) {this.attributoChiave = value;}
	public String getAttributoChiave() {return attributoChiave;}
	public void setModificatore(int value) {this.modificatore = value;}
	public Integer getModificatore() {return modificatore;}
	public void setDiClasse(boolean value) { this.diClasse = value;	}
	public boolean getDiClasse() {return diClasse;	}
	public void setScheda(IScheda value) {this.scheda = (Scheda)value; }
	public Scheda getScheda() {return scheda; }
}
