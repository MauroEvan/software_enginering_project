package domain;

import java.util.ArrayList;
import java.rmi.RemoteException;

import ManaGDR_RemoteInterface.*;
import java.io.Serializable;


public class RegolaComposite implements RegolaComponent, Serializable
{
	private int ID;
	private String nome;
	private ModuloDiRegolamento modulo;
	private java.util.Set regolaGenerica = new java.util.HashSet();
	private java.util.Set regolaCampoNumerico = new java.util.HashSet();
	private java.util.Set regolaCampoResistenze = new java.util.HashSet();
	private java.util.Set regolaCampoAttributo = new java.util.HashSet();
	private java.util.Set regolaCampoAbilità = new java.util.HashSet();
	private java.util.Set regolaCampoOggetto = new java.util.HashSet();
	private java.util.Set regolaCampoTestuale = new java.util.HashSet();
    private ArrayList<RegolaComponent> children = new ArrayList<>();


	public RegolaComposite() {}


	public java.util.Iterator iterator()
	{
		return new RegolaIterator(children.iterator());
	}


	public void setID(int value) {
		this.ID = value;
	}

	public int getID() {
		return ID;
	}

	public int getORMID() {
		return getID();
	}

	public void setNome(String value) {
		this.nome = value;
	}

	public String getNome() {
		return nome;
	}

	public void setModulo(domain.ModuloDiRegolamento value) {
		this.modulo = value;
	}

	public IModuloDiRegolamento getModulo() {
		return modulo;
	}

	public void setRegolaGenerica(java.util.Set value) {
		this.regolaGenerica = value;
	}

	public java.util.Set getRegolaGenerica() {
		return regolaGenerica;
	}


	public void setRegolaCampoNumerico(java.util.Set value) {
		this.regolaCampoNumerico = value;
	}

	public java.util.Set getRegolaCampoNumerico() {
		return regolaCampoNumerico;
	}


	public void setRegolaCampoResistenze(java.util.Set value) {
		this.regolaCampoResistenze = value;
	}

	public java.util.Set getRegolaCampoResistenze() {
		return regolaCampoResistenze;
	}


	public void setRegolaCampoAttributo(java.util.Set value) {
		this.regolaCampoAttributo = value;
	}

	public java.util.Set getRegolaCampoAttributo() {
		return regolaCampoAttributo;
	}


	public void setRegolaCampoAbilità(java.util.Set value) {
		this.regolaCampoAbilità = value;
	}

	public java.util.Set getRegolaCampoAbilità() {
		return regolaCampoAbilità;
	}


	public void setRegolaCampoOggetto(java.util.Set value) {
		this.regolaCampoOggetto = value;
	}

	public java.util.Set getRegolaCampoOggetto() {
		return regolaCampoOggetto;
	}


	public void setRegolaCampoTestuale(java.util.Set value) {
		this.regolaCampoTestuale = value;
	}

	public java.util.Set getRegolaCampoTestuale() {
		return regolaCampoTestuale;
	}


	public RegolaComponent getChild(int index) throws IndexOutOfBoundsException {
		if( index >= children.size() )
			throw new IndexOutOfBoundsException();
		return children.get(index);
	}

	public void addChild(RegolaComponent regola) {
		children.add(regola);

	}

	public void removeChild(int index) throws IndexOutOfBoundsException {
		if( index >= children.size() )
			throw new IndexOutOfBoundsException();
		children.remove(index);
	}

	public boolean Applica(Object o) throws RemoteException
	{
		// Scorre tutti i figli e applica
		for( RegolaComponent c : children ) {
			if( c.Applica(o) == false ) {
				// L'applicazione della regola è fallita
				return false;
			}
		}

		return true;
	}

	/*
	public String toString()
	{
		return "RegolaComposite\n" +
			"[Nome]: " + nome + "\n\t" +
			regolaGenerica + "\n\t" +
			regolaCampoNumerico +"\n\t" +
			regolaCampoResistenze +"\n\t" +
			regolaCampoAttributo +"\n\t" +
			regolaCampoAbilità +"\n\t" +
			regolaCampoOggetto +"\n\t" +
			regolaCampoTestuale +"\n\y" +
			"[Figli]" +  children;
	}*/
}
