/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegolaCampoOggettoCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression maxQuantità;
	public final StringExpression nomeCampo;
	public final IntegerExpression regolaCompositeId;
	public final AssociationExpression regolaComposite;
	
	public RegolaCampoOggettoCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		maxQuantità = new IntegerExpression("maxQuantità", this);
		nomeCampo = new StringExpression("nomeCampo", this);
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this);
		regolaComposite = new AssociationExpression("regolaComposite", this);
	}
	
	public RegolaCampoOggettoCriteria(PersistentSession session) {
		this(session.createCriteria(RegolaCampoOggetto.class));
	}
	
	public RegolaCampoOggettoCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public RegolaCompositeCriteria createRegolaCompositeCriteria() {
		return new RegolaCompositeCriteria(createCriteria("regolaComposite"));
	}
	
	public RegolaCampoOggetto uniqueRegolaCampoOggetto() {
		return (RegolaCampoOggetto) super.uniqueResult();
	}
	
	public RegolaCampoOggetto[] listRegolaCampoOggetto() {
		java.util.List list = super.list();
		return (RegolaCampoOggetto[]) list.toArray(new RegolaCampoOggetto[list.size()]);
	}
}

