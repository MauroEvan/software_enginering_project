/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class SchedaCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final CollectionExpression campoResistenze;
	public final CollectionExpression campoAbilità;
	public final CollectionExpression campoTestuale;
	public final CollectionExpression campoNumerico;
	public final CollectionExpression campoOggetto;
	public final CollectionExpression campoAttributo;
	
	public SchedaCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		campoResistenze = new CollectionExpression("campoResistenze", this);
		campoAbilità = new CollectionExpression("campoAbilità", this);
		campoTestuale = new CollectionExpression("campoTestuale", this);
		campoNumerico = new CollectionExpression("campoNumerico", this);
		campoOggetto = new CollectionExpression("campoOggetto", this);
		campoAttributo = new CollectionExpression("campoAttributo", this);
	}
	
	public SchedaCriteria(PersistentSession session) {
		this(session.createCriteria(Scheda.class));
	}
	
	public SchedaCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public CampoResistenzeCriteria createCampoResistenzeCriteria() {
		return new CampoResistenzeCriteria(createCriteria("campoResistenze"));
	}
	
	public CampoAbilitaCriteria createCampoAbilitàCriteria() {
		return new CampoAbilitaCriteria(createCriteria("campoAbilità"));
	}
	
	public CampoTestualeCriteria createCampoTestualeCriteria() {
		return new CampoTestualeCriteria(createCriteria("campoTestuale"));
	}
	
	public CampoNumericoCriteria createCampoNumericoCriteria() {
		return new CampoNumericoCriteria(createCriteria("campoNumerico"));
	}
	
	public CampoOggettoCriteria createCampoOggettoCriteria() {
		return new CampoOggettoCriteria(createCriteria("campoOggetto"));
	}
	
	public CampoAttributoCriteria createCampoAttributoCriteria() {
		return new CampoAttributoCriteria(createCriteria("campoAttributo"));
	}
	
	public Scheda uniqueScheda() {
		return (Scheda) super.uniqueResult();
	}
	
	public Scheda[] listScheda() {
		java.util.List list = super.list();
		return (Scheda[]) list.toArray(new Scheda[list.size()]);
	}
}

