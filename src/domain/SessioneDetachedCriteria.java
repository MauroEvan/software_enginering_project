/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class SessioneDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression gmId;
	public final AssociationExpression gm;
	public final IntegerExpression mappaId;
	public final AssociationExpression mappa;
	public final StringExpression descr;
	public final CollectionExpression moduloRegolamento;
	public final CollectionExpression eventi;
	public final CollectionExpression schede;
	
	public SessioneDetachedCriteria() {
		super(domain.Sessione.class, domain.SessioneCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		gmId = new IntegerExpression("gm.ID", this.getDetachedCriteria());
		gm = new AssociationExpression("gm", this.getDetachedCriteria());
		mappaId = new IntegerExpression("mappa.ID", this.getDetachedCriteria());
		mappa = new AssociationExpression("mappa", this.getDetachedCriteria());
		descr = new StringExpression("descr", this.getDetachedCriteria());
		moduloRegolamento = new CollectionExpression("moduloRegolamento", this.getDetachedCriteria());
		eventi = new CollectionExpression("eventi", this.getDetachedCriteria());
		schede = new CollectionExpression("schede", this.getDetachedCriteria());
	}
	
	public SessioneDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.SessioneCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		gmId = new IntegerExpression("gm.ID", this.getDetachedCriteria());
		gm = new AssociationExpression("gm", this.getDetachedCriteria());
		mappaId = new IntegerExpression("mappa.ID", this.getDetachedCriteria());
		mappa = new AssociationExpression("mappa", this.getDetachedCriteria());
		descr = new StringExpression("descr", this.getDetachedCriteria());
		moduloRegolamento = new CollectionExpression("moduloRegolamento", this.getDetachedCriteria());
		eventi = new CollectionExpression("eventi", this.getDetachedCriteria());
		schede = new CollectionExpression("schede", this.getDetachedCriteria());
	}
	
	public GiocatoreDetachedCriteria createGmCriteria() {
		return new GiocatoreDetachedCriteria(createCriteria("gm"));
	}
	
	public MappaDetachedCriteria createMappaCriteria() {
		return new MappaDetachedCriteria(createCriteria("mappa"));
	}
	
	public ModuloDiRegolamentoDetachedCriteria createModuloRegolamentoCriteria() {
		return new ModuloDiRegolamentoDetachedCriteria(createCriteria("moduloRegolamento"));
	}
	
	public EventoDetachedCriteria createEventiCriteria() {
		return new EventoDetachedCriteria(createCriteria("eventi"));
	}
	
	public SchedaDetachedCriteria createSchedeCriteria() {
		return new SchedaDetachedCriteria(createCriteria("schede"));
	}
	
	public Sessione uniqueSessione(PersistentSession session) {
		return (Sessione) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Sessione[] listSessione(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Sessione[]) list.toArray(new Sessione[list.size()]);
	}
}

