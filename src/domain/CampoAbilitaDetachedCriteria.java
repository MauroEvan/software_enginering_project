/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CampoAbilitaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final StringExpression nome;
	public final IntegerExpression valore;
	public final StringExpression attributoChiave;
	public final IntegerExpression modificatore;
	public final BooleanExpression diClasse;
	public final IntegerExpression schedaId;
	public final AssociationExpression scheda;
	
	public CampoAbilitaDetachedCriteria() {
		super(domain.CampoAbilita.class, domain.CampoAbilitaCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		valore = new IntegerExpression("valore", this.getDetachedCriteria());
		attributoChiave = new StringExpression("attributoChiave", this.getDetachedCriteria());
		modificatore = new IntegerExpression("modificatore", this.getDetachedCriteria());
		diClasse = new BooleanExpression("diClasse", this.getDetachedCriteria());
		schedaId = new IntegerExpression("scheda.ID", this.getDetachedCriteria());
		scheda = new AssociationExpression("scheda", this.getDetachedCriteria());
	}
	
	public CampoAbilitaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.CampoAbilitaCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		valore = new IntegerExpression("valore", this.getDetachedCriteria());
		attributoChiave = new StringExpression("attributoChiave", this.getDetachedCriteria());
		modificatore = new IntegerExpression("modificatore", this.getDetachedCriteria());
		diClasse = new BooleanExpression("diClasse", this.getDetachedCriteria());
		schedaId = new IntegerExpression("scheda.ID", this.getDetachedCriteria());
		scheda = new AssociationExpression("scheda", this.getDetachedCriteria());
	}
	
	public SchedaDetachedCriteria createSchedaCriteria() {
		return new SchedaDetachedCriteria(createCriteria("scheda"));
	}
	
	public CampoAbilita uniqueCampoAbilita(PersistentSession session) {
		return (CampoAbilita) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public CampoAbilita[] listCampoAbilita(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (CampoAbilita[]) list.toArray(new CampoAbilita[list.size()]);
	}
}

