package domain;

import org.orm.PersistentException;

// SINGLETON
public class LibreriaRegolamenti
{
	private static LibreriaRegolamenti handle = null;


	protected LibreriaRegolamenti() {};


	public static LibreriaRegolamenti getInstance()
	{
		if( handle == null )
			handle = new LibreriaRegolamenti();
		return handle;
	}


	public void AvviaCreazioneModulo() {}


	public void InviaModulo( ModuloDiRegolamento m )
	{
		if( m.Verifica() == true )
		{
			try {
				PersistenceFacade.getInstance().SalvaModulo(m);
			} catch( PersistentException pe ) {
				pe.printStackTrace();
				System.err.println( "Impossibile salvare il regolamento" );
			}
		}
	}
}
