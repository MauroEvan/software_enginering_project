/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegolaCompositeDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final StringExpression nome;
	public final IntegerExpression moduloId;
	public final AssociationExpression modulo;
	public final CollectionExpression regolaGenerica;
	public final CollectionExpression regolaCampoNumerico;
	public final CollectionExpression regolaCampoResistenze;
	public final CollectionExpression regolaCampoAttributo;
	public final CollectionExpression regolaCampoAbilità;
	public final CollectionExpression regolaCampoOggetto;
	public final CollectionExpression regolaCampoTestuale;
	
	public RegolaCompositeDetachedCriteria() {
		super(domain.RegolaComposite.class, domain.RegolaCompositeCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		moduloId = new IntegerExpression("modulo.ID", this.getDetachedCriteria());
		modulo = new AssociationExpression("modulo", this.getDetachedCriteria());
		regolaGenerica = new CollectionExpression("regolaGenerica", this.getDetachedCriteria());
		regolaCampoNumerico = new CollectionExpression("regolaCampoNumerico", this.getDetachedCriteria());
		regolaCampoResistenze = new CollectionExpression("regolaCampoResistenze", this.getDetachedCriteria());
		regolaCampoAttributo = new CollectionExpression("regolaCampoAttributo", this.getDetachedCriteria());
		regolaCampoAbilità = new CollectionExpression("regolaCampoAbilità", this.getDetachedCriteria());
		regolaCampoOggetto = new CollectionExpression("regolaCampoOggetto", this.getDetachedCriteria());
		regolaCampoTestuale = new CollectionExpression("regolaCampoTestuale", this.getDetachedCriteria());
	}
	
	public RegolaCompositeDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.RegolaCompositeCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		moduloId = new IntegerExpression("modulo.ID", this.getDetachedCriteria());
		modulo = new AssociationExpression("modulo", this.getDetachedCriteria());
		regolaGenerica = new CollectionExpression("regolaGenerica", this.getDetachedCriteria());
		regolaCampoNumerico = new CollectionExpression("regolaCampoNumerico", this.getDetachedCriteria());
		regolaCampoResistenze = new CollectionExpression("regolaCampoResistenze", this.getDetachedCriteria());
		regolaCampoAttributo = new CollectionExpression("regolaCampoAttributo", this.getDetachedCriteria());
		regolaCampoAbilità = new CollectionExpression("regolaCampoAbilità", this.getDetachedCriteria());
		regolaCampoOggetto = new CollectionExpression("regolaCampoOggetto", this.getDetachedCriteria());
		regolaCampoTestuale = new CollectionExpression("regolaCampoTestuale", this.getDetachedCriteria());
	}
	
	public ModuloDiRegolamentoDetachedCriteria createModuloCriteria() {
		return new ModuloDiRegolamentoDetachedCriteria(createCriteria("modulo"));
	}
	
	public RegolaGenericaDetachedCriteria createRegolaGenericaCriteria() {
		return new RegolaGenericaDetachedCriteria(createCriteria("regolaGenerica"));
	}
	
	public RegolaCampoNumericoDetachedCriteria createRegolaCampoNumericoCriteria() {
		return new RegolaCampoNumericoDetachedCriteria(createCriteria("regolaCampoNumerico"));
	}
	
	public RegolaCampoResistenzeDetachedCriteria createRegolaCampoResistenzeCriteria() {
		return new RegolaCampoResistenzeDetachedCriteria(createCriteria("regolaCampoResistenze"));
	}
	
	public RegolaCampoAttributoDetachedCriteria createRegolaCampoAttributoCriteria() {
		return new RegolaCampoAttributoDetachedCriteria(createCriteria("regolaCampoAttributo"));
	}
	
	public RegolaCampoAbilitaDetachedCriteria createRegolaCampoAbilitàCriteria() {
		return new RegolaCampoAbilitaDetachedCriteria(createCriteria("regolaCampoAbilità"));
	}
	
	public RegolaCampoOggettoDetachedCriteria createRegolaCampoOggettoCriteria() {
		return new RegolaCampoOggettoDetachedCriteria(createCriteria("regolaCampoOggetto"));
	}
	
	public RegolaCampoTestualeDetachedCriteria createRegolaCampoTestualeCriteria() {
		return new RegolaCampoTestualeDetachedCriteria(createCriteria("regolaCampoTestuale"));
	}
	
	public RegolaComposite uniqueRegolaComposite(PersistentSession session) {
		return (RegolaComposite) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public RegolaComposite[] listRegolaComposite(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (RegolaComposite[]) list.toArray(new RegolaComposite[list.size()]);
	}
}

