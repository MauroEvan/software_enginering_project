package domain;

import java.util.Iterator;
import java.util.Stack;

import ManaGDR_RemoteInterface.*;


// pattern ITERATOR
public class RegolaIterator implements Iterator
{
	Stack<Iterator<RegolaComponent>> stack = new Stack<>();


	public RegolaIterator(Iterator<RegolaComponent> i) {stack.push(i);}


	public boolean hasNext()
	{
		if (stack.empty())
			return false;

		Iterator<RegolaComponent> t = stack.peek();

		if (t.hasNext())
			return true;

		stack.pop();
		return this.hasNext();
	}


	public RegolaComponent next()
	{
		if (hasNext() == false)
			return null;

		Iterator<RegolaComponent> t = stack.peek();
		RegolaComponent rc = t.next();

		if (rc instanceof RegolaComposite)
			stack.push(((RegolaComposite) rc).iterator());
		return rc;
	}
}
