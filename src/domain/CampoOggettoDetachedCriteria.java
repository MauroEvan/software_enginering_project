/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CampoOggettoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final StringExpression nome;
	public final IntegerExpression quantità;
	public final IntegerExpression schedaId;
	public final AssociationExpression scheda;
	
	public CampoOggettoDetachedCriteria() {
		super(domain.CampoOggetto.class, domain.CampoOggettoCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		quantità = new IntegerExpression("quantità", this.getDetachedCriteria());
		schedaId = new IntegerExpression("scheda.ID", this.getDetachedCriteria());
		scheda = new AssociationExpression("scheda", this.getDetachedCriteria());
	}
	
	public CampoOggettoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.CampoOggettoCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		quantità = new IntegerExpression("quantità", this.getDetachedCriteria());
		schedaId = new IntegerExpression("scheda.ID", this.getDetachedCriteria());
		scheda = new AssociationExpression("scheda", this.getDetachedCriteria());
	}
	
	public SchedaDetachedCriteria createSchedaCriteria() {
		return new SchedaDetachedCriteria(createCriteria("scheda"));
	}
	
	public CampoOggetto uniqueCampoOggetto(PersistentSession session) {
		return (CampoOggetto) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public CampoOggetto[] listCampoOggetto(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (CampoOggetto[]) list.toArray(new CampoOggetto[list.size()]);
	}
}

