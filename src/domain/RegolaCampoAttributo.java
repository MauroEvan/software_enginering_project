package domain;

import ManaGDR_RemoteInterface.*;


public class RegolaCampoAttributo extends RegolaCampo
{
	private int ID;	
	private String nomeCampo;	
	private int sogliaValore;	
	private int sogliaModificatore;	
	private RegolaComposite regolaComposite;

	
	public RegolaCampoAttributo() {}


	public RegolaCampoAttributo( String nomeCampo, int sogliaValore, int sogliaModificatore )
	{
		this.sogliaValore = sogliaValore;
		this.nomeCampo = nomeCampo;
		this.sogliaModificatore = sogliaModificatore;
	}

	
	public boolean ApplicaRegolaSuScheda(domain.Scheda s)
	{
		//Verifichiamo la presenza del campo
		if( s.getChiaviCampi().contains(nomeCampo) ) {
			Campo c = s.getCampo(nomeCampo);
			
			if( c instanceof CampoAttributo ) {// Se è della classe giusta..
				CampoAttributo ca = (CampoAttributo)c;
				return ControllaModificatore(ca) && ControllaValore(ca);
			}
		}
		return false;
	}
	
	
	private boolean ControllaValore(domain.CampoAttributo ca)
	{
		int val = ca.getValore();
		
		// (se è uguale a 0 lo ignoriamo
		if( val >= sogliaValore || val == 0 )
			return true;
		return false;
	}
	
	
	private boolean ControllaModificatore(domain.CampoAttributo ca)
	{
		int mod = ca.getModificatore();
		int val = ca.getValore();

		if (mod == (val - 10) / 2 || mod == (((val - 10) / 2) - 1))
			return true;
		return false;
	}
	
	public String toString()
	{
		return "RegolaCampoAttributo#" + String.valueOf(getID());
	}
	
	
	public void setID(int value) {this.ID = value;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setNomeCampo(String value) {this.nomeCampo = value;}
	public String getNomeCampo() {return nomeCampo;}
	public void setSogliaValore(int value) {this.sogliaValore = value;}
	public int getSogliaValore() {return sogliaValore;}
	public void setSogliaModificatore(int value) {this.sogliaModificatore = value;}
	public int getSogliaModificatore() {return sogliaModificatore;}
	public void setRegolaComposite(RegolaComposite value) {this.regolaComposite = value;}
	public RegolaComposite getRegolaComposite() {return regolaComposite;}
}
