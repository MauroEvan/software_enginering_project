/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class GiocatoreCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final StringExpression nomeGiocatore;
	
	public GiocatoreCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		nomeGiocatore = new StringExpression("nomeGiocatore", this);
	}
	
	public GiocatoreCriteria(PersistentSession session) {
		this(session.createCriteria(Giocatore.class));
	}
	
	public GiocatoreCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public Giocatore uniqueGiocatore() {
		return (Giocatore) super.uniqueResult();
	}
	
	public Giocatore[] listGiocatore() {
		java.util.List list = super.list();
		return (Giocatore[]) list.toArray(new Giocatore[list.size()]);
	}
}

