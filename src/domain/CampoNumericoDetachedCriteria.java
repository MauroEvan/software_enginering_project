/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CampoNumericoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final StringExpression nome;
	public final IntegerExpression valore;
	public final IntegerExpression schedaId;
	public final AssociationExpression scheda;
	
	public CampoNumericoDetachedCriteria() {
		super(domain.CampoNumerico.class, domain.CampoNumericoCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		valore = new IntegerExpression("valore", this.getDetachedCriteria());
		schedaId = new IntegerExpression("scheda.ID", this.getDetachedCriteria());
		scheda = new AssociationExpression("scheda", this.getDetachedCriteria());
	}
	
	public CampoNumericoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.CampoNumericoCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		nome = new StringExpression("nome", this.getDetachedCriteria());
		valore = new IntegerExpression("valore", this.getDetachedCriteria());
		schedaId = new IntegerExpression("scheda.ID", this.getDetachedCriteria());
		scheda = new AssociationExpression("scheda", this.getDetachedCriteria());
	}
	
	public SchedaDetachedCriteria createSchedaCriteria() {
		return new SchedaDetachedCriteria(createCriteria("scheda"));
	}
	
	public CampoNumerico uniqueCampoNumerico(PersistentSession session) {
		return (CampoNumerico) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public CampoNumerico[] listCampoNumerico(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (CampoNumerico[]) list.toArray(new CampoNumerico[list.size()]);
	}
}

