/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class MappaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	
	public MappaDetachedCriteria() {
		super(domain.Mappa.class, domain.MappaCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
	}
	
	public MappaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.MappaCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
	}
	
	public Mappa uniqueMappa(PersistentSession session) {
		return (Mappa) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Mappa[] listMappa(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Mappa[]) list.toArray(new Mappa[list.size()]);
	}
}

