/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class MappaCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	
	public MappaCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
	}
	
	public MappaCriteria(PersistentSession session) {
		this(session.createCriteria(Mappa.class));
	}
	
	public MappaCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public Mappa uniqueMappa() {
		return (Mappa) super.uniqueResult();
	}
	
	public Mappa[] listMappa() {
		java.util.List list = super.list();
		return (Mappa[]) list.toArray(new Mappa[list.size()]);
	}
}

