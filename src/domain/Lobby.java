package domain;

import java.util.ArrayList;
import java.util.Arrays;
import org.orm.PersistentException;

import ManaGDR_RemoteInterface.*;


// pattern GRASP - CONTROLLER
// SINGLETON
public class Lobby implements ILobby
{
	private static Lobby handle;
	private ArrayList<IGiocatore> lista = new ArrayList<>();
	private ArrayList<ITavolo> tavoli = new ArrayList<>();


	public static Lobby getInstance() throws PersistentException
	{
		if (handle == null)
			handle = new Lobby();
		return handle;
	}


	public Lobby() throws PersistentException
	{
		// Caso d'uso di avviamento: Tutti i giocatori già nella lobby ad attendere
		lista.addAll(Arrays.asList( PersistenceFacade.getInstance().getGiocatoriLoggati() ));
	}


	public Tavolo CreaNuovoTavolo()
	{
		// pattern GRASP -> CREATOR
		Tavolo t = new Tavolo();
		tavoli.add(t);
		return t;
	}


	public ArrayList<IGiocatore> getGiocatoriConnessi() {return lista;}
	public void AggiungiGiocatore(IGiocatore g) {lista.add(g);}
}
