/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CampoNumericoCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final StringExpression nome;
	public final IntegerExpression valore;
	public final IntegerExpression schedaId;
	public final AssociationExpression scheda;
	
	public CampoNumericoCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		nome = new StringExpression("nome", this);
		valore = new IntegerExpression("valore", this);
		schedaId = new IntegerExpression("scheda.ID", this);
		scheda = new AssociationExpression("scheda", this);
	}
	
	public CampoNumericoCriteria(PersistentSession session) {
		this(session.createCriteria(CampoNumerico.class));
	}
	
	public CampoNumericoCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public SchedaCriteria createSchedaCriteria() {
		return new SchedaCriteria(createCriteria("scheda"));
	}
	
	public CampoNumerico uniqueCampoNumerico() {
		return (CampoNumerico) super.uniqueResult();
	}
	
	public CampoNumerico[] listCampoNumerico() {
		java.util.List list = super.list();
		return (CampoNumerico[]) list.toArray(new CampoNumerico[list.size()]);
	}
}

