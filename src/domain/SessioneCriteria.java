/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class SessioneCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression gmId;
	public final AssociationExpression gm;
	public final IntegerExpression mappaId;
	public final AssociationExpression mappa;
	public final StringExpression descr;
	public final CollectionExpression moduloRegolamento;
	public final CollectionExpression eventi;
	public final CollectionExpression schede;
	
	public SessioneCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		gmId = new IntegerExpression("gm.ID", this);
		gm = new AssociationExpression("gm", this);
		mappaId = new IntegerExpression("mappa.ID", this);
		mappa = new AssociationExpression("mappa", this);
		descr = new StringExpression("descr", this);
		moduloRegolamento = new CollectionExpression("moduloRegolamento", this);
		eventi = new CollectionExpression("eventi", this);
		schede = new CollectionExpression("schede", this);
	}
	
	public SessioneCriteria(PersistentSession session) {
		this(session.createCriteria(Sessione.class));
	}
	
	public SessioneCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public GiocatoreCriteria createGmCriteria() {
		return new GiocatoreCriteria(createCriteria("gm"));
	}
	
	public MappaCriteria createMappaCriteria() {
		return new MappaCriteria(createCriteria("mappa"));
	}
	
	public ModuloDiRegolamentoCriteria createModuloRegolamentoCriteria() {
		return new ModuloDiRegolamentoCriteria(createCriteria("moduloRegolamento"));
	}
	
	public EventoCriteria createEventiCriteria() {
		return new EventoCriteria(createCriteria("eventi"));
	}
	
	public SchedaCriteria createSchedeCriteria() {
		return new SchedaCriteria(createCriteria("schede"));
	}
	
	public Sessione uniqueSessione() {
		return (Sessione) super.uniqueResult();
	}
	
	public Sessione[] listSessione() {
		java.util.List list = super.list();
		return (Sessione[]) list.toArray(new Sessione[list.size()]);
	}
}

