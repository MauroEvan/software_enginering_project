package domain;

import java.util.Random;
import ManaGDR_RemoteInterface.*;

public class CampoTestuale implements Campo
{
	private int ID;
	private String nome;
	private String valore;
	private Scheda scheda;

	
	public CampoTestuale() {}

	
	public CampoTestuale(String nomeCampo, String valore)
	{
		this.nome = nomeCampo;
		this.valore = valore;
	}

	
	public CampoTestuale(String nomeCampo)
	{
		this.nome = nomeCampo;
		this.valore = Randomizer(nomeCampo);
	}

	
	private String Randomizer(String nomeCampo)
	{
		if (nomeCampo.equals("razza"))
			return RandomizerRazza();
		if (nomeCampo.equals("classe"))
			return RandomizerClasse();
		if (nomeCampo.equals("allineamento"))
			return RandomizerAllineamento();
		return RandomizerNome();
	}

	private String RandomizerNome()
	{
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random rand = new Random();
		
		for (int i = 0; i<7; i++) {
			char c = chars[rand.nextInt(chars.length)];
			sb.append(c);
		}
		
		sb.append(' ');
		
		for (int i = 0; i < 7; i++) {
			char c = chars[rand.nextInt(chars.length)];
			sb.append(c);
		}
		
		return sb.toString();
	}

	
	private String RandomizerRazza()
	{
		String[] razze = new String[4];
		razze[0] = "Umano";
		razze[1] = "Elfo";
		razze[2] = "Gnomo";
		razze[3] = "Nano";

		Random rand = new Random();
		return razze[rand.nextInt(3)];
	}

	
	private String RandomizerAllineamento()
	{
		String[] first = new String[3];
		first[0] = "Legale";
		first[1] = "Neutrale";
		first[2] = "Caotico";

		String[] second = new String[3];
		second[0] = "Buono";
		second[1] = "Neutrale";
		second[2] = "Malvagio";

		Random rand = new Random();
		StringBuilder sb = new StringBuilder();

		sb.append(first[rand.nextInt(3)]).append(" ").append(second[rand.nextInt(3)]);
		return sb.toString();
	}

	
	private String RandomizerClasse()
	{
		String[] razze = new String[3];
		razze[0] = "Guerriero";
		razze[1] = "Mago";
		razze[2] = "Ladro";

		Random rand = new Random();
		return razze[rand.nextInt(3)];
	}

	
	public String toString()
	{
		return this.getNome() + " " + this.getValore();
	}

	
	public void setID(int value) {this.ID = value;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setNome(String value) {this.nome = value;}
	public String getNome() {return nome;}
	public void setValore(String value) {this.valore = value;}
	public String getValore() {return valore;}
	public void setScheda(IScheda value) {this.scheda = (Scheda)value;}
	public domain.Scheda getScheda() {return scheda;}
	public Integer getModificatore() {return null;}
}
