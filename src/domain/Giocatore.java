package domain;

import java.io.Serializable;
import java.util.ArrayList;

import ManaGDR_RemoteInterface.*;

public class Giocatore implements IGiocatore
{
	private int ID;
	private String nomeGiocatore;
	private ArrayList<Scheda> schedeGiocatore = new ArrayList<>();


	public Giocatore() {}
	

	public String toString()
	{
		if (nomeGiocatore == null)
			return "SenzaNome";
		return nomeGiocatore;
	}
	

	public void setID(int value) {this.ID = value;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setNomeGiocatore(String value) {this.nomeGiocatore = value;}
	public String getNomeGiocatore() {return nomeGiocatore;}
	public void aggiungiScheda(IScheda s) {schedeGiocatore.add((Scheda)s);}
}
