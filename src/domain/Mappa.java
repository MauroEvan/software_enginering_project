package domain;

import ManaGDR_RemoteInterface.*;


public class Mappa implements IMappa
{
	private int ID;
	private ID idMappa;

	
	public Mappa() {}

	
	public void setID(int value)
	{
		this.ID = value;
		this.setIdMappa(new ID(value));
	}

	
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public String toString() {return String.valueOf(getID());}
	public ID getIdMappa() {return this.idMappa;}
	public void setIdMappa(IID idMappa) {this.idMappa = (ID)idMappa;}
}
