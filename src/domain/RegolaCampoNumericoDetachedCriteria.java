/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegolaCampoNumericoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression limiteSuperiore;
	public final IntegerExpression limiteInferiore;
	public final StringExpression nomeCampo;
	public final IntegerExpression regolaCompositeId;
	public final AssociationExpression regolaComposite;
	
	public RegolaCampoNumericoDetachedCriteria() {
		super(domain.RegolaCampoNumerico.class, domain.RegolaCampoNumericoCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		limiteSuperiore = new IntegerExpression("limiteSuperiore", this.getDetachedCriteria());
		limiteInferiore = new IntegerExpression("limiteInferiore", this.getDetachedCriteria());
		nomeCampo = new StringExpression("nomeCampo", this.getDetachedCriteria());
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this.getDetachedCriteria());
		regolaComposite = new AssociationExpression("regolaComposite", this.getDetachedCriteria());
	}
	
	public RegolaCampoNumericoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, domain.RegolaCampoNumericoCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		limiteSuperiore = new IntegerExpression("limiteSuperiore", this.getDetachedCriteria());
		limiteInferiore = new IntegerExpression("limiteInferiore", this.getDetachedCriteria());
		nomeCampo = new StringExpression("nomeCampo", this.getDetachedCriteria());
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this.getDetachedCriteria());
		regolaComposite = new AssociationExpression("regolaComposite", this.getDetachedCriteria());
	}
	
	public RegolaCompositeDetachedCriteria createRegolaCompositeCriteria() {
		return new RegolaCompositeDetachedCriteria(createCriteria("regolaComposite"));
	}
	
	public RegolaCampoNumerico uniqueRegolaCampoNumerico(PersistentSession session) {
		return (RegolaCampoNumerico) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public RegolaCampoNumerico[] listRegolaCampoNumerico(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (RegolaCampoNumerico[]) list.toArray(new RegolaCampoNumerico[list.size()]);
	}
}

