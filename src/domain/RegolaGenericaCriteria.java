/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegolaGenericaCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final StringExpression nomeRegola;
	public final StringExpression testoRegola;
	public final IntegerExpression regolaCompositeId;
	public final AssociationExpression regolaComposite;
	
	public RegolaGenericaCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		nomeRegola = new StringExpression("nomeRegola", this);
		testoRegola = new StringExpression("testoRegola", this);
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this);
		regolaComposite = new AssociationExpression("regolaComposite", this);
	}
	
	public RegolaGenericaCriteria(PersistentSession session) {
		this(session.createCriteria(RegolaGenerica.class));
	}
	
	public RegolaGenericaCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public RegolaCompositeCriteria createRegolaCompositeCriteria() {
		return new RegolaCompositeCriteria(createCriteria("regolaComposite"));
	}
	
	public RegolaGenerica uniqueRegolaGenerica() {
		return (RegolaGenerica) super.uniqueResult();
	}
	
	public RegolaGenerica[] listRegolaGenerica() {
		java.util.List list = super.list();
		return (RegolaGenerica[]) list.toArray(new RegolaGenerica[list.size()]);
	}
}

