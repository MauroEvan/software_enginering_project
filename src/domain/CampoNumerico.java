package domain;

import java.util.Random;
import ManaGDR_RemoteInterface.*;

public class CampoNumerico implements Campo
{
	private int ID;
	private String nome;
	private int valore;
	private Scheda scheda;
	

	public CampoNumerico() {}

	
	public CampoNumerico(String nomeCampo)
	{
		this.nome = nomeCampo;
		this.valore = Randomizer(nomeCampo);
	}

	
	public CampoNumerico(String nomeCampo, int valore)
	{
		this.nome = nomeCampo;
		this.valore = valore;
	}


	private int Randomizer(String nomeCampo)
	{
		final int min = 0;
		final int max = 200000;
		final int maxLvl = 20;
		
		Random rand = new Random();

		if (nomeCampo == "livello")
			return rand.nextInt((maxLvl - min) + 1) + min;
		
		return rand.nextInt((max - min) + 1) + min;
	}

	
	public String toString()
	{
		return this.getNome() + " " + this.getValore();
	}
	
	
	public void setID(int value) {this.ID = value;}
	public int getID() {return ID;}
	public int getORMID() {return getID();}
	public void setNome(String value) {this.nome = value;}
	public String getNome() {return nome;}
	public void setValore(int value) {this.valore = value;}
	public Integer getValore() {return valore;}
	public Integer getModificatore() {return null;}
	public void setScheda(IScheda value) {this.scheda = (Scheda)value;}
	public domain.Scheda getScheda() {return scheda;}
}
