/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universita degli Studi dell'Aquila
 * License Type: Academic
 */
package domain;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RegolaCampoResistenzeCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression minArmatura;
	public final IntegerExpression minVolonta;
	public final IntegerExpression minTempra;
	public final IntegerExpression minRiflessi;
	public final StringExpression nomeCampo;
	public final IntegerExpression regolaCompositeId;
	public final AssociationExpression regolaComposite;
	
	public RegolaCampoResistenzeCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		minArmatura = new IntegerExpression("minArmatura", this);
		minVolonta = new IntegerExpression("minVolonta", this);
		minTempra = new IntegerExpression("minTempra", this);
		minRiflessi = new IntegerExpression("minRiflessi", this);
		nomeCampo = new StringExpression("nomeCampo", this);
		regolaCompositeId = new IntegerExpression("regolaComposite.ID", this);
		regolaComposite = new AssociationExpression("regolaComposite", this);
	}
	
	public RegolaCampoResistenzeCriteria(PersistentSession session) {
		this(session.createCriteria(RegolaCampoResistenze.class));
	}
	
	public RegolaCampoResistenzeCriteria() throws PersistentException {
		this(domain.ManaGDRPersistentManager.instance().getSession());
	}
	
	public RegolaCompositeCriteria createRegolaCompositeCriteria() {
		return new RegolaCompositeCriteria(createCriteria("regolaComposite"));
	}
	
	public RegolaCampoResistenze uniqueRegolaCampoResistenze() {
		return (RegolaCampoResistenze) super.uniqueResult();
	}
	
	public RegolaCampoResistenze[] listRegolaCampoResistenze() {
		java.util.List list = super.list();
		return (RegolaCampoResistenze[]) list.toArray(new RegolaCampoResistenze[list.size()]);
	}
}

